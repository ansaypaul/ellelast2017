<?php
/*
Template Name: POPUP
*/

?>
<div id="popup-container" class="<?php _e("elle-a-table", "html5blank"); ?> active-on">
	<div class="eat-popup">
		<span class="btn-close"></span>
		<div class="popup-left">
			<img src="<?php echo get_template_directory_uri(); ?>/img/eat-popup.gif" alt="Pop Up EAT">
		</div>
		<div class="popup-right">
			<div class="site-logo sprite">
			</div>
			<div><?php _e( "Recettes inratables, chefs à suivre, vidéos, ...", "html5blank" ); ?></div>
			<div><?php _e( "Abonnez-vous à la newsletter pour ne rien louper!", "html5blank" ); ?></div>
			<form method="post" action="<?php echo get_site_url(); ?>/newsletters">
			   <input type="text" name="footer_email" placeholder="<?php _e( 'Votre e-mail', 'html5blank' ); ?>">
			   <input type="submit" value="<?php _e( "S'inscrire", 'html5blank' ); ?>" class="submit sprite">
			</form>
			<div><?php _e( "Et rejoignez notre communauté instagram ", "html5blank" ); ?><a href="http://instagram.com/elleatable.be/" target="_blank"><?php _e( "@elleatable.be", "html5blank" ); ?></a></div>
		</div>
	</div>
</div>