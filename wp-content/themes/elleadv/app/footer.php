	<?php global $_theme, $last_magazine, $_id_article;?>
			<div id="backtotop" class="sprite"></div>
			<div id="cookie-box">
				<button class="btn-close" title="Close"></button>
				<div class="site-inner">	
					<?php _e( "En poursuivant votre navigation sur ce site, vous acceptez l'utilisation de cookies.", 'html5blank' ); ?>
				</div>
			</div>
			<div class="social-wrapper">
				<div class="site-inner">
					<?php if($_theme == 'elle'){ 
						get_template_part('template-parts/general/social-links'); }
					if($_theme == 'elle-decoration'){ 
						get_template_part('template-parts/general/social-links-deco'); }
					else{ 
						get_template_part('template-parts/general/social-links-eat');
					} ?>
				</div>
			</div>
			<?php 
			if(get_field('popup_active', $last_magazine[0]->ID) == 1){
				get_template_part('template-parts/popup/pop-up-online'); 
			}

			?>
			<!-- footer -->
			<footer id="footer" class="footer">
				<!-- site-inner -->
				<div class="site-inner">
					<?php get_template_part('template-parts/general/follow-us'); ?>
					<?php get_template_part('template-parts/general/magazine-content'); ?>
					<?php //get_template_part('template-parts/general/unfold-menu'); ?>
					<div class="footer-menu-wrapper">
						<a href="<?php echo home_url_elle(); ?>/">
							<div class="sprite site-logo" title="ELLE logo"></div>
						</a>
						<!-- Add all the footer-menu Mode-->
						<?php html5blank_nav("footer-menu-2018","footer-menu"); ?>
					</div>
					<?php html5blank_nav_encode_64("footer-nav-menu-2018","footer-nav-menu"); ?>

					<small class="legals"><?php _e( "ELLE.be participe à divers programmes d’affiliation ce qui signifie que nous recevons une commission sur les ventes générées par le biais de certains liens créés. Le pourcentage du revenu généré par ces ventes n’affecte pas les achats effectués par l’utilisateur ou l’expérience offerte sur le site du marchand. ", "html5blank" ); ?><a href="<?php _e( "https://www.elle.be/fr/vie-privee", "html5blank" ); ?>"> <?php _e( "Plus d'infos", "html5blank" ); ?></a>.</small>

				</div>
				<!-- site-inner -->
			</footer>
			<!-- /footer -->
			<!-- Ventures -->
			<div class="footer-ventures footer-black">
				<div class="site-inner">
					<span data-atc="<?php _encode_base64('http://www.editionventures.be'); ?>" data-outlink="true" class="ventures-logo atc">
						<?php 
							$img_html = "<img src='".get_template_directory_uri()."/img/logo_ventures.png' alt='logo Edition Ventures' />";
							$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
							echo $img_html;
						?>
					</span>
					<?php if(is_home()){ ?>
					<div class="footer-right">
						<span><small>© <?php echo date("Y"); ?>&nbsp;</small>Edition Ventures</span>
						<ul class="sites_selector">
							<li><a href="https://www.elle.be/" target="_blank">ELLE</a></li>
							<li><a href="https://www.marieclaire.be/" target="_blank">Marie Claire</a></li>
							<li><a href="http://www.psychologies.com/" target="_blank">Psychologies</a></li>
							<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
								<li><a href="https://www.decoidees.be/" target="_blank">Déco Idées</a></li>
							<?php } else { ?>
								<li><a href="https://www.actiefwonen.be/" target="_blank">Actief Wonen</a></li>
							<?php } ?>
							<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
								<li><a href="https://www.elle.be/fr/tag" target="">Tag</a></li>
							<?php } else { ?>
								<li><a href="https://www.elle.be/nl/tag" target="">Tag</a></li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>
				</div>
			</div>

		<!--<div id="splash" class="ads splash" data-categoryAd="" data-formatMOB="MSP" data-refadMOB="pebbleMSP" data-format="Splash" data-refad="pebbleSplash" data-location="" data-position=""></div>-->
		
		<?php wp_footer(); ?>

		<?php if(get_field('identifiant_pebble',$_id_article) != 'no-ads'){ ?>
			<div id="gpt-ad-SKY_RIGHT" class="rmgAd ads u-visually-hidden" data-type="SKY_RIGHT" data-device="all"></div>
		<?php } ?>


		<noscript><img alt="facebook_id" height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=<?php echo get_field('variable_js','option')['id_facebook_pixel']; ?>&ev=PageView&noscript=1"
		/></noscript>

		<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>

		<script type="text/javascript">
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1476999718978965'); // Insert your pixel ID here.
			fbq('track', 'PageView');
		</script>

		 <!-- Pinterest Pixel Base Code -->
		 <script type="text/javascript">
		   !function(e){if(!window.pintrk){window.pintrk=function(){window.pintrk.queue.push(
		     Array.prototype.slice.call(arguments))};var
		     n=window.pintrk;n.queue=[],n.version="3.0";var
		     t=document.createElement("script");t.async=!0,t.src=e;var
		     r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
		   pintrk('load', '2614322781925');
		   pintrk('page');
		 </script>
		 <script>pintrk('track', 'pagevisit');</script>
		 <noscript>
		   <img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?tid=2614322781925&event=init&noscript=1" />
		 </noscript>
		 <!-- End Pinterest Pixel Base Code -->

		<?php } else { ?>

		<script type="text/javascript">
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1839471399637884'); // Insert your pixel ID here.
			fbq('track', 'PageView');
		</script>

		 <!-- Pinterest Pixel Base Code -->
		 <script type="text/javascript">
		   !function(e){if(!window.pintrk){window.pintrk=function(){window.pintrk.queue.push(
		     Array.prototype.slice.call(arguments))};var
		     n=window.pintrk;n.queue=[],n.version="3.0";var
		     t=document.createElement("script");t.async=!0,t.src=e;var
		     r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
		   pintrk('load', '2613021047841');
		   pintrk('page');
		 </script>
		 <script>pintrk('track', 'pagevisit');</script>
		 <noscript>
		   <img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?tid=2613021047841&event=init&noscript=1" />
		 </noscript>
		 <!-- End Pinterest Pixel Base Code -->

		<?php } ?>

		<?php if((is_user_logged_in() == false || $_GET['ean-test-native'] == true) && is_commercial() == '0'){ ?>
			<script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"></script>
		<?php } ?>

		<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
		<script src="https://quizz.elle.be/v1/loader/script?c=RHqku2LwqVEL2RWiZ8XJqw&s=_3eQAKcednP6tbsixiFZoQ&d=quizz.elle.be" async></script>
		<?php } else { ?>
		<script src="https://quizz.elle.be/v1/loader/script?c=RHqku2LwqVEL2RWiZ8XJqw&s=Rfjlo-E8WeGq_tTWi5xYSw&d=quizz.elle.be" async></script>
		<?php } ?>

		<script type="text/javascript"> (function(s,p,o,t,T){ T=p.getElementsByTagName('head')[0]; t=p.createElement('script');t.async=1;t.src=o; T.appendChild(t); })(window,document,'https://static-cdn.spott.ai/embed/embed.js'); </script> 
		
	</body>
</html>