<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" class="main main-category">
	<div class="site-inner">
		<h1>
			<?php _e('Jobs/Stages'); ?>
		</h1>
		<p><?php echo get_field('add_jobsstages_description','option' )?></p>

		<div class="articles"> 
			<div class="content entry-content">
			<?php 
			$jobsstages = get_posts(array( 'post_type' => 'jobsstages', 'order' => 'DESC'));  
				if( $jobsstages ){ 
					foreach ( $posts as $post ) : setup_postdata( $post ); the_post(); $current_post_id++; ?>
						<?php get_template_part('template-parts/general/article'); ?>
						<?php if ($current_post_id == 1){ ?> <?php } ?>
						<?php if ($current_post_id == 3){ ?> 
							<div class="most-popular-ajax"></div>
							<?php } ?> 
					<?php endforeach; wp_reset_postdata();
				} else{ ?>
					<p><?php _e( "Il n'y a pas d'offre pour le moment.", 'html5blank' ); ?></p>
				<?php } ?>
			</div>
			<aside class="sidebar sidebar-index" role="complementary">
				<div id="halfpage-1" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="homepage" data-position=""></div>
				<?php get_template_part('template-parts/general/most-popular'); ?>
				<div id="halfpage-2" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
			</aside> 
			<div class="navigation">
				<?php pressPagination('',3); ?>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>