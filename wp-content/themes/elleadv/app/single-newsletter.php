<?php
if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
    require_once('include/newsletters.php'); 
}
else if(get_field('variable_js','option')['lang_identifiant'] == 'NL'){
    require_once('include/newsletters_nl.php');       
}else{
    require_once('include/newsletters.php'); 
}
?> 
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    <script
              src="https://code.jquery.com/jquery-3.2.1.min.js"
              integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
              crossorigin="anonymous">
    </script>
    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->

    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

    <!-- CSS Reset : BEGIN -->
    <style>

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        *[x-apple-data-detectors],  /* iOS */
        .x-gmail-data-detectors,    /* Gmail */
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
           display: none !important;
           opacity: 0.01 !important;
       }
       /* If the above doesn't work, add a .g-img class to any image in question. */
       img.g-img + div {
           display: none !important;
       }

       /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */
        /* Thanks to Eric Lepetit (@ericlepetitsf) for help troubleshooting */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }

        @media screen and (max-width: 480px) {
            /* What it does: Forces Gmail app to display email full width */
            u ~ div .email-container {
                min-width: 100vw;
                width: 100% !important;
            }
        }

    </style>
    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>
        .area_copy{
                position: fixed;
                top:20px;
                left:20px;
        }

        .button_copy{
                padding: 10px;
                background: #FFFFFF;
                border: 2px solid #000000;
                font-family: sans-serif;
                font-size: 15px;
                line-height: 110%;
                text-align: center;
                text-decoration: none;
                display: block;
                font-weight: bold;
                text-transform: uppercase;
                cursor: pointer;
        }
        
        .message_copy_green{
                padding: 10px;
                background: #4BB543;
                border: 2px solid #000000;
                font-family: sans-serif;
                font-size: 15px;
                line-height: 110%;
                text-align: center;
                text-decoration: none;
                display: block;
                font-weight: bold;
                text-transform: uppercase;
                cursor: pointer;  
                display: none;
        }

        .menu-container{display: block; }
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }

        /* Media Queries */

        @media screen and (max-width: 464px) {
        .logo-img{width: 100%!important;height: auto !important;}
        }

        @media screen and (max-width: 600px) {
            .email-container, .footer-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

            /* What it does: Adjust typography on small screens to improve readability */
            .email-container p {
                font-size: 17px !important;
            }

            .footer-menu {font-size: 10px !important;}
            .menu-container{display: none !important;}
            .icon-fleche {display: none !important;}
            .lien-footer-h2{padding-bottom: 20px !important;}
            h1, .columns-title{font-size: 20px !important; line-height: 125% !important;}
            .h2paddingbottom{padding-bottom: 55px !important}
            .h3paddingbottom{padding-bottom: 30px !important}
            .fct_2_3_articles_2018_img{padding:0px !important;}
        }

    </style>
    <!-- Progressive Enhancements : END -->

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>
<body width="100%" bgcolor="#FFFFFF" style="margin: 0; mso-line-height-rule: exactly;">

<div class="area_copy">
    <button class="button_copy">Copier la newsletter</button>
    <p class="message_copy_green"></p>
</div>

    <center style="width: 100%; background: #FFFFFF; text-align: left;">
        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div class="first_display_text" style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
           
        </div>
        <?php fct_header_2018(); ?>

        <?php 
        global $post;
        global $blank_ligne;
        $blank_ligne = '';
        foreach(get_field('newsletter') as $key=>$bloc)
        {
            switch ($bloc['acf_fc_layout']) 
                        {
                            case '3_articles':
                                fct_3articles_2018($key);
                                break;
                            case '3_articles_com':
                                fct_3articles_2018($key);
                                break;
                            case '2_articles':
                                fct_2articles_2018($key);
                                break;   
                            case 'hero_article':
                                fct_hero_2018($key);
                                break;  
                            case 'related_article':
                                //fct_related_article($key);
                                break;  
                            case 'concours':
                                fct_concours_2018($key);
                                break;  
                            case 'texte':
                                fct_textlibre_2018($key);
                                break;                             
                            default:
                                break;
                        }
        }      
        ?>
        <?php 
        // surchargé le text d'intro // 
        if(get_field('sous_titre_email')){ $blank_ligne = get_field('sous_titre_email'); } else { $blank_ligne = substr($blank_ligne, 0, -2); }
        ?>
        <?php echo "<p style='display:none' class='hidden_div_first_display_text'>".$blank_ligne."</p>"; ?>
        <?php fct_footer_2018(); ?>

    </center>
<script>

        $( "#aoatfcss" ).remove();
        var first_display_text = $(".hidden_div_first_display_text").text();
        $(".first_display_text" ).text(first_display_text);

        var page_content = '';
        page_content = '<!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">';
        page_content += $("html").clone().find("script,noscript,.area_copy").remove().end().html();
        page_content += '</html>';

        $('.button_copy').click(function(){
          var txt = page_content;
          if(!txt || txt == ''){
            return;
          }
          
          copyTextToClipboard(txt);
          $('textarea').val('').focus();
        });

        function copyTextToClipboard(text) {
          var textArea = document.createElement("textarea");

          textArea.style.position = 'fixed';
          textArea.style.top = 0;
          textArea.style.left = 0;
          textArea.style.width = '2em';
          textArea.style.height = '2em';
          textArea.style.padding = 0;
          textArea.style.border = 'none';
          textArea.style.outline = 'none';
          textArea.style.boxShadow = 'none';
          textArea.style.background = 'transparent';
          textArea.value = text;
          document.body.appendChild(textArea);
          textArea.select();
          try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
            $(".message_copy_green" ).text('Newsletter Copiée !');
            $(".message_copy_green").fadeIn().delay(5000).fadeOut();
          } catch (err) {
            console.log('Oops, unable to copy');
          }
          document.body.removeChild(textArea);
        }
</script>
</body>
</html>
