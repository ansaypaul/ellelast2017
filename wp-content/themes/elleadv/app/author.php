<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php 
global $id_author;
$id_author = get_the_author_meta( 'ID' );
?>
<main role="main" class="main main-author" data-author="<?php echo $id_author; ?>">
	<div class="site-inner">
		<?php 
			$user_email = get_the_author_meta( 'user_email' );
			$user_title = get_the_author_meta( 'authortitle' ); 
			$user_3questions = get_field('3questions', 'user_'.$id_author);
			$user_profilpicture = get_field('profil_pic', 'user_'.$id_author);
			$user_twitter = get_the_author_meta( 'twitter' ); 
			$user_pinterest = get_the_author_meta( 'pinterest' ); 
			$user_linkedin = get_the_author_meta( 'linkedin' ); 
			$user_instagram = get_the_author_meta( 'instagram' ); 
			$fname = get_the_author_meta('first_name');
			$lname = get_the_author_meta('last_name');
			$full_name = '';
			if( empty($fname)){
			    $full_name = $lname;
			} elseif( empty( $lname )){
			    $full_name = $fname;
			} else {
			    $full_name = "{$fname} {$lname}";
			}
		?>
		<div class="author vcard" vocab="http://schema.org/" typeof="Person">
			<div class="author-left">
				<div class="author-avatar" property="image">
					<img src="<?php echo $user_profilpicture; ?>" alt="ELLE.be - <?php echo $full_name; ?>">
				</div>
				<?php if ( $user_instagram != '' || $user_twitter != '' || $user_pinterest != '' || $user_linkedin != '' || $user_email != ''){ ?>
					<div class="author-social">
					  	<?php if ( $user_instagram != '' ){ ?>
							<a href="<?php echo $user_instagram; ?>" property="url" class="instagram sprite sprite-instagram" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_twitter != '' ){ ?>
							<a href="https://twitter.com/<?php echo $user_twitter; ?>" property="url" class="twitter sprite sprite-twitter" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_pinterest != '' ){ ?>
							<a href="<?php echo $user_pinterest; ?>" property="url" class="pinterest sprite sprite-pint" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_linkedin != '' ){ ?>
							<a href="<?php echo $user_linkedin; ?>" property="url" class="linkedin sprite  sprite-linkedin" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_email != '' ){ ?>
							<a href="mailto:<?php echo $user_email; ?>" property="email" class="email sprite sprite-mail" target="_blank"></a>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<div class="author-right">
				<h1 property="author" typeof="Person">
					<a class="url fn n author-name" property="name" href="<?php echo esc_url( get_author_posts_url( $id_author ) ); ?>">
						<?php echo $full_name; ?>
					</a>é
				</h1>
				<span property="jobTitle"><?php echo $user_title; ?></span>
				<p><?php echo $user_3questions; ?></p>
			</div>
		</div>

		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$posts = get_posts(array('posts_per_page' => get_option( 'posts_per_page' ), 'post__not_in' => $excludeIDS,'author'  =>  $id_author, 'paged' => $paged )); 
			$time_most_popular = '99999 days ago';
   			$posts_popular = get_posts(array('author'  =>  $id_author, 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC',
            'date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
			_feed_article_block($posts, $posts_popular);
		?>

	</div>
</main>
<?php get_footer(); ?>
