<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" class="main main-category main-category-eaf">
	<div class="site-inner">
		<h1>
			<?php   
			if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
				echo 'Les intervenants';
			}else{
				echo 'De sprekers';
			}
    		?>
		</h1>

			<div class="main-content-wrapper content-wrapper">
			<div class="content">
			<?php 
			$args = array( "post_type" => "speakers-eaf", "numberposts" => -1, "orderby"=> "title", "order" => "ASC" );
			$posts = get_posts($args);  
			//var_dump($speakers);
				if( $posts ){ 
					foreach ( $posts as $post ) : setup_postdata( $speaker ); $current_post_id++; ?>
						<div class="speakers_eaf_main">
							<a href="<?php the_permalink();?>" class="post-title">
								<span class="title-h2"><?php relevanssi_the_title(); ?></span>
							</a>
							<div class="fonction"><?php echo get_field('fonction'); ?></div>
							<div class="description"><?php the_content(); ?></div>
							<div class="social-links">
								<?php if(get_field('link_facebook')) { ?><span class="link_facebook sprite"><a rel="nofollow" href="<?php echo get_field('link_facebook'); ?>" target="_blank">Facebook</a></span> <?php } ?>
								<?php if(get_field('link_siteweb')) { ?><span class="link_siteweb sprite"><a rel="nofollow" href="<?php echo get_field('link_siteweb'); ?>" target="_blank">Site Web</a></span> <?php } ?>
								<?php if(get_field('link_linkedin')) { ?><span class="link_linkedin sprite"><a rel="nofollow" href="<?php echo get_field('link_linkedin'); ?>" target="_blank">Linkedin</a></span> <?php } ?>
							</div>
						</div>
					<?php endforeach; wp_reset_postdata(); ?>
				<?php } ?>
			</div> 
		</div>
	</div>
</main>
<?php get_footer(); ?>