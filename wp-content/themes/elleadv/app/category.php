<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" class="main main-category">
	<div class="site-inner">
		<h1>
			<?php single_cat_title(); ?>
		</h1>
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
		<?php echo category_description(); ?>
		<?php get_template_part('template-parts/general/searchform'); ?>
		<?php 

		$category = get_queried_object();
        $id_category_current = $category->term_id;

       
		if($paged < 2){
			 $posts = get_posts(array('posts_per_page' => 1,'category'=> $id_category_current, 'post__not_in' => $excludeIDS));
			_first_article_block($posts,'','',0); 

			$posts = get_posts(array('posts_per_page' => 6,'category'=> $id_category_current, 'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,$id_category_current,'',''); 

			$time_most_popular = '30 days ago';
			$posts = get_posts(array('posts_per_page' => 3,'category'=> $id_category_current, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			if(count($posts < 3)){
				$time_most_popular = '90 days ago';
				$posts = get_posts(array('posts_per_page' => 3,'category'=> $id_category_current, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			}
			if($posts){ _top_article_block($posts,$id_category_current,''); }
		}

		if(get_field('liste_des_categories',$category) && $paged < 2){
			$ids_category = get_field('liste_des_categories',$category);
			foreach ($ids_category as $id_category) {
				$posts = get_posts(array('posts_per_page' => 1, 'category__and'=> array($id_category_current,$id_category), 'post__not_in' => $excludeIDS));
				_first_article_block($posts,$id_category,'',1); 

				$posts = get_posts(array('posts_per_page' => 6, 'category__and'=> array($id_category_current,$id_category), 'post__not_in' => $excludeIDS));
				_swiper_article_block($posts,$id_category,'',''); 

				$time_most_popular = '30 days ago';
				$posts = get_posts(array('posts_per_page' => 3,'category__and'=> array($id_category_current,$id_category), 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
					if(count($posts) == 0 || !isset($posts)){
						$time_most_popular = '360 days ago';
						$posts = get_posts(array('posts_per_page' => 3,'category__and'=> array($id_category_current,$id_category), 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 	
					}

				if($posts){ _top_article_block($posts,$id_category,''); }
			}

			$posts = get_posts(array('posts_per_page' => get_option( 'posts_per_page' ), 'category' => $id_category_current, 'post__not_in' => $excludeIDS, 'paged' => $paged));
			$time_most_popular = '30 days ago';
   			$posts_popular = get_posts(array('category'  =>  $id_category_current , 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC',
            'date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
            if(!$posts_popular){
            	$time_most_popular = '120 days ago';
            	$posts_popular = get_posts(array('category'  =>  $id_category_current , 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC',
           		 'date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
            	}
			_feed_article_block($posts, $posts_popular);
		}else{
			$posts = get_posts(array('posts_per_page' => get_option( 'posts_per_page' ), 'category' => $id_category_current, 'post__not_in' => $excludeIDS, 'paged' => $paged));
			$time_most_popular = '30 days ago';
   			$posts_popular = get_posts(array('category'  =>  $id_category_current , 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC',
            'date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
            if(!$posts_popular){
            		$time_most_popular = '120 days ago';
            		$posts_popular = get_posts(array('category'  =>  $id_category_current , 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC',
           		 'date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
            }

			_feed_article_block($posts, $posts_popular);
		}

		/*
		if($paged < 2){
			 $posts = get_posts(array('posts_per_page' => 1,'category'=> $id_category_current, 'post__not_in' => $excludeIDS));
			_first_article_block($posts,'','',0); 

			$posts = get_posts(array('posts_per_page' => 6,'category'=> $id_category_current, 'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,$id_category_current,'',''); 
		}

		$time_most_popular = '30 days ago';
		$posts_feeds = get_posts(array('posts_per_page' => 30, 'category' => $id_category_current, 'post__not_in' => $excludeIDS, 'paged' => $paged));
   		$posts_popular = get_posts(array('category'  =>  $id_category_current , 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num','post__not_in' => $excludeIDS,  'order' => 'DESC',
            'date_query' => array(array('after' => $time_most_popular)))); 
		?>
		<div class="articles">
		    <div class="content-article-ajax content entry-content">
		        <?php 
		        $current_post = 0;
		        foreach ( $posts_feeds as $post ) : setup_postdata( $post ); $current_post++;  $excludeIDS[] = $post->ID;
		            global $post;
		            get_template_part('template-parts/general/article'); ?>
		            <?php if ($current_post == 1){ ?> 
		            <div class="site-inner-ads no_print">
		                <div id="billboard-<?php display_id_random();?>" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="MOB640x150" data-refadMOB="pebbleMOB640x150" data-format="" data-refad="" data-location="" data-position="">
		                </div>
		            </div>
		            <?php } ?>
		            <?php if ($current_post == 3){ ?> 
		            <div id="halfpage-mob-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="homepage" data-position="2"></div>
		            <?php } ?> 
		            <?php if ($current_post == 8){ ?> 
		            <div id="halfpage-mob-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="homepage" data-position="2"></div>
		            <?php } ?> 
		            <?php endforeach; wp_reset_postdata(); 
		        ?>
		    </div>
		    <!-- sidebar -->
		    <aside class="sidebar sidebar-index" role="complementary">
		        <div id="halfpage-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="homepage" data-position=""></div> 
		            <div class="most-popular">
		                <span class="title-h3"><?php _e( 'les + lus', 'html5blank' ); ?></span>
		                <ul class="most-popular-titles" >
		                <?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; ?>
		                    <li>
		                        <span class="title-h4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
		                    </li>
		                <?php endforeach; wp_reset_postdata(); ?>
		                </ul>
		            </div>
		            <?php if(isset(autopromo_objet_mag()[0])) { ?>
		               <!--
		               <a class="autopromo-abonnement" href="<?php echo get_field('autopromo_link',autopromo_objet_mag()[0]->ID); ?>">
		                    <img src="<?php echo get_field('autopromo_imu',autopromo_objet_mag()[0]->ID); ?>" alt="Abonnement ELLE" />
		                </a>
		                -->
		            <?php } ?>
		        <div id="halfpage-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
		    </aside>     
		    <div class="navigation">
		     <?php pressPagination('',3); ?>
		    </div>       
		</div>
		*/
		?>
	</div>
</main>
<?php get_footer(); ?>