<?php get_header(); ?>
<main role="main" class="main single-autopromo autopromo">
	<div class="site-inner">
		<h1 class="single-shop">
			<?php the_title(); ?>
		</h1>
		<div class="content entry-content">
			<div class="shop-item">
				<?php $autopromoimu = get_field( 'autopromo_imu' ); ?>
				<?php $autopromolb = get_field( 'autopromo_lb' ); ?>
				<?php $autopromomlb = get_field( 'autopromo_mlb' ); ?>
				<?php $autopromolink = get_field( 'autopromo_link' ); ?>
				<a href="<?php echo $autopromolink; ?>" target="_blank"><img src="<?php echo $autopromoimu; ?>" alt="ELLE IMU"></a>
				<a href="<?php echo $autopromolink; ?>" target="_blank"><img src="<?php echo $autopromolb; ?>" alt="ELLE LEADERBOARD"></a>
				<a href="<?php echo $autopromolink; ?>" target="_blank"><img src="<?php echo $autopromomlb; ?>" alt="ELLE MOBILE LEADERBOARD"></a>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>
