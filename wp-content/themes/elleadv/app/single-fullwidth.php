<?php 

/*
 * Template Name: Article Fullwidth
 * Template Post Type: post, page, product
 */

  
get_header(); 
?>
<?php $excludeIDS[] = get_the_ID(); ?> 
<?php 
	global $post;
	$list_categorie = get_the_category(); 
	$main_categorie = $list_categorie[0];
	if(count($list_categorie) > 1 ){
		$sous_categorie = $list_categorie[1];
	}
	$list_tags = get_the_tags();
	foreach ($list_categorie as $categorie) {
		$list_categorie_name[] = $categorie->slug;
	}
?>
<div class="site-inner site-inner-ads center no_print">
	<div id="billboard-<?php display_id_random();?>" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position="">
	</div>
</div>
<main role="main" class="main main-single">
	<div class="single">
		<!-- site-inner -->
		<div class="site-inner single-wrapper">
			<?php while (have_posts()) : the_post(); setup_postdata($post); ?>
				<!-- article -->
				<div id='pixel_client'><?php $pixel_client = get_field('pixel_client'); echo $pixel_client ?></div>
				<?php
				if(in_array('recettes', $list_categorie_name ) || in_array('recepten', $list_categorie_name ) ){
					get_template_part('template-parts/general/recettes-wrapper'); 
				}else if(in_array('diy', $list_categorie_name )){
					get_template_part('template-parts/general/diy-wrapper'); 
				}else{
					get_template_part('template-parts/general/content-wrapper-fullwidth'); 
				}
				?>
				<!-- /article -->
			<?php endwhile; ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>