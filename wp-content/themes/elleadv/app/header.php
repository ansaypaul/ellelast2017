<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="194x194" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/favicon-194x194.png">
		<link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/android-chrome-192x192.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/favicon-16x16.png">
		
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/img/icons/elle_2018_sprite.svg?v=02072018" as="image">
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/img/icons/elle_background_pattern.svg" as="image">

		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ElleGaborStd-Text.woff" as="font" type="font/woff" crossorigin="anonymous">
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ElleGaborStd-Medium.woff" as="font" type="font/woff" crossorigin="anonymous">
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/ElleGaborStd-DemiBold.woff" as="font" type="font/woff" crossorigin="anonymous">
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/Interstate-LightCondensed.woff2" as="font" type="font/woff" crossorigin="anonymous">
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/Interstate-LightCondItalic.woff2" as="font" type="font/woff" crossorigin="anonymous">
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/Interstate-BoldCondensed.woff2" as="font" type="font/woff" crossorigin="anonymous">

		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/icons/fav-icon/safari-pinned-tab.svg" color="#5bbad5">
		<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
		<!--<script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/pebble/io_fr.js"></script>-->
		<meta name="apple-mobile-web-app-title" content="ELLE Belgique">
		<meta name="application-name" content="ELLE Belgique">
		<meta name="msvalidate.01" content="696D5343DA6D2AA4FA9419EB6C1E93B7" />
		<?php } else { ?>
		<!--<script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/pebble/io_nl.js"></script>-->
		<meta name="apple-mobile-web-app-title" content="ELLE België">
		<meta name="application-name" content="ELLE België">
		<meta name="msvalidate.01" content="696D5343DA6D2AA4FA9419EB6C1E93B7" />
		<?php } ?>

		<meta name="msapplication-TileColor" content="#ff0000">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<?php /*if (is_single()){ ?><link rel="amphtml" href="<?php echo get_site_url(); ?>/amp/?slug=<?php echo $post->post_name; ?>"><?php }*/ ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php 

		$restricted_ads =  get_field('restricted_ads','option'); 
		$actual_link = $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
		$url = strtok($actual_link, '?');

		if(is_home() || is_front_page())
		{
			require_once('ad360/dfpads.php');
		}
		elseif(is_category() && !is_category('sexe')){
			require_once('ad360/dfpads.php');
		}
		elseif(is_page()){
			require_once('ad360/dfpads.php');
		}
		elseif(is_single() && get_field('identifiant_pebble',$_id_article) != 'no-ads' && !has_category('sexe') && get_field('remove_google_tag') != 1 && strpos($restricted_ads, $url) == false) 
		{
			require_once('ad360/dfpads.php');
		}
		else
		{
			//require_once('ad360/dfpads.php');
		}
	


		?>

		<?php wp_head(); ?>

		<script type="text/javascript">
		var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>",
        adposition = "<?php echo mb_strtoupper(ad_position()['cat']); ?>",
        adpositionclean = "<?php echo ad_position()['cat']; ?>",
        adpositiongemius = "<?php echo stripAccents(ad_position()['cat']); ?>",
        adpositionpebble = "<?php echo ad_position()['pebble']; ?>",
        adpositionpebble_categories = "<?php echo ad_position()['pebble_categories']; ?>",
        adpositionbis = "<?php echo html_entity_decode(ad_position()['subcat']); ?>",
        adpositionter = "<?php echo html_entity_decode(ad_position_ter()); ?>",
        adpositiontype = "<?php echo ad_position()['type']; ?>",
        io_title_generic = "<?php echo get_page_title_generic(); ?>",
        io_type_generic = "<?php echo get_page_type_generic(); ?>",
        id_post = "<?php echo get_the_ID(); ?>",
        id_author = "<?php the_author(); ?>",
        id_author_nb = "<?php echo get_the_author_meta('ID'); ?>",
        id_commercial = "<?php echo get_field('variable_js','option')['id_commercial']; ?>",
        article_title = "<?php echo html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8' ); ?>",
        article_link = "<?php the_permalink(); ?>",
        article_thumb = "<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>",
        article_category = "<?php echo substr(get_category_article()['list_categories'],0,-1); ?>",
        article_date = "<?php echo get_the_date('r'); ?>",
        article_date_ga = "<?php echo get_field('date_de_creation'); ?>",
		article_tag = "<?php echo htmlspecialchars_decode(substr(get_category_article()['list_tags'],0,-1)); ?>",
		article_query_search = "<?php echo get_search_query(); ?>",
        lang_identifiant = "<?php echo get_field('variable_js','option')['lang_identifiant']; ?>",
 		lang_identifiantPebble = "<?php echo get_field('variable_js','option')['lang_identifiantpebble']; ?>",
 		lang_identifiantGoogleAnalytics = "<?php echo get_field('variable_js','option')['lang_identifiantgoogleanalytics']; ?>",
 		id_google_analytics_specifique = "<?php echo get_field('variable_js','option')['id_google_analytics_specifique']; ?>",
 		lang_identifiantGemius = "<?php echo get_field('variable_js','option')['lang_identifiantgemius']; ?>",
 		lang_identifiantFacebookSDK= "<?php echo get_field('variable_js','option')['lang_identifiantfacebooksdk']; ?>",
 		id_facebook_pixel = "<?php echo get_field('variable_js','option')['id_facebook_pixel']; ?>",
 		id_inread_pebble = "<?php echo get_field('variable_js','option')['id_inread_pebble']; ?>",
 		id_category_elletv = "<?php echo get_field('categorie_video','option'); ?>",
 		autopromo_imu_src = "<?php echo get_field('autopromo_imu',autopromo_objet()[0]->ID); ?>",
 		autopromo_lb_src = "<?php echo get_field('autopromo_lb',autopromo_objet()[0]->ID); ?>",
 		autopromo_mlb_src = "<?php echo get_field('autopromo_mlb',autopromo_objet()[0]->ID); ?>",
 		autopromo_link_src = "<?php echo get_field('autopromo_link',autopromo_objet()[0]->ID); ?>",
 		no_skimlink_script = "<?php echo get_field('no_skimlink_script',get_the_ID()); ?>",
        height,
        width; 

        var init_GPDR = "<?php echo GDPR ?>";
        console.log('Init de la langue : ' + lang_identifiant);
        console.log('Facebook pixel : ' + id_facebook_pixel);
        </script>

		<!-- Quantcast Choice. Consent Manager Tag v2.0 (for TCF 2.0) -->
		<script type="text/javascript" async=true>
		(function() {
		  var host = window.location.hostname;
		  var element = document.createElement('script');
		  var firstScript = document.getElementsByTagName('script')[0];
		  var url = 'https://quantcast.mgr.consensu.org'
		    .concat('/choice/', 'jd-n9APXgjNXk', '/', host, '/choice.js')
		  var uspTries = 0;
		  var uspTriesLimit = 3;
		  element.async = true;
		  element.type = 'text/javascript';
		  element.src = url;

		  firstScript.parentNode.insertBefore(element, firstScript);

		  function makeStub() {
		    var TCF_LOCATOR_NAME = '__tcfapiLocator';
		    var queue = [];
		    var win = window;
		    var cmpFrame;

		    function addFrame() {
		      var doc = win.document;
		      var otherCMP = !!(win.frames[TCF_LOCATOR_NAME]);

		      if (!otherCMP) {
		        if (doc.body) {
		          var iframe = doc.createElement('iframe');

		          iframe.style.cssText = 'display:none';
		          iframe.name = TCF_LOCATOR_NAME;
		          doc.body.appendChild(iframe);
		        } else {
		          setTimeout(addFrame, 5);
		        }
		      }
		      return !otherCMP;
		    }

		    function tcfAPIHandler() {
		      var gdprApplies;
		      var args = arguments;

		      if (!args.length) {
		        return queue;
		      } else if (args[0] === 'setGdprApplies') {
		        if (
		          args.length > 3 &&
		          args[2] === 2 &&
		          typeof args[3] === 'boolean'
		        ) {
		          gdprApplies = args[3];
		          if (typeof args[2] === 'function') {
		            args[2]('set', true);
		          }
		        }
		      } else if (args[0] === 'ping') {
		        var retr = {
		          gdprApplies: gdprApplies,
		          cmpLoaded: false,
		          cmpStatus: 'stub'
		        };

		        if (typeof args[2] === 'function') {
		          args[2](retr);
		        }
		      } else {
		        queue.push(args);
		      }
		    }

		    function postMessageEventHandler(event) {
		      var msgIsString = typeof event.data === 'string';
		      var json = {};

		      try {
		        if (msgIsString) {
		          json = JSON.parse(event.data);
		        } else {
		          json = event.data;
		        }
		      } catch (ignore) {}

		      var payload = json.__tcfapiCall;

		      if (payload) {
		        window.__tcfapi(
		          payload.command,
		          payload.version,
		          function(retValue, success) {
		            var returnMsg = {
		              __tcfapiReturn: {
		                returnValue: retValue,
		                success: success,
		                callId: payload.callId
		              }
		            };
		            if (msgIsString) {
		              returnMsg = JSON.stringify(returnMsg);
		            }
		            event.source.postMessage(returnMsg, '*');
		          },
		          payload.parameter
		        );
		      }
		    }

		    while (win) {
		      try {
		        if (win.frames[TCF_LOCATOR_NAME]) {
		          cmpFrame = win;
		          break;
		        }
		      } catch (ignore) {}

		      if (win === window.top) {
		        break;
		      }
		      win = win.parent;
		    }
		    if (!cmpFrame) {
		      addFrame();
		      win.__tcfapi = tcfAPIHandler;
		      win.addEventListener('message', postMessageEventHandler, false);
		    }
		  };

		  makeStub();

		  var uspStubFunction = function() {
		    var arg = arguments;
		    if (typeof window.__uspapi !== uspStubFunction) {
		      setTimeout(function() {
		        if (typeof window.__uspapi !== 'undefined') {
		          window.__uspapi.apply(window.__uspapi, arg);
		        }
		      }, 500);
		    }
		  };

		  var checkIfUspIsReady = function() {
		    uspTries++;
		    if (window.__uspapi === uspStubFunction && uspTries < uspTriesLimit) {
		      console.warn('USP is not accessible');
		    } else {
		      clearInterval(uspInterval);
		    }
		  };

		  if (typeof window.__uspapi === 'undefined') {
		    window.__uspapi = uspStubFunction;
		    var uspInterval = setInterval(checkIfUspIsReady, 6000);
		  }
		})();
		</script>
		<!-- End Quantcast Choice. Consent Manager Tag v2.0 (for TCF 2.0) -->


		<!-- Quantcast Tag -->
		<script type="text/javascript">
		var _qevents = _qevents || [];

		(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
		})();

		_qevents.push({
		qacct:"p-jd-n9APXgjNXk"
		});
		</script>

		<noscript>
		<div style="display:none;">
		<img src="//pixel.quantserve.com/pixel/p-jd-n9APXgjNXk.gif" border="0" height="1" width="1" alt="Quantcast"/>
		</div>
		</noscript>
		<!-- End Quantcast tag -->

		<!--
		<script>window.__cmp('getVendorConsents', null, function(vendorConsents) { console.log(vendorConsents) });</script>
		<script>window.__cmp('getPublisherConsents', null, function(publisherConsents) { console.log(publisherConsents) });</script>
		-->

		<script type="text/javascript">
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		</script>
		
		<?php  if ( GDPR != 1 ) { ?>
		<style>
			.iLgUEF.iLgUEF{background-color: #000 !important}
			.xdebug-var-dump{font-size: 10px}
			.gdpr-privacy-bar{display: none !important}
			.Inarticle {min-height: auto !important;}
			.LagHeader-progress{
			height: 6px;
			border-top: 1px solid #000;
			background-color: #f5f5f5;
			}
			.LagHeader-progress-bar {
		    float: left;
		    height: 100%;
		    width: 0;
		    -webkit-transition: width .6s;
		    -o-transition: width .6s;
		    transition: width .6s;
		    background-color: #000000;
			}
			main .site-inner {
    		margin: 0 auto;
    		font-size: 14px;
			}

			#pebbleTopLarge{height: auto !important}

			.ads {overflow: hidden;}
			body.takeover .header {max-width: 1050px;margin: 0 auto;margin-top: -5px}
			#ctcg_frame_65349_0 {display: none;}
			.margin_bottom_20 {margin-bottom : 20px; }
			.margin_top_20 {margin-top : 20px; }
			.pop-online {height: auto;max-height: none}
			.spott-image-source {position: relative !important; color:black !important;text-align: center;}

			.rmgAd[data-type="SKY_RIGHT"] {
			    top: 42px;
			    bottom: auto;
			    left: calc(50% + 520px);
			    transform: none;
			}

			@media only screen and (min-width: 600px) {
			.rmgAd[data-type="SKY_RIGHT"] {
			    top: 80px;
				}
			}

			.rmgAd[data-type="SKY_RIGHT"] {
			    position: fixed;
			    margin-bottom: 0;
			    z-index: 6000;
			}
			.u-visually-hidden{visibility: hidden !important;}
			.video-seo-title {text-align: center;display: block;font-style: italic;}
			.clever_wrapper {position: relative; padding-bottom: 56.25%; height: 0}
			.clever_iframe {position: absolute !important; top: 0; left: 0; width: 100%; height: 100%; border: 0}
			.main-single .atc {text-decoration: underline;cursor:pointer }
			.link-disabled {text-decoration: none;}
			.link-disabled:hover {color:#000 !important;cursor: default;}
			.qc-cmp2-persistent-link {z-index: 1}

			/*
			body.language-FR.category-elle-sante .site-logo, body.language-FR.elle-sante .site-logo{
			    background: url(https://www.elle.be/fr/wp-content/uploads/2020/11/logo_elle-sante_fr_hr-1.jpg) !important;
			    background-size: cover !important;
			}
			body.language-NL.category-elle-gezondheid .site-logo, body.language-NL.elle-gezondheid .site-logo{
			    background: url(https://www.elle.be/fr/wp-content/uploads/2020/11/logo_elle-sante_nl_hr.jpg) !important;
			    background-size: cover !important;
			}
			*/
		</style>
       <?php } ?>

</head> 

<?php 
global $excludeIDS, $category_home_promo, $magazine_cover_src, $var_eat, $_theme, $_id_article;
global $last_magazine;

$_id_article = get_the_id();
$last_magazine = get_last_magazine();

?>
<?php $excludeIDS = array(); ?>
<body <?php body_class(); ?>>

<div id="gpt-ad-OUT_OF_PAGE" class="" data-device="all"></div>

<?php 

$tag_cim = '';
$tag_cim = get_field('tag_cim','option', false, false);

if ( is_category() ) {
	$cat_id = get_query_var( 'cat' );
	if( get_field('tag_cim', 'category_'.$cat_id)) { $tag_cim = get_field('tag_cim', 'category_'.$cat_id, false, false); }
}

if ( is_single() ) {
	$cat_id_array = get_the_category();
	foreach ($cat_id_array as $cat_objet) {	
		if(get_field('tag_cim', 'category_'.$cat_objet->term_id) && get_field('tag_cim', 'category_'.$cat_objet->term_id) != '') { 
			$tag_cim = get_field('tag_cim', 'category_'.$cat_objet->term_id, false, false); 
			if($cat_objet->slug == 'deco' || $cat_objet->slug == 'elle-a-table' || $cat_objet->slug == 'elle-eten'){ break ;}
		}
	}

}

echo $tag_cim;

?>
	<div class="side-fade"></div>
	<!-- header -->

	<header id="header" class="header entry-header clear" role="banner">
		<div class="site-inner">
			<div>
				<a href="<?php echo home_url_elle(); ?>/" class="site-logo sprite">
				<?php if(is_home()){ _e( "<h1>Toutes les infos mode, beauté et lifestyle sur le site ELLE</h1>", 'html5blank' ); } ?>
				</a>
			</div>
			<span data-atc="<?php _encode_base64(__( 'https://www.elle.be/nl/', 'html5blank' )); ?>" class="no_desktop switch-version atc"> ><?php _e( 'NL Versie', 'html5blank' ); ?></span>
			<div class="left-menu">
				<button class="btn-activate burger-btn">
					<span></span>
					<small class="btn-activate button-name"><?php _e( 'Menu', 'html5blank' ); ?></small>
				</button>
				<button class="btn-activate news-btn sprite sprite-news">
					<span class="button-name"><?php _e( 'News', 'html5blank' ); ?></span>
						<span class="notif-number hidden"></span>
				</button>
				<button class="btn-activate search-btn sprite sprite-search" >
					<span class="button-name"><?php _e( 'Recherche', 'html5blank' ); ?></span>
				</button>
				<span data-atc="<?php _encode_base64(__( 'https://www.elle.be/nl/', 'html5blank' )); ?>" class="btn-activate language-btn sprite sprite-language atc">
					<div class="button-name"><?php _e( 'NL Versie', 'html5blank' ); ?></div>
				</span>
			</div>
			<nav class="nav main-nav scrollx" role="navigation">
				<button class="sprite sprite-close">
				</button>
				<div class="scrollbar-wrapper">
					<?php if($_theme == 'elle'){ 
						html5blank_nav("header-menu-2018","main-nav-menu"); 
						get_template_part('template-parts/general/follow-us'); 
						html5blank_nav("footer-nav-menu-2018","footer-nav-menu"); 
					 // 	}else if($_theme == 'elle-active'){ 
						// html5blank_nav("eaf-header-menu-2018","main-nav-menu"); 
						// get_template_part('template-parts/general/follow-us'); 
						// html5blank_nav("footer-nav-menu-2018","footer-nav-menu"); 
						}else if($_theme == 'elle-a-table'){ 
						html5blank_nav("eat-header-menu-2018","main-nav-menu"); 
						get_template_part('template-parts/general/follow-us'); 
						html5blank_nav("footer-nav-menu-2018","footer-nav-menu"); 
						}else if($_theme == 'elle-decoration'){ 
						html5blank_nav("deco-header-menu-2020","main-nav-menu"); 
						get_template_part('template-parts/general/follow-us'); 
						html5blank_nav("footer-nav-menu-2018","footer-nav-menu"); 
						}else{
						html5blank_nav("header-menu-2018","main-nav-menu"); 
						get_template_part('template-parts/general/follow-us'); 
						html5blank_nav("footer-nav-menu-2018","footer-nav-menu"); 
						} ?>
				</div>
			</nav>
			<div class="nav main-news" role="navigation">

				<button class="sprite sprite-close-news">
				</button>
				<div class="scrollbar-wrapper">
					<div class="ajax-news"><span class="loading"><?php _e( 'Chargement en cours', 'html5blank' ); ?>...</span></div>
				</div>
			</div>
			<div class="mobile-menu">
				<ul>
					<li>
						<button class="sprite sprite-goback" title="Go back button"></button>
					</li>
					<li>
						<button class="btn-activate sprite sprite-search" title="Search button"></button>
					</li>
					<li>
						<button class="btn-activate burger-mobile burger-btn" title="Menu button">
							<span></span>
						</button>
					</li>
					<li>
						<button class="btn-activate sprite sprite-news news-btn" title="News button">
							<span class="notif-number hidden"></span>
						</button>
					</li>
					<li>
						<button class="btn-activate sprite sprite-social" title="Social button"></button>
					</li>
				</ul>
				<div class="display-social display-social-mobile">
				</div>
			</div>
			<div class="right-menu">
				<button class="btn-activate account-btn sprite sprite-account" >
					<span class="button-name"><?php _e( 'Mon compte', 'html5blank' ); ?></span>
				</button>

				<button class="btn-activate follow-btn sprite sprite-social">
					<span class="button-name"><?php _e( 'Suivez-nous', 'html5blank' ); ?></span>
					<span class="display-social display-social-desktop"></span>
				</button>
				<a class="btn-activate newsletter-btn sprite sprite-newsletter" href="<?php echo home_url(); ?>/newsletters">
					<div class="button-name">
						<?php _e('Newsletter'); ?>
					</div>
				</a>
				<?php if($_theme == 'elle'){ ?>
				<a rel="nofollow" href="<?php echo get_field('magazine_viapress',$last_magazine[0]->ID); ?>" class="cover-link" target="_blank">	
					<?php 
						$img_html = "<img src='". get_the_post_thumbnail_url($last_magazine[0]->ID)."' alt='ELLE Cover' />";
						$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
						echo $img_html;
					?>
					<div class="button-name"><?php _e( 'Abonnez-vous', 'html5blank' ); ?></div>
				</a>
				<?php } else { ?>
				<span class="cover-link" target="_blank">	
					<?php 
						$img_html = "<img src='". get_the_post_thumbnail_url($last_magazine[0]->ID)."' alt='ELLE Cover' />";
						$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
						echo $img_html;
					?>
				</span>
				<?php } ?>
			</div>
		</div>
		<nav class="nav second-nav" role="navigation">
			<?php if($_theme == 'elle'){ 
					html5blank_nav("second-nav-menu-2018","second-nav-menu"); 
				// } else if($_theme == 'elle-active'){ 
				// 	html5blank_nav("eaf-second-nav-menu-2018","second-nav-menu"); 
				} else if($_theme == 'elle-a-table'){ 
					html5blank_nav("eat-second-nav-menu-2018","second-nav-menu"); 
				} else if($_theme == 'elle-decoration'){ 
					html5blank_nav("deco-second-nav-menu-2020","second-nav-menu"); 
				} else {
					html5blank_nav("second-nav-menu-2018","second-nav-menu"); 
				}?>
		</nav>
	</header>

	<div class="search-form"><?php get_template_part('template-parts/general/searchform'); ?></div>

	<div class="floating-second-nav">
		<div class="site-inner">
			<button class="btn-activate burger-mobile burger-btn" title="Menu button">
				<span></span>
			</button>
			<button class="btn-activate sprite sprite-search" title="Search button"></button>
			<div class="site-logo-wrapper"><a href="<?php echo home_url(); ?>/" class="site-logo sprite" title="ELLE logo"></a></div>
		<nav class="nav second-nav" role="navigation">
			<?php if($_theme == 'elle'){ 
					html5blank_nav("second-nav-menu-2018","second-nav-menu"); 
				// } else if($_theme == 'elle-active'){ 
				// 	html5blank_nav("eaf-second-nav-menu-2018","second-nav-menu"); 
				} else if($_theme == 'elle-a-table'){ 
					html5blank_nav("eat-second-nav-menu-2018","second-nav-menu"); 
				} else if($_theme == 'elle-decoration'){ 
					html5blank_nav("deco-second-nav-menu-2020","second-nav-menu"); 
				} else {
					html5blank_nav("second-nav-menu-2018","second-nav-menu"); 
				}?>
		</nav>
			<button class="btn-activate sprite sprite-social" title="Social button">
				<span class="display-social-desktop"></span>
			</button>
		</div>
		<!--<div class="LagHeader-progress"><div class="LagHeader-progress-bar"></div></div>-->
	</div>

	<div class="site-inner-ads center no_print">
		<div id="gpt-ad-BANNER_ABOVE" class="rmgAd" data-device="all" data-type="BANNER_ABOVE">
		</div>
	</div>