<?php get_header(); ?>
<?php global $post; global $last_magazine; global $excludeIDS; ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" class="main main-category eat">
	<div class="site-inner">
		<h1><?php single_cat_title(); ?></h1>
		<?php echo category_description(); ?>
		<!-- search -->
		<?php get_template_part('template-parts/general/searchform'); ?>
		<!-- /search -->
		<?php 
			$id_category_current = get_field('id_category_elle_a_table','option');
			$posts = get_posts(array('posts_per_page' => 1, 'category'=> $id_category_current, 'post__not_in' => $excludeIDS));
			_first_article_block($posts,'','',0);

			$posts = get_posts(array('posts_per_page' => 6, 'category'=> $id_category_current, 'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,$id_category_current,'',__('+ de ELLE à table', "html5blank")); 
		?>
		<div id="halfpage-mob-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="homepage" data-position="2"></div>
		
		<?php
			$tag_id = get_field('tag_mis_en_avant',$last_magazine[0]->ID);
			$posts = get_posts(array('posts_per_page' => 3,'tag__in' => array($tag_id),'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,'',$tag_id,__("Les recettes du moment", "html5blank")); 
			$time_most_popular = '99999 days ago';
			$posts = get_posts(array('posts_per_page' => 3,'tag__in' => array($tag_id), 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			_top_article_block($posts,'',__('Quoi de neuf ?', "html5blank" )); 
		?>

		 <!-- MENU DE LA SEMAINE -->
		<h2><?php _e( "Le menu de la semaine", "html5blank" ); ?></h2>
		<div class="articles"> 
			<div class="content entry-content">
			<?php 
			if(get_field('variable_js','option')['lang_identifiant'] == 'FR')
			{
				$arg_entree = array('posts_per_page' => 1,'category_name' => 'recettes','meta_query' => array( array( 'key'   => 'recipe-type', 'value' => 'Entrée', 'compare'   => 'LIKE' ) ) );
				$arg_plat = array('posts_per_page' => 1,'category_name' => 'recettes','meta_query' => array( array( 'key'   => 'recipe-type', 'value' => 'Lunch/dîner', 'compare'   => 'LIKE' ) ) );
				$arg_dessert = array('posts_per_page' => 1,'category_name' => 'recettes','meta_query' => array( array( 'key'   => 'recipe-type', 'value' => 'Dessert', 'compare'   => 'LIKE' ) ) );
			}else{
				$arg_entree = array('posts_per_page' => 1,'category_name' => 'recepten','meta_query' => array( array( 'key'   => 'soort_gerecht', 'value' => 'Ontbijt', 'compare'   => 'LIKE' ) ) );
				$arg_plat = array('posts_per_page' => 1,'category_name' => 'recepten','meta_query' => array( array( 'key'   => 'soort_gerecht', 'value' => 'Lunch/Diner', 'compare'   => 'LIKE' ) ) );
				$arg_dessert = array('posts_per_page' => 1,'category_name' => 'recepten','meta_query' => array( array( 'key'   => 'soort_gerecht', 'value' => 'Dessert', 'compare'   => 'LIKE' ) ) );
			}

				$posts = get_posts($arg_entree); 
				foreach ( $posts as $post ) :  
				setup_postdata( $post ); get_template_part('template-parts/general/article-recette'); 
				endforeach; wp_reset_postdata(); 
			?>
			<?php 
				$posts = get_posts($arg_plat); 
				foreach ( $posts as $post ) :  
				setup_postdata( $post ); get_template_part('template-parts/general/article-recette'); 
				endforeach; wp_reset_postdata();  
			?>
			<?php 
				$posts = get_posts($arg_dessert); 
				foreach ( $posts as $post ) :  
				setup_postdata( $post ); get_template_part('template-parts/general/article-recette'); 
				endforeach; wp_reset_postdata(); 
			?>
			<span><a class="custom-cta" href="<?php _e("https://www.elle.be/fr/recettes", "html5blank" ); ?>"><?php _e("Voir + de recettes", "html5blank" ); ?></a></span>
			<div id="halfpage-mob-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="homepage" data-position="2"></div>
			</div>
			<!-- sidebar -->
			<aside class="sidebar sidebar-index" role="complementary">
				<div id="halfpage-1" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="" data-position=""></div>
				<?php get_template_part('template-parts/elle-a-table/eat-categories'); ?>
			</aside> 
			<!-- /sidebar -->
		</div>

		<?php		
			$time_most_popular = '60 days ago';
			$id_category = get_field('id_category_recette','option');
			$posts = get_posts(array('posts_per_page' => 3,'category'=> $id_category, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			if($posts){ _top_article_block($posts,'',__('Le top des recettes', "html5blank" )); }
		?>

		<!-- LES ADRESSES -->
		<h2><?php _e( "Les adresses", "html5blank" ); ?></h2>
		<div class="articles"> 
			<div class="content entry-content">
				<?php
					$id_category_current_adresse = get_field('id_category_adresses','option');
					$posts = get_posts(array('posts_per_page' => 3, 'category'=> $id_category_current_adresse));
					foreach ( $posts as $post ) :  
					setup_postdata( $post ); get_template_part('template-parts/general/article'); 
					endforeach; wp_reset_postdata(); 
				?>
			<span><a class="custom-cta" href="<?php _e('https://www.elle.be/fr/adresses', 'html5blank' ); ?>"><?php _e("Voir + d'adresses", "html5blank" ); ?></a></span>
			<div id="halfpage-mob-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="homepage" data-position="2"></div>

			</div>
			<!-- sidebar -->	
			<aside class="sidebar sidebar-index" role="complementary">
				<div id="halfpage-2" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="" data-position="2"></div>
				<?php get_template_part('template-parts/elle-a-table/eat-adresses'); ?>
			</aside> 
			<!-- /sidebar -->
		</div>

		<?php		
			$time_most_popular = '60 days ago';
			$id_category = 1451;
			$posts = get_posts(array('posts_per_page' => 3,'category'=> $id_category_current_adresse , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			if($posts){ _top_article_block($posts,'',__('Le top des adresses', "html5blank" )); }
		?>


		<?php 
			$id_category_current_pratique = get_field('id_category_pratique','option');
			$posts = get_posts(array('posts_per_page' => 6, 'category'=> $id_category_current_pratique, 'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,$id_category_current_pratique,'',''); 
		?>

	<?php get_template_part('template-parts/general/swiper-instagram'); ?>
	</div>
</main>
<?php get_footer(); ?>