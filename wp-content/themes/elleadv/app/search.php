<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php


$search_refer = $_GET['site_section'];

if ($search_refer == 'recipe'){ 
	$_GET['category'] == get_field('id_category_recette');
}
elseif ($search_refer == 'site-search'){ }; 

$args = array(
    's' => $_GET['s'],
    'paged' => $paged,
    'post_status' => 'publish',
    'posts_per_page' => get_option( 'posts_per_page' ),
	//'meta_query' => array(array('key' => 'ou_','compare' => 'IN','value' => $_GET['loc'])),
    'orderby' =>  $_GET['sortedBy'],
    'date_query' => array(array('after' => $_GET['date'].' days ago')),
    'no_found_rows'          => true,
    'update_post_term_cache' => false,
    'update_post_meta_cache' => false,
    'cache_results'          => false
);

if(isset($_GET['category']) && $_GET['category'] != ''){ $args['category'] = $_GET['category']; }

if(isset($_GET['loc']) && $_GET['loc'] != ''){ $args['meta_query'][] = array('key' => 'ou_','compare' => 'IN','value' => $_GET['loc']); }
if(isset($_GET['loctype']) && $_GET['loctype'] != ''){ $args['meta_query'][] = array('key' => 'quoi_','compare' => 'IN','value' => $_GET['loctype']); }

if(isset($_GET['dishtypes']) && $_GET['dishtypes'] != ''){ 
	$args['meta_query']['dishtypes'] = array('relation' => 'OR');
	foreach ($_GET['dishtypes'] as $dishtypes) {
		$args['meta_query']['dishtypes'][] = array('key' => 'recipe-type','compare' => 'LIKE','value' => '"'.$dishtypes.'"'); 
	}
}

if(isset($_GET['diff']) && $_GET['diff'] != ''){ $args['meta_query'][] = array('key' => 'recipe-difficulties','compare' => 'IN','value' => $_GET['diff']); }

$posts =  get_posts($args);

global $wp_query;
$num_pages = $wp_query->found_posts;

?>

<main role="main" class="main main-category">
	<div class="site-inner">

	<form class="" role="search"  action="<?php echo get_home_url();?>/" method="get">
			<input type="hidden" name="search_referer" value="<?php echo $_GET['search_referer']; ?>">
			<input type="hidden" name="category[]" value="<?php echo $_GET['category'][0]; ?>"/>


			<div class="site-inner">
				<div class="search-input-wrapper">
					<label for="search_input"><?php _e('Recherche', 'html5blank' ); ?>
						<input id="search_input" autocomplete="off" class="input_ajax search-input" type="text" name="s" placeholder="<?php _e('Rechercher un article', 'html5blank' ); ?>" value="<?php echo $_GET['s']; ?>" >
						<input type="submit" class="custom-cta"  value="<?php _e( "Rechercher", "html5blank" ); ?>"></span>
					</label>
				</div>

			</div>

			<!-- sidebar -->
			<aside class="sidebar sidebar-index sidebar-search" role="complementary">
				<?php get_template_part('template-parts/general/searchform-sidebar'); ?>
			</aside> 
			<!-- /sidebar -->


		<div class="articles articles-search">

			<div class="content entry-content">
			<?php if( $posts ){ ?>
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); the_post(); $current_post_id++; ?>
						<?php 
							$list_categorie = get_the_category(); 
							$list_categorie_name = array();
							foreach ($list_categorie as $categorie) {
								$list_categorie_name[] = $categorie->slug;
							}
							if(in_array('recettes', $list_categorie_name ) || in_array('recepten', $list_categorie_name ) ){
								get_template_part('template-parts/general/article-recette'); 
							}else if(in_array('diy', $list_categorie_name )){
								get_template_part('template-parts/general/article'); 
							}else{
								get_template_part('template-parts/general/article'); 
							}
						?>
						<?php endforeach; wp_reset_postdata(); ?>
			<?php } else { ?>
				<p><?php _e( "Il n'y a pas de contenu pour le moment.", 'html5blank' ); ?></p>
			<?php } ?>
			</div>
			<div class="navigation">
				<?php pressPagination('',3); ?>
			</div>
		</div>
</form>

		<?php if($paged == 1 && $display_slider == 1){ ?> 
		<div class="swiper-article-ajax">
			<span class="loading"><?php _e( 'Chargement en cours', 'html5blank' ); ?>...</span>
		</div> 
		<?php } ?>
	</div>
</main>
<?php get_footer(); ?>