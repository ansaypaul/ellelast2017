<?php

/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

/*
 *  Author: Ansay Pauly
 *  URL: 
 *  Custom functions, support, custom post types and more.
 */ 

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// Load any external files you have here
//require_once('include/maintenance.php');
//die();

require_once('include/helper.php');
require_once('include/custom_post.php');
require_once('include/gallery.php');
require_once('include/block.php');
require_once('include/rss_common_horo.php');

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_5de5296827707',
    'title' => 'GOOGLE DV360',
    'fields' => array (
        array (
            'key' => 'field_5de52a20c327a',
            'label' => 'TAG DV ACCOUNT',
            'name' => 'tag_dv_account',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_5de52a3a765f2',
            'label' => 'TAG SITE',
            'name' => 'tag_dv_site',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_5de52a4bae5fa',
            'label' => 'TAG CATEGORY',
            'name' => 'tag_dv_category',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_5de52a3a76748',
            'label' => 'Active Takeover',
            'name' => 'activeTakeover',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'message' => '',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array (
            'key' => 'field_5de52a3a76521',
            'label' => 'Active DEBUG',
            'name' => 'activeDebug',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'message' => '',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'taxonomy',
                'operator' => '==',
                'value' => 'category',
            ),
        ),
        array (
            array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

add_filter('body_class','add_category_to_single');
  function add_category_to_single($classes) {
    if (is_single() ) {
      global $post;
      foreach((get_the_category($post->ID)) as $category) {
        // add category slug to the $classes array
        $classes[] = $category->category_nicename;
      }
    }
    // return the $classes array
    return $classes;
  }

add_filter('body_class', 'append_language_class');
    function append_language_class($classes){
    $classes[] = 'language-'.get_field('variable_js','option')['lang_identifiant']; 
    return $classes;
}

function the_content_filter_links( $value ) {

    $doc = new DOMDocument();
    $doc->loadHTML($value);
    // 1. Find all links that contain the string `specific-string`
    if (strpos($value, 'a href')) {
        // 2. Remove all attributes from `a` tag.    
        $link = $doc->getElementsByTagName('a')->item(0);
        // 3. Change opening and closing `a` tags to `span`.
        $value = '<span href="'.$link->getAttribute('href').'">'.$link->nodeValue.'</span>';
        // Output the content
        return $value; 
    }
    return false;
}; 

//add_filter( 'the_content', 'the_content_filter_links', 10, 1 ); 

add_filter('the_content', 'removelink_content',1);

function removelink_content($content = '')
{
    global $post;
    if(get_field('obs_link') == true){
        var_dump(get_field('obs_link',$post->ID));
        preg_match_all('#<a href="(.*?)">(.*?)</a>#i',$content, $matches);
        $num = count($matches[0]);
        for($i = 0;$i < $num;$i++){

           // $content = str_replace($matches[0][$i] , $matches[2][$i] , $content);
            
            /*
            $url_encode = base64_encode($url);
            $string = '<span class="atc" data-atc="'.$url_encode.'">'.$txt.'</span>';
            */
            $link = $matches[1][$i];

            $url_encode = base64_encode($link);
            if($link != 'https://www.blackfriday-france.com/' ){
                $content = str_replace($matches[0][$i] , '<span class="atc" data-atc="'.$url_encode.'"> '.$matches[2][$i].'</span>' , $content);
            }

        }
    }

    return $content;
}


function wp_rocket_add_purge_posts_to_author() {
    // gets the author role object
    $role = get_role('administrator');
 
    // add a new capability
    $role->add_cap('rocket_manage_options', true);
}
add_action('init', 'wp_rocket_add_purge_posts_to_author', 12);

add_filter('the_posts', 'show_all_future_posts');

function show_all_future_posts($posts)
{
   global $wp_query, $wpdb;
   if(is_single() && $wp_query->post_count == 0)
   {
      $posts = $wpdb->get_results($wp_query->request);
      if(count($posts) > 0){
        $status = get_post_status($posts[0]->ID);
        if(($status == 'trash' ||  $status == 'draft') && ! is_user_logged_in()){$posts = array();}
      }
   }
   return $posts;
}

/* ACF options + link file */
if( function_exists('acf_add_options_page') ) {
    require_once('include/acf.php');
    acf_add_options_page(); 
}

$_options_acf = get_field('variable_js','option');

global $_theme;
global $member, $actito_bdd, $version, $_id_article;
$version = '20112019';

$_theme = 'elle';

add_filter('wpacu_access_role', function($role) { return 'superadmin'; });

/****/
// Désactive Contact Form 7 + Google reCaptcha de tout le site sauf 3 pages

/*** EAF SHORTCODE **/
function eaf_shortcode_2() {  
    $output = '';
    $output .= '<div class="event_eaf_main">';  

    if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
        $_array_jours = array('Vendredi 15 novembre 2019','Samedi 16 novembre 2019');
        $_array_jours_titre = array('Vendredi 15 novembre 2019','Samedi 16 novembre 2019');
    }else{
        $_array_jours = array('Vendredi 15 novembre 2019','Samedi 16 novembre 2019');
        $_array_jours_titre = array('Vrijdag 15 November 2019','Zaterdag 16 November 2019');
    }
    $_index_array = 0;

    foreach( $_array_jours as $_array_jour ):
        $output .= '<div class="events_jours">';
        $output .= '<span class="events_jours_titre">'.$_array_jours_titre[$_index_array].'</span>';
        $jours_events = get_field('jours_events');
        $salles = get_posts(array( 'post_type' => 'salle-eaf','orderby' => 'title','orderby'=>'ID', 'order' => 'ASC', 'posts_per_page' => -1));  
        if( $salles ){ 
            foreach( $salles as $post ) : setup_postdata( $post ); 
                $output .= '<div class="events_salle">';
                $output .= '<span class="events_salle_nom">'.get_the_title($post->ID).'</span>';
                $output .= '<span class="events_salle_nombre_place">'.get_the_content($post->ID).'</span>';
                $arg = array(
                'post_type' => 'programme-eaf',
                'orderby' => 'title',
                'posts_per_page' => -1,
                'meta_key' => 'salle',
                'meta_value'=> $post->ID,
                'orderby', 'meta_value',
                'order', 'ASC');

                $arg = array(
                'post_type' => 'programme-eaf',
                'orderby' => 'title',
                'posts_per_page' => -1,
                'orderby'   => 'meta_value', 
                'order' => 'ASC',
                'meta_key' => 'date__heure',
                'meta_query' => array(
                    'relation' => 'AND',
                    array('key' => 'salle','value' => $post->ID),
                    array('jour' => 'salle','value' => $_array_jour)

                    )
                );
                $programmes = get_posts($arg);  
                foreach( $programmes as $programme ) : setup_postdata( $programme );
                    $intervenant_id = get_field('intervenant',$programme->ID);
                    $output .= '<div class="programme">';
                    $output .= '<span class="programme_heure">'.get_field('date__heure',$programme->ID).'</span>';   
                    if(get_field('table_ronde',$programme->ID) == true){ 
                        if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
                            $output .= '<span class="programme_intervenant">Table ronde</span>';
                        }else{
                            $output .= '<span class="programme_intervenant">Ronde tafel</span>';
                        }
                    }
                    else{$output .= '<span class="programme_intervenant">'.get_the_title($intervenant_id).'</span>';}   
                    
                    $output .= '<span class="programme_sujet">'.get_the_title($programme->ID).'</span>';
                    $output .= '<div class="programme-hide">';
                    $output .= '<span class="programme_lang">'.get_field('langue',$programme->ID).'</span>';
                    $output .= '<span class="programme_duree">'.get_field('duree',$programme->ID).' min</span>';
                    $output .= '<span class="programme_description">'.get_the_content($programme->ID).'</span>';
                    if($intervenant_id != '') { $output .= '<span class="programme_intervenant"><a target="_blank" rel="nofollow" href="'.get_the_permalink($intervenant_id).'">'.get_the_title($intervenant_id).'</a></span>'; }
                    $output .= '</div>';                                   
                    $output .= '</div>';
                endforeach; wp_reset_postdata();
                $output .= '</div>';
            endforeach; wp_reset_postdata();
        }
        $output .= '</div>';
        $_index_array++;
    endforeach;
    $output .= '</div>';
    return $output;
}

add_shortcode('eaf_shortcode_2', 'eaf_shortcode_2');


function eaf_shortcode() {    
    $jours_events =  get_field('jours_events');
    $jours_events_o = json_decode(json_encode($jours_events), FALSE);
    $object = (object) $jours_events_o;
    $output = '';
    $output .= '<div class="event_eaf_main">';
    foreach ($object as $jour_events) {
        $output .= '<div class="events_jours">';
        $output .= '<span class="events_jours_titre">'.$jour_events->titre.'</span>';
           foreach ($jour_events->jours as $jour) {
                foreach ($jour as $salle) {
                     $output .= '<div class="events_salle">';
                     $output .= '<span class="events_salle_nom">'.$salle->nom_salle.'</span>';
                     $output .= '<span class="events_salle_nombre_place">'.$salle->nombre_de_place.'</span>';
                     foreach ($salle->programme as $programme) {
                        $output .= '<div class="programme">';
                        $output .= '<span class="programme_heure">'.$programme->heure.'</span>';            
                        $output .= '<span class="programme_intervenant">'.$programme->intervenant->post_title.'</span>';
                        $output .= '<span class="programme_sujet">'.$programme->sujet.'</span>';
                        $output .= '<span class="programme_duree">'.$programme->duree.'</span>';
                        $output .= '<span class="programme_lang">FR/NL</span>';
                        $output .= '<div class="programme-hide">';
                        $output .= '<span class="programme_description">'.$programme->description.'</span>';
                        $output .= '<span class="programme_intervenant"><a target="_blank" rel="nofollow" href="'.get_the_permalink($programme->intervenant->ID).'">'.$programme->intervenant->post_title.'</a></span>';
                        $output .= '</div>';                                   
                        $output .= '</div>';
                    }
                    $output .= '</div>';
                }
        }
        $output .= '</div>';
    }
    $output .= '</div>';
    return $output;
}

add_shortcode('eaf_shortcode', 'eaf_shortcode');

add_action( 'wp_print_scripts', 'my_deregister_newsletter', 100 );
function my_deregister_newsletter() {
    if ( is_single() ) {
        wp_deregister_script( 'jqueryvalidate' );
        wp_dequeue_script('jqueryvalidate');

        wp_deregister_script( 'jqueryvalidatelangfr' );
        wp_dequeue_script('jqueryvalidatelangfr');

        wp_deregister_script( 'ev-newsletter' );
        wp_dequeue_script('ev-newsletter');

    }
}


function eaf_partenaire() {    
    $partenaires =  get_field('partenaire');
    $partenaires_o = json_decode(json_encode($partenaires), FALSE);
    $objects = (object) $partenaires_o;
   // var_dump($partenaires_o);
    $output .= '<div class="event_eaf_main">';  
    $output .= '<div class="events_jours">';
    
    foreach ($objects as $object) {

        $output .= '<div class="events_salle">';
        $output .= '<span class="events_salle_nom">'.$object->nom.'</span>';
        $output .= '<span class="events_salle_logo"><img style="padding: 10px;" src="'.$object->logo.'"/></span>';
         foreach ($object->programme as $programme) {
            $output .= '<div class="programme">';
            $output .= '<span class="programme_sujet">'.$programme->sujet.'</span>';                                
            $output .= '</div>';
         }

        $output .= '</div>';
    }

    $output .= '</div>';
    $output .= '</div>';
    return $output;
}

add_shortcode('eaf_partenaire', 'eaf_partenaire');


// Désactive bhittani_plugin_kksr_js

add_action( 'wp_print_scripts', 'my_deregister_kkstarrating', 100 );
function my_deregister_kkstarrating() 
{
    $list_categorie = get_the_category(); 
    foreach ($list_categorie as $categorie) {
        $list_categorie_name[] = $categorie->slug;
    }

    if(!in_category('recettes') && !in_category('recepten')){
        wp_deregister_script( 'bhittani_plugin_kksr_js' );
        wp_dequeue_script('bhittani_plugin_kksr_js');
    }
}


/****/

if ( ! defined( 'ABSPATH' ) ) exit;
function seomix_remove_homepage_pagination( $query ) {
 if ( is_home() && $query->is_main_query() ) {
   if ( !$query->get( 'page_id' ) != get_option( 'page_on_front' ) ) {
       $query->set('no_found_rows', true);
   }
 }
}
add_action( 'pre_get_posts', 'seomix_remove_homepage_pagination' );

function seomix_redirect_homepage_pagination () {
 global $paged, $page;
 if ( is_front_page() && is_home() && ( $paged >= 2 || $page >= 2 ) ) {
    wp_redirect( home_url().'/' , '301' );
    die;
 }
}
add_action( 'template_redirect', 'seomix_redirect_homepage_pagination' );


function _encode_base64($url){
    $url_encode = base64_encode($url);
    echo $url_encode;
}

function __encode_base64($url){
    $url_encode = base64_encode($url);
    return $url_encode;
}

function encode_link_base64($url = '',$txt = '',$classe = ''){
    $url_encode = base64_encode($url);
    $string = '<span class="atc" data-atc="'.$url_encode.'">'.$txt.'</span>';
    return $string;
}

function home_url_elle() {
    if (is_category() || is_single()) {
        if ( is_category('elle-a-table') || has_category('elle-a-table')){
            $idObj = get_category_by_slug('elle-a-table'); 
            $id = $idObj->term_id;
            $output = get_term_link( $id  );
            return $output;
        }else if(is_category('elle-eten') || has_category('elle-eten')){
            $idObj = get_category_by_slug('elle-eten'); 
            $id = $idObj->term_id;
            $output = get_term_link( $id  );
            return $output;
        }else if(is_category('elle-active') || has_category('elle-active')){
            $idObj = get_category_by_slug('elle-active'); 
            $id = $idObj->term_id;
            $output = get_term_link( $id  );
            return $output;
        }else if(is_category('elle-decoration') || has_category('elle-decoration')){
            $idObj = get_category_by_slug('elle-decoration'); 
            $id = $idObj->term_id;
            $output = get_term_link( $id  );
            return $output;
        }else{
            return home_url();
        }
    }else{
         return home_url();
    }
}

add_filter( 'body_class', 'add_eat_body_class');
function add_eat_body_class( $classes ) {
    global $_theme;
    if (is_category() || is_single() || is_post_type_archive()) {
        if(is_category('elle-active') || has_category('elle-active') || is_singular( 'speakers-eaf' ) || is_post_type_archive('speakers-eaf')){
            $classes[] = 'elle-active';
            $_theme = 'elle-active';
        }else if(is_category('elle-a-table') || has_category('elle-a-table') || is_category('pratique') || is_category('actu-food')  || is_category('recettes') || has_category('recettes')){
            $classes[] = 'elle-a-table';
            $_theme = 'elle-a-table';
        }else if(is_category('elle-decoration') || has_category('elle-decoration') || has_category('bezoeken') || has_category('tips-deco') || has_category('ruimten') || has_category('trends') || is_category('tendances-deco') || is_category('pieces') || is_category('astuces') || is_category('visites')){
            $classes[] = 'elle-decoration';
            $_theme = 'elle-decoration';
        }else if(is_category('elle-eten') || has_category('elle-eten') || is_category('recepten') || is_category('trends-food') || is_category('tips') || has_category('recepten')){
            $classes[] = 'elle-eten';
            $_theme = 'elle-a-table';
        }
    }
return $classes; 
}



function bpc_remove_schedule_delete() {
    remove_action( 'wp_scheduled_delete', 'wp_scheduled_delete' );
}
add_action( 'init', 'bpc_remove_schedule_delete' );

function pressPagination($pages = '', $range = 2)
{
    global $paged;
    $showitems= ($range * 2)+1;
 
    if(empty($paged)) $paged = 1;
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
                   $pages = 1;
        }
    }

    $default_posts_per_page = get_option( 'posts_per_page' );
    $nb_pages_div =  intval($paged / $default_posts_per_page); //1,2
    $nb_pages_max =  intval($pages / $default_posts_per_page); //13
    
    if(1 != $pages)
    {        
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                $link = get_pagenum_link($i);
                if($i == 1){$link = rtrim($link,'/'); }
                echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".$link."' class='inactive' >".$i."</a>";
            }
        }

        if($nb_pages_div + 2 < $nb_pages_max){
            $nb_pages_div_10 = ($nb_pages_div + 1) * 10;
            $nb_pages_div_20 = ($nb_pages_div + 2) * 10;
            //echo  'page'.$nb_pages_div_10;
            echo "<span class='next_bullets'>...</span>";
            echo "<a href='".get_pagenum_link($nb_pages_div_10)."' class='inactive' >".$nb_pages_div_10."</a>";
            echo "<a href='".get_pagenum_link($nb_pages_div_20)."' class='inactive' >".$nb_pages_div_20."</a>";
        }  
    }
 
}

function display_single_RS(){ ?>
<a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="facebook" class="st-custom-button sprite facebook sprite-fb"></a>
<a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="twitter" class="st-custom-button sprite twitter sprite-twitter"></a>
<a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="pinterest" class="st-custom-button sprite pinterest sprite-pint"></a>
<a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="whatsapp" class="st-custom-button sprite whatsapp sprite-whatsapp"></a>
<?php }

function display_RS(){ ?>
<ul class='article-social-links'>
    <li class='sprite sprite-social-article'></li>
    <li>
        <a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="facebook" class="st-custom-button sprite facebook sprite-fb"></a>
    </li>

    <li>
        <a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="twitter" class="st-custom-button sprite twitter sprite-twitter"></a>
    </li>
    <li>
        <a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="pinterest" class="st-custom-button sprite pinterest sprite-pint"></a>
    </li>
    <li class="hidden">
       <a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="email" class="st-custom-button sprite mail sprite-mail">Mail</a>
    </li>
    <li>
        <a data-image="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" data-url="<?php echo get_the_permalink(); ?>" data-title="<?php echo get_the_title(); ?>" data-network="whatsapp" class="st-custom-button sprite whatsapp sprite-whatsapp"></a>
    </li>
</ul>
<?php }

function is_ventures_connected(){
    $ip = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
    if($ip == '91.183.209.143' || is_user_logged_in()){
        return true;
    }else{
        return false;
    }
}


function mtb_canonical_ssl($url) {
   $url = preg_replace("/^http:/i", "https:", $url);
   return $url;
}
add_filter( 'wpseo_canonical', 'mtb_canonical_ssl' );


function yoast_seo_canonical_change_home( $url ) {
    global $_options_acf;
    if ( is_front_page() ) {
        if($_options_acf['lang_identifiant'] == 'FR'){
            return 'https://www.elle.be/fr/';
        }else if($_options_acf['lang_identifiant'] == 'NL'){
            return 'https://www.elle.be/nl/';    
        }
    }
    return $url;
}

add_filter( 'wpseo_canonical', 'yoast_seo_canonical_change_home', 10, 1 );

add_filter('jpeg_quality', create_function('$quality', 'return 80;'));

function ad_update_jpeg_quality($meta_id, $attach_id, $meta_key, $attach_meta) {

    if ($meta_key == '_wp_attachment_metadata') {

        $post = get_post($attach_id);

        if ($post->post_mime_type == 'image/jpeg' && is_array($attach_meta['sizes'])) {

            $pathinfo = pathinfo($attach_meta['file']);
            $uploads = wp_upload_dir();
            $dir = $uploads['basedir'] . '/' . $pathinfo['dirname'];

            foreach ($attach_meta['sizes'] as $size => $value) {

                $image = $dir . '/' . $value['file'];
                $resource = imagecreatefromjpeg($image);

                if ($size == 'content2018prog') {
                    //$image = str_replace('480x546','480x545_prog',$image);
                    // set the jpeg quality for 'spalsh' size
                    imagejpeg($resource, $image, 15);
                } else {
                    // set the jpeg quality for the rest of sizes
                    imagejpeg($resource, $image, 100);
                }

                // or you can skip a paticular image size
                // and set the quality for the rest:
                // if ($size == 'splash') continue;

                imagedestroy($resource);
            }
        }
    }
}


add_filter('nav_menu_item_id', 'clear_nav_menu_item_id', 10, 3);
function clear_nav_menu_item_id($id, $item, $args) {
    return "";
}

add_filter('nav_menu_css_class', 'clear_nav_menu_item_class', 10, 3);
function clear_nav_menu_item_class($classes, $item, $args) {
    return array();
}

function add_alt_tags($content)
{
        global $post;
        preg_match_all('/<img (.*?)\/>/', $content, $images);
        if(!is_null($images))
        {
                foreach($images[1] as $index => $value)
                {
                        if(preg_match('/alt=""/', $value))
                        {
                            if (strpos($images[0][$index], 'lazy') !== false) {
                                $post_title = str_replace('"', '', $post->post_title);
                                $index_pic = $index+1;
                                $new_img = str_replace('<img', '<img alt="'.$post_title.' - '.$index_pic.'"', $images[0][$index]);
                                $content = str_replace($images[0][$index], $new_img, $content);
                            }
                        }
                }
        }
        return $content;
}
add_filter('the_content', 'add_alt_tags', 99999);



function alter_att_attributes_wpse_102079($attr) {
  $image_alt = $attr['alt'];
    if(isset($id)){
        if($image_alt == ''){ $image_alt = get_the_title($id);  $attr['alt'] = $image_alt . ' 150*150'; }
    }
  return $attr;
}

add_filter( 'wp_get_attachment_image_attributes', 'alter_att_attributes_wpse_102079');

//Reduce link in sitemap

function max_entries_per_sitemap() {
    return 300;
}

add_filter( 'wpseo_sitemap_entries_per_page', 'max_entries_per_sitemap' );


function has_consent_over($type){
    if (function_exists('has_consent')) {
        if(has_consent( $type )){
            return true;
        }
    } 
}

function redirect_302(){
    if(is_single()){
      global $page;
      if($page > 1){
        $url = get_permalink();
        wp_redirect($url,301);
        exit;
      }
    }
  }

add_action( 'wp_head', function() { 
  redirect_302();
});

function is_commercial(){
    global $_options_acf;
    $id_commercial = '';
    $id_commercial = $_options_acf['id_commercial'];
    $id_author_nb = get_the_author_meta('ID');
    if(($id_commercial == $id_author_nb) && is_single()){ return 1; }else{ return 0; }
}

function get_content_without_first_paragraph_content(){
    global $post;
    $str = wpautop( get_the_content() );
    $str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
    return $str;
}

function get_content_without_first_paragraph(){
    global $post;
    $content_replace = $post->post_content;

    $str = $content_replace;
    $str = wpautop($str);
    $str_first = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
    $content_replace = substr($str,strlen($str_first),strlen($str));
    
    $content_replace = str_replace('http://www.elle.be','https://www.elle.be',$content_replace);
    $content_replace = str_replace('http://quizz.elle.be','https://quizz.elle.be',$content_replace);
    $content_replace = str_replace('noopener', '', $content_replace);
    $content_replace = str_replace('<h1>', '<h2>', $content_replace);
    $content_replace = str_replace('</h1>', '</h2>', $content_replace);
    $content_replace = str_replace("www.instagram.com","instagram.com",$content_replace);
    
    //$content_replace = apply_filters('ez_toc_maybe_apply_the_content_filter', $content_replace );
    $content_replace = apply_filters( 'the_content', $content_replace );
    return $content_replace;
}

function get_content_without_first_paragraph_recette(){
    global $post;
    $content_replace = $post->post_content;
    $content_replace = str_replace('http://www.elle.be/','https://www.elle.be/',$content_replace);
    $content_replace = str_replace('http://quizz.elle.be/','https://quizz.elle.be/',$content_replace);
    $content_replace = str_replace('noopener', '', $content_replace);
    if(substr_count($content_replace,'<li>') > 0 ){
        $content_replace = str_replace('<li>','<li class="recipe-instructions" property="recipeInstructions">',$content_replace);
        $content_replace = apply_filters( 'the_content', $content_replace );
    }else{
        return '<div class="recipe-instructions" property="recipeInstructions">'.apply_filters( 'the_content', $content_replace ).'</div>';
    }
}



//Insert ads after second paragraph of single post content.
//add_filter( 'the_content', 'prefix_insert_post_ads' );

function prefix_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
     $closing_p = '</p>';
     $paragraphs = explode( $closing_p, $content );
     foreach ($paragraphs as $index => $paragraph) {
         if ( trim( $paragraph ) ) {
            $paragraphs[$index] .= $closing_p;
         }
         if ( $paragraph_id == $index + 1 ) {
            $paragraphs[$index] .= $insertion;
         }
     }
     
     return implode( '', $paragraphs );
}


function prefix_insert_post_toc( $content ) {
         $toc_code = do_shortcode('[toc]');
         if ( 'post' == get_post_type() && is_single()) {
            return prefix_insert_after_paragraph( $toc_code, 1, $content );
         }
    return $content;
}

function prefix_insert_before_h2( $insertion, $paragraph_id, $content ) {
     $closing_p = '<h2>';
     $paragraphs = explode( $closing_p, $content );
     foreach ($paragraphs as $index => $paragraph) {
         if ( trim( $paragraph ) ) {
            $paragraphs[$index] .= $closing_p;
         }
         if ( $paragraph_id == $index + 1 ) {
            $paragraphs[$index] .= $insertion;
         }
     }
     
     return implode( '', $paragraphs );
}


function prefix_insert_post_ads( $content ) {
         $ads_code = '<div id="gpt-ad-INPAGE" class="rmgAd center" data-type="INPAGE" data-device="all"></div>';
         if ( 'post' == get_post_type() && is_single() && get_field('identifiant_pebble',$_id_article) != 'no-ads') {
            return prefix_insert_after_paragraph( $ads_code, 1, $content );
         }
    return $content;
}

function prefix_insert_post_ads_3( $content ) {
         $ads_code = '<div id="gpt-ad-RECT_MIDDLE" class="rmgAd center" data-device="mobile" data-type="RECT_MIDDLE"></div>';
         if ( 'post' == get_post_type() && is_single() && get_field('identifiant_pebble',$_id_article) != 'no-ads') {
            return prefix_insert_after_paragraph( $ads_code, 6, $content );
         }
    return $content;
}

function prefix_insert_post_ads_6( $content ) {
         $ads_code = '<div id="gpt-ad-RECT_MIDDLE" class="rmgAd center" data-device="mobile" data-type="RECT_MIDDLE"></div>';
         if ( 'post' == get_post_type() && is_single() && get_field('identifiant_pebble',$_id_article) != 'no-ads') {
            return prefix_insert_after_paragraph( $ads_code, 12, $content );
         }
    return $content;
}
/*
function prefix_insert_post_ads_6( $content ) {
         $ads_code = '<div class="rmgAd center" data-device="mobile" data-type="RECT"></div>';
         if ( 'post' == get_post_type() && is_single() && get_field('identifiant_pebble',$_id_article) != 'no-ads') {
            return prefix_insert_after_paragraph( $ads_code, 6, $content );
         }
    return $content;
}*
*/

function prefix_insert_post_ads_video( $content ) {
         global $post;

         $category_post = wp_get_post_categories( $post->ID );
         $_options_acf = get_field('variable_js','option');

         $arg = array(
            'posts_per_page' => 1,
            'post_type' => 'video-seo',
            'orderby' => 'rand',
            'order'=> 'ASC',
            'category' => $category_post,
            );

         $video_seo = get_posts($arg);

          if ( ! empty( $video_seo ) ) {
              //Do the loop
            if($_options_acf['lang_identifiant'] == 'FR'){
              $video_cat_title = "La video ".strtolower(ad_position()['cat'])." du jour : ";
            }else{
              $video_cat_title = "De ".strtolower(ad_position()['cat'])."video van vandaag : ";
            }

          } else {
            $arg = array(
              'posts_per_page' => 1,
              'post_type' => 'video-seo',
              'orderby' => 'rand',
              'order'=> 'ASC',
            );

            $video_seo = get_posts($arg);

            if($_options_acf['lang_identifiant'] == 'FR'){
              $video_cat_title = "La video ELLE du jour : ";
            }else{
              $video_cat_title = "De ELLE video van vandaag : ";
            }

          }

         $code_video_seo = get_field('code_video',$video_seo[0]->ID);
         $id_clevercast = get_field('id_clevercast','option');
         if($_options_acf['lang_identifiant'] == 'FR'){
             $ads_code = '
             <div class="video-seo">
             <span class=""><strong>'.$video_cat_title.'</strong></span>
             <div class="clever_wrapper"><iframe class="clever_iframe" src="//player.clevercast.com?account_id=VPeL1n&amp;player_id=ALK1vX&amp;item_id='.$code_video_seo.'" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
             <span class="video-seo-title">'.$video_seo[0]->post_title.'</span></div>';
         }else{
            $ads_code = '
             <div class="video-seo">
             <span class=""><strong>'.$video_cat_title.'</strong></span>
             <div class="clever_wrapper"><iframe class="clever_iframe" src="//player.clevercast.com?account_id=VPeL1n&amp;player_id=AJNOBl&amp;item_id='.$code_video_seo.'" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
             <span class="video-seo-title">'.$video_seo[0]->post_title.'</span></div>';
         }

         if ( 'post' == get_post_type() && is_single() && get_field('code_video',$video_seo[0]->ID) && get_field('identifiant_pebble',$_id_article) != 'no-ads') {
            return prefix_insert_after_paragraph( $ads_code, 4, $content );
         }

    return $content;
}

add_filter( 'the_content', 'prefix_insert_post_ads' );
add_filter( 'the_content', 'prefix_insert_post_ads_3' );
add_filter( 'the_content', 'prefix_insert_post_ads_6' );
add_filter( 'the_content', 'prefix_insert_post_ads_video' );


/*
function prefix_insert_post_ads( $content ) {
         $ad_code = '<div class="qtx_placement"></div><script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"></script>';
         if ( 'post' == get_post_type() && is_single() && ! is_admin() && is_commercial() == 0) {
         return prefix_insert_after_paragraph( $ad_code, 4, $content );
         }
    return $content;
}
*/

/*
add_filter( 'the_content', 'prefix_insert_post_toc' );

function prefix_insert_post_toc( $content ) {
         $ad_code = '[toc]';
         if ( 'post' == get_post_type() && is_single() ) {
         return prefix_insert_after_paragraph( $ad_code, 1, $content );
         }
    return $content;
}
*/


//add_filter( 'the_content', 'prefix_insert_post_toc' );
/*
function prefix_insert_post_toc( $content ) {
         $toc_code = do_shortcode( '[toc]' );
         if ( 'post' == get_post_type() && is_single() ) {
         return prefix_insert_after_paragraph( $toc_code, 1, $content );
         }
    return $content;
}
*/

/*
$show_after_p = 2;
$content = apply_filters('the_content', $post->post_content);
if(substr_count($content, '<p>') > $show_after_p)
{
        $contents = explode("</p>", $content);
        $p_count = 1;
        foreach($contents as $content)
        {
            echo $content;
            if($p_count == $show_after_p)
            {
                echo '<div class="test">TEST</div>';
            }
            echo "</p>";
            $p_count++;
        
    }
}
*/

/** WALKER MENU AMP **/
class amp_menu_walker extends Walker_Nav_Menu {
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= ' <ul>';
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $output .= '</ul>';
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }

        $active_class = '';
        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }

        $output .= '<li class="ampstart-nav-item"><a class="ampstart-nav-link" href="' . $item->url . '">' . $item->title . '</a></li>';
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '</li>';
    }
}

if($_options_acf['lang_identifiant'] == 'FR'){
    require_once('include/newsletters.php'); 
}
else if($_options_acf['lang_identifiant'] == 'NL'){
    require_once('include/newsletters_nl.php');       
}else{
    require_once('include/newsletters.php'); 
}

/**
* Twitter helpfully won't show the image if it's served from an SSL server.
* Most servers are configured to handle images on non-SSL ports so this should
* work.
*
* @param  string $image URL for the twitter card image
* @return none
*/
function check_ssl_twitter_card_image($image) {
    return preg_replace("/https/", 'http', $image);
}
// Hook into the Yoast plugin's hooks for handling the Twitter image
//add_action('wpseo_twitter_image', 'check_ssl_twitter_card_image');


if(get_field('mode_maintenance','option') == 1 && $_SERVER['REMOTE_ADDR'] != '91.183.209.143'){ 
    if($_options_acf['lang_identifiant'] == 'FR'){
        require_once('index_maintenance_fr.php');
    }
    else if($_options_acf['lang_identifiant'] == 'NL'){
        require_once('index_maintenance_nl.php');       
    }
    die();
}

function fb_move_admin_bar() { 
if ( is_admin_bar_showing() ) { 
?>

    <style type="text/css" media="screen">
    #wpadminbar{display: none !important}
    #wp-admin-bar-autoptimize, #wp-admin-bar-delete-cache{display: none !important}
        @media screen and (min-width:480px){
        #wpadminbar{display: block !important}
        }
    .floating-second-nav.active-on {
        top: 30px !important;
    }
    </style>

<?php } 
}
// on backend area
add_action( 'wp_head', 'fb_move_admin_bar' );

/*------------------------------------*\
    HTTPS HACK
\*------------------------------------*/
function get_page_title_generic(){
    $title_generic = '';
    if(is_home()){ $title_generic = 'Home'; }
    if(is_single()){ $title_generic = get_the_title(); }
    if(is_category()){ $title_generic = single_cat_title(); }
    if(is_tag()){ $title_generic = single_tag_title(); }
    return $title_generic;
}

function  get_page_type_generic(){
    $type_generic = 'default';
    if(is_home()){ $type_generic = 'main'; }
    if(is_single()){ $type_generic = 'article'; }
    return $type_generic;
}

add_filter('acf/load_field/name=date_de_creation', 'my_acf_default_date');
function my_acf_default_date($field) {
  $field['default_value'] = get_the_date('Ymd');
  return $field;
}


add_action( 'after_setup_theme', 'wnd_default_image_settings' );

function wnd_default_image_settings() {
    update_option( 'image_default_align', 'center' );
    update_option( 'image_default_link_type', 'none' );
    update_option( 'image_default_size', 'large' );
}

function superGallery_options(){
    add_action('print_media_templates', function(){
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<script type="text/html" id="tmpl-custom-gallery-setting">
            <label class="setting">
              <span>Type : </span>
              <select data-setting="ds_template">
                <option value="template-slider">Slider</option>
                <option value="template-mosaique">Mosaique</option>
              </select>
            </label>
</script>
 <script>

        jQuery(document).ready(function()
            {
                _.extend(wp.media.gallery.defaults, {
                ds_template: '',
                });

                wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
                template: function(view){
                  return wp.media.template('gallery-settings')(view)
                       + wp.media.template('custom-gallery-setting')(view);
                }
                });

            });

</script>
<?php
    });
}

add_action( 'admin_init', 'superGallery_options' );

/*------------------------------------*\
    OLD ELLE FUNCTION
\*------------------------------------*/


 add_action( 'admin_init', 'wpse_55202_do_terms_exclusion' );

function wpse_55202_do_terms_exclusion() { add_filter( 'list_terms_exclusions', 'wpse_55202_list_terms_exclusions', 10, 2 ); }

function wpse_55202_list_terms_exclusions($exclusions,$args) { 
    
    $cats_remove = array();
    
   /* if($_options_acf['lang_identifiant'] == 'FR'){
        $cats_remove = array(21, 10, 15, 12, 22, 18, 46, 82, 81, 79, 46, 83, 24, 80, 47, 2096, 40, 41, 106, 42, 43, 45, 44, 58, 891, 892, 893, 1192, 138, 1106, 1149, 163, 13, 11, 14, 66, 263, 19, 7, 1500, 571, 520, 3, 669, 4, 16, 65, 9, 20, 23, 8, 601, 772, 2351, 17); // FR
    }else if($_options_acf['lang_identifiant'] == 'NL'){
        $cats_remove = array(3596, 3648, 959, 129, 561, 562, 563, 565, 566, 3451, 3321, 3330, 3328, 2151, 227, 1183, 1179, 1180, 1181, 1182, 228, 1292, 1291, 1290, 131, 135, 136, 134, 133, 229, 1515, 1516, 1517, 2234, 29, 3329, 2229, 1283, 3618, 2227); // NL  
    }else{
        $cats_remove = array(21, 10, 15, 12, 22, 18, 46, 82, 81, 79, 46, 83, 24, 80, 47, 2096, 40, 41, 106, 42, 43, 45, 44, 58, 891, 892, 893, 1192, 138, 1106, 1149, 163, 13, 11, 14, 66, 263, 19, 7, 1500, 571, 520, 3, 669, 4, 16, 65, 9, 20, 23, 8, 601, 772, 2351, 17); // FR
    }
    */

    $string_remove = '';
    foreach($cats_remove as $cat_remove) {
        $string_remove .= ' AND ( t.term_id <> '.$cat_remove.' ) ';
    }

    return $exclusions . $string_remove; 
    
}



/*----------------------------------*\
    OLD ELLE FUNCTION
\*------------------------------------*/

/*
function wpse_77670_filterGetTermArgs($args, $taxonomies) {
    global $typenow;

    if ($typenow == 'post') {
        // check whether we're currently filtering selected taxonomy
        if (implode('', $taxonomies) == 'category') {
            $cats = array(); // as an array

            if (empty($cats))
                $args['not_in'] = array(4); // no available categories
            else
                $args['include'] = $cats;
        }
    }

    return $args;
}

if (is_admin()) {
    add_filter('get_terms_args', 'wpse_77670_filterGetTermArgs', 10, 2);
}
*/


function ssl_post_thumbnail_urls($url, $post_id) {

  //Skip file attachments
  if(!wp_attachment_is_image($post_id)) {
    return $url;
  }

  //Correct protocol for https connections
  list($protocol, $uri) = explode('://', $url, 2);

  if(is_ssl()) {
    if('http' == $protocol) {
      $protocol = 'https';
    }
  } else {
    if('https' == $protocol) {
      $protocol = 'http';
    }
  }

  return $protocol.'://'.$uri;
}
add_filter('wp_get_attachment_url', 'ssl_post_thumbnail_urls', 10, 2);


add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
function title_like_posts_where( $where, &$wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $post_title_like ) ) . '%\'';
    }
    return $where;
}

/*------------------------------------*\
    TESTING FCT
\*------------------------------------*/
/*
function cbnet_parameter_queryvars( $qvars ) {
    $qvars[] = 'all';
    return $qvars;
}
add_filter( 'query_vars', 'cbnet_parameter_queryvars' );
*/

//[pagebreak]
/*
function pagebreak_func( $atts ){
    return "foo and bar";
}
add_shortcode( '<!--nextpage-->', 'pagebreak_func' );*/

add_filter('the_content', 'replace_pagebreak');  

function replace_pagebreak( $content ) {
    $content_new = str_replace("<!--nextpage-->","[multipage_block]",$content);
    return $content_new;
}   


function default_target_blank() { ?>
    <script>
        jQuery(document).on( 'wplink-open', function( wrap ) {
            if ( jQuery( 'input#wp-link-url' ).val() <= 0 )
                jQuery( 'input#wp-link-target' ).prop('checked', true );
        });
    </script>
<?php }
add_action( 'admin_footer-post-new.php', 'default_target_blank', 10, 0 );
add_action( 'admin_footer-post.php', 'default_target_blank', 10, 0 );


function acfstyle() {
    wp_register_style( 'acfstylecss',  get_template_directory_uri() . '/css/admin.css', array(), $version, 'all');
    wp_enqueue_style('acfstylecss'); // Enqueue it!
}
add_action( 'admin_enqueue_scripts', 'acfstyle' );

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = $filename = rand(1,9999999999).'.jpg';
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $filename_sanitize = sanitize_file_name($filename);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $check_current_media = post_exists($filename);
    if($check_current_media == false){
        $attach_id = wp_insert_attachment( $attachment, $file );
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
        //$res2= set_post_thumbnail( $post_id, $attach_id );
        return $attach_id;
    }else{
        return $check_current_media;
    }
}



/****** HACK URL ****/
/*
add_filter('the_content', 'filter');
function filter($content) {
    return str_replace('http://localhost:8080/ellelast2017/', 'http://localhost/ellelast2017/', $content);
}*/

add_filter('the_content', function( $content ){
    //--Remove all inline styles--
    $content = preg_replace('/ style=("|\')(.*?)("|\')/','',$content);
    return $content;
}, 20);

function _display_id_random(){
        $multiblock_ads_randorm = rand(1,999999999);
        return $multiblock_ads_randorm;
}

function display_id_random(){
        $multiblock_ads_randorm = rand(1,999999999);
        echo $multiblock_ads_randorm;
}


function get_post_news(){
    global $post;
    $args = array('numberposts'=> 20 ,'date_query' => array('column'  => 'post_date','after'   => '- 130 days'));
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
        foreach ( $posts as $post ) : setup_postdata( $post );
              get_template_part('template-parts/general/article-search');
        endforeach; wp_reset_postdata(); 
    else :
        echo 'Aucun articles disponibles !';
    endif;
}


function set_cookie_elle($cookie_name){
    setCookie($cookie_name, time(), time()+604800);
}

/*function get_cookie_elle($cookie_name){
    return $_COOKIE[$cookie_name];
}*/

function get_last_login_hours(){
    $now = time(); // or your date as well
    $your_date = 1510501736;
    if(isset($_COOKIE['last_time_login'])){
        $your_date = $_COOKIE['last_time_login'];
        //echo '<script>alert('.$your_date.')</script>';
    }
    $datediff = $now - $your_date;
    //return $your_date;
    return floor($datediff / (24));
}

function bloc_multipage_shortcode() {
    global $multipage_number_current;
    $multipage_number_current++; 
    $multiblock_ads_randorm = rand(1,999999999);
    $multipage_block = '';
    /*if($multipage_number_current > 1 && ($multipage_number_current == 2 || $multipage_number_current == 4 || $multipage_number_current == 6)){ */
    if(is_commercial() == 0){
        $multipage_block .= '<div class="rmgAd" data-device="mobile" data-type="RECT"></div>';
    }
   /*}*/
    $multipage_block .= '<div class="segment-multipage"><span class="icon-multipage"><div><span class="current-multipage">'.$multipage_number_current.'</span>/<span class="total-multipage"></span></div></span></div>';
    return $multipage_block;
}

add_shortcode('multipage_block', 'bloc_multipage_shortcode');


function my_img_caption_shortcode_filter( $val, $attr, $content = null ) {
  extract( shortcode_atts( array(
    'id'      => '',
        'align'   => 'aligncenter',
        'width'   => '',
        'caption' => ''
    ), $attr ) );
    
    // No caption, no dice...
    if ( 1 > (int) $width || empty( $caption ) )
        return $val;
 
    if ( $id )
        $id = esc_attr( $id );
     
    // Add itemprop="contentURL" to image - Ugly hack
    $content = str_replace('<img', '<img itemprop="contentURL"', $content);
    $img_html = apply_filters( 'bj_lazy_load_html', $content ); 
    return '<figure id="' . $id . '" aria-describedby="figcaption_' . $id . '" class="wp-caption ' . esc_attr($align) . '" itemscope itemtype="http://schema.org/ImageObject">' . do_shortcode( $img_html ) . '<figcaption id="figcaption_'. $id . '" class="wp-caption-text" itemprop="description">' . $caption . '</figcaption></figure>';
}

add_filter( 'img_caption_shortcode', 'my_img_caption_shortcode_filter', 10, 3 );

function my_post_object_query( $args, $field, $post )
{
    // modify the order
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    $args['post_status'] = array('publish');
    return $args;
}

// filter for a specific field based on it's name
add_filter('acf/fields/post_object/query', 'my_post_object_query', 10, 3);

// Newsletters
function get_the_permalink_news($id = '' , $post = ''){
    global $post;
    return get_the_permalink($id).'?utm_medium=email&utm_source=newsletter&utm_campaign='.$post->post_name;
}


function register_my_session()
{
  if( !session_id() )
  {
    session_start();
  }
}

add_action('init', 'register_my_session');


if (!isset($content_width))
{
    $content_width = 670;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    /* CURRENT IMAGE */
    add_image_size( 'content', 970, 508, true ); //OKAI
    add_image_size( 'content2018', 480, 545, true ); //OKAI
    add_image_size( 'content2018mid', 232, 263, true ); //OKAI
    add_image_size( 'gallery2018', 9999, 400, false );
    add_image_size( 'gallery2018mid', 9999, 1000, false );
    add_image_size( 'maxcontent2018', 670, 9999, false );
    add_image_size( 'instagram', 300, 300, true );
    add_image_size( 'sidebar_thumb', 95, 108, true );

    /*
    add_image_size( 'cover-img', 68, 94, true);
    add_image_size( 'cover-img-2', 136, 188, true);
    */
    

    //add_image_size( 'content2018prog', 480, 544, true );

    /** ADD HAUTEUR MAX GALLERY **/

    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

add_filter( 'image_size_names_choose', 'my_custom_sizes' );
 
function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'maxcontent2018' => __( 'Format ELLE 2018 (670 MAX)' ),
    ) );
}

function elle_get_picture_srcs_nolazyload( $image, $format) {
    $arr = array();
    $mappings = array(
        0 => 'content2018',
        480 => 'content2018',
        640 => 'content2018',
        960 => 'content2018',
        1050 => 'content2018'
    );
    foreach ( $mappings as $size => $type ) {
        $image_src = wp_get_attachment_image_src( $image, $type );
        $arr[] = '<source srcset="'. $image_src[0] . '" media="(min-width: '. $size .'px)">';
    }
    return implode( array_reverse ( $arr ) );
}

function elle_get_picture_srcs( $image , $format) {
    $arr = array();
    $mappings = array(
        0 => 'content2018',
        480 => 'content2018',
        640 => 'content2018',
        960 => $format,
        1050 => $format
    );
    foreach ( $mappings as $size => $type ) {
        $image_src = wp_get_attachment_image_src( $image, $type );
        $arr[] = '<source data-srcset="'. $image_src[0] . '" media="(min-width: '. $size .'px)">';
    }
    return implode( array_reverse ( $arr ) );
}

function elle_get_picture_srcs_picture( $image ) {
    $arr = array();
    $mappings = array(
        0 => 'content2018',
        600 => 'content2018',
        1000 => 'content2018'
    );
    foreach ( $mappings as $size => $type ) {
        $image_src = wp_get_attachment_image_src( $image, $type );
        $arr[] = '<source data-srcset="'. $image_src[0] . '" media="(min-width: '. $size .'px)">';
    }
    return implode( array_reverse ( $arr ) );
}


function html5blank_nav($name_menu = "header-menu", $class_menu = "main-nav-menu", $walker_menu = '')
{
    $arg = array(
        'theme_location'  => $name_menu,
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu main-nav-menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="'.$class_menu.'">%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
        );
    wp_nav_menu($arg);
}

function html5blank_nav_encode_64($name_menu = "header-menu", $class_menu = "main-nav-menu")
{
    $arg = array(
        'theme_location'  => $name_menu,
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu main-nav-menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="'.$class_menu.'">%3$s</ul>',
        'depth'           => 0,
        'walker'          => new encode_64_menu()
        );
    wp_nav_menu($arg);
}

class encode_64_menu extends Walker_Nav_Menu {
    
    // Displays start of an element. E.g '<li> Item Name'
    // @see Walker::start_el()
    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
        $object = $item->object;
        $type = $item->type;
        $title = $item->title;
        $description = $item->description;
        $permalink = $item->url;
        $output .= "<li class='" .  implode(" ", $item->classes) . "'>";
        
      //Add SPAN if no Permalink
      if( $permalink && $permalink != '#' ) {
        if (strpos($permalink, 'https://www.elle.be') !== false) {
            $output .= '<span class="atc" data-atc="' . __encode_base64($permalink) . '">';
        }else{
            $output .= '<span class="atc" data-outlink="true" data-atc="' . __encode_base64($permalink) . '">';
        }

      } else {
        $output .= '<span>';
      }
       
      $output .= $title;
      /*if( $description != '' && $depth == 0 ) {
        $output .= '<small class="description">' . $description . '</small>';
      }*/
      if( $permalink && $permalink != '#' ) {
        $output .= '</span>';
      } else {
        $output .= '</span>';
      }
    }
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    global $version;
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_deregister_script( 'jquery' );
        wp_register_script('jquery', get_template_directory_uri() . '/js/lib/jquery-2.2.4.min.js', array(), '2.2.4', true); // true will place script in the footer
        wp_enqueue_script( 'jquery' );

        wp_enqueue_script( 'google-analytics-js', get_bloginfo('template_directory') . '/js/google-analytics.js',array('jquery'), false );

        wp_register_script('postscribe', get_template_directory_uri() . '/js/lib/postscribe.js', array('jquery'), '1.0.0', true); // picturefill
        wp_enqueue_script('postscribe');

        wp_register_script('swiper', get_template_directory_uri() . '/js/lib/swiper.min.js', array('jquery'), '3.4.5', true); // Custom scripts
        wp_enqueue_script('swiper');

        wp_register_script( 'masonry',   get_template_directory_uri() . '/js/lib/masonry.pkgd.min.js', array( 'jquery' ), $version,true);
        wp_enqueue_script('masonry');
        
        wp_register_script( 'fancybox', get_template_directory_uri() . '/js/lib/jquery.fancybox.min.js', array( 'jquery' ), $version,true);  
        wp_enqueue_script('fancybox'); 

        wp_register_script( 'sharethis', get_template_directory_uri() . '/js/lib/sharethis.js', array( 'jquery' ), $version,true);  
        wp_enqueue_script('sharethis');  
    
        wp_register_script( 'pinterestembed', get_template_directory_uri() . '/js/lib/pinit.js', array( 'jquery' ), $version,true);  
        wp_enqueue_script('pinterestembed'); 

        wp_register_script( 'jqueryvisible', get_template_directory_uri() . '/js/lib/jquery.visible.js', array( 'jquery' ), $version,true);  
        wp_enqueue_script('jqueryvisible');

/*
        wp_register_script('slotmachine', get_template_directory_uri() . '/js/lib/slotmachine.js', array('jquery'), $version,true); // Custom scripts
        wp_enqueue_script('slotmachine');

        wp_register_script('jqueryslotmachine', get_template_directory_uri() . '/js/lib/jquery.slotmachine.js', array('jquery'), $version,true); // Custom scripts
        wp_enqueue_script('jqueryslotmachine');
*/
        /* 
        wp_register_script('dfpads', get_template_directory_uri() . '/js/dfp_ads.js', array('jquery'), $version,true); // Custom scripts
        wp_enqueue_script('dfpads');
        */

        wp_register_script('fotterscript', get_template_directory_uri() . '/js/footer-script.js', array('jquery'), $version,true); // Custom scripts
        wp_enqueue_script('fotterscript');

    }
}

function add_async_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_async = array('pebblescriptinit', 'pebblescript','fotterscript');
   
   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', ' async="async" src', $tag);
      }
   }
   return $tag;
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

/*
add_filter('autoptimize_filter_js_defer','my_ao_override_defer',10,1);
function my_ao_override_defer($defer) {
 return $defer."async ";
}*/

/*
add_filter('autoptimize_filter_cache_getname','protocollesser');
add_filter('autoptimize_filter_base_replace_cdn','protocollesser');
function protocollesser($urlIn) {
  $urlOut=preg_replace('/https?:/i','',$urlIn);
  return $urlOut;
}*/

// Load HTML5 Blank styles
function html5blank_styles()
{
    global $version;
   /* wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!*/

    wp_register_style('html5blank', get_template_directory_uri() . '/css/styles.css', array(), $version, 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!

    wp_register_style('swiper', get_template_directory_uri() . '/css/swiper.min.css', array(), $version, 'all');
    wp_enqueue_style('swiper'); // Enqueue it!

   /* wp_register_style('jqueryui', get_template_directory_uri() . '/css/jquery-ui.css', array(), '1.12.1', 'all');
    wp_enqueue_style('jqueryui'); // Enqueue it!*/

    wp_register_style('customscrollbar', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.min.css', array(), $version, 'all');
    wp_enqueue_style('customscrollbar'); // Enqueue it!

}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu-2018' => __('Header Menu 2018', 'html5blank'), // Main Navigation
        'main-nav-menu-2018' => __('Main Menu 2018', 'html5blank'), // menu sidebar
        'second-nav-menu-2018' => __('Second Nav Menu 2018', 'html5blank'), // menu top
        'footer-menu-2018' => __('Footer Menu 2018', 'html5blank'), // Menu sitemap
        'footer-nav-menu-2018' => __('Footer Nav Menu 2018', 'html5blank'), // Menu bottom + bottom sidebar

        'eat-header-menu-2018' => __('EAT Header Menu 2018', 'html5blank'), // Main Navigation
        'eat-main-nav-menu-2018' => __('EAT Main Menu 2018', 'html5blank'), // menu sidebar
        'eat-second-nav-menu-2018' => __('EAT Second Nav Menu 2018', 'html5blank'), // menu top
        'eat-footer-menu-2018' => __('EAT Footer Menu 2018', 'html5blank'), // Menu sitemap
        'eat-footer-nav-menu-2018' => __('EAT Footer Nav Menu 2018', 'html5blank'), // Menu bottom + bottom sidebar

        'deco-header-menu-2020' => __('DECO Header Menu 2020', 'html5blank'), // Main Navigation
        'deco-main-nav-menu-2020' => __('DECO Main Menu 2020', 'html5blank'), // menu sidebar
        'deco-second-nav-menu-2020' => __('DECO Second Nav Menu 2020', 'html5blank'), // menu topsidebar


        'eaf-main-nav-menu-2018' => __('EAF Main Menu 2018', 'html5blank'), // menu sidebar
        'eaf-header-menu-2018' => __('EAF Header Menu 2018', 'html5blank'), // menu sidebar
        'eaf-second-nav-menu-2018' => __('EAF Second Nav Menu 2018', 'html5blank'), // menu top

    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}
// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}


/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('init', 'init_variable_global'); // Add Custom Scripts to wp_head

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

add_filter( 'wpseo_next_rel_link', 'custom_remove_wpseo_next' );
add_filter( 'wpseo_prev_rel_link', 'custom_remove_wpseo_prev' );

function custom_remove_wpseo_next( $link ) {
     /* Make the magic happen here
      * Example below removes the rel=”next” link on your homepage
      */
     if ( is_single() ) {
          return false;
     } else { 
          return $link;
     }
}

function custom_remove_wpseo_prev( $link ) {
     /* Make the magic happen here
      * Example below removes the rel=”next” link on your homepage
      */
     if ( is_single() ) {
          return false;
     } else { 
          return $link;
     }
}

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
    ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}


function get_last_magazine(){
     $last_magazine = get_posts(array('posts_per_page' => 1,'post_type' => 'magazines'));
     if (is_category() || is_single()) {
        if ( is_category('elle-a-table') || has_category('elle-a-table') || has_category('recettes')){
            $last_magazine = get_posts(array('posts_per_page' => 1,'post_type' => 'magazines-eat'));
        }else if(is_category('elle-eten') || has_category('elle-eten') || has_category('recepten')){
            $last_magazine = get_posts(array('posts_per_page' => 1,'post_type' => 'magazines-eat'));
        }
    }
    return $last_magazine;
}


/*------------------------------------*\
    Elle 2017 Functions
\*------------------------------------*/

/* Global value */
global $excludeIDS,$category_principales, $current_post_id, $id_video_category;


function init_variable_global(){

    global $excludeIDS, $autopromo, $autopromo_mag, $magazine_viapress_link, $magazine_cover_src, $magazine, $multipage_number_current, $main_categorie, $last_time_login;
    $excludeIDS = array();

    $autopromo = get_posts(array('posts_per_page' => 2, 'orderby'   => 'rand', 'post_type' => 'autopromo')); 
    $autopromo_mag = get_posts(array('posts_per_page' => 1, 'orderby'   => 'rand', 'post_type' => 'autopromo', 'meta_query' => array(
                array('key' => 'abonnement', 'value' => '1', 'compare'   => 'LIKE' ))));

    function set_excluded_id($id)
    {
        global $excludeIDS;
        $excludeIDS[] = $id;
    }

    function get_excluded_id(){
        global $excludeIDS;
        return $excludeIDS;
    }
}

function id_video_category(){
    global $id_video_category;
    $id_video_category = get_field('categorie_video','option'); 
    return $id_video_category;
}

function autopromo_objet_mag(){
    global $autopromo_mag;
    return $autopromo_mag;
}

function autopromo_objet(){
    global $autopromo;
    return $autopromo;
}

function category_principales(){
    $category_principales = "";
    if( class_exists('acf') ) {
        $category_principales = get_field('categories_principales','option'); 
    }
    return $category_principales;
}

function tags_principales(){
    $tags_principales = "";
    if( class_exists('acf') ) {
        $tags_principales = get_field('tags_principales','option'); 
    }
    return $tags_principales;
}


function category_sans_thumb(){
    $categories_sans_thumb = "";
    if( class_exists('acf') ) {
        $categories_sans_thumb = get_field('categories_sans_thumb','option'); 
    }
    return $categories_sans_thumb;
}


/* ACF options + link file */

/* AJAX function */
add_action( 'wp_ajax_nopriv_ajax_search', 'my_ajax_search' );
add_action( 'wp_ajax_ajax_search', 'my_ajax_search' );

function my_ajax_search() {
    global $post;
    
    //$posts = get_posts($args); 

    if($_POST['s'] == ''){ 
        $args = array('numberposts'=> 20 ,'s'=> $_POST['s'],'category'=> $_POST['category']);
        $posts = get_posts($args);  
    }
    else{
        $query->query_vars['s'] = $_POST['s'];
        $query->query_vars['post_status'] = 'publish';
        $query->query_vars['category__in'] = $_POST['category'];
        $query->query_vars['posts_per_page'] = 20;
        relevanssi_do_query($query);
        $posts = $query->posts; 
    }

    if (count($posts) > 0 ) :
        $current_post = 0;
        foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
            if ($current_post%2 == 0){ get_template_part('template-parts/general/article-horizontal-right'); } 
            else{get_template_part('template-parts/general/article-horizontal-left'); }
        endforeach; wp_reset_postdata(); 
    else :
        if($_options_acf['lang_identifiant'] == 'FR'){
        echo 'Aucun article disponible !'; } else{
        echo 'Geen artikel gevonden !';
        }
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_recette_search', 'my_ajax_recette_search' );
add_action( 'wp_ajax_ajax_recette_search', 'my_ajax_recette_search' );

function my_ajax_recette_search() {
    global $post;
    if($_options_acf['lang_identifiant'] == 'FR'){
        if($_POST['quoi'] == 'Type de recette ?'){ $_POST['quoi']  = ''; }
        if($_POST['style_boutique'] == 'Difficulté ?'){ $_POST['style_boutique']  = ''; }
        $args = array('s'=> $_POST['s'],'numberposts'=> 15,
                'category_name' => 'recettes',
                'meta_query' => array(
                'relation' => 'AND',
                array( array( 'key'   => 'recipe-type', 'value' => $_POST['quoi'], 'compare'   => 'LIKE' ) ),
                array( array( 'key'   => 'recipe-difficulties', 'value' => $_POST['style_boutique'], 'compare'   => 'LIKE' ) ),
            ),
        );

    }else if($_options_acf['lang_identifiant'] == 'NL'){
        if($_POST['quoi'] == 'Soort Gerecht ?'){ $_POST['quoi']  = ''; }
        if($_POST['style_boutique'] == 'Mood ?'){ $_POST['style_boutique']  = ''; }
        $args = array('s'=> $_POST['s'],'numberposts'=> 15,
                'category_name' => 'good-food',
                'meta_query' => array(
                'relation' => 'AND',
                array( array( 'key'   => 'soort_gerecht', 'value' => $_POST['quoi'], 'compare'   => 'LIKE' ) ),
                array( array( 'key'   => 'mood', 'value' => $_POST['style_boutique'], 'compare'   => 'LIKE' ) ),
            ),
        );
    }
    

    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
    foreach ( $posts as $post ) : setup_postdata( $post );
        get_template_part('template-parts/general/article');
    endforeach; wp_reset_postdata(); 
    else :
        if($_options_acf['lang_identifiant'] == 'FR'){
            echo "Aucune recette n'est disponible !";
        }
        else if($_options_acf['lang_identifiant'] == 'NL'){
            echo "Geen beschikbaar recept !";
        } 
        else{
            echo "Aucune recette n'est disponible !";
        } 
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_adresses_search', 'my_ajax_adresses_search' );
add_action( 'wp_ajax_ajax_adresses_search', 'my_ajax_adresses_search' );

function my_ajax_adresses_search() {
    global $post;

    switch($_POST['quoi']){
            case 'Boutique': $key = 'quel_style_boutique'; $style_value = $_POST['style_boutique']; break;
            case 'Restaurant': $key = 'quel_style_restaurant'; $style_value = $_POST['style_restaurant']; break;
            case 'Bar': $key = 'quel_style_bar'; $style_value = $_POST['style_bar']; break;
            case 'Hôtel': $key = 'quel_style_hotel'; $style_value = $_POST['style_hotel']; break; 
            default: $key = '';  $style_value = ''; break; 
    } 

 if($_options_acf['lang_identifiant'] == 'FR'){
        $args = array('s'=> $_POST['s'],'numberposts'=> 15,
                'category_name' => 'adresses',
                'meta_query' => array(
                'relation' => 'AND',
                array( array( 'key'   => 'quoi_', 'value' => $_POST['quoi'], 'compare'   => 'LIKE' ) ),
                array( array( 'key'   => 'ou_', 'value' => $_POST['ville'], 'compare'   => 'LIKE' ) ),
            ),
        );

        if($key != ''){
            $args = array('s'=> $_POST['s'],'numberposts'=> 15,
                    'meta_query' => array(
                    'relation' => 'AND',
                    array( array( 'key'   => 'quoi_', 'value' => $_POST['quoi'], 'compare'   => 'LIKE' ) ),
                    array( array( 'key'   => $key, 'value' => $style_value, 'compare'   => 'LIKE' ) ),
                    array( array( 'key'   => 'ou_', 'value' => $_POST['ville'], 'compare'   => 'LIKE' ) ),
                ),
            );  
        }
    }else if($_options_acf['lang_identifiant'] == 'NL'){
        if($_POST['quoi'] == 'Wat ?'){ $_POST['quoi']  = ''; }
        if($_POST['style_boutique'] == 'Met wie ?'){ $_POST['style_boutique']  = ''; }
        if($_POST['ville'] == 'Stad ?'){ $_POST['ville']  = ''; }
        $args = array('s'=> $_POST['s'],'numberposts'=> 15,
            'category_name' => 'hotspots',
            'meta_query' => array(
            'relation' => 'AND',
                array( array( 'key'   => 'wat', 'value' => $_POST['quoi'], 'compare'   => 'LIKE' ) ),
                array( array( 'key'   => 'met_wie', 'value' => $_POST['style_boutique'], 'compare'   => 'LIKE' ) ),
                array( array( 'key'   => 'stad_', 'value' => $_POST['ville'], 'compare'   => 'LIKE' ) ),
            ),
        );
    }

    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
    foreach ( $posts as $post ) : setup_postdata( $post );
        get_template_part('template-parts/general/article');
    endforeach; wp_reset_postdata(); 
    else :
        if($_options_acf['lang_identifiant'] == 'FR'){
            echo "Aucune adresse n'est disponible !";
        }
        else if($_options_acf['lang_identifiant'] == 'NL'){
            echo "Geen beschikbaar adres !";
        } 
        else{
            echo "Aucune adresse n'est disponible !";
        } 
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_load_news', 'my_ajax_load_news' );
add_action( 'wp_ajax_ajax_load_news', 'my_ajax_load_news' );

function my_ajax_load_news() {
    global $post;
    $args = array('numberposts'=> 5 ,'date_query' => array('column'  => 'post_date','after'   => '- '.$_POST['time_news'].' mins'));
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
        $current_post = 0;
        foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
            if ($current_post%2 == 0){ get_template_part('template-parts/general/article-horizontal-right'); } 
            else{ get_template_part('template-parts/general/article-horizontal-left'); }
        endforeach; wp_reset_postdata(); 
    else :
        if($_options_acf['lang_identifiant'] == 'FR'){
            echo "Un peu de patience, un nouvel article est en préparation...";
        }
        else if($_options_acf['lang_identifiant'] == 'NL'){
            echo "Geen artikel beschikbaar !";
        } 
        else{
            echo "Un peu de patience, un nouvel article est en préparation...";
        } 
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_display_popup', 'my_ajax_display_popup' );
add_action( 'wp_ajax_ajax_display_popup', 'my_ajax_display_popup' );

function my_ajax_display_popup(){
    get_template_part('popup');
    die();
}

add_action( 'wp_ajax_nopriv_ajax_count_news', 'my_ajax_count_news' );
add_action( 'wp_ajax_ajax_count_news', 'my_ajax_count_news' );

function my_ajax_count_news() {
    $args = array('numberposts'=> 5 ,'date_query' => array('column'  => 'post_date','after'   => '- '.$_POST['time_news'].' mins'));
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
         echo count($posts);
    else :
         echo '';
    endif;
    die();
}


add_action( 'wp_ajax_nopriv_ajax_update_pv_lastyear', 'my_ajax_update_pv_lastyear' );
add_action( 'wp_ajax_ajax_update_pv_lastyear', 'my_ajax_update_pv_lastyear' );

function my_ajax_update_pv_lastyear() {
    global $post; global $current_post;
    $args = array('post__in' => array($_POST['post_id']));
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
         update_field('sessions_1_an', $_POST['post_pv_lastyear'], $_POST['post_id']);
         echo 'MISE A JOUR OKAI :'.$_POST['post_id'];
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_update_pv_totale', 'my_ajax_update_pv_totale' );
add_action( 'wp_ajax_ajax_update_pv_totale', 'my_ajax_update_pv_totale' );

function my_ajax_update_pv_totale() {
    global $post; global $current_post;
    $args = array('post__in' => array($_POST['post_id']));
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
         update_field('sessions_1_an_totale', $_POST['post_pv_lastyear'], $_POST['post_id']);
         echo 'MISE A JOUR OKAI :'.$_POST['post_id'];
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_remove_post', 'my_ajax_remove_post' );
add_action( 'wp_ajax_ajax_remove_post', 'my_ajax_remove_post' );

function my_ajax_remove_post() {
    if ( is_admin()) {
        global $post; global $current_post;
        $args = array('post__in' => array($_POST['post_id']));
        $posts = get_posts($args); 
        if (count($posts) > 0 ) :
            $date = date('d/m/Y');
            update_field('date_suppression', $date, $_POST['post_id']);
            wp_delete_post($_POST['post_id']);
            echo 'SUP OKAI :'.$_POST['post_id'];
        endif;
    }
    die();
}

add_action( 'wp_ajax_nopriv_ajax_set_status', 'my_ajax_set_status' );
add_action( 'wp_ajax_ajax_set_status', 'my_ajax_set_status' );

function my_ajax_set_status() {
    global $post; global $current_post;
    $args = array('post__in' => array($_POST['post_id']));
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
         //wp_delete_post($_POST['post_id']);
         update_field('status_updated', array($_POST['status_updated']), $_POST['post_id']);
         echo 'UPD STATUT OKAI :'. $_POST['status_updated'].' ID : '.$_POST['post_id'];
    endif;
    die();
}

add_action( 'wp_ajax_nopriv_ajax_load_siderbar_article', 'my_ajax_load_siderbar_article' );
add_action( 'wp_ajax_ajax_load_siderbar_article', 'my_ajax_load_siderbar_article' );

function my_ajax_load_siderbar_article() {
    global $post; global $current_post;


    if(isset($_POST['cat'])){
        $args = array(
            'numberposts'=> $_POST['nb'],
            'post_status' => array('publish'),
            'cat'=> $_POST['cat'],
            'post__not_in'=> $_POST['excluded_id'],
            'orderby'   => 'rand',
            'date_query' => array(array('after' => '90 days ago'))
            );
    }
    /*if(isset($_POST['tag'])){$args = array('numberposts'=> $_POST['nb'],'post_status' => array('publish'),'tag_id'=> $_POST['tag'],'post__not_in'=> $_POST['excluded_id'],'orderby'   => 'rand', 'date_query' => array(array('after' => '90 days ago')));}*/


    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
        $current_post = 0;
        foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
          get_template_part('template-parts/general/article-related-block');
        endforeach; wp_reset_postdata(); 
    else:
        if($_options_acf['lang_identifiant'] == 'FR'){
            echo "Aucun article n'est disponible !";
        }
        else if($_options_acf['lang_identifiant'] == 'NL'){
            echo "Geen artikel beschikbaar !";
        } 
        else{
            echo "Aucun article n'est disponible !";
        } 
    endif;
    die();
}

/**** AJAX CONTENT *****/
add_action( 'wp_ajax_nopriv_ajax_load_content_article', 'my_ajax_load_content_article' );
add_action( 'wp_ajax_ajax_load_content_article', 'my_ajax_load_content_article' );
function my_ajax_load_content_article() {
    global $post; global $current_post;
    $args = array('numberposts'=> 15);
    $posts = get_posts($args); 
    if (count($posts) > 0 ) :
        $current_post = 0;
        foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
            if($current_post > $_POST['nb']){
                get_template_part('template-parts/general/article');
            }
        endforeach; wp_reset_postdata(); 
    else :
        if($_options_acf['lang_identifiant'] == 'FR'){
            echo "Aucun article n'est disponible !";
        }
        else if($_options_acf['lang_identifiant'] == 'NL'){
            echo "Geen artikel beschikbaar !";
        } 
        else{
            echo "Aucun article n'est disponible !";
        } 
    endif;
    die();
}


function pw_load_scripts() {
    wp_enqueue_script('custom-js', get_template_directory_uri() . '/js/custom-admin.js', array('jquery') );
        // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
    wp_localize_script( 'custom-js', 'ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) );
}

add_action('admin_enqueue_scripts', 'pw_load_scripts');

/* INSTAGRAM ADMIN AJAX */
add_action( 'wp_ajax_nopriv_erase_uploaded_images', 'erase_uploaded_images' );
add_action( 'wp_ajax_erase_uploaded_images', 'erase_uploaded_images' );

function erase_uploaded_images() {

    $url_insta = "https://api.instagram.com/oembed/?url=".$_POST['url_instagram'];
    $json = file_get_contents($url_insta);
    $json_data = json_decode($json,true);
    $insta_author_name = $json_data['author_name'];
    $insta_title = $json_data['title'];
    $insta_thumb = $json_data['thumbnail_url'];
    $insta_thumb_img_id = Generate_Featured_Image($json_data['thumbnail_url']);
    $json_data['insta_thumb_img_id'] = $insta_thumb_img_id;
    echo json_encode($json_data);
    die();
}

/*** GET INSTAGRAM *///
function get_instagram(){
    $url_insta = "https://api.instagram.com/oembed/?url=https://www.instagram.com/p/Bcow7bHlseu";
    $json = file_get_contents($url_insta);
    $json_data = json_decode($json,true);
    echo $insta_author_name = $json_data['author_name'];
    echo $insta_title = $json_data['title'];
    echo $insta_thumb = '<img src="'.$json_data['thumbnail_url'].'" alt=""/>';
    var_dump($json_data);
        // Create post object
    $my_post = array(
      'post_title'    => $url_insta,
      'post_content'  => $insta_title,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_type'=> 'instagram',
    );
     
    // Insert the post into the database
    $my_post_id = wp_insert_post($my_post, true);

}
/* ADS TEMPLATE */
add_action( 'wp_ajax_nopriv_ajax_load_ads_loading', 'my_ajax_load_ads_loading' );
add_action( 'wp_ajax_ajax_load_ads_loading', 'my_ajax_load_ads_loading' );

function my_ajax_load_ads_loading() {  
       get_template_part('template-parts/general/template-ads');
    die();
}

/* SLIDER VIDEO */
add_action( 'wp_ajax_nopriv_ajax_load_slider_video', 'my_ajax_load_slider_video' );
add_action( 'wp_ajax_ajax_load_slider_video', 'my_ajax_load_slider_video' );

function my_ajax_load_slider_video() {  
       get_template_part('template-parts/general/swiper-video');
    die();
}

/* SLIDER ARTICLE */
add_action( 'wp_ajax_nopriv_ajax_load_slider_article', 'my_ajax_load_slider_article' );
add_action( 'wp_ajax_ajax_load_slider_article', 'my_ajax_load_slider_article' );

function my_ajax_load_slider_article() { 
        get_template_part('template-parts/general/swiper-article');
    die();
}

/* SLIDER MOBILE */
add_action( 'wp_ajax_nopriv_ajax_load_slider_article_mobile', 'my_ajax_load_slider_article_mobile' );
add_action( 'wp_ajax_ajax_load_slider_article_mobile', 'my_ajax_load_slider_article_mobile' );

function my_ajax_load_slider_article_mobile() {  
       get_template_part('template-parts/general/article-related-mobile');
    die();
}


/* SLIDER MOBILE */
add_action( 'wp_ajax_nopriv_ajax_load_article', 'my_ajax_load_article' );
add_action( 'wp_ajax_ajax_load_article', 'my_ajax_load_article' );

function my_ajax_load_article() {
    global $post;
    $args = array('post_status' => array('publish'),'posts_per_page'=> 1);
    if(isset($_POST['cat'])){$args = array('post_status' => array('publish'),'posts_per_page'=> 1,'cat'=> $_POST['cat'],'post__not_in'=> $_POST['excluded_id']);}
    if(isset($_POST['tag'])){$args = array('post_status' => array('publish'),'posts_per_page'=> 1,'tag_id'=> $_POST['tag'],'post__not_in'=> $_POST['excluded_id']);}    
    $url     = wp_get_referer();
    $post_id = url_to_postid( $url ); 

    $id_commercial = get_field('variable_js','option')['id_commercial'];
    $id_author_nb = get_post_field( 'post_author', $post_id );

    if($id_commercial == $id_author_nb){ $args['author'] = '-'.get_field('variable_js','option')['id_commercial']; }
    
    $custom_query = new WP_Query($args);
    while($custom_query->have_posts()) : $custom_query->the_post();  
    $list_categorie = get_the_category(); 
        foreach ($list_categorie as $categorie) {
            $list_categorie_name[] = $categorie->slug;
        }
        set_query_var( 'ajax_load_var', '1' );
        if(in_array('recettes', $list_categorie_name ) || in_array('recepten', $list_categorie_name ) ){
            get_template_part('template-parts/general/recettes-wrapper'); 
        }else if(in_array('diy', $list_categorie_name )){
            get_template_part('template-parts/general/diy-wrapper'); 
        }else{
            get_template_part('template-parts/general/content-wrapper'); 
        }

    endwhile; wp_reset_postdata();

    
    die();
}

/* AJAX function */


/* Ajout option multipage à wordpress */
add_filter( 'mce_buttons', 'mc_editor' );
function mc_editor( $mce_buttons ) {
    $pos = array_search( 'wp_more', $mce_buttons, true );
    if ( $pos !== false ) {
        $tmp_buttons = array_slice( $mce_buttons, 0, $pos+1 );
        $tmp_buttons[] = 'wp_page';
        $mce_buttons = array_merge( $tmp_buttons, array_slice( $mce_buttons, $pos+1 ) );
    }
    return $mce_buttons;
}
/* Ajout option multipage à wordpress */

/* Ajout icone pour les articles */
function modify_post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr = array()) {
        global $multipage;

        $id = get_post_thumbnail_id(); // gets the id of the current post_thumbnail (in the loop)
        if(isset($post_id)){$id = get_post_thumbnail_id($post_id);}
        if(get_field('image_verticale',$post_id)){$id = get_field('image_verticale',$post_id);}
        $src = wp_get_attachment_image_src($id, $size); // gets the image url specific to the passed in size (aka. custom image size)
        $src_prog = wp_get_attachment_image_src($id, 'content2018prog'); // gets the image url specific to the passed in size (aka. custom image size)
        $image_alt = get_the_title($post_id);
        $content = get_the_content($post_id);
        $list_categorie = get_the_category($post_id); 
        foreach ($list_categorie as $categorie) {
            $list_categorie_name[] = $categorie->slug;
        }

        $icone = '';
            if($size != 'news-thumb' && $size != 'square' && $size != 'instagram'){
                if($attr[0] == 'lazy'){ 
                $image_src = wp_get_attachment_image_src(  $id, $size );
                $img_html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                $html = apply_filters( 'bj_lazy_load_html', $img_html );
                }else{
                $image_src = wp_get_attachment_image_src(  $id, $size );
                $html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                }
            }

            if(get_field("gif",$post_id)){
                 if($attr[0] == 'lazy'){ 
                    $html = '<img class="no-lazy-img lazy-img" src="'.get_template_directory_uri().'/img/blank.gif" data-srcset="'.get_field("gif",$post_id).'" alt="' . $image_alt . '">';
                }else{
                    $html = '<img class="no-lazy-img" srcset="'.get_field("gif",$post_id).'" alt="' . $image_alt . '">';
                }
            }
                
            if($size == 'instagram'){
            $image_src = wp_get_attachment_image_src(  $id, 'instagram' );
                 if($attr[0] == 'lazy'){ 
                    $img_html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                    $html = apply_filters( 'bj_lazy_load_html', $img_html );
                }else{
                    $image_src = wp_get_attachment_image_src(  $id, 'instagram' );
                    $html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                }
            }
            
            if(count($attr) > 3){
                if($attr[3] == 1 && get_field("url_video",$post_id)){
                    $ID_video = explode('/', get_field("url_video",$post_id))[3];
                    $url_video = "http://www.youtube.com/embed/".$ID_video."?enablejsapi=1&autoplay=0&showinfo=0&controls=1&autohide=1"; 
                    if(is_ssl()){$url_video = "https://www.youtube.com/embed/".$ID_video."?enablejsapi=1&autoplay=0&showinfo=0&controls=1&autohide=1";}

                    $html = '<div class="player_youtube_wrapper"><iframe id="player" type="text/html" width="640" height="360" data-srcset="'.$url_video.'" frameborder="0"></iframe></div>';
                }
            }

            if($attr[1] == 1){ 
                $html .= '<div class="icon-div">';
                $icone = '<span class="sprite"></span>';
                if (strpos($content, '[gallery') !== false) {
                    $icone = '<span class="icon-gallery sprite"></span>';
                } else {}

                if (0 !== $multipage) {
                    $icone = '<span class="icon-multipage sprite"></span>';
                } else {}
                
                if (in_array('recettes', $list_categorie_name)) {
                    $icone = '<span class="icon-recipe sprite"></span>';
                } else {}

                if (strpos($content, 'youtu') !== false || strpos($content, 'youtube') !== false || strpos($content, 'dailymotion') !== false || strpos($content, 'facebook.com/plugins/video') !== false) {
                    $icone = '<span class="icon-video sprite"></span>';
                } else {}

                if (strpos($content, 'qualifio') !== false) {
                    $icone = '<span class="icon-concours sprite"></span>';
                } else {}

                if(in_array("elle-tv", $list_categorie_name)){
                    $icone = '<span class="icon-video sprite"></span>';
                } else {}

                if(in_array("elle-a-table", $list_categorie_name) || in_array("elle-eten", $list_categorie_name)){
                    $icone = '<span class="icon-eat sprite"></span>';
                } else {}

                $html .= $icone; $html .= '</div>';
            }

            if($attr[2] == 1){ 
                $html .= '<span class="caption-div">';
                $image = get_post($id);
                $image_caption = $image->post_excerpt;
                $html .= $image_caption;
                $html .= '</span>';
            }

      return $html;
}

add_filter('post_thumbnail_html', 'modify_post_thumbnail_html', 99, 5);
/* Ajout icone pour les articles */


/* Lazyload image */ 

/* REST API v2 ADD IMAGED */
add_action( 'rest_api_init', 'add_thumbnail_to_JSON' );
function add_thumbnail_to_JSON() {
//Add featured image
register_rest_field( 'post',
    'featured_image_src', //NAME OF THE NEW FIELD TO BE ADDED - you can call this anything
    array(
        'get_callback'    => 'get_image_src',
        'update_callback' => null,
        'schema'          => null,
         )
    );
}

function get_image_src( $object, $field_name, $request ) {
    $feat_img_array = wp_get_attachment_image_src($object['featured_media'], 'thumbnail', true);
    return $feat_img_array[0];
}

// A trier >>> Elodie

//Cacher les infos inutiles de d'admin
// function remove_personal_options(){
//     echo '<script type="text/javascript">jQuery(document).ready(function($) {
//     $(\'table.form-table tr.user-url-wrap\').remove();// remove the "Website" field in the "Contact Info" section
      
//     $(\'h2:contains("About Yourself"), h2:contains("About the user")\').remove(); // remove the "About Yourself" and "About the user" titles
      
//     $(\'form#your-profile tr.user-description-wrap\').remove(); // remove the "Biographical Info" field
      
//     $(\'form#your-profile tr.user-profile-picture\').remove(); // remove the "Profile Picture" field
      
//     });</script>';
// }
  
// add_action('admin_head','remove_personal_options');
// Page author
/**
    Ne montrer que ses posts à l'utilisateur (VIP)
    *   
    */
    
  if ( is_admin()) {
    /*
  function seomix_adm_user_show_myposts($query) {
    global $user_ID;
    $user_info = get_userdata( $user_ID ); 
    global $user_level;
    //var_dump($user_level);
    if ($user_info->allcaps['vip'] == true){
      $query->set('author',$user_ID);
      unset($user_ID);
      $screen = get_current_screen();
      add_filter('views_'.$screen->id, 'seomix_adm_user_remove_post_counts');} 
    return $query;}

  function seomix_adm_user_remove_post_counts($views) {
    $views = array_intersect_key($views, array_flip(array('mine','trash')));
    return $views;}
  add_filter('pre_get_posts', 'seomix_adm_user_show_myposts');
  */
  }

add_action( 'init', function() {
    $author_role = get_role( 'author' );
    add_role( 'vip', 'VIP', $author_role->capabilities );
    $vip_role = get_role( 'vip' );
    $vip_role->remove_cap( 'publish_posts' );
    add_role( 'stagiaire', 'Stagiaire', $author_role->capabilities );
    $stagiaire_role = get_role( 'stagiaire' );
    $stagiaire_role->add_cap( 'unfiltered_html' );
    $stagiaire_role->remove_cap( 'publish_posts' );
    $stagiaire_role->add_cap( 'edit_others_posts' ); 
    $stagiaire_role->add_cap( 'edit_files' ); 
    $editor_role = get_role( 'editor' );
    $editor_role->add_cap( 'list_users' );
    $editor_role->add_cap( 'edit_users' );
    $editor_role->add_cap( 'unfiltered_html' );
    $editor_role->add_cap( 'publish_posts' );

    $admin = get_role( 'administrator' );

    add_role( 'superadmin', 'Super Admin', $admin->capabilities );
    $superadmin = get_role( 'superadmin' );
    $superadmin->add_cap( 'delete_published_posts');

    $admin->add_cap( 'delete_published_posts');
    $vip_role->remove_cap( 'delete_published_posts');
    $editor_role->remove_cap( 'delete_published_posts');
    $stagiaire_role->remove_cap( 'delete_published_posts');

} );


add_filter( 'user_contactmethods', function( $user_contact ) {
    $user_contact[ 'authortitle' ] = 'Titre';
    $user_contact[ 'instagram' ] = 'Instagram profile URL';
    $user_contact[ 'twitter' ] = 'Twitter URL';
    $user_contact[ 'pinterest' ] = 'Pinterest profile URL';
    $user_contact[ 'linkedin' ] = 'Linkedin profile URL';
    $user_contact[ 'quota' ] = 'Number of article';
    $user_contact[ 'quotavideo' ] = 'Number of video';
    unset( $user_contact[ 'aim' ] );
    unset( $user_contact[ 'jabber' ] );
    unset( $user_contact[ 'yim' ] );

    return $user_contact;
} );


//Personnalisation login admin
function bweb_custom_login() {
     echo '<style type="text/css">
     h1 a { background-image:url('. get_stylesheet_directory_uri() . '/img/icons/ellebelgique.png) !important; margin-bottom: 0px !important; padding: 0px !important; width:238px !important; height:101.5px !important; background-size: contain !important; }
     #user_login:focus, #user_pass:focus{ border-color: #e53d3e !important; box-shadow: 0 0 2px rgba(229,61,62,.8); !important;}
     #wp-submit{ background-color: #e53d3e; text-shadow: 0 -1px 1px #e53d3e,1px 0 1px #e53d3e,0 1px 1px #e53d3e,-1px 0 1px #e53d3e; box-shadow: 0 1px 0 #e53d3e; border-color: #e53d3e #e53d3e #e53d3e; border-radius:0;}
     #rememberme{color: #e53d3e; border-color: #e53d3e;
    -webkit-box-shadow: 0 0 2px rgba(229,61,62,.8);
    box-shadow: 0 0 2px rgba(229,61,62,.8);
}}
     </style>';
}
add_action( 'login_head', 'bweb_custom_login');

function bweb_login_url(){
     return ( home_url() );
}
add_filter('login_headerurl', 'bweb_login_url');

function bweb_login_title(){
    return get_bloginfo('name');
}
add_filter('login_headertitle', 'bweb_login_title');

function stripAccents( $str, $charset = 'utf-8' ) {
    $str = htmlentities( $str, ENT_NOQUOTES, $charset);
    $str = preg_replace( '#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str );
    $str = preg_replace( '#&([A-za-z]{2})(?:lig);#', '\1', $str );
    $str = preg_replace( '#&[^;]+;#', '', $str );
    return $str;
}

//////********** ANALYTICS FCT ********///////

function ad_position($post_id = ''){ 
        global $post;
        $ad_position['cat'] = 'OTHER';
        $ad_position['subcat'] = 'OTHER';
        $ad_position['pebble'] = 'others';
        $ad_position['pebble_categories'] = '';
        $ad_position['type'] = '';

    if ( is_attachment() ) {
        $post_id = get_post()->post_parent;
        $ad_position['cat'] = 'IMAGE';
        $ad_position['subcat'] = 'OTHER';
        $ad_position['pebble'] = 'media';
        $ad_position['type'] = ' (Media)';
    }

    if ( is_home() && is_front_page() ) {
        /** HOME PAGE **/
        $ad_position['cat'] = 'HOME';
        $ad_position['subcat'] = 'HOME';
        $ad_position['pebble'] = 'homepage';
        $ad_position['type'] = ' (Navigation)';
    } else if ( is_category() ) {
        /** CATEGORIE PAGE **/
        $ad_position['cat'] = 'OTHER';
        $ad_position['subcat'] = 'OTHER';
        $ad_position['pebble'] = 'others';
        $ad_position['type'] = ' (Navigation)';

       $cat_id = get_query_var( 'cat' );
       $category_main = category_principales();
        if ( in_array( $cat_id, $category_main ) ) {
            $ad_position['cat'] = get_cat_name($cat_id);
            $ad_position['subcat'] = get_cat_name($cat_id);
        }else{
            $ad_position['cat'] = get_cat_name($cat_id);
            $ad_position['subcat'] = get_cat_name($cat_id);
        }

        $identifiant_pebble = get_field("identifiant_pebble", 'category_'.$cat_id);
        if($identifiant_pebble){ $ad_position['pebble'] = $identifiant_pebble; }

        } else if( is_page() ) {
        $ad_position['cat'] = 'OTHER';
        $ad_position['subcat'] = 'OTHER';
        $ad_position['pebble'] = 'others';
        $ad_position['pebble_categories'] = '';
        $ad_position['type'] = ' (Navigation)';
        
        } else if( is_post_type_archive( 'linkinbio' )) {
        $ad_position['cat'] = 'LINKINBIO';
        $ad_position['subcat'] = 'LINKINBIO';
        $ad_position['pebble'] = 'linkinbio';
        $ad_position['type'] = ' (Navigation)';

        } else {
        /** CATEGORIE ARTICLE **/
        $ad_position['cat'] = 'OTHER';
        $ad_position['subcat'] = 'OTHER';
        $ad_position['pebble'] = 'others';
        $ad_position['type'] = ' (Article)';

        $list_categorie_id = array();
        $list_categorie = get_the_category($post_id);
        $category_main = category_principales();
        $temp_secondaire = '';
        foreach ($list_categorie as $categorie) {

            $list_categorie_id[] = $categorie->term_id;
            $temp_secondaire .= $categorie->name.',';
              if(in_array($categorie->term_id, $category_main)){
                /* DETECTION DE LA MAIN CATEGORY */
                $ad_position['cat'] = $categorie->name;
                $identifiant_pebble = get_field("identifiant_pebble", 'category_'.$categorie->term_id);
                if($identifiant_pebble){ $ad_position['pebble'] = $identifiant_pebble; }
              }
        }

        $ad_position['subcat'] = substr($temp_secondaire,0,-1);

        if(in_array(category_sans_thumb(), $list_categorie_id)){ 
            //CONCOURS
            $ad_position['cat'] = get_cat_name(category_sans_thumb());
            //$ad_position['subcat'] = get_cat_name(category_sans_thumb());
            $identifiant_pebble = get_field("identifiant_pebble", 'category_'.category_sans_thumb());
            if($identifiant_pebble){ $ad_position['pebble'] = $identifiant_pebble; }
        }

        $identifiant_pebble_single = get_field("identifiant_pebble", $post_id);
        if($identifiant_pebble_single){ $ad_position['pebble'] = $identifiant_pebble_single;}

        $identifiant_pebble_categories = get_field("categories_ad_pebble", $post_id);
        if($identifiant_pebble_categories){ $ad_position['pebble_categories'] = $identifiant_pebble_categories;}

    }

    return $ad_position;
}

function ad_position_ter() {
    $tag_id = '';
    if ( is_tag() ) {
        $tag_id = single_tag_title( '', false );
    }
    else if ( is_single() || is_attachment() ) {
        $posttags = get_the_tags();
        if ( $posttags ) {
            foreach( $posttags as $tag ) {
                $tag_id .= $tag->name .', ';
            }
            $tag_id = substr( $tag_id, 0, -2 );
        }
        else {
            $tag_id = 0;
        }
    }
    else {
        $tag_id = 0;
    }

    return $tag_id;
}

//////********** ANALYTICS FCT ********///////



/** RETIRE TITRE DE LA BALISE OG TITLE **/
add_filter('wpseo_opengraph_title','mysite_ogtitle', 999);
function mysite_ogtitle($title) {

    $sepparator = " - ";
    $the_website_name = $sepparator . get_bloginfo( 'name' );
    return str_replace( $the_website_name, '', $title ) ;

}


/** Fonction article **/
function get_category_article(){
        if(is_single()){
            $slug = get_post_field( 'post_name', get_post() ); 
            $categories = get_the_category(get_the_ID());
            $tags =  wp_get_post_tags(get_the_ID());
            $info_article['list_categories'] = '';
            $info_article['list_tags'] = '';
            $list_categories = '';
            $list_tags = '';
            foreach ($categories as $categorie) {
                $list_categories .=  $categorie->name . ',';
            }
            if(isset($list_categories) && $list_categories != ''){ $info_article['list_categories'] = $list_categories; }
            foreach ($tags as $tag) {
                $list_tags .=  $tag->name . ',';
            }
            if(isset($list_tags) && $list_tags != ''){ $info_article['list_tags'] = $list_tags; }
            return $info_article;
        }else{
            $info_article['list_categories'] = '';
            $info_article['list_tags'] = '';
            return $info_article;
        }
}

function get_first_paragraph($id){
    
    $content_post = get_post($id);
    $str = wpautop($content_post->post_content);
    $str = apply_filters('the_content', $str);
    $str = str_replace(']]>', ']]&gt;', $str);

    $str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
    $str = strip_tags($str, '<a><strong><em>');
    return '<p>' . $str . '</p>';
}

//A trier
remove_filter('the_content', 'wptexturize');

add_filter('relevanssi_hits_filter', 'rlv_hits_filter');
function rlv_hits_filter($hits) {
    if ($hits[0] == null) {
    // no search hits, so must create new
    $date = substr($_GET['date'], 0, -1);
    $args = array(
        's' => $_GET['s'],
        'paged' => $paged,
        'category__in' => $_GET['category'],
        'numberposts' => 30,
        'post_status' => 'publish',
        'date_query' =>  array(array('after' => $date.' days ago'))
        // Add the required parameters here.
    );

    $hits[0] = get_posts($args);
    }
    else {
    // posts available, take only those that match the conditions
    $ok = array();
    foreach ($hits[0] as $hit) {
            array_push($ok, $hit);
    }
    $hits[0] = $ok;
    }
    return $hits;
}