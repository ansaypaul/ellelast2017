<?php 
/*
Template Name: Analyse GA
*/


  $daydate_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y"));
  $lastyear_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1);

  $daydate = date('Ymd',$daydate_unix);
  $lastyear_format = date('Ymd',$lastyear_unix);

class Article // Présence du mot-clé class suivi du nom de la classe.
  {
    public $_articles = array();      
    public $_id = '';
    public $_title = '';
    public $_content = '';
    public $_word_count = 0;
    public $_link_count = 0;
    public $_link_external_count = 0;
    public $_alt_image_checker = 0;
    public $_title_checker_h1 = 0;
    public $_title_checker_h2 = 0;
    public $_title_checker_h3 = 0;
    public $_yoast_keywork_count = 0;
    public $_yoast_keywork = 0;
    public $_yoast_keywork_count_title = 0;
    public $_is_a_question = 0;
    public $_score = 0;

    public function set_article($post){
    $this->_id = $post->ID;
  $this->_content = $post->ID;
  $this->_title = $post->post_title;
  
    }

}

$author_id = get_current_user_id();


$score = 0;
global $post;

function substri_count($haystack, $needle)
{
    return substr_count(strtolower($haystack), strtolower($needle));
}

function word_count($content = '') {
    $word_count = str_word_count( strip_tags( $content ) );
    return $word_count;
}

function link_count($content = '') {
    $link_count = substri_count($content,'<a href="https://www.elle.be');
    $link_count += substri_count($content,'<a href="http://www.elle.be');
    return $link_count;
}

function link_external_count($content = '') {
    $link_count = substri_count($content,'<a href="https://www.elle.be');
    $link_count += substri_count($content,'<a href="http://www.elle.be');
    $link_external_count = substri_count($content,'<a href="');
    $link_external_count = $link_external_count - $link_count;
    return $link_external_count;
}

function alt_image_checker($content = '') {
  $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
  $dom = new DOMDocument();
    @$dom->loadHTML($content);
  $images = $dom->getElementsByTagName("img");
  $checker = 'Aucune image';
  if(count($images) > 0 ){
    foreach ($images as $image) {
        if (!$image->hasAttribute("alt") || $image->getAttribute("alt") == "") {
            $checker = 'Automatique';
            return $checker;
            break;
        }else{}
    }
  }else{}
}

function formatbytes($file)
{
   $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
   if($filesize <= 0){
      return $filesize = 'unknown file size';}
   else{return round($filesize, 2).$type;}
}

function weight_image_checker($content = '') {
  $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
  $dom = new DOMDocument();
    @$dom->loadHTML($content);
  $images = $dom->getElementsByTagName("img");
  $weight = 0;
  if(count($images) > 0 ){
    foreach ($images as $image) {
      $img = get_headers($image->getAttribute("src"), 1);
      if(isset($img["Content-Length"])){
        $weight += $img["Content-Length"]* .0009765625 * .0009765625;
      }
    }
    return $weight;
  }else{}
}

function title_checker_h1($content = ''){
    $h1 = substri_count($content,'</h1>');
    return $h1;
}

function title_checker_h2($content = ''){
    $h2 = substri_count($content,'</h2>');
    return $h2;
}

function title_checker_h3($content = ''){
    $h3 = substri_count($content,'</h3>');
    return $h3;
}


function yoast_keywork($post_id = ''){
   global $post;
   $yoast_title = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
   if(isset($yoast_title) && $yoast_title != ''){ $post->score = 10; }
   return $yoast_title;
}

function yoast_keywork_count_title($post_id = ''){
   $yoast_title = get_post_meta($post_id, '_yoast_wpseo_title', true);
   if($yoast_title == '' || $yoast_title == '%%title%% %%page%% %%sep%% %%sitename%%'){$yoast_title = get_the_title($post_id); }
   $yoast_kw = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
   if($yoast_title != '' && $yoast_kw != ''){
     $keyword_count = substri_count($yoast_title,$yoast_kw);
     //echo '<script>console.log("'.$yoast_title.' : '.$yoast_kw.'");</script>';
    return $keyword_count;
  }else{
    return '';
  }
}

function yoast_keywork_count($post_id = '',$content = ''){
   $yoast_kw = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
   if($yoast_kw != ''){
     $keyword_count = substri_count($content,$yoast_kw);
    return $keyword_count;
  }else{
    return '';
  }
}

function get_author($post){
  return get_the_author_meta('display_name');
}


function display_score($score){
  return $score;
}

function is_a_question($post){
  $question_count = substri_count($post->post_title,'?');
    return $question_count;
}

function permalink_for_ga(){
  $url_format = str_replace('https://www.elle.be', '', get_the_permalink());
  return $url_format;
}

function display_date_crea(){
  if(get_field('date_de_creation')){
    $date_creation = substr(get_field('date_de_creation'),6,2).'/'.substr(get_field('date_de_creation'),4,2).'/'.substr(get_field('date_de_creation'),0,4);
  }else{
    $date_creation = get_the_date('d/m/Y');
  }
  return $date_creation;
}

function link_for_ga($post){

  $daydate_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y"));
  $lastyear_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1);

  $daydate = date('Ymd',$daydate_unix);
  $lastyear_format = date('Ymd',$lastyear_unix);

  $permalink =  get_the_permalink();
  $permalink = str_replace("https://www.elle.be/fr/", "",$permalink);
  $permalink = str_replace("https://www.elle.be/nl/", "",$permalink);
  $permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71942965p74244334/_u.date00='.$lastyear_format.'&_u.date01='.$daydate.'&_.useg=builtin1&explorer-table.filter='.$permalink.'&explorer-table.plotKeys=%5B%5D&explorer-table.secSegmentId=analytics.sourceMedium&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22RE%22,'.$permalink.',0%5D%5D/';
/*
https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71948641p74251911/_u.date00=20171009&_u.date01=20181009&_.useg=builtin1&explorer-table.filter=86516-5-make-uplesjes-voor-brildragers.html&explorer-table.plotKeys=%5B%5D&explorer-table.secSegmentId=analytics.sourceMedium&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22RE%22,86516-5-make-uplesjes-voor-brildragers.html,0%5D%5D/

  https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71942965p74244334/_u.date00=20170814&_u.date01=20180924&_.useg=builtin1&explorer-table.filter=200607-tartelettes-aux-fruits-secs-minces-pies.html&explorer-table.plotKeys=%5B%5D&explorer-table.secSegmentId=analytics.sourceMedium&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22RE%22,%22200607-tartelettes-aux-fruits-secs-minces-pies.html%22,0%5D%5D/

  /*$permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/trafficsources-all-traffic/a42249957w71942965p74244334/_u.date00=20170814&_u.date01=20180820&_.useg=builtin1&explorer-table.plotKeys=%5B%5D&explorer-table.secSegmentId=analytics.pagePath&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22PT%22,'.$permalink.',0%5D%5D&_r.drilldown=analytics.sourceMedium:google%20~2F%20organic/';*/
    return $permalink_ga;
}


function get_pv_lastyear($post_id = ''){
   $pv_lastyear = get_post_meta($post_id, 'sessions_1_an', true);
   if($pv_lastyear != ''){
    return $pv_lastyear;
  }else{
    return '0';
  }
}

function get_pv_lastyear_totale($post_id = ''){
   $pv_lastyear = get_post_meta($post_id, 'sessions_1_an_totale', true);
   if($pv_lastyear != ''){
    return $pv_lastyear;
  }else{
    return '0';
  }
}


$arg = array(
'posts_per_page' => 2000,
//'author'        =>  $author_id,
//'post_status' => array('publish','draft', 'pending', 'future', 'private', 'inherit'),
);

if($_GET['dev'] == 1) {
	$arg = array(
	'posts_per_page' => 10000,
	'category__not_in' => array(1961), 
	//'author'        =>  $author_id,
	'post_status' => array('publish') ,
	'year'  => '2020',  
	//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
	);
}


if($_GET['dev'] == 3) {
$arg = array(
//'post__in' => array('44994'),
'posts_per_page' => 99999,
'category__not_in' => array(1961), 
//'author'        =>  $author_id,
'post_status' => array('publish') ,
    'meta_query' => array(
        array(
            'key' => 'pages_vues_1_an',
            'compare' => 'NOT EXISTS' // this should work...
        )
    ),


//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
);
}


if($_GET['dev'] == 4) {
$arg = array(
//'post__in' => array('44994'),
'posts_per_page' => 10000,
'category__not_in' => array(1961), 
//'author'        =>  $author_id,
'post_status' => array('publish') ,
    'meta_query' => array( 
      'relation' => 'AND',
        array(
            'key' => 'pages_vues_1_an',
            'compare' => 'NOT EXISTS' // this should work...
        ),
        array(
            'key' => 'pages_vues_totale',
        'compare' => 'NOT EXISTS' // this should work...
        ),
        /*array(
            'key' => 'status_updated',
        'compare' => 'NOT EXISTS' // this should work...
        )*/
    ),
//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
    'post_date' => 'ASC'
);
}

if($_GET['dev'] == 5) {
$arg = array(
//'post__in' => array('44994'),
'posts_per_page' => 10000,
'category__not_in' => array(1961), 
//'author'        =>  $author_id,
'post_status' => array('publish') ,
    'meta_query' => array( 
        array(
            'key' => 'pages_vues_totale',
            'compare' => 'NOT EXISTS' // this should work...
        ),

        /*array(
            'key' => 'status_updated',
        'compare' => 'NOT EXISTS' // this should work...
        )*/
    ),
//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
    'post_date' => 'ASC'
);
}

if($_GET['dev'] == 8) {
$arg = array(
//'post__in' => array('44994'),
'posts_per_page' => 10000,
'category__in' => array(1), 
//'author'        =>  $author_id,
'post_status' => array('publish') ,
//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
    'post_date' => 'ASC'
);
}

if($_GET['dev'] == '99') {
$arg = array(
//'post__in' => array('44994'), 
'posts_per_page' => 99999,
'category__not_in' => array(1961), 

'post_status' => array('publish') ,
    'meta_query' => array( 
        array(
        'key' => 'status_updated',
        'value' => 'mettreajour',
        'compare' => 'LIKE',
        ),
    ),
//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
);
}


if($_GET['dev'] == 0) {
	$arg = array(
	//'post__in' => array('44994'),
	'year'  => $_GET['year_nb'],  
	'posts_per_page' => $_GET['nb_pages'],
	//'category__in' => array(1), 
	'category__not_in' => array(1961), 
	'author'        => $_GET['author_id'],
	'post_status' => array('publish'),
	'orderby' => 'ID',
	'meta_query' => array( 
			'relation' => 'AND',
			array(
				'relation' => 'OR',
					array(
					    'key' => 'pages_vues_totale',
					    'compare' => 'NOT EXISTS' // this should work...
					),
					array(
					'key' => 'pages_vues_totale',
					'value' => 10,
					'compare' => '<=',
					'type'    => 'numeric',
					),
				),
				array(
					'key' => 'status_updated',
					'compare' => 'NOT EXISTS' // this should work...
				),
			),
	);
}

if($_GET['dev'] == 'top') {
  $arg = array(
  //'post__in' => array('44994'),
  'year'  => $_GET['year_nb'],  
  'posts_per_page' => $_GET['nb_pages'],
  'category__in' => array(1), 
  'category__not_in' => array(1961), 
  'author'        => $_GET['author_id'],
  'post_status' => array('publish'),
  'orderby' => 'ID',
    'meta_query' => array( 
      'relation' => 'AND',
      array(
        'relation' => 'OR',
          array(
              'key' => 'pages_vues_totale',
              'compare' => 'NOT EXISTS' // this should work...
          ),
          array(
          'key' => 'pages_vues_totale',
          'value' => 10,
          'compare' => '<=',
          'type'    => 'numeric',
          ),
        ),
        array(
          'key' => 'status_updated',
          'compare' => 'NOT EXISTS' // this should work...
        ),
      ),
  );
}

if($_GET['dev'] == 10) {
  $arg = array(
  //'post__in' => array('44994'),
  'posts_per_page' => $_GET['nb_pages'],
  //'category__not_in' => array(1961), 

  'post_status' => array('trash'),
  //'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
  );
}

if($_GET['dev'] == 100) {
	$arg = array(
	//'post__in' => array('44994'),
	'year'  => $_GET['year_nb'],  
	'posts_per_page' => $_GET['nb_pages'],
	//'category__in' => array(1), 
	'category__not_in' => array(1961), 
	'author'        => $_GET['author_id'],
	'post_status' => array('publish'),
	'orderby' => 'ID'
	);
}

$posts_popular = get_posts($arg);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Hello Analytics Reporting API V4</title>
  <meta name="google-signin-client_id" content="1066911604673-2usen9clcj40ujpanh33f4gahuqfm3ud.apps.googleusercontent.com">
  <meta name="google-signin-scope" content="https://www.googleapis.com/auth/analytics.readonly">
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
  </script>

</head>
<body>
<!-- The Sign-in button. This will run `queryReports()` on success. -->

<p class="g-signin2"></p>
<button onclick="tracking_lauch();">GO </button>
<button onclick="tracking_lauch_total();">GO TOTAL</button>
<button onclick="tracking_1000('0');">GO 1000</button>
<button onclick="delete_post_action_all();">CLEAR</button>
<br/>

<label for="year">Post Year:</label> 
<select name="year">
  <option value="">-</option>
  <option value="2018">2018</option>
  <option value="2017">2017</option>
  <option value="2016">2016</option>
  <option value="2015">2015</option>
  <option value="2014">2014</option>
  <option value="2013">2013</option>
</select>

<h1>Analyse SEO : <?php echo "Nombre d'article : ".count($posts_popular); ?></h1>
<table id="myTable" width="98%">
  <thead>
    <tr>
      <th>ID Article</th>
      <th>Date</th>
      <th>Titre</th>
      <th>Catégorie</th>
      <th>Auteur</th>
      <th>KW Yoast</th>
      <th>KW Content</th>
      <th>KW Titre</th>
      <th>H1</th>
      <th>H2</th>
      <th>H3</th>
      <th>Alt images</th>
      <!-- <th>Weight images</th> -->
      <th>Nombre de mots</th>
      <th>Liens internes</th>
      <th>Liens externes</th>
      <th>?</th>
      <th>Sessions organic</th>
      <th>Sessions Total</th>
      <th>Link GA</th>
      <th>GA</th>
      <th>TOT</th>
      <th>Status</th>
      <th>Edit</th>
      <th>Delete</th>
      <!--<th>Score</th>-->
</tr>
</thead>
<tbody>
<?php
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
global $post;
  $content = get_post_field( 'post_content', $post->ID );
  $score = 0;
  $main_categorie = '';
  $list_categorie = get_the_category(); 

  if(isset($list_categorie)){
    $main_categorie = $list_categorie[0];
  }

  ?>
  <tr>
    <td><?php echo $post->ID ?></td>
    <td><?php echo display_date_crea();  ?></td>
    <td><a target="_blank" class="url_parser" href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></td>
    <td><?php echo $main_categorie->name; ?></td>
    <td><?php echo get_author($post->ID); ?></td>
    <td <?php if(yoast_keywork($post->ID) == ''){ echo "style='background:#ff7f7f'"; } ?>><?php echo yoast_keywork($post->ID); ?></td>
    <td <?php if(yoast_keywork_count($post->ID,$content) < 1){ echo "style='background:#ff7f7f'"; } ?>><?php echo yoast_keywork_count($post->ID,$content); ?></td>
    <td <?php if(yoast_keywork_count_title($post->ID) < 1){ echo "style='background:#ff7f7f'"; } ?>><?php echo yoast_keywork_count_title($post->ID); ?></td>
    <td><?php echo title_checker_h1($content); ?></td>
    <td><?php echo title_checker_h2($content); ?></td>
    <td><?php echo title_checker_h3($content); ?></td>
    <td <?php if(alt_image_checker($content) == "Non"){ echo "style='background:#ff7f7f'"; } ?>><?php echo alt_image_checker($content); ?></td>
    <!-- <td>0</td> -->
    <td <?php if(word_count($content) < 300){ echo "style='background:#ff7f7f'"; } ?>><?php echo word_count($content); ?></td>
    <td <?php if(link_count($content) < 2){ echo "style='background:#ff7f7f'"; } ?>><?php echo link_count($content); ?></td>
    <td <?php if(link_external_count($content) < 1){ echo "style='background:#ff7f7f'"; } ?>><?php echo link_external_count($content); ?></td>
    <td><?php echo is_a_question($post); ?></td>
    <td id="<?php echo $post->ID; ?>" <?php if(get_pv_lastyear($post->ID) == 0){ echo "style='background:#ff7f7f'"; } ?>><?php echo get_pv_lastyear($post->ID); ?></td>
    <td id="total_<?php echo $post->ID; ?>" <?php if(get_pv_lastyear_totale($post->ID) == 0){ echo "style='background:#ff7f7f'"; } ?>><?php echo get_pv_lastyear_totale($post->ID); ?></td>
    <!--<td><?php echo $post->score; ?></td>-->
    <td><a target="_blank" href="<?php echo link_for_ga($post); ?>">GA</a></td>
    <td class='analyse_ga' data-urlarticle="<?php the_permalink(); ?>" >GA</td>
    <td class='analyse_ga_total' data-urlarticle="<?php the_permalink(); ?>" >TOT</td>
    <td>
    <select class="select_status" data-idarticle="<?php echo $post->ID  ?>">
      <option value=""> - </option> 
      <option value="misajour">Mis à jour</option>
      <option value="mettreajour">Mettre à jour</option>
      <option value="supprimer">Supprimer</option>
      <option value="desindexer">Désindexer</option>
    </select>
    </td>
    <td><a target="_blank" href="https://www.elle.be/fr/wp-admin/post.php?post=<?php echo $post->ID  ?>&action=edit">EDIT</a></td>
    <td class='delete_post' data-idarticle="<?php echo $post->ID  ?>" >DELETE</td>
  </tr>
<?php
endforeach; wp_reset_postdata(); 
?>
</tbody>
</table>

<?php 
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
global $post;
the_permalink();
echo '<br/>';
endforeach; wp_reset_postdata(); 
?>

<table id="table1000">

</table>

<script>
    var path_windows = window.location.href;
    var arr = path_windows.split("/");
    var elle_path = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];

    /* <![CDATA[ */
    var ajax_search = {"ajaxurl": elle_path + "/wp-admin/admin-ajax.php"};
    /* ]]> */

    if(path_windows.indexOf('/fr/') > 0){
      lang = '/fr/';
        var VIEW_ID = '74244334';
    }else{
      lang = '/nl/';
        var VIEW_ID = '74251911';
    }

  console.log('Langue Site : ' + lang);

  function set_pv_lastyear(post_id,pv_lastyear){
      console.log(post_id);
      $.ajax({
        url: ajax_search.ajaxurl,
        type: 'post',
        data: { action: 'ajax_update_pv_lastyear', post_id: post_id, post_pv_lastyear: pv_lastyear},
        beforeSend: function() {
        
        },
        success: function( html ) {
          if(html != ''){
              console.log('UPDATE OKAI');
            }else{
              console.log('NO UPDATE');               
            }

            },
            error: function (xhr) {
              console.log('error'); 
              } 
        })
  }

  function set_pv_total(post_id,pv_lastyear){
      console.log(post_id);
      $.ajax({
        url: ajax_search.ajaxurl,
        type: 'post',
        data: { action: 'ajax_update_pv_totale', post_id: post_id, post_pv_lastyear: pv_lastyear},
        beforeSend: function() {
        
        },
        success: function( html ) {
          if(html != ''){
              console.log('UPDATE OKAI');
            }else{
              console.log('NO UPDATE');               
            }

            },
            error: function (xhr) {
              console.log('error'); 
              } 
        })
  }



  function displayResults(response) {
    var formattedJson = JSON.stringify(response.result, null, 2);
    var json = JSON.parse(formattedJson);
    var url_value = 0;
    console.log(json);
    if(json['reports'][0]['data']['totals'][0]['values'][0] > 0 ){    
        var url_link = json['reports'][0]['data']['rows'][0]['dimensions'][0];

        var url_link = url_link.replace('/fr/', '');
        var url_link = url_link.replace('/nl/', '');
        console.log(url_link);
        var res = url_link.split("-");

        var url_value = json['reports'][0]['data']['totals'][0]['values'][0];
        $('#'+res[0]).html(url_value);
      set_pv_lastyear(res[0],url_value);
  }else{

  }
  }


  function displayResultstotal(response) {
    var formattedJson = JSON.stringify(response.result, null, 2);
    var json = JSON.parse(formattedJson);
    var url_value = 0;
    console.log(json);
    if(json['reports'][0]['data']['totals'][0]['values'][0] > 0 ){  
        console.log('>0');  
        var url_link = json['reports'][0]['data']['rows'][0]['dimensions'][0];

        var url_link = url_link.replace('/fr/', '');
        var url_link = url_link.replace('/nl/', '');
        console.log(url_link);
        var res = url_link.split("-");

        var url_value = json['reports'][0]['data']['totals'][0]['values'][0];
        $('#total_'+res[0]).html(url_value);
        set_pv_total(res[0],url_value);
  }else{
        console.log('PAS DE RESULTAT'); 
  }
}


  function check_http_header(url_parse){
      return $.ajax(url_parse, {
         type:  "GET",
         async: false,
         data: "",
         statusCode: {
            200: function (response) {
               return '200';
            },
            201: function (response) {
               return '201';
            },
            400: function (response) {
               return '400';
            },
            404: function (response) {
               return '404';
            },
            410: function (response) {
               return '410';
            }
         }, success: function () {
          return 1;
         },
      });
  }


  function display_tracking_1000(response){

    var formattedJson = JSON.stringify(response.result, null, 2);
    var json = JSON.parse(formattedJson);
    console.log(json);
    console.log(json['reports'][0]['data']['rows'].length);
    for (var i = 0, len = json['reports'][0]['data']['rows'].length; i < len; i++) {
      var id_article = '';
      var url_link = json['reports'][0]['data']['rows'][i]['dimensions'][0];
      var url_value = json['reports'][0]['data']['rows'][i]['metrics'][0]['values'][0];  
     
      var url_code = '';
      content += '<tr><td>'+url_link+'</td><td>'+url_value+'</td><td>'+url_code+'</td></tr>';
    }

    $('#table1000').append(content);

    if(json['reports'][0]['nextPageToken']){
        tracking_1000("10000");
    }
  }

    var pageToken = "0";

    function tracking_1000(pageToken){
        gapi.client.request({
          path: '/v4/reports:batchGet',
          root: 'https://analyticsreporting.googleapis.com/',
          method: 'POST',
          body: {
            reportRequests: [
              {
                viewId: VIEW_ID,
                dateRanges: [
                  {
                    startDate: '365daysAgo',
                    endDate: 'today'
                  }
                ],
                metrics: [
                  {
                    expression: 'ga:uniquePageviews'
                  }
                ],
                dimensions: [
                  {
                    name: "ga:pagePath"
                  }
                ],
              dimensionFilterClauses: [
                {
                    filters: [
                      {
                       "dimensionName": "ga:pagePath",
                       "operator": "ENDS_WITH",
                       "expressions": 'html'
                      }
                    ]
                },
              ],
              "orderBys": [
                {
                  "fieldName": "ga:uniquePageViews",
                  "sortOrder": "ASCENDING"
                }
              ],
              "pageSize": "10000",
              "pageToken": pageToken,

              }
            ]
          }
        }).then(display_tracking_1000, console.error.bind(console));
    }


    // Query the API and print the results to the page.
    function displayResultsFull(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
      var json = JSON.parse(formattedJson);
      var url_value = 0;
      console.log(json);
  
    }


  function tracking_lauch_one_total(){
    $( ".analyse_ga_total" ).click(function( index ) {

        var self = this

        var url_post = $( self ).data('urlarticle');
      var url_post = url_post.replace('https://www.elle.be', '');
      var url_post = url_post.replace('/fr/', '');
      var url_post = url_post.replace('/nl/', '');
        var res = url_post.split("-");

        var url_post = res[0];
        var url_post = url_post.replace('https://www.elle.be', '');
      url_post = lang + url_post + '-';
      console.log('CLICK SOLO TOTAL' + index + ' ID ARTICLE : '+ url_post );

        gapi.client.request({
          path: '/v4/reports:batchGet',
          root: 'https://analyticsreporting.googleapis.com/',
          method: 'POST',
          body: {
            reportRequests: [
              {
                viewId: VIEW_ID,
                dateRanges: [
                  {
                    startDate: '365daysAgo',
                    endDate: 'today'
                  }
                ],
                metrics: [
                  {
                    expression: 'ga:uniquePageviews'
                  }
                ],
                dimensions: [
                  {
                    name: "ga:pagePath"
                  }
                ],
              dimensionFilterClauses: [
                {
                    filters: [
                      {
                       "dimensionName": "ga:pagePath",
                       "operator": "BEGINS_WITH",
                       "expressions": [url_post]
                      }
                    ]
                }
              ]

              }
            ]
          }
        }).then(displayResultstotal, console.error.bind(console));
    });
  }

  function tracking_lauch_one(){
    $( ".analyse_ga" ).click(function( index ) {

      var self = this

      var url_post = $( self ).data('urlarticle');
      var url_post = url_post.replace('https://www.elle.be', '');
      var url_post = url_post.replace('/fr/', '');
      var url_post = url_post.replace('/nl/', '');
      var res = url_post.split("-");

        var url_post = res[0];
        var url_post = url_post.replace('https://www.elle.be', '');
      url_post = lang + url_post + '-';
      console.log('CLICK SOLO ' + index + ' ID ARTICLE : '+ url_post );

        gapi.client.request({
          path: '/v4/reports:batchGet',
          root: 'https://analyticsreporting.googleapis.com/',
          method: 'POST',
          body: {
            reportRequests: [
              {
                viewId: VIEW_ID,
                dateRanges: [
                  {
                    startDate: '365daysAgo',
                    endDate: 'today'
                  }
                ],
                metrics: [
                  {
                    expression: 'ga:uniquePageviews'
                  }
                ],
                dimensions: [
                  {
                    name: "ga:pagePath"
                  }
                ],
              dimensionFilterClauses: [
                {
                    filters: [
                      {
                       "dimensionName": "ga:pagePath",
                       "operator": "PARTIAL",
                       "expressions": [url_post]
                      }
                    ]
                },
                {
                  filters: [
                      {
                       "dimensionName": "ga:medium",
                       "operator": "EXACT",
                       "expressions": 'organic'
                      }
                    ]
                }
              ]

              }
            ]
          }
        }).then(displayResults, console.error.bind(console));
    });
  }

  function tracking_lauch(){
    $( ".url_parser" ).each(function( index ) {

        var self = this;
      setTimeout(function () {

        var url_post = $( self ).attr('href');
      var url_post = url_post.replace('https://www.elle.be', '');
      var url_post = url_post.replace('/fr/', '');
      var url_post = url_post.replace('/nl/', '');

        var res = url_post.split("-");

        var url_post = res[0];
        var url_post = url_post.replace('https://www.elle.be', '');
      url_post = lang + url_post + '-';
      console.log(index + ' ID ARTICLE : '+ url_post );

        gapi.client.request({
          path: '/v4/reports:batchGet',
          root: 'https://analyticsreporting.googleapis.com/',
          method: 'POST',
          body: {
            reportRequests: [
              {
                viewId: VIEW_ID,
                dateRanges: [
                  {
                    startDate: '365daysAgo',
                    endDate: 'today'
                  }
                ],
                metrics: [
                  {
                    expression: 'ga:uniquePageviews'
                  }
                ],
                dimensions: [
                  {
                    name: "ga:pagePath"
                  }
                ],
              dimensionFilterClauses: [
                {
                    filters: [
                      {
                       "dimensionName": "ga:pagePath",
                       "operator": "PARTIAL",
                       "expressions": [url_post]
                      }
                    ]
                },
                {
                  filters: [
                      {
                       "dimensionName": "ga:medium",
                       "operator": "EXACT",
                       "expressions": 'organic'
                      }
                    ]
                }
              ]

              }
            ]
          }
        }).then(displayResults, console.error.bind(console));

       }, index*2000);
    });
  }

  function tracking_lauch_total(){
    $( ".url_parser" ).each(function( index ) {

        var self = this;
      setTimeout(function () {

        var url_post = $( self ).attr('href');
      var url_post = url_post.replace('https://www.elle.be', '');
      var url_post = url_post.replace('/fr/', '');
      var url_post = url_post.replace('/nl/', '');

        var res = url_post.split("-");

        var url_post = res[0];
        var url_post = url_post.replace('https://www.elle.be', '');
      url_post = lang + url_post + '-';
      console.log(index + ' ID ARTICLE : '+ url_post );


        gapi.client.request({
          path: '/v4/reports:batchGet',
          root: 'https://analyticsreporting.googleapis.com/',
          method: 'POST',
          body: {
            reportRequests: [
              {
                viewId: VIEW_ID,
                dateRanges: [
                  {
                    startDate: '365daysAgo',
                    endDate: 'today'
                  }
                ],
                metrics: [
                  {
                    expression: 'ga:uniquePageviews'
                  }
                ],
                dimensions: [
                  {
                    name: "ga:pagePath"
                  }
                ],
              dimensionFilterClauses: [
                {
                    filters: [
                      {
                       "dimensionName": "ga:pagePath",
                       "operator": "PARTIAL",
                       "expressions": [url_post]
                      }
                    ]
                }
              ]

              }
            ]
          }
        }).then(displayResultstotal, console.error.bind(console));

       }, index*2000);
    });
  }

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = year + ' ' + month + ' ' + date ;
  return time;
}

function udapte_post_action(){
    $( ".select_status" ).change(function( index ) {
      var self = this;
      console.log($(this).val());

      var post_id = $( self ).data('idarticle');
      console.log('Upd status : ' + post_id);

      $.ajax({
        url: ajax_search.ajaxurl,
        type: 'post',
        data: { action: 'ajax_set_status', post_id: post_id, status_updated: $(this).val()},
        beforeSend: function() {
        
        },
        success: function( html ) {
          if(html != ''){
              console.log(html);
            }else{
              console.log(html);                
            }

            },
            error: function (xhr) {
              console.log('error'); 
              } 
        })

     });
  }

function delete_post_action(){
    $( ".delete_post" ).click(function( index ) {
      var self = this;
      var post_id = $( self ).data('idarticle');
      console.log('Delete : ' + post_id);

      $.ajax({
        url: ajax_search.ajaxurl,
        type: 'post',
        data: { action: 'ajax_remove_post', post_id: post_id},
        beforeSend: function() {
        
        },
        success: function( html ) {
          if(html != ''){
              $( self ).parent().remove();
              console.log(html);
            }else{
              console.log(html);                
            }

            },
            error: function (xhr) {
              console.log('error'); 
              } 
        })

     });
  }

function delete_post_action_all(){
	var delete_pass = '<?php echo $_GET['deletedpass']; ?>';
	if(delete_pass == 'tara1180'){
	    $( ".delete_post" ).each(function( index ) {
	    var self = this;
		      var post_id = $( self ).data('idarticle');
		      console.log('Delete : ' + post_id);

		      $.ajax({
		      	async: false,
		        url: ajax_search.ajaxurl,
		        type: 'post',
		        data: { action: 'ajax_remove_post', post_id: post_id},
		        beforeSend: function() {
		        
		        },
		        success: function( html ) {
		          if(html != ''){
		              $( self ).parent().remove();
		              console.log(html);
		            }else{
		              console.log(html);                
		            }

		            },
		            error: function (xhr) {
		              console.log('error'); 
		              } 
		        })
		     });
	}else{
		console.log('ERROR');
	}
  }

  $( "li" ).each(function( index ) {
  console.log( index + ": " + $( this ).text() );
});


$( document ).ready(function() {
    console.log( "ready!" );
    tracking_lauch_one();
    tracking_lauch_one_total();
    delete_post_action();
    udapte_post_action();

});

</script>

<style>
  table {
      border-collapse: collapse;
  }

  table, th, td {
      border: 1px solid black;
      padding: 0 5px;
  }

  tr:hover {
    background: #d3d3d3;
  }

  * {font-size:14px};
</style>

<!-- Load the JavaScript API client and Sign-in library. -->
<script src="https://apis.google.com/js/client:platform.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
</body>
</html>