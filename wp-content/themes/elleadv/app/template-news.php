<?php
/*
Template Name: AMP NEWS ???
*/

function ampify($html='') {
    # Replace img, audio, and video elements with amp custom elements
    $html = str_ireplace(
        ['<img','<video','/video>','<audio','/audio>'],
        ['<amp-img  layout="responsive"','<amp-video','/amp-video>','<amp-audio','/amp-audio>'],
        $html
    );
    # Add closing tags to amp-img custom element
    $html = preg_replace('/<amp-img(.*?)>/', '<amp-img$1></amp-img>',$html);
    # Whitelist of HTML tags allowed by AMP
    $html = strip_tags($html,'<h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>');
    return $html;
}


$args = array(
  'numberposts' => 1
);
 
$latest_post = get_posts( array( 'numberposts' => 1 ) );
$image_src = get_the_post_thumbnail_url($latest_post[0]->ID);
$content = get_the_content($latest_post[0]->ID);

?>
<!doctype html>
<html ⚡ lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

    <link rel="canonical" href="/article.html">
    <link rel="shortcut icon" href="amp_favicon.png">

    <title>News Article</title>

    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <style amp-custom>
  

    #carousel-with-preview amp-img {
      transition: opacity 1s ease-in-out;
    }
    .carousel-preview {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .carousel-preview button {
      height: 40px;
      width: 60px;
      min-width: 60px;
      margin: 4px;
      padding: 0;
    }
    .carousel-preview .amp-carousel-slide {
      margin: 4px;
      padding: 0;
    }
    .carousel-preview button:active {
      opacity: 0.5;
    }
    .carousel1, .carousel2 {
      background: #eee;
      margin: 16px 0;
    }
    .carousel1 .slide > amp-img > img{
      object-fit: contain;
    }
    .carousel2 .slide > amp-img > img{
      object-fit: contain;
    }
    .carousel2 .caption {
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      padding: 8px;
      background: rgba(0, 0, 0, 0.6);
      color: #ddd;
      font-size: smaller;
      max-height: 30%;
    }
    .selected {
      border-width: 1px;
      border-style: solid;
    }

    /* define some contants */
    .collapsible-captions {
      --caption-height: 32px;
      --image-height: 300px;
      --caption-padding: 1rem;
      --button-size: 28px;
      --caption-color: #f5f5f5;;
      --caption-bg-color: #111;
      background: var(--caption-bg-color);
    }
    .collapsible-captions * {
      /* disable chrome touch highlight */
      -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
      box-sizing: border-box;
    }
    /* see https://ampbyexample.com/advanced/how_to_support_images_with_unknown_dimensions/ */
    .collapsible-captions .fixed-height-container {
      position: relative;
      width: 100%;
      height: var(--image-height);
    }
    .collapsible-captions amp-img img {
      object-fit: contain;
    }
    .collapsible-captions figure {
      margin: 0;
      padding: 0;
    }
    /* single line caption */
    .collapsible-captions figcaption {
      position: absolute;
      bottom: 0;
      margin: 0;
      width: 100%;
      /* inital height is one line */
      max-height: var(--caption-height);
      line-height: var(--caption-height);
      padding: 0 var(--button-size);
      /* cut text after first line and show an ellipsis */
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      /* animate expansion */
      transition: max-height 200ms cubic-bezier(0.4, 0, 0.2, 1);
      /* overlay the carousel icons */
      z-index: 1000;
      /* some styling */
      color: var(--caption-color);
      background: var(--caption-bg-color);      
    }
    /* expanded caption */
    .collapsible-captions figcaption.expanded {
      /* add padding and show all of the text */
      padding: var(--button-size);
      line-height: inherit;
      white-space: normal;
      text-overflow: auto;
      max-height: calc(var(--caption-height) + var(--image-height));
      /* show scrollbar in case caption is larger than image */
      overflow: auto;
    }
    /* don't show focus highlights in chrome */
    .collapsible-captions figcaption:focus {
      outline: none;
      border: none;
    }
    /* the expand/collapse icon */
    .collapsible-captions figcaption span {
      display: block;
      position: absolute;
      top: calc((var(--caption-height) - var(--button-size)) / 2);
      right: 2px;
      width: var(--button-size);
      height: var(--button-size);
      line-height: var(--button-size);
      text-align: center;
      font-size: 12px;
      color: inherit;
      cursor: pointer; 
    }

    .related {
        background-color: #f5f5f5;
        margin: 0.5rem;
        /*display: flex;*/
        color: #111;
        padding: 0;
        box-shadow: 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 1px -1px rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
        text-decoration: none;
    }


    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>

    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


    <script type="application/ld+json">
    {
     "@context": "http://schema.org",
     "@type": "NewsArticle",
     "mainEntityOfPage":{
       "@type":"WebPage",
       "@id":"https://example.com/my-article.html"
     },
     "headline": "My First AMP Article",
     "image": {
       "@type": "ImageObject",
       "url": "https://example.com/article_thumbnail1.jpg",
       "height": 800,
       "width": 800
     },
     "datePublished": "2015-02-05T08:00:00+08:00",
     "dateModified": "2015-02-05T09:20:00+08:00",
     "author": {
       "@type": "Person",
       "name": "John Doe"
     },
     "publisher": {
       "@type": "Organization",
       "name": "⚡ AMP Times",
       "logo": {
         "@type": "ImageObject",
         "url": "https://example.com/amptimes_logo.jpg",
         "width": 600,
         "height": 60
       }
     },
     "description": "My first experience in an AMPlified world"
    }
    </script>
  </head>
  <body>
    <header>
      <amp-img src="<?php echo $image_src ?>" layout="responsive" width="266" height="150"></amp-img>
      <amp-sidebar id="sidebar"
        layout="nodisplay"
        side="left">
        <amp-img class="amp-close-image"
          src="/img/ic_close_black_18dp_2x.png"
          width="20"
          height="20"
          alt="close sidebar"
          on="tap:sidebar.close"
          role="button"
          tabindex="0"></amp-img>
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          <li> Nav item 1</li>
          <li> Nav item 2 - Image
            <amp-img class="amp-sidebar-image"
              src="/img/favicon.png"
              width="20"
              height="20"
              alt="an image"></amp-img>
          </li>
          <li> Nav item 3</li>
          <li> Nav item 4</li>
        </ul>
      </amp-sidebar>
      <button on="tap:sidebar.toggle" class="ampstart-btn caps m2">Toggle sidebar</button>
    </header>
    <article>
    <h2>RELATED ARTICLE</h2>
       <!--
       <amp-list width="auto"
          height="500"
          layout="fixed-height"
          items="."
          src="http://localhost:8080/ellelast2017/wp-json/wp/v2/posts?per_page=5&_embed">
          <template type="amp-mustache">
         {{#_embedded}}{{#wp:featuredmedia}}
          <amp-img layout="responsive" src="{{source_url}}" width="400" height="300"></amp-img>
          {{/wp:featuredmedia}}{{/_embedded}}
           <amp-img layout="fixed" src="{{featured_image_src}}" width="100" height="100"></amp-img>
          </template>
        </amp-list>
        -->


    <amp-list width="300" height="100" items="." layout="fixed-height" src="http://localhost:8080/ellelast2017/wp-json/wp/v2/posts?per_page=5&_embed" class="i-amphtml-element i-amphtml-layout-responsive i-amphtml-layout-size-defined i-amphtml-layout" aria-live="polite" style="height: 600px;">
      <template type="amp-mustache">
        <a class="card related" href="{{link}}">
          <amp-img layout="fixed" width="101" height="100" src="{{featured_image_src}}"></amp-img>
          <span>{{#title}}{{rendered}}{{/title}}</span>
        </a>
      </template>
    </amp-list>

      <p class="heading">
      <amp-social-share type="twitter"
        width="45"
        height="33"></amp-social-share>
      <amp-social-share type="facebook"
        width="45"
        height="33"
        data-attribution="254325784911610"></amp-social-share>
      <amp-social-share type="gplus"
        width="45"
        height="33"></amp-social-share>
      <amp-social-share type="email"
        width="45"
        height="33"></amp-social-share>
      <amp-social-share type="pinterest"
        width="45"
        height="33"></amp-social-share>
    </p>
      <h1><?php echo $latest_post[0]->post_title; ?></h1>
      <p>
      <?php echo get_post_field('post_content', $latest_post[0]->post_id)); ?>
      <?php //echo ampify($latest_post[0]->post_content); ?>
      </p>

      <amp-carousel class="carousel2" layout="responsive" width="400" height="300" type="slides" >
      <div class="slide">
      <amp-img layout="fill" src="http://www.elle.be/fr/wp-content/uploads/2017/10/IMG_3419.jpg" width="400" height="300"></amp-img>
        <div class="caption">
          Suspensions vintage, carrelages orientaux, plantes à foison et chaises d'écolier, Seedz c'est aussi une jolie déco pour un déjeuner décontracté.
        </div>
      </div>
      <div class="slide">
      <amp-img layout="fill" src="http://www.elle.be/fr/wp-content/uploads/2017/10/Seedz-Buddha-bowl2.jpg" width="400" height="300"></amp-img>
        <div class="caption">
          Buddha Bowl et jus extrait à froid au menu du jour de Seedz à Ixelles
        </div>
      </div>
      <div class="slide">
      <amp-img layout="fill" src="http://www.elle.be/fr/wp-content/uploads/2017/10/Seedz-Michael-Binkin.jpg" width="400" height="300"></amp-img>
        <div class="caption">
          Chez Seedz les salades healthy sont à composer selon son humeur et son appétit.
        </div>
      </div>
      </amp-carousel>
    </article>
  </body>
</html>