<?php get_header(); ?>
	<main role="main" class="main single-magazine magazine">
		<div class="site-inner">
			<div class="content entry-content">
				<?php get_template_part('template-parts/general/magazine-content'); ?>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>