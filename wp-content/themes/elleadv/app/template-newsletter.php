<?php
/*
Template Name: newsletter 2018
*/
?>

<?php get_header(); ?>
<?php $email = ''; if(isset($_POST['footer_email'])){ $email = $_POST['footer_email']; } ?>
<main role="main" class="main main-page">
  <div class="page">
    <div class="site-inner">
		<div class="newsletter-page">
			<div class="newsletter-ask">
				<h1 class="entry-title"><?php _e("Inscrivez-vous à notre newsletter", 'html5blank' ); ?></h1>
				<p><?php _e("Recevez chaque semaine le meilleur du ELLE Belgique sur les dernières tendances mode, beauté, lifestyle &amp; déco, mais également des infos et invitations relatives aux évènements ELLE Belgique, contenus sponsorisés, concours ainsi que nos communications sur les éventuelles promotions proposées par nos partenaires commerciaux. N’attendez plus !", 'html5blank' );?>
				</p>
				<!--
				<button class="fb-connect"><span><?php _e("Inscription via Facebook", 'html5blank' ); ?></span></button>
				<span>ou</span>
				-->
				<div class="newsletter-content">
				<form method="POST" role="form" action="" name="profil_popup_form" id="profil_popup_form">
					<input type="hidden" name="profil_creationSource" value="<?php _e('ELLE-FR', 'html5blank' ); ?>">
					<input type="hidden" name="profil_motherLanguage" value="<?php _e('FR', 'html5blank' ); ?>">
			    	<span class="sprite"></span>
			        <input type="hidden" name="Token" value="*">
			        <label>
			        	<div class="newsletter-input-wrapper">
				        	<input id="profil_email" class="newsletter-input profil_email" name="profil_email" value="<?php echo $email; ?>" type="email" placeholder="<?php _e('Entrez votre e-mail', 'html5blank' ); ?> ">
				        </div>
						<div>
							<label for="check-all"><?php _e( "Sélectionner toutes les newsletters" ); ?></label>
					       	<input id="check-all" class="newsletter-optin" type="checkbox" name="check-all" value="">
						</div>
						<div>
							<label for="profil_subscriptions"><?php _e( "J’ai lu et j’accepte la déclaration sur la vie privée instaurée par Edition Ventures.", 'html5blank' ); ?></label>
							<input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_marketingConsent" value="">
						</div>
				     	<button class="btn-v1" type="submit" id="newsletter-submit"/><?php _e( 'Valider', 'html5blank' ); ?></button>
				     	<div>
							<label for="profil_subscriptions"><?php _e( "Des newsletters générales reprenant les articles du ELLE Belgique comprenant du contenu rédactionnel &amp; sponsorisé mais aussi les évènements du ELLE Belgique, les concours, actions et promotions.", 'html5blank' ); ?></label>
					       <input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_subscriptions[]" value="ELLE" required>
				       </div>
						<div>
							<label for="profil_subscriptions"><?php _e( "Des newsletters dédiées à nos partenaires comprenant exclusivement des publications sponsorisées et les concours.", 'html5blank' ); ?></label>
					        <input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_subscriptions[]" value="ELLE Partenaire">
						</div>
						<div>
					        <label for="profil_subscriptions"><?php _e( "Des sondages et enquêtes pour participer à l’évolution constante du ELLE Belgique.", 'html5blank' ); ?></label>
					        <input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_subscriptions[]" value="ELLE Sondage">
						</div>
			     	</label>
			    </form>

			    <div class="mentions-legales">
			    <?php _e( "
				<h4>Sachez que vous disposez des droits suivants :</h4>
				    <ol>
				  	  <li>Vous avez le droit d'information et d'accès à vos données personnelles.</li>
				  	  <li>Vous avez le droit de rectification, d’effacement et de limitation du traitement de vos données personnelles.</li>
				  	  <li>Vous avez le droit d'opposition.</li>
				  	  <li>Vous avez le droit à la portabilité de vos données personnelles. </li>
				    </ol>
				    <p>Sachez également que vous avez le droit de retirer votre consentement au traitement de vos données à tout moment.</p>
				    <p>Nous recueillons uniquement les données dont nous avons besoin pour vous informer des activités du Elle Belgique.</p>
				    <p>Si vous souhaitez exercer un de ces droits, nous vous invitons à nous envoyer un e-mail à l’adresse suivante <a href='mailto:privacy@editionventures.be'>privacy@editionventures.be</a> ou à consulter la déclaration sur le respect de la vie privée du Groupe Edition Ventures.</p>
				    <p>Vous pouvez vous désabonner à tout moment en cliquant sur le lien présent dans le bas de chacun de nos courriels.</p>"
				    , 'html5blank' ); 
				?>
			    </div>
			</div>
		    <div class="newsletter-result hide">Votre inscription a bien été prise en compte.</div>
		    <div class="newsletter-error hide">Une erreur est survenue durant votre inscription.</div>
		  </div>
		</div>
	</div>
  </div>
</main>

<?php get_footer(); ?> 