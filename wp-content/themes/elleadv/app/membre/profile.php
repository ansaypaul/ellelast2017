<?php
/*
Template Name: mon-profil 
*/
echo $_SESSION['profil_id'];
if(!isset($_SESSION['profil_email'])){ header('Location: http://localhost:8080/ellelast2017/'); }
if(isset($_POST['btnSubmitProfil'])){	
	$member->set_membre();
	$member->set_session_membre();
	$member->update_member();
}
?>
<?php get_header(); ?>
<main role="main main-page">
	<!-- site-inner -->
	<div class="site-inner">
		<div class="content">
				<header class="entry-header">
					<h1 class="entry-title">Complétez votre profil</h1>
					<?php echo $_SESSION['profil_prenom']; ?>
				</header>
				<div class="entry-content">
					<form method="POST" role="form" id="profil_news"> 
			          <div class="news-input civil">
			          	<div>Vous êtes*</div>
			            <input <?php if($_SESSION['profil_civilite'] == 'Homme'){ echo'checked'; } ?> name="profil_civilite" id="civilMonsieur" value="Homme"  type="radio"><label for="civilMonsieur">Un homme</label>
			            <input <?php if($_SESSION['profil_civilite'] == 'Femme'){ echo'checked'; } ?> name="profil_civilite" id="civilMadame" value="Femme" type="radio"><label for="civilMadame">Une femme</label>
			          </div> 
			          <div class="news-input required">Champs obligatoires*</div>
			          <div class="news-input">
			         	<label for='profil_prenom'>Votre prénom*</label>
			            <input name='profil_prenom' value='<?php echo $_SESSION['profil_prenom']; ?>'>
			          </div>
			          <div class="news-input">
			          	<label for='profil_nom'>Votre nom*</label>
			            <input name='profil_nom' value='<?php echo $_SESSION['profil_nom']; ?>'>
			          </div>
			          <div class="news-input">
			          	<label for='profil_email'>Votre e-mail*</label>
			            <input name='profil_email' value='<?php echo $_SESSION['profil_email']; ?>' required>
			          </div>
			          <div class="select-date news-input">
			     		  <p>Date: <input name="profil_anniversaire"  value='<?php echo $_SESSION['profil_anniversaire']; ?>' type="text" id="datepicker"></p>
			          </div>
			          <div class="news-input">
			          	<label for='profil_gsm'>Votre GSM*</label>
			            <input name='profil_gsm' value='<?php echo $_SESSION['profil_gsm']; ?>' title="Portable" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$">
			          </div>
	
			         <div id="locationField">
      					<input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" type="text"></input>
      				 </div>

			          <div class="news-input">
			          	<label for='profil_adresseStreet'>Votre rue*</label>
			            <input id="route" name='profil_adresseStreet' value='<?php echo htmlspecialchars($_SESSION['profil_adresseStreet'], ENT_QUOTES);  ?>'>
			          </div>

			          <div class="news-input">
			          	<label for='profil_addressNumber'>Le numéro de votre rue*</label>
			            <input id="street_number" name='profil_addressNumber' value='<?php echo $_SESSION['profil_addressNumber'];  ?>'>
			          </div>

			          <div class="news-input">
			          	<label for='profil_addressPostalCode'>Votre code postal*</label>
			            <input id="postal_code" name='profil_addressPostalCode'  value='<?php echo $_SESSION['profil_addressPostalCode']; ?>'>
			          </div>

			          <div class="news-input">
			          	<label for='profil_addressLocality'>Votre Ville*</label>
			            <input id="locality"  name='profil_addressLocality' value='<?php echo htmlspecialchars($_SESSION["profil_addressLocality"], ENT_QUOTES); ?>' >
			          </div>
			        
			          <input type='hidden' name='attributes[10][value]' value='fr'>

			          <div class="news-input-full">
			          	<label class="display-block">
			            	<input type='checkbox' name='subscriptions[]' value='ELLE Partenaire'<?php if(in_array( 'ELLE Partenaire',$_SESSION['profil_subscriptions'])){echo 'checked';}?>/> Je souhaite bénéficier des cadeaux, bons de réduction et offres promotionnelles des partenaires du ELLE.be
			            </label>
				      </div>
				        <input type='submit' name="btnSubmitProfil" value="Envoyer"/>
				        <?php if(isset($_POST)  && !empty($_POST)) {echo "<h2 class='entry-title'>Merci d'avoir complété votre profil ELLE.be !</h2>";}?>
			         </form>
				</div>
				<footer class="entry-footer"></footer>
			</div>
	</div>
  </main>
  <script>
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key= AIzaSyAb2dpyArpaOBt649ccFa_T7vSTLEYQvn0&libraries=places&callback=initAutocomplete" async defer></script>
<?php get_footer(); ?>