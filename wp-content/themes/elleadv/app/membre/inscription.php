<?php
/*
Template Name: Inscription
*/
?>
<?php get_header(); ?>
<?php
if (!empty($_POST)){
	$member = new Member;
	$member->set_membre();
	if($member->if_exist_membre()){
		$member->create_member();
		$member->auth_membre();
		//$member->synchro_actito();
		//var_dump($member);
	}
}
?>
<main role="main main-page">
	<!-- site-inner -->
	<div class="site-inner">
		<div class="content">
			<form method="POST" role="form" id="profil"> 
					<div class="news-input">
					<label for='profil_prenom'>Votre prénom*</label>
					<input name='profil_prenom' value='Nicolas' required>
					</div>

					<div class="news-input">
					<label for='profil_nom'>Votre nom*</label>
					<input name='profil_nom' value='Feuillit' required>
					</div>

					<div class="news-input">
					<label for='profil_email'>Votre e-mail*</label>
					<input name='profil_email' value='nfe@editionventures.be' required>
					</div>

					<div class="news-input">
					<label for='profil_mdp'>Mot de passe*</label>
					<input type='password' name='profil_mdp' value='soumsoum' required>
					</div>

					<input type='submit' value="Inscription"/>
				</form>
				<footer class="entry-footer"></footer>
			</div>
	</div>
</main>
<?php get_footer(); ?>
