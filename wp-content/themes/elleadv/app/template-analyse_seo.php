<?php 
/*
Template Name: Analyse SEO
*/




class Article // Présence du mot-clé class suivi du nom de la classe.
  {
    public $_articles = array();      
    public $_id = '';
    public $_title = '';
    public $_content = '';
    public $_word_count = 0;
    public $_link_count = 0;
    public $_link_external_count = 0;
    public $_alt_image_checker = 0;
    public $_title_checker_h1 = 0;
    public $_title_checker_h2 = 0;
    public $_title_checker_h3 = 0;
    public $_yoast_keywork_count = 0;
    public $_yoast_keywork = 0;
    public $_yoast_keywork_count_title = 0;
    public $_is_a_question = 0;
    public $_score = 0;

    public function set_article($post){
    $this->_id = $post->ID;
	$this->_content = $post->ID;
	$this->_title = $post->post_title;
	
    }

}

$author_id = get_current_user_id();
if($_GET['author_id']) {
	$author_id = $_GET['author_id'];
}

$score = 0;
global $post;

function substri_count($haystack, $needle)
{
    return substr_count(strtolower($haystack), strtolower($needle));
}

function word_count($content = '') {
    $word_count = str_word_count( strip_tags( $content ) );
    return $word_count;
}

function link_count($content = '') {
    $link_count = substri_count($content,'<a href="https://www.elle.be');
    $link_count += substri_count($content,'<a href="http://www.elle.be');
    return $link_count;
}

function link_external_count($content = '') {
    $link_count = substri_count($content,'<a href="https://www.elle.be');
    $link_count += substri_count($content,'<a href="http://www.elle.be');
    $link_external_count = substri_count($content,'<a href="');
    $link_external_count = $link_external_count - $link_count;
    return $link_external_count;
}

function alt_image_checker($content = '') {
	$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
	$dom = new DOMDocument();
  	@$dom->loadHTML($content);
	$images = $dom->getElementsByTagName("img");
	$checker = 'Aucune image';
	if(count($images) > 0 ){
		foreach ($images as $image) {
		    if (!$image->hasAttribute("alt") || $image->getAttribute("alt") == "") {
		        $checker = 'Automatique';
		        return $checker;
		        break;
		    }else{}
		}
	}else{}
}

function formatbytes($file)
{
   $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
   if($filesize <= 0){
      return $filesize = 'unknown file size';}
   else{return round($filesize, 2).$type;}
}

function weight_image_checker($content = '') {
	$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
	$dom = new DOMDocument();
  	@$dom->loadHTML($content);
	$images = $dom->getElementsByTagName("img");
	$weight = 0;
	if(count($images) > 0 ){
		foreach ($images as $image) {
			$img = get_headers($image->getAttribute("src"), 1);
			if(isset($img["Content-Length"])){
				$weight += $img["Content-Length"]* .0009765625 * .0009765625;
			}
		}
		return $weight;
	}else{}
}

function title_checker_h1($content = ''){
	  $h1 = substri_count($content,'<h1>');
	  return $h1;
}

function title_checker_h2($content = ''){
	  $h2 = substri_count($content,'<h2>');
	  return $h2;
}

function title_checker_h3($content = ''){
	  $h3 = substri_count($content,'<h3>');
	  return $h3;
}


function yoast_keywork($post_id = ''){
	 global $post;
	 $yoast_title = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
	 if(isset($yoast_title) && $yoast_title != ''){ $post->score = 10; }
	 return $yoast_title;
}

function yoast_keywork_count_title($post_id = ''){
	 $yoast_title = get_post_meta($post_id, '_yoast_wpseo_title', true);
	 if($yoast_title == '' || $yoast_title == '%%title%% %%page%% %%sep%% %%sitename%%'){$yoast_title = get_the_title($post_id); }
	 $yoast_kw = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
	 if($yoast_title != '' && $yoast_kw != ''){
		 $keyword_count = substri_count($yoast_title,$yoast_kw);
		 //echo '<script>console.log("'.$yoast_title.' : '.$yoast_kw.'");</script>';
	 	return $keyword_count;
	}else{
		return '';
	}
}

function yoast_keywork_count($post_id = '',$content = ''){
	 $yoast_kw = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
	 if($yoast_kw != ''){
		 $keyword_count = substri_count($content,$yoast_kw);
	 	return $keyword_count;
	}else{
		return '';
	}
}

function get_pv_lastyear($post_id = ''){
	 $pv_lastyear = get_post_meta($post_id, 'pages_vues_1_an', true);
	 if($pv_lastyear != ''){
	 	return $pv_lastyear;
	}else{
		return '0';
	}
}

function get_author($post){
	return get_the_author_meta('display_name');
}


function display_score($score){
	return $score;
}

function is_a_question($post){
	$question_count = substri_count($post->post_title,'?');
    return $question_count;
}

function link_for_ga($post){
	$permalink =  get_the_permalink();
	$permalink = str_replace("https://www.elle.be/fr/", "",$permalink);
	$permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71942965p74244334/_u.date00=20170814&_u.date01=20180820&_.useg=builtin1&explorer-table.filter='.$permalink.'&explorer-table.plotKeys=%5B%5D';
	/*$permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/trafficsources-all-traffic/a42249957w71942965p74244334/_u.date00=20170814&_u.date01=20180820&_.useg=builtin1&explorer-table.plotKeys=%5B%5D&explorer-table.secSegmentId=analytics.pagePath&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22PT%22,'.$permalink.',0%5D%5D&_r.drilldown=analytics.sourceMedium:google%20~2F%20organic/';*/
    return $permalink_ga;
}

function display_date_crea(){
	if(get_field('date_de_creation')){
		$date_creation = substr(get_field('date_de_creation'),6,2).'/'.substr(get_field('date_de_creation'),4,2).'/'.substr(get_field('date_de_creation'),0,4);
	}else{
		$date_creation = get_the_date('d/m/Y');
	}
	return $date_creation;
}

$arg = array(
'posts_per_page' => 20000,
'author'        =>  $author_id,
'post_status' => array('publish','draft', 'pending', 'future', 'private', 'inherit') ,
'year'  => '2018',   
//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
'orderby' => 'ID',
);

if($_GET['dev'] == 1) {
$arg = array(
'posts_per_page' => 10000,
//'author'        =>  $author_id,
'post_status' => array('publish') ,
'year'  => '2018',   
//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
);
}

if($_GET['dev'] == 3) {
$arg = array(
'posts_per_page' => 10000,
//'author'        =>  $author_id,
'post_status' => array('publish') ,
'year'  => '2013',  
    'meta_query' => array(
        array(
            'key' => 'pages_vues_1_an',
           	'compare' => 'NOT EXISTS' // this should work...
        )
    ),


//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
);
}

if($_GET['dev'] == 4) {
$arg = array(
'posts_per_page' => 300,
'author'        =>  $author_id,
'post_status' => array('publish') ,
'meta_key' => 'pages_vues_1_an', 'orderby' => 'meta_value_num', 'order' => 'DESC',

//'meta_key' => 'date_de_creation', 'orderby' => 'meta_value_num', 'order' => 'DESC',
);
}

$posts_popular = get_posts($arg);


/*
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
	$content = get_post_field( 'post_content', $post->ID );
	//$percent_wordcount = (word_count($content) / 600)*100;
	//echo $post->post_title.' : Mots '.word_count($content).' : '.$percent_wordcount.' %<br/>';
	//echo $post->post_title.' : Link  '.link_count($content).'<br/>';
	//alt_image_checker($content);
	//echo $post->post_title.' : Yoast  '.yoast_keywork($post->ID).'<br/>';
endforeach; wp_reset_postdata(); 

/*** DISPLAY FOR FUN ***/

?>
<h1>Analyse SEO <?php echo get_the_author_meta('display_name', $author_id ); ?> : <?php echo "Nombre d'article : ".count($posts_popular); ?></h1>
<table width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>Date</th>
			<th>Titre</th>
			<!--<th>Auteur</th>-->
			<th>KW Yoast</th>
			<th>KW Content</th>
			<th>KW Titre</th>
			<th>H1</th>
			<th>H2</th>
			<th>H3</th>
			<th>Alt images</th>
			<!-- <th>Weight images</th> -->
			<th>Nombre de mots</th>
			<th>Liens internes</th>
			<th>Liens externes</th>
			<th>?</th>
			<th>PV (1 an)</th>
			<th>Action</th>
			<th>Status</th>
			<!--<th>Score</th>-->
</tr>
</thead>
<tbody>
<?php
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
global $post;
	$content = get_post_field( 'post_content', $post->ID );
	$score = 0;
	?>
	<tr>
		<td><?php echo $post->ID ?></td>
		<td><?php echo display_date_crea();  ?></td>
		<td><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></td>
		<!--<td><?php echo get_author($post->ID); ?></td>-->
		<td <?php if(yoast_keywork($post->ID) == ''){ echo "style='background:#ff7f7f'"; } ?>><?php echo yoast_keywork($post->ID); ?></td>
		<td <?php if(yoast_keywork_count($post->ID,$content) < 1){ echo "style='background:#ff7f7f'"; } ?>><?php echo yoast_keywork_count($post->ID,$content); ?></td>
		<td <?php if(yoast_keywork_count_title($post->ID) < 1){ echo "style='background:#ff7f7f'"; } ?>><?php echo yoast_keywork_count_title($post->ID); ?></td>
		<td><?php echo title_checker_h1($content); ?></td>
		<td><?php echo title_checker_h2($content); ?></td>
		<td><?php echo title_checker_h3($content); ?></td>
		<td <?php if(alt_image_checker($content) == "Non"){ echo "style='background:#ff7f7f'"; } ?>><?php echo alt_image_checker($content); ?></td>
		<!-- <td>0</td> -->
		<td <?php if(word_count($content) < 300){ echo "style='background:#ff7f7f'"; } ?>><?php echo word_count($content); ?></td>
		<td <?php if(link_count($content) < 2){ echo "style='background:#ff7f7f'"; } ?>><?php echo link_count($content); ?></td>
		<td <?php if(link_external_count($content) < 1){ echo "style='background:#ff7f7f'"; } ?>><?php echo link_external_count($content); ?></td>
		<td><?php echo is_a_question($post); ?></td>
		<td <?php if(get_pv_lastyear($post->ID) == 0){ echo "style='background:#ff7f7f'"; } ?>><?php echo get_pv_lastyear($post->ID); ?></td>
		<td><a target="_blank" href="<?php echo link_for_ga($post); ?>">GA</a></td>
		<td>
		<select>
		  <option value=""> - </option>
		  <option value="volvo">Mettre à jour</option>
		  <option value="saab">Supprimer</option>
		  <option value="mercedes">Désindexer</option>
		</select>
		</td>
		<!--<td><?php echo $post->score; ?></td>-->
	</tr>
<?php
endforeach; wp_reset_postdata(); 
?>
</tbody>
</table>


<style>
	table {
	    border-collapse: collapse;
	}

	table, th, td {
	    border: 1px solid black;
	    padding: 0 5px;
	}

	tr:hover {
		background: #d3d3d3;
	}
</style>
<?php
/*
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
echo yoast_keywork($post->ID).'<br/>';
endforeach; wp_reset_postdata(); 
*/