<?php
/*
Template Name: Indexation des tags
 */

?>
<?php get_header(); ?>
<?php $excludeIDS[] = get_the_ID(); ?> 
<?php

function sortOrder($a, $b) {
    if(strtolower($a->name) == strtolower($b->name)){ return 0 ; }
    return (strtolower($a->name) < strtolower($b->name)) ? -1 : 1;
}

function pagination($number_articles = '',$link_tag = ''){
    $nb_pages_max =  intval($number_articles / 100);
    if($nb_pages_max > 1){
        for ($i=1; $i <= $nb_pages_max + 1 ; $i++)
        {
               echo "<a href='".$link_tag."/page/".$i."'>".$i."</a>";
        }
    }
}

$tags = get_tags();
$categories = get_categories();
usort($tags, 'sortOrder');
usort($categories, 'sortOrder');

?>

<main role="main" class="main main-single">
  <div class="single">
    <!-- site-inner -->
    <div class="site-inner single-wrapper">
        <h2>Catégorie</h2>
        <ul>
        <?php

        foreach($categories as $categorie) {
            echo "<li style='font-size:16px'><a href=\""
                        .get_category_link($categorie->term_id)."\">"
                        .ucwords($categorie->name)
                        ."</a>(".$categorie->count." articles)";  
            pagination($categorie->count,get_tag_link($categorie->term_id));  
            echo "</li>";
            }
        ?>
        </ul>
        
		<h2>Etiquettes</h2>
		<ul>
        <?php

        foreach($tags as $tag) {
            echo "<li style='font-size:16px'><a href=\""
                        .get_tag_link($tag->term_id)."\">"
                        .ucwords($tag->name)
                        ."</a>(".$tag->count." articles)";  
            pagination($tag->count,get_tag_link($tag->term_id));  
            echo "</li>";
            }
        ?>

		</ul>
    </div>
  </div>
</main>
<?php get_footer(); ?>
