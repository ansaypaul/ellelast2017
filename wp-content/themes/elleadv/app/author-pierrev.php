<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php 
global $id_author;
$id_author = 87;
?>
<main role="main" class="main main-author" data-author="<?php echo $id_author; ?>">
	<div class="site-inner">
		<?php 
			$user_email = get_the_author_meta( 'user_email' , $id_author);
			$user_title = get_the_author_meta( 'authortitle', $id_author); 
			$user_3questions = get_field('3questions', 'user_'.$id_author);
			$user_profilpicture = get_field('profil_pic', 'user_'.$id_author);
			$user_twitter = get_the_author_meta( 'twitter', $id_author); 
			$user_pinterest = get_the_author_meta( 'pinterest', $id_author); 
			$user_linkedin = get_the_author_meta( 'linkedin', $id_author); 
			$user_instagram = get_the_author_meta( 'instagram', $id_author); 
			$fname = get_the_author_meta('first_name', $id_author);
			$lname = get_the_author_meta('last_name', $id_author);
			$full_name = '';
			if( empty($fname)){
			    $full_name = $lname;
			} elseif( empty( $lname )){
			    $full_name = $fname;
			} else {
			    $full_name = "{$fname} {$lname}";
			}
		?>
		<div class="author vcard" vocab="http://schema.org/" typeof="Person">
			<div class="author-left">
				<div class="author-avatar" property="image">
					<img src="<?php echo $user_profilpicture; ?>" alt="ELLE.be - <?php echo $full_name; ?>">
				</div>
				<?php if ( $user_instagram != '' || $user_twitter != '' || $user_pinterest != '' || $user_linkedin != '' || $user_email != ''){ ?>
					<div class="author-social">
					  	<?php if ( $user_instagram != '' ){ ?>
							<a href="<?php echo $user_instagram; ?>" property="url" class="instagram sprite sprite-instagram" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_twitter != '' ){ ?>
							<a href="https://twitter.com/<?php echo $user_twitter; ?>" property="url" class="twitter sprite sprite-twitter" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_pinterest != '' ){ ?>
							<a href="<?php echo $user_pinterest; ?>" property="url" class="pinterest sprite sprite-pint" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_linkedin != '' ){ ?>
							<a href="<?php echo $user_linkedin; ?>" property="url" class="linkedin sprite  sprite-linkedin" target="_blank"></a>
						<?php } ?>
						<?php if ( $user_email != '' ){ ?>
							<a href="mailto:<?php echo $user_email; ?>" property="email" class="email sprite sprite-mail" target="_blank"></a>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<div class="author-right">
				<h1 property="author" typeof="Person">
					<a class="url fn n author-name" property="name" href="<?php echo esc_url( get_author_posts_url( $id_author ) ); ?>">
						<?php echo $full_name; ?>
					</a>é
				</h1>
				<span property="jobTitle"><?php echo $user_title; ?></span>
				<p><?php echo $user_3questions; ?></p>
			</div>
		</div>

		<div class="articles"> 
			<div class="content entry-content">
				<?php $posts = get_posts(array(
				'posts_per_page' => get_option( 'posts_per_page' ), 
				'post__not_in' => $excludeIDS,
				'paged' => $paged ,
				'meta_query' => array(
				       array(
				           'key' => 'credits_photographe',
				           'value' => 'Pierre Vachaudez',
				           'compare' => '=',
				       )
				   )

				)); ?> 

				<?php foreach ( $posts as $post ) : setup_postdata( $post ); the_post(); $current_post_id++; ?>
					
					<?php get_template_part('template-parts/general/article'); ?>

					<?php if ($current_post_id == 1){ ?> <?php } ?>
					<?php if ($current_post_id == 3){ ?> 
						<div class="most-popular-ajax"></div>
					<?php } ?> 

				<?php endforeach; wp_reset_postdata(); ?>
			</div>
			
			<!-- sidebar -->
			<aside class="sidebar sidebar-index" role="complementary">
				<div id="halfpage-1" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="homepage" data-position=""></div>
				<?php //get_template_part('template-parts/general/most-popular'); ?>
				<div id="halfpage-2" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
			</aside>

			<div class="navigation">
				<?php pressPagination('',3); ?>
			</div>
		</div>

		<div class="swiper-article-ajax"><span class="loading"><?php _e( 'Chargement en cours', 'html5blank' ); ?>...</span></div>
	</div>
</main>
<?php get_footer(); ?>
