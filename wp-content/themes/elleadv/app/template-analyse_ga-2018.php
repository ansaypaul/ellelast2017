<?php 
/*
Template Name: Analyse GA 2018
*/



$daydate_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y"));
$lastyear_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1);

$daydate = date('Ymd',$daydate_unix);
$lastyear_format = date('Ymd',$lastyear_unix);

class Article // Présence du mot-clé class suivi du nom de la classe.
  {
    public $_articles = array();      
    public $_id = '';
    public $_referal = '';
    public $_date = '';
    public $_date_update = '';
    public $_permalink = '';
    public $_title = '';
    public $_category = '';
    public $_author = '';
    public $_content = '';
    public $_word_count = 0;
    public $_link_count = 0;
    public $_link_external_count = 0;
    public $_alt_image_checker = 0;
    public $_title_checker_h1 = 0;
    public $_title_checker_h2 = 0;
    public $_title_checker_h3 = 0;
    public $_yoast_keywork_count = 0;
    public $_yoast_keywork = 0;
    public $_yoast_keywork_count_title = 0;
    public $_is_a_question = 0;
    public $_nb_session = 0;
    public $_nb_session_array = array();
    public $_score = 0;
    public $_link_ga = '';
    public $_link_edit = '';
    public $_link_delete = '';
    public $_statut = '';

    public $_impressions = '';
    public $_clics = '';
    public $_position = '';

    public function set_article($post){
    $this->_permalink_full = utf8_encode(get_permalink($this->_id));
    $this->_permalink_full = str_replace('%e2%80%89',' ',$this->_permalink_full);
    $this->_permalink_full = str_replace('__trashed','',$this->_permalink_full);

    $this->_permalink = permalink_for_ga(get_permalink($this->_id));
    $this->_permalink = str_replace('__trashed','',$this->_permalink);
    $this->_id = $post->ID;
    $this->_date = display_date_crea($post->ID);
    $this->_date_update = display_date_update($post->ID);
    $this->_content = $post->ID;
    $this->_category = get_category_main($post->ID);
    $this->_author = get_author($post->ID); 
    $this->_title = $post->post_title;
    $this->_content = get_post_field( 'post_content', $this->_id );
    $this->_word_count = word_count($this->_content);
    $this->_link_count = link_count($this->_content);
    $this->_alt_image_checker = alt_image_checker($this->_content);
    $this->_link_external_count = link_external_count($this->_content);
    $this->_title_checker_h1 = title_checker_h1($this->_content);
    $this->_title_checker_h2 = title_checker_h2($this->_content);
    $this->_title_checker_h3 = title_checker_h3($this->_content);
    $this->_yoast_keywork = yoast_keywork($this->_id);
    $this->_yoast_keywork_count = yoast_keywork_count($this->_id);
    $this->_yoast_keywork_count_title = yoast_keywork_count_title($this->_id);
    $this->_is_a_question = is_a_question($post);
    $this->_link_for_ga = link_for_ga($post);
    $this->_score = 0;
    }
}


$array_article = array();
$array_article_ajax = array();

$author_id = get_current_user_id();

$score = 0;
global $post;

function get_category_main($post_id = ''){
  $list_categorie = get_the_category($post_id); 
  if(isset($list_categorie)){
    $main_categorie = $list_categorie[0];
    return $main_categorie->name;
  }
}

function substri_count($haystack, $needle)
{
    return substr_count(strtolower($haystack), strtolower($needle));
}

function word_count($content = '') {
    $word_count = str_word_count( strip_tags( $content ) );
    return $word_count;
}

function link_count($content = '') {
    $link_count = substri_count($content,'<a href="https://www.elle.be');
    $link_count += substri_count($content,'<a href="http://www.elle.be');
    return $link_count;
}

function link_external_count($content = '') {
    $link_count = substri_count($content,'<a href="https://www.elle.be');
    $link_count += substri_count($content,'<a href="http://www.elle.be');
    $link_external_count = substri_count($content,'<a href="');
    $link_external_count = $link_external_count - $link_count;
    return $link_external_count;
}

function alt_image_checker($content = '') {
  $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
  $dom = new DOMDocument();
    @$dom->loadHTML($content);
  $images = $dom->getElementsByTagName("img");
  $checker = 'Aucune image';
  if(count($images) > 0 ){
    foreach ($images as $image) {
        if (!$image->hasAttribute("alt") || $image->getAttribute("alt") == "") {
            $checker = 'Automatique';
            return $checker;
            break;
        }else{}
    }
  }else{}
}

function formatbytes($file)
{
   $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
   if($filesize <= 0){
      return $filesize = 'unknown file size';}
   else{return round($filesize, 2).$type;}
}

function weight_image_checker($content = '') {
  $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
  $dom = new DOMDocument();
    @$dom->loadHTML($content);
  $images = $dom->getElementsByTagName("img");
  $weight = 0;
  if(count($images) > 0 ){
    foreach ($images as $image) {
      $img = get_headers($image->getAttribute("src"), 1);
      if(isset($img["Content-Length"])){
        $weight += $img["Content-Length"]* .0009765625 * .0009765625;
      }
    }
    return $weight;
  }else{}
}

function title_checker_h1($content = ''){
    $h1 = substri_count($content,'</h1>');
    if($h1 == 0){$h1 = 1;}else{$h1 = 'error';}
    return $h1;
}

function title_checker_h2($content = ''){
    $h2 = substri_count($content,'</h2>');
    return $h2;
}

function title_checker_h3($content = ''){
    $h3 = substri_count($content,'</h3>');
    return $h3;
}


function yoast_keywork($post_id = ''){
   global $post;
   $yoast_title = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
   if(isset($yoast_title) && $yoast_title != ''){ $post->score = 10; }
   return $yoast_title;
}

function yoast_keywork_count_title($post_id = ''){
   $yoast_title = get_post_meta($post_id, '_yoast_wpseo_title', true);
   if($yoast_title == '' || $yoast_title == '%%title%% %%page%% %%sep%% %%sitename%%'){$yoast_title = get_the_title($post_id); }
   $yoast_kw = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
   if($yoast_title != '' && $yoast_kw != ''){
     $keyword_count = substri_count($yoast_title,$yoast_kw);
     //echo '<script>console.log("'.$yoast_title.' : '.$yoast_kw.'");</script>';
    return $keyword_count;
  }else{
    return '';
  }
}

function yoast_keywork_count($post_id = '',$content = ''){
   $yoast_kw = get_post_meta($post_id, '_yoast_wpseo_focuskw', true);
   if($yoast_kw != ''){
     $keyword_count = substri_count($content,$yoast_kw);
    return $keyword_count;
  }else{
    return '';
  }
}

function get_author($post){
  return get_the_author_meta('display_name');
}

function display_score($score){
  return $score;
}

function is_a_question($post){
  $question_count = substri_count($post->post_title,'?');
    return $question_count;
}

function permalink_for_ga($link = ''){
  $url_format = str_replace('https://www.elle.be', '', $link);
  return $url_format;
}

function display_date_crea($post_id){
  if(get_field('date_de_creation',$post_id)){
    $date_creation = substr(get_field('date_de_creation',$post_id),6,2).'/'.substr(get_field('date_de_creation',$post_id),4,2).'/'.substr(get_field('date_de_creation',$post_id),0,4);
  }else{
    $date_creation = get_the_date('d/m/Y',$post_id);
  }
  return $date_creation;
}

function display_date_update($post_id){
  $date_creation = get_the_modified_date('d/m/Y',$post_id);
  return $date_creation;
}

function link_for_ga($post){

  $daydate_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y"));
  $lastyear_unix = mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1);

  $daydate = date('Ymd',$daydate_unix);
  $lastyear_format = date('Ymd',$lastyear_unix);

  $permalink =  get_the_ID();
  $permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71942965p74244334/_u.date00='.$lastyear_format.'&_u.date01='.$daydate.'&_.useg=builtin1&explorer-table.filter='.$permalink.'&explorer-table.plotKeys=%5B%5D&explorer-table.secSegmentId=analytics.sourceMedium&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22RE%22,'.$permalink.',0%5D%5D/';
 
  $pos = strpos($_SERVER['REQUEST_URI'], 'fr/');
  if ($pos !== false) {
    $permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71953665p74246955/_u.date00='.$lastyear_format.'&_u.date01='.$daydate.'&_.useg=builtin1&explorer-table.plotKeys=%5B%5D&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22PT%22,%22~2Ffr~2F'.$permalink.'-%22,0%5D%5D/';
  }else{
    $permalink_ga = 'https://analytics.google.com/analytics/web/?authuser=1#/report/content-pages/a42249957w71953665p74246955/_u.date00='.$lastyear_format.'&_u.date01='.$daydate.'&_.useg=builtin1&explorer-table.plotKeys=%5B%5D&explorer-table.advFilter=%5B%5B0,%22analytics.pagePath%22,%22PT%22,%22~2Fnl~2F'.$permalink.'-%22,0%5D%5D/';
  }

    return $permalink_ga;
}


function get_pv_lastyear($post_id = ''){
   $pv_lastyear = get_post_meta($post_id, 'sessions_1_an', true);
   if($pv_lastyear != ''){
    return $pv_lastyear;
  }else{
    return '0';
  }
}

function get_pv_lastyear_totale($post_id = ''){
   $pv_lastyear = get_post_meta($post_id, 'sessions_1_an_totale', true);
   if($pv_lastyear != ''){
    return $pv_lastyear;
  }else{
    return '0';
  }
}



$arg = array(
'posts_per_page' => 3000,
'post_status' => 'publish',
'author' => '-47',
//'post__in' => array(249478),
'year' => $_GET['time'],
's' => $_GET['search'],
);


/*

$arg = array(
'posts_per_page' => 5000,
'post_status' => 'publish',
//'post__in' => array(249478),
);


*/

/*
$arg = array(
'posts_per_page' => 3000,
'post_status' => 'publish',
//'post__in' => array(249478),
   'date_query' => array(
        array(
            'year'  => '2019',
            'month' => '6'
        ),
    ),
'author'=> 77,
);
*/

if($_GET['status'] == 'delete'){
  $arg = array(
  'posts_per_page' => 3000,
  'post_status' => 'trash',
  //'post__in' => array(249478),
  'year' => $_GET['time'],
  );
}
 
$posts_popular = get_posts($arg);
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
  $article = new Article();
  $article->set_article($post);

  $moyenne_nombre_mot += $article->_word_count;

  $array_article[] = $article;
endforeach; wp_reset_postdata(); 

$moyenne_nombre_mot = $moyenne_nombre_mot / count($posts_popular);
/*
echo '<pre>';
  var_dump($array_article);
echo '<pre>';
*/

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Hello Analytics Reporting API V4</title>
  <meta name="google-signin-client_id" content="1066911604673-2usen9clcj40ujpanh33f4gahuqfm3ud.apps.googleusercontent.com">
  <meta name="google-signin-scope" content="https://www.googleapis.com/auth/analytics.readonly https://www.googleapis.com/auth/webmasters https://www.googleapis.com/auth/webmasters.readonly">
  <script src="https://apis.google.com/js/api.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
  </script>
  <script type="text/javascript">
    var array_article = <?php echo json_encode($array_article); ?>;
    console.log(array_article);
  </script>
  <style>
  #liste_delete
  {
    width: 1000px;float: right;
  }
   #liste_keywords
  {
    width: 1000px;float: right;
  }
  </style>
</head>
<body>
<!-- The Sign-in button. This will run `queryReports()` on success. -->

<p class="g-signin2"></p>
<!--
<button onclick="loadClient_only();">GO CLIENT ONLY</button>
<button onclick="analyse_gsc_key();">TEST KEYWORD ONLY</button>
-->
<button onclick="loadClient();">GO</button>
<button class="generate_table" onclick="generate_table();" disabled >Generate_table</button>

<br/>

<textarea id="liste_delete" class="liste_delete"></textarea>
<textarea id="liste_keywords" class="liste_keywords"></textarea>

<label for="year">Post Year:</label> 
<select name="year">
  <option value="">-</option>
  <option value="2018">2018</option>
  <option value="2017">2017</option>
  <option value="2016">2016</option>
  <option value="2015">2015</option>
  <option value="2014">2014</option>
  <option value="2013">2013</option>
</select>

<h1>Analyse SEO : <?php echo "Nombre d'article : ".count($posts_popular); ?></h1>
<span>Nombre moyen de mot : <?php echo $moyenne_nombre_mot; ?></span><br/>


<strong>Nombre de mot : </strong><input type="text" id="min" name="max"><label> - </label><input type="text" id="max" name="max"><br/>
<strong>Nombre de vue unique : </strong><input type="text" id="min_vue" name="max_vue"><label> - </label><input type="text" id="max_vue" name="max">

<table id="myTable" width="98%">
  <thead>
    <tr>
      <th>ID Article</th>
      <th>Date</th>
      <th>Date mis à jour</th>
      <th>Titre</th>
      <th>Catégorie</th>
      <th>Auteur</th>
      <th>KW Yoast</th>
      <th>KW Content</th>
      <th>KW Titre</th>
      <th>H1</th>
      <th>H2</th>
      <th>H3</th>
      <th>Alt images</th>
      <!-- <th>Weight images</th> -->
      <th>Nombre de mots</th>
      <th>Liens internes</th>
      <th>Liens externes</th>
      <th>?</th>
      <th>Direct</th>
      <th>Google</th>
      <!--<th>Facebook</th>
      <th>Instagram</th>
      <th>Stories</th>
      <th>Pinterest</th>
      <th>Email</th>-->
      <th>Impressions</th>
      <th>Clics</th>
      <th>Position</th>
      <th>Link GA</th>
      <!--
      <th>Status</th>
      <th>Edit</th>
      <th>Delete</th>
      -->
      <!--<th>Score</th>-->
</tr>
</thead>
<tbody id="tbody">

</tbody>
</table>

<?php 
/*
foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
global $post;
the_permalink();
echo '<br/>';
endforeach; wp_reset_postdata();
*/ 
?>

<table id="table1000">

</table>

<script>

    var path_windows = window.location.href;
    var arr = path_windows.split("/");
    var elle_path = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];

    /* <![CDATA[ */
    var ajax_search = {"ajaxurl": elle_path + "/wp-admin/admin-ajax.php"};
    /* ]]> */

    if(path_windows.indexOf('/fr/') > 0){
      lang = '/fr/';
        var VIEW_ID = '74244334';
    }else{
      lang = '/nl/';
        var VIEW_ID = '74251911';
    }

  console.log('Langue Site : ' + lang);

  function generate_table(){
        generate_array(array_article);
        generate_data_table();
        delete_post_action();
        init_analyse_gsc_key();
  }

  function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }

  function check_http_header(url_parse){
      return $.ajax(url_parse, {
         type:  "GET",
         async: false,
         data: "",
         statusCode: {
            200: function (response) {
               return '200';
            },
            201: function (response) {
               return '201';
            },
            400: function (response) {
               return '400';
            },
            404: function (response) {
               return '404';
            },
            410: function (response) {
               return '410';
            }
         }, success: function () {
          return 1;
         },
      });
  }


    function analyse_array(a,b){
      console.log('GO analyse_array V2');
      for (var i = 0, len = a.length; i < len; i++) { 
      	  //console.log('B:' + a[i]._permalink);
      	  //var b_length = Object.keys(array_article_ajax).length;
          for (var key in b) {
              if (a[i]._permalink == b[key]._permalink) {

              	//var array_source = [];
              	//for (var z = 0, len = b[key].length; z < len; z++) { 
              		//console.log(b[key][z]['_referal']);
              		//console.log(b[key][z]['_nb_session']);
              		//key_ref = b[key][i]['_referal'];
              		//value_ref = b[key][i]['_nb_session'];
              		//array_source[key_ref] = value_ref;       
              	//}
              	
              	//console.log(array_source);
				//console.log(b[key]);

                a[i]._nb_session_array = b[key]['sources'];
                console.log(b[key]['sources']);
               }
          }
      }
      console.log(a);
      alert("ANALYTICS");
    }

     function analyse_array_gsc(a,b){
      console.log('GO analyse_array_gsc');
      for (var i = 0, len = a.length; i < len; i++) { 
          for (var j = 0, len2 = b.length; j < len2; j++) { 
              if (a[i]._permalink_full == b[j]._permalink_full) {
                a[i]._impressions = b[j]._impressions;
                  a[i]._clicks = b[j]._clicks;
                  a[i]._position = b[j]._position;
              }

          }
      }
      console.log(a);
      alert("GSC");
    }

  var array_article_ajax = [];
  var array_article_gsc = [];

   function loadClient_only() {
    gapi.client.setApiKey("AIzaSyBrE_aY0EbNY_X8b4Hntu5gUvl6UpoHJJc");
    return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/webmasters/v3/rest")
        .then(function() { console.log("GAPI client loaded for API"); },
              function(err) { console.error("Error loading GAPI client for API", err); });
  }

   function loadClient() {
    gapi.client.setApiKey("AIzaSyBrE_aY0EbNY_X8b4Hntu5gUvl6UpoHJJc");
    return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/webmasters/v3/rest")
        .then(function() { console.log("GAPI client loaded for API"); tracking_1000('0'); analyse_gsc(); },
              function(err) { console.error("Error loading GAPI client for API", err); });
  }


  function formatDate(date) {
      var d = new Date(date),
          month = '' + d.getMonth(),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
  }


   function analyse_gsc() {

      var d_last_year = new Date();
      var pastYear = d_last_year.getFullYear() - 1;
      d_last_year.setFullYear(pastYear);
      var date_last_year = formatDate(d_last_year);

      var d_current_year = new Date();
      d_current_year.setFullYear(d_current_year);
      var date_current_year = formatDate(d_current_year);



      return gapi.client.webmasters.searchanalytics.query({

        "siteUrl": "https://www.elle.be",
        "resource": {
        "startDate": "2019-02-19",
        "endDate": "2020-02-19",
        "dimensions": ["page"],
        "searchType": "web",
        "aggregationType": "byPage",
        "rowLimit":25000
        }
      })

      .then(function(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
      var json = JSON.parse(formattedJson);
      console.log(json);
      for (var i = 0, len = json['rows'].length; i < len; i++) {
        var article = new Object();
        article._permalink_full = json['rows'][i]['keys'][0];
	      article._impressions = json['rows'][i]['impressions'];
	      article._clicks = json['rows'][i]['clicks'];
	      article._position = parseInt(json['rows'][i]['position']);
	      array_article_gsc.push(article);
    }

     analyse_array_gsc(array_article,array_article_gsc);

     $('.generate_table').removeAttr("disabled");
      },
       function(err) { console.error("Execute error", err); });
  }

     function analyse_gsc_key(url_to_parse) {
      return gapi.client.webmasters.searchanalytics.query({
      "siteUrl": "https://www.elle.be",
      "resource": {
        "startDate": "2019-02-19",
        "endDate": "2020-02-19",
       "dimensionFilterGroups": [
        {
         "filters": [
          {
           "expression": url_to_parse,
           "dimension": "page",
           "operator": "equals"
          }
         ]
        }
       ],
       "dimensions": [
        "query"
       ],
       "rowLimit": 25
      }
      })
      .then(function(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
      var json = JSON.parse(formattedJson);
        $("textarea#liste_keywords").val('');
      for (var i = 0, len = json['rows'].length; i < len; i++) {
        console.log(json['rows'][i]['keys'][0]);
        var txt = $("textarea#liste_keywords");
        txt.val( txt.val() + json['rows'][i]['keys'][0] + "\n");
      
      }

      },
       function(err) { console.error("Execute error", err); });
  }



  function display_tracking_1000(response){
    var formattedJson = JSON.stringify(response.result, null, 2);
    var json = JSON.parse(formattedJson);
    console.log(json);

    for (var i = 0, len = json['reports'][0]['data']['rows'].length; i < len; i++) {

      var url_link = json['reports'][0]['data']['rows'][i]['dimensions'][0];
      var url_referal = json['reports'][0]['data']['rows'][i]['dimensions'][1];
      var url_referal_sub = json['reports'][0]['data']['rows'][i]['dimensions'][2];
      var url_value = json['reports'][0]['data']['rows'][i]['metrics'][0]['values'][0];  

      var id_article = '';
      var url_code = '';
       
      var article = new Object();
      article._permalink = url_link;
      article._referal = url_referal;
      article._nb_session = url_value;

      if( array_article_ajax[url_link] === undefined ) {
      	array_article_ajax[url_link] = [];
      	array_article_ajax[url_link]['sources'] = [];
      	
      	/*
      	array_article_ajax[url_link]['sources']['Direct'] = 0;
      	array_article_ajax[url_link]['sources']['Organic Search'] = 0;
      	array_article_ajax[url_link]['sources']['Social'] = 0;
      	array_article_ajax[url_link]['sources']['Email'] = 0;
      	array_article_ajax[url_link]['sources']['Instagram'] = 0;
      	array_article_ajax[url_link]['sources']['Facebook'] = 0;
      	array_article_ajax[url_link]['sources']['Pinterest'] = 0;
      	*/
  	  }

  	  if(url_referal_sub != '(not set)'){ url_referal = url_referal_sub;}

  	  array_article_ajax[url_link]._permalink = url_link;
      array_article_ajax[url_link]['sources'][url_referal] = url_value;

    }

    if(json['reports'][0]['nextPageToken']){
        tracking_1000(json['reports'][0]['nextPageToken']);
    }else{
    	//console.log(array_article_ajax);
       	analyse_array(array_article,array_article_ajax);
        //array_article.sort(compare);
    }
  }

  function compare(a,b) {
    if (parseInt(a._nb_session) < parseInt(b._nb_session))
      return -1;
    if (parseInt(a._nb_session) > parseInt(b._nb_session))
      return 1;
    return 0;
  }



  function generate_data_table(){
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = parseInt( $('#min').val(), 10 );
            var max = parseInt( $('#max').val(), 10 );
            var age = parseFloat( data[12] ) || 0; // use data for the age column
     
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                 ( isNaN( min ) && age <= max ) ||
                 ( min <= age   && isNaN( max ) ) ||
                 ( min <= age   && age <= max ) )
            {
                return true;
            }
            return false;
        }
    );

    var table = $('#myTable').DataTable({
        dom: 'Bfrtip',
       buttons: [
          'copy', 'excel', 'pdf'
      ],
       paging: false,
          initComplete: function () {
                this.api().columns([4,5]).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
    });
     
      $('#min, #max').keyup( function() {
          table.draw();
      } );
  }

  function isUndefined(value){
      // Obtain `undefined` value that's
      // guaranteed to not have been re-assigned
      var undefined = void(0);
      if(value === undefined){ return 0; }else{ return value; }
  }

  function generate_array(array_article){
    for (var i = 0, len = array_article.length; i < len; i++) {

    if(isUndefined(array_article[i]._nb_session_array['Direct']) == 0){array_article[i]._nb_session_array['Direct'] = '0';}
    if(isUndefined(array_article[i]._nb_session_array['Organic Search']) == 0){array_article[i]._nb_session_array['Organic Search'] = '0';}
    if(isUndefined(array_article[i]._nb_session_array['Social']) == 0){array_article[i]._nb_session_array['Social'] = '0';}
    if(isUndefined(array_article[i]._nb_session_array['Email']) == 0){array_article[i]._nb_session_array['Email'] = '0';}
  	if(isUndefined(array_article[i]._nb_session_array['Pinterest']) == 0){array_article[i]._nb_session_array['Pinterest'] = '0';}
    if(isUndefined(array_article[i]._nb_session_array['Facebook']) == 0){array_article[i]._nb_session_array['Facebook'] = '0';}
    if(isUndefined(array_article[i]._nb_session_array['Instagram']) == 0){array_article[i]._nb_session_array['Instagram'] = '0';}
    if(isUndefined(array_article[i]._nb_session_array['Instagram Stories']) == 0){array_article[i]._nb_session_array['Instagram Stories'] = '0';}

      line_string = '<tr>'+
      '<td>' + array_article[i]._id + '</td>'+
      '<td>' + array_article[i]._date + '</td>'+
      '<td>' + array_article[i]._date_update + '</td>'+
      '<td><a href="'+array_article[i]._permalink_full+'">' + array_article[i]._title + '</a></td>'+
      '<td>' + array_article[i]._category + '</td>'+
      '<td>' + array_article[i]._author + '</td>'+
      '<td>' + array_article[i]._yoast_keywork + '</td>'+
      '<td>' + array_article[i]._yoast_keywork_count + '</td>'+
      '<td>' + array_article[i]._yoast_keywork_count_title + '</td>'+
      '<td>' + array_article[i]._title_checker_h1 + '</td>'+
      '<td>' + array_article[i]._title_checker_h2 + '</td>'+
      '<td>' + array_article[i]._title_checker_h3 + '</td>'+
      '<td>' + array_article[i]._alt_image_checker + '</td>'+
      '<td>' + array_article[i]._word_count + '</td>'+
      '<td>' + array_article[i]._link_count + '</td>'+
      '<td>' + array_article[i]._link_external_count + '</td>'+
      '<td>' + array_article[i]._is_a_question + '</td>'+
      '<td>' + array_article[i]._nb_session_array['Direct'] + '</td>'+
      '<td>' + array_article[i]._nb_session_array['Organic Search'] + '</td>'+
     /* '<td>' + array_article[i]._nb_session_array['Facebook'] + '</td>'+
      '<td>' + array_article[i]._nb_session_array['Instagram'] + '</td>'+
      '<td>' + array_article[i]._nb_session_array['Instagram Stories'] + '</td>'+
      '<td>' + array_article[i]._nb_session_array['Pinterest'] + '</td>'+
      '<td>' + array_article[i]._nb_session_array['Email'] + '</td>'+*/
      '<td>' + array_article[i]._impressions + '</td>'+
      '<td>' + isUndefined(array_article[i]._clicks) + '</td>'+
      '<td class="analyse_gsc_key" data-permalink_full="' + array_article[i]._permalink_full + '">' + array_article[i]._position + '</td>'+
      '<td><a target="_blank" href="'+ array_article[i]._link_for_ga + '">GA</a></td>'+
      //'<td>' + array_article[i]._statut + '</td>'+
      //'<td>' + array_article[i]._link_edit + '</td>'+
      //'<td class="delete_post" data-permalink="' + array_article[i]._permalink_full + '" data-idarticle="' + array_article[i]._id + '">Delete</td>'+
      '</tr>';

      $('#tbody').append(line_string);
    }
  }

    var pageToken = "0";

    function tracking_1000(pageToken){
        gapi.client.request({
          path: '/v4/reports:batchGet',
          root: 'https://analyticsreporting.googleapis.com/',
          method: 'POST',
          body: {
            reportRequests: [
              {
                viewId: VIEW_ID,
                dateRanges: [
                  {
                    startDate: '365daysAgo',
                    endDate: 'today'
                  }
                ],
                metrics: [
                  {expression: 'ga:uniquePageviews'},{expression: 'ga:sessions'}
                ],
                dimensions: [
                  {name: "ga:pagePath"},{name: "ga:channelGrouping"},{name: "ga:socialNetwork"}
                  /*
                  {
                    name: "ga:month"
                  }
                  */
                ],
              dimensionFilterClauses: [
                {
                    filters: [
                      {
                       "dimensionName": "ga:pagePath",
                       "operator": "ENDS_WITH",
                       "expressions": '.html'
                      }
                    ]
                },
              ],

              "orderBys": [
                {
                  "fieldName": "ga:pagePath",
                  "sortOrder": "ASCENDING"
                }
              ],
              "pageSize": "10000",
              "pageToken": pageToken,

              }
            ]
          }
        }).then(display_tracking_1000, console.error.bind(console));
    }

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = year + ' ' + month + ' ' + date ;
  return time;
}

function udapte_post_action(){
    $( ".select_status" ).change(function( index ) {
      var self = this;
      console.log($(this).val());

      var post_id = $( self ).data('idarticle');
      console.log('Upd status : ' + post_id);

      $.ajax({
        url: ajax_search.ajaxurl,
        type: 'post',
        data: { action: 'ajax_set_status', post_id: post_id, status_updated: $(this).val()},
        beforeSend: function() {
        
        },
        success: function( html ) {
          if(html != ''){
              console.log(html);
            }else{
              console.log(html);                
            }

            },
            error: function (xhr) {
              console.log('error'); 
              } 
        })

     });
  }

function init_analyse_gsc_key(){
  $( ".analyse_gsc_key" ).click(function( index ) {
      var self = this;
      var _permalink_full = $( self ).data('permalink_full');
      console.log('Keywork : ' + _permalink_full);
      analyse_gsc_key(_permalink_full);
      });
}

function delete_post_action(){
    $( ".delete_post" ).click(function( index ) {
      var self = this;
      var post_id = $( self ).data('idarticle');
      var permalink = $( self ).data('permalink');
      var permalink_trashed = $( self ).data('permalink');

      permalink_trashed = permalink_trashed.substring(0, permalink_trashed.length - 5);
      permalink_trashed = permalink_trashed + '__trashed.html';
      console.log('Delete : ' + post_id);
      $( self ).parent().css("background-color", "red");
      var txt = $("textarea#liste_delete");
      txt.val( txt.val() + permalink + "\n" + permalink_trashed + "\n");
      
      
      $.ajax({
        url: ajax_search.ajaxurl,
        type: 'post',
        data: { action: 'ajax_remove_post', post_id: post_id},
        beforeSend: function() {
        
        },
        success: function( html ) {
          if(html != ''){
              console.log(html);
            }else{
              console.log(html);                
            }

            },
            error: function (xhr) {
              console.log('error'); 
              } 
        })

      
     });      
  }

/*
function delete_post_action_all(){
  var delete_pass = '<?php echo $_GET['deletedpass']; ?>';
  if(delete_pass == 'tara1180'){
      $( ".delete_post" ).each(function( index ) {
      var self = this;
          var post_id = $( self ).data('idarticle');
          console.log('Delete : ' + post_id);

          $.ajax({
            async: false,
            url: ajax_search.ajaxurl,
            type: 'post',
            data: { action: 'ajax_remove_post', post_id: post_id},
            beforeSend: function() {
            
            },
            success: function( html ) {
              if(html != ''){
                  $( self ).parent().remove();
                  console.log(html);
                }else{
                  console.log(html);                
                }

                },
                error: function (xhr) {
                  console.log('error'); 
                  } 
            })
         });
  }else{
    console.log('ERROR');
  }
  }
  */

  $( "li" ).each(function( index ) {
  console.log( index + ": " + $( this ).text() );
});


$( document ).ready(function() {
    console.log( "ready!" );
});

</script>


<style>
  table {
      border-collapse: collapse;
  }

  table, th, td {
      border: 1px solid black;
      padding: 0 5px;
  }

  tr:hover {
    background: #d3d3d3;
  }

  * {font-size:14px};
</style>

<!-- Load the JavaScript API client and Sign-in library. -->
<script src="https://apis.google.com/js/client:platform.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
</body>
</html>