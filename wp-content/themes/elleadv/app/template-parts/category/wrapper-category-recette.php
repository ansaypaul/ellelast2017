<?php get_header(); ?>
	<main role="main" class="main main-category category-recherche category-recette">
		<div class="site-inner">
			<h1>
				<?php single_cat_title(); ?>
			</h1>

			<?php echo category_description(); ?>
			<?php $category = get_category( get_query_var( 'cat' ) ); $cat_id = $category->cat_ID; ?>
			<?php 
				$field_quoi = get_field_object('recipe-type'); $choices_quoi = $field_quoi['choices'];
				$field_boutique = get_field_object('recipe-difficulties'); $choices_boutique = $field_boutique['choices'];

				$quoi_init_value = __( 'Type de recette', 'html5blank' ).' ?';
				if(isset($_GET['type'])){ $quoi_init_value = $_GET['type']; }
			?>
			<?php 
			// if(get_field('variable_js','option')['lang_identifiant'] == "FR"){ ?>
			<form method="post" class="search-recherche search-adresse">
				<ul class="category-search-quoi cols-3">
					<li data-category="" class="search-cat-rubric search-cat-rubric-main search-cat-rubric-quoi cols-3"><?php echo $quoi_init_value; ?></li>
					<li data-category="" class="search-cat-rubric search-cat-rubric-adresses search-cat-rubric-quoi"><?php _e( 'Type de recette', 'html5blank' ); ?> ?</li>
					<?php foreach($choices_quoi as $choice) { ?>
						<li data-category="<?php echo $choice ?>" class="search-cat-rubric-adresses search-cat-rubric search-cat-rubric-quoi type-recette"><?php echo $choice ?></li>
					<?php } ?>
				</ul>

				<ul class="search-cat-rubric-style search-cat-rubric-boutique cols-3">
					<li data-category="" class="search-cat-rubric search-cat-rubric-main type-recette"><?php _e( 'Difficulté', 'html5blank' ); ?> ?</li>
					<li data-category="" class="search-cat-rubric search-cat-rubric-adresses search-cat-rubric-boutique"><?php _e( 'Difficulté', 'html5blank' ); ?> ?</li>
					<?php foreach($choices_boutique as $choice) { ?>
						<li data-category="<?php echo $choice ?>" class="search-cat-rubric-adresses search-cat-rubric search-cat-rubric-boutique type-recette"><?php echo $choice ?></li>
					<?php } ?>
				</ul>
			   	<p class="recherche_adresse-wrapper cols-3">
			       	<input id="input_recherche_adresse" type="text" name="input_recherche_adresse" placeholder="<?php _e('Recherche', 'html5blank' ); ?>">
			    </p>
			</form>
			<?php //} ?>
			<div class="articles">
				<div class="content entry-content">
					<div id="result_ajax_adresses"></div>
				</div>
				<aside class="sidebar sidebar-index" role="complementary">
					<div id="halfpage-1" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="homepage" data-position=""></div>
					<?php get_template_part('template-parts/general/most-popular'); ?>
					<div id="halfpage-2" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>		
				</aside>
			</div>
		</div>
	</main>

<?php get_footer(); ?>