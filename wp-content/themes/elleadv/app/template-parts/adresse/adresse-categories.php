<!-- EAT Catégories -->
<div class="eat-categories">
	<span class="eat-categories-title"><?php _e( 'Catégories', 'html5blank' ); ?></span>
	<ul class="eat-categories-items" >
		<li>
			<a href="#">Petit déjeuner</a>
		</li>
		<li>
			<a href="#">Cat 2</a>
		</li>
		<li>
			<a href="#">Cat 3</a>
		</li>
		<li>
			<a href="#">Cat 4</a>
		</li>
		<li>
			<a href="#">Cat 5</a>
		</li>
		<li>
			<a href="#">Cat 6</a>
		</li>
		<li>
			<a href="#">Cat 7</a>
		</li>
		<li>
			<a href="#">Cat 8</a>
		</li>
	</ul>
</div>
<!-- EAT Catégories -->