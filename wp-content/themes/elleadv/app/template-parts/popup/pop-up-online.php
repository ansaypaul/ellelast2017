<?php global $last_magazine; ?>
<div class="popup-container popup-online">
	<div class="pop-online">
		<div class="popup-close btn-close" id="btn-close-popup-online"></div>
		<div class="popup-left">
			<?php echo $last_magazine[0]->ID; ?>
			<p class="title-h3"><?php echo get_field( 'promo_digital_titre', $last_magazine[0]->ID); ?></p>
			<?php echo get_field('promo_digital_contenu', $last_magazine[0]->ID); ?>
			<!-- Ajouter link mag online ds options WP -->
			<a rel="nofollow" href="<?php echo get_field('promo_digital_lien', $last_magazine[0]->ID); ?>" class="cta" target="_blank"><?php echo get_field( 'promo_texte_avant_cta', $last_magazine[0]->ID); ?></a>
		</div>
		<div class="popup-right">
			<!-- Ajouter link mag online ds options WP -->
			<a rel="nofollow" href="<?php echo get_field('promo_digital_lien', $last_magazine[0]->ID); ?>" target="_blank">
				<?php 
					$img_html = "<img src='". get_the_post_thumbnail_url($last_magazine[0]->ID)."' alt='ELLE Cover' />";
					$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
					echo $img_html;
				?>
			</a>
		</div>
	</div>
</div>