<?php 
global $post;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
$sous_categorie = $list_categorie[0];
if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
	$main_categorie = $list_categorie[0];
	$sous_categorie = $list_categorie[0];
}

if(count($list_categorie) > 1 ){
	if(in_array($list_categorie[0]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[0];
		$sous_categorie = $list_categorie[1];
	}

	if(in_array($list_categorie[1]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[1];
		$sous_categorie = $list_categorie[0];
	}
}

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
if($sous_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
if($main_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
?>
<div class="main-content-wrapper diy-wrapper" data-io-article-url="<?php the_permalink(); ?>">
	<div class="content" data-tag="<?php echo $main_tag ?>" data-cat="<?php echo $sous_categorie->cat_ID; ?>">
	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class("h-entry category-recherche"); ?> data-id = "<?php the_ID(); ?>" >
		<div itemscope itemtype="http://schema.org/HowTo">
			<header class="entry-header">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/ellebelgique.png" alt="ELLE Logo" class="elle_logo only_print">
				<!-- post-category -->
				<div class="post-category">
					<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
						<?php echo $main_categorie->name; ?>
					</a>
					<?php if(isset($sous_categorie) && ($main_categorie->cat_ID != $sous_categorie->cat_ID)){ ?>
						<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
					<?php } ?>
				</div>
				<!-- post-title -->
				<h1 itemprop="name">
					<a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
				</h1>
				<!-- post-details -->
				<div class="post-details">
					<span class="date">	
						<?php $str_update =  __( 'Publié le', 'html5blank' ); ?>
						<?php if(get_the_modified_time('Y-m-j') !=  get_the_time('Y-m-j')){ $str_update = __( 'Mis à jour le', 'html5blank' ); } ?>
							<span class="dt-modified" property="dateModified" content="<?php the_modified_time('Y-m-j'); ?>"> <?php echo $str_update; ?> <?php the_modified_time('j F Y'); ?></span>
							<span class="dt-published hidden" property="datePublished" content="<?php the_time('Y-m-j'); ?>"> <?php the_time('l, j F Y'); ?>
							</span>
					</span>
					<a href="javascript:window.print()" id="print-button"><span>
					<?php _e( 'Imprimer la fiche DIY', 'html5blank' ); ?></span></a>
					<span class="author vcard p-author h-card" property="author" typeof="Person">	<?php _e( 'par', 'html5blank' ); ?>
				      	<span property="name" class="hidden"><?php the_author(); ?></span>
				      	<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author(); ?></a>
				      	<?php if( $authortitle = get_the_author_meta( 'authortitle' ) ) { ?>
							<span class="author-title hidden" property="jobTitle"><?php echo $authortitle; ?></span>
						<?php } ?>
						<?php $credits_auteur_initial = get_field( "credits_auteur_initial" );
						if ( $credits_auteur_initial ) { ?>
							<span class="credit-trad"> <?php _e( 'et', 'html5blank' ); ?> <?php echo $credits_auteur_initial; ?></span>
						<?php } ?>
						<?php $credits_link = get_field( "credits_link" );
						$credits_photographe = get_field( "credits_photographe" );
						if ( $credits_photographe ) { ?>
							<span class="credit-photo"><?php _e( 'Photos', 'html5blank' ); ?>: 
								<?php if ( $credits_link ) { ?>
									<a href="<?php echo $credits_link; ?>" target="_blank">
								<?php } ?>
								<?php echo $credits_photographe; ?></span>
								<?php if ( $credits_link ) { ?>
									</a>
								<?php } ?>
						<?php } ?>
						<div property="image" class="hidden"><?php echo get_avatar(get_the_author_meta( 'user_email'),220); ?></div>
					</span>
					<div property="publisher" typeof="Organization" class="hidden">
						<span property="name"><?php _e( 'ELLE Belgique', 'html5blank' ); ?></span>
						<div property="logo" typeof="ImageObject">
						    <link property="url" href="<?php echo home_url(); ?>/img/icons/ellebelgique.png" />
						    <meta property="height" content="125" />
						    <meta property="width" content="320" />
						</div>
					</div>
				</div>
				<div class="diy-informations">
					<?php 
					$diypreptime = explode(':',get_field( "diy-preptime" )); 
					$diypreptime_structure = "00";
					if(count($diypreptime) > 1){
						$diypreptime_heure = intval($diypreptime[0]);
						$diypreptime_min = intval($diypreptime[1]);
						$diypreptime_s = intval($diypreptime[2]);
						$diypreptime_structure = $diypreptime_heure * 60 + $diypreptime_min + ($diypreptime_s / 60); 
					}?>
					<?php $diyprice = get_field( "diy-price" ); ?>
					<?php $diydifficulties = get_field( "diy-difficulties" ); ?>
					<ul class="informations-list">
		     			<li>
		     				<div class="sprite sprite-prep"></div>
		     				<span itemprop="totalTime"><?php if(isset($diypreptime_heure) && $diypreptime_heure != '00') { echo $diypreptime_heure. ' h'; } ?> 
		     				<?php if(isset($diypreptime_min) && $diypreptime_min != '00') { echo $diypreptime_min. ' m'; } ?></span>
						</li>
		   				<li>
		   					<div class="sprite sprite-price"></div>
		   					<span itemprop="estimatedCost"><?php echo $diyprice; ?> €</span></li>
		   				<li>
		   					<div class="sprite sprite-diff-diy"></div>
		   					<span><?php echo $diydifficulties; ?></span>
		   				</li>
			   		</ul>
				</div>
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
						<?php the_post_thumbnail('content2018',array('',0,1,1)); ?>
					</a>
					<a href="<?php echo get_the_post_thumbnail_url();?>" property="image" class="hidden"></a>
				<?php endif; ?>
			</header>
			<div class="site-inner-ads no_print">
				<div id="gpt-ad-RECT_ABOVE" class="rmgAd center margin_bottom_20" data-device="desktop" data-type="RECT_ABOVE"></div>
			</div>
			<div class="entry-content">
				<?php $diyintro = get_field( "diy-intro" ); ?>
				<p class="content-intro" property="description"><?php echo $diyintro; ?></p>
			    	<div class="diy-materiel">
			    		<h2 itemprop="name">
			    			<?php _e( 'Matériel', 'html5blank' ); ?>
			    		</h2>
			    		<ul class='materiel-list'>
				    		<?php
							if( have_rows('diy-materiel') ):
							    while ( have_rows('diy-materiel') ) : the_row();
							    	echo "<li itemscope itemtype='http://schema.org/HowToItem'><span itemprop='name'>";
							        the_sub_field('materiel');
							        echo "</span></li>";
							    endwhile;
							endif; ?>
						</ul>
			    		
			    	</div>
			    	<div class="diy-instructions e-content" itemscope itemtype="http://schema.org/HowToDirection">
			    		<h2><?php _e( 'Préparation', 'html5blank' ); ?></h2>
						<?php echo get_content_without_first_paragraph_content(); ?>
						<?php if($ajax_load_var != true){ echo do_shortcode('[toc]'); } ?>
						<?php echo get_content_without_first_paragraph(); ?>
			    	</div>
			</div>
			<div class="elleteam_cta">
				<?php _e( "Rejoignez notre team d'influenceuses ELLE sur les réseaux sociaux et partagez vos photos sur votre compte Instagram avec le hashtag ", 'html5blank' ); ?>
				<a href="https://www.instagram.com/explore/tags/ellebelgique/" target="_blank">#ELLEBelgique</a>!
				<span><?php _e( 'Source', 'html5blank' ); ?>: ELLE.be</span>
			</div>
			<div class="site-breadcrumb" typeof="BreadcrumbList" vocab="http://schema.org/">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
			<footer class="entry-footer">
				<span class="tags-wrapper">
					<?php $post_tags = get_the_tags();
					if ( $post_tags ) { ?>
						<span class="site-tags">Tags: </span>
						<span class="tags-list">
							<?php
							$list_tags_str = '';
						    foreach( $post_tags as $tag ) {
						    	$list_tag_link = "<a href='".home_url()."/tag/".$tag->slug."' property='keywords' typeof='text'>".ucfirst($tag->name)."</a>";
						    	$list_tags_str .= $list_tag_link.', '; 
						    } 
						    $list_tags_str = substr($list_tags_str, 0, -2).'.';
						    echo $list_tags_str;
						    ?>
						</span>
					<?php } ?>
				</span>
				<div id="social-share">
					<?php display_single_RS(); ?>
				</div>
			</footer>
			<?php  // comments_template(); ?>
		</div>
	</article>
	<!-- /article -->
	</div>
		<!-- sidebar -->
		<aside class="sidebar" role="complementary">
			<!-- single-sidebar-related -->
			<div class="site-inner-ads no_print">
				<?php if($ajax_load_var != 1) { ?>
				<div class="rmgAd center margin_bottom_20" data-device="desktop" data-type="RECT_ABOVE"></div>
				<?php } else { ?> 
				<div class="rmgAd center" data-device="desktop" data-type="RECT"></div>
				<?php } ?>
			</div>

			<?php if(get_field('identifiant_youtube','option')){ ?>
				<!-- (1) video wrapper -->
				<div class="sidebar_youtube" data-embed="<?php echo get_field('identifiant_youtube','option'); ?>" data-picture="<?php echo get_field('image_de_couverture','option'); ?>"> 		 
				    <!-- (2) the "play" button -->
				    <div class="play-button"></div> 		     
				</div>
			<?php } ?>
			<?php 
				if(is_commercial()){
					$posts = get_posts(array('author' => '-'.get_field('variable_js','option')['id_commercial'], 'posts_per_page' => 6, 'category'=> $main_categorie->cat_ID, 'orderby' => 'rand', 'order' => 'DESC','date_query' => array(array('after' => '30 days ago')), 'post__not_in' => $excludeIDS));
				}else {
					$posts = get_posts(array('posts_per_page' => 6, 'category'=> $main_categorie->cat_ID, 'orderby' => 'rand', 'order' => 'DESC','date_query' => array(array('after' => '30 days ago')), 'post__not_in' => $excludeIDS)); }
					
				_sidebar_article_block($posts,$main_categorie->cat_ID);
			?>
		<div class="site-inner-ads center no_print margin_top_20">
			<div id="gpt-ad-RECT_MIDDLE" class="rmgAd center" data-device="desktop" data-type="RECT_MIDDLE"></div>
		</div>
		</aside>
		<!-- /sidebar -->
</div> 

<!-- Intégration OUTBRAIN -->
<?php 
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>			
<div class="OUTBRAIN margin_bottom_20" data-src="<?php echo $current_url; ?>" data-widget-id="GS_5"></div> 
<script type="text/javascript" async="async"src="//widgets.outbrain.com/outbrain.js"></script>

<!-- Intégration OUTBRAIN -->

<?php get_template_part('template-parts/general/article-related-bottom');  ?>

<div class="site-inner-ads center no_print margin_top_20 margin_bottom_20">
	<div id="gpt-ad-BANNER_BELOW" class="rmgAd" data-device="all" data-type="BANNER_BELOW">
	</div>
</div>