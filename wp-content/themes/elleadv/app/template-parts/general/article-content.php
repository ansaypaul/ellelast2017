<?php 
 	elle_set_post_views();
	$list_categorie = get_the_category(); 
	$main_categorie = $list_categorie[0];
	if(count($list_categorie) > 1 ){
		$sous_categorie = $list_categorie[1];
	}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-id = "<?php the_ID(); ?>">
	<header class="entry-header">
		<div class="post-category">
			<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
				<?php echo $main_categorie->name; ?>
			</a>
			<?php if(isset($sous_categorie)){ ?>
				<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
			<?php } ?>
		</div>
		<h1>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="entry-title"><?php the_title(); ?></a>
		</h1>
		<div class="post-details">
			<span class="date">
				<?php the_time('l, j F Y'); ?>
				<?php if ( get_the_time( ' j F Y' ) !== get_the_modified_time( ' j F Y' ) ) { ?>
				<span> - <?php _e( 'Mis à jour', 'html5blank' ); ?> le <?php the_modified_time('j F Y'); ?></span>
				<?php } ?>
			</span>
			<span class="author vcard">
				<?php _e( 'Par ', 'html5blank' ); ?> <?php the_author_posts_link(); ?></a>
			</span>
		</div>
		<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
				<?php the_post_thumbnail('large',array('',0,1)); ?>
			</a>
		<?php endif; ?>
	</header>
	<div class="entry-content">
		<?php the_content(); // Dynamic Content ?>
	</div>
	<footer class="entry-footer">
		<?php the_tags( 'Tags // ', ', ', '<br>');?>
	</footer>
	<?php  // comments_template(); ?>
</article>