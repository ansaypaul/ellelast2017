<?php 
/*
$category_search a adapter en fonction des catgories choisies (rendre dynamique ? option ACF ?)
*/
$category_search = category_principales();

?>
<!-- search -->
<div class="search-form-sidebar">
	<div class="form-group">
		<label class="control-label required"><?php _e("tri", 'html5blank' ); ?></label>
		<div id="sortedBy">
			<div class="radio"><label class="required" for="sortedBy_0"><input type="radio" id="sortedBy_0" name="sortedBy" value="post_date" 
			<?php if($_GET['sortedBy'] == 'post_date'){ echo "checked=''"; } ?>><?php _e("Par date", 'html5blank' ); ?></label></div>
			<div class="radio"><label class="required" for="sortedBy_1"><input type="radio" id="sortedBy_1" name="sortedBy" value="relevance" 
			<?php if(!isset($_GET['sortedBy']) || $_GET['sortedBy'] == 'relevance'){ echo "checked=''"; } ?>><?php _e("Par Pertinence", 'html5blank' ); ?></label></div>
		</div>
	</div>

	<?php if($_GET['search_referer'] == 'global'){ ?>
	<div class="form-group">
	<label class="control-label required"><?php _e("par date", 'html5blank' ); ?></label>
		<div id="date">
			<div class="radio"><label class="required" for="date_0"><input type="radio" id="date_0" name="date" value="1" <?php if($_GET['date'] == '1'){ echo "checked=''"; } ?>><?php _e("Aujourd'hui", 'html5blank' ); ?></label></div>
			<div class="radio"><label class="required" for="date_1"><input type="radio" id="date_1" name="date" value="7" <?php if($_GET['date'] == '7'){ echo "checked=''"; } ?>><?php _e("Sur 7 jours", 'html5blank' ); ?></label></div>
			<div class="radio"><label class="required" for="date_2"><input type="radio" id="date_2" name="date" value="30" <?php if($_GET['date'] == '30'){ echo "checked=''"; } ?>><?php _e("Sur 1 mois", 'html5blank' ); ?></label></div>
			<div class="radio"><label class="required" for="date_3"><input type="radio" id="date_3" name="date" value="90" <?php if($_GET['date'] == '90'){ echo "checked=''"; } ?>><?php _e("Sur 3 mois", 'html5blank' ); ?></label></div>
			<div class="radio"><label class="required" for="date_4"><input type="radio" id="date_4" name="date" value="9999" <?php if($_GET['date'] == '9999'){ echo "checked=''"; } ?>><?php _e("Tout l'historique", 'html5blank' ); ?></label></div>
		</div>
	</div>
	<div id="search_rubric" class="form-group">
		<label class="control-label required"><?php _e("par rubrique", 'html5blank' ); ?></label>
		<div id="rubric">
			<?php foreach ($category_search as $category) {
				if(in_array($category, $_GET['category'])){ ?>
					<label class="checkbox" for="rubric_4"><input checked="" type="checkbox" id="rubric_4" name="category[]" value="<?php echo $category; ?>"><?php echo get_cat_name($category); ?></label>
				<?php }else{ ?>
					<label class="checkbox" for="rubric_4"><input type="checkbox" id="rubric_4" name="category[]" value="<?php echo $category; ?>"><?php echo get_cat_name($category); ?></label>
				<?php }
				}
			?>
	   </div>
   </div>
   <?php } ?>

   <?php if($_GET['search_referer'] == 'recipe'){ ?>
   <!--** Recherche pour les recettes **/-->
	<div class="form-group form-group-recettes">
	<label class="control-label required"><?php _e("Type de plat", 'html5blank' ); ?></label>
		<div id="dishtypes">
		    <?php
		   		if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
					$field_key = "field_5a421b774e17d";
				}else{
					$field_key = "field_59b7e816b19c1";//NL
				}
				
				$field = get_field_object($field_key);
				if( $field )
				{
				        foreach( $field['choices'] as $k => $v )
				        {
				        	if(in_array($v, $_GET['dishtypes'])){ 
				            	echo '<label class="checkbox" for="' . $k . '"><input checked="" type="checkbox" id="' . $k . '" name="dishtypes[]" value="' . $k . '">' . $v . '</label>';
				        	}else{
				        		echo '<label class="checkbox" for="' . $k . '"><input type="checkbox" id="' . $k . '" name="dishtypes[]" value="' . $k . '">' . $v . '</label>';
				        	}
				        }
				}
		    ?>
		</div>
	</div>

	<div class="form-group form-group-recettes">
	<label class="control-label required"><?php _e("Difficulté", 'html5blank' ); ?></label>
		<div id="difficultyLevels">
		    <?php
				$field_key = "field_5a42600a882e5";
				$field = get_field_object($field_key);
				if( $field )
				{
				        foreach( $field['choices'] as $k => $v )
				        {
				        	if(in_array($v, $_GET['diff'])){ 
				            echo '<label class="checkbox" for="' . $k . '"><input checked="" type="checkbox" id="' . $k . '" name="diff[]" value="' . $k . '">' . $v . '</label>';
				        	}else{
				        	echo '<label class="checkbox" for="' . $k . '"><input type="checkbox" id="' . $k . '" name="diff[]" value="' . $k . '">' . $v . '</label>';
				        	}
				        }
				}
		    ?>
		</div>
	</div>
	<?php } ?>

	<?php if($_GET['search_referer'] == 'adress'){ ?>
	<!--** Recherche pour les adresses **/-->
	<div class="form-group form-group-adresses">
	<label class="control-label required"><?php _e("Quoi ?", 'html5blank' ); ?></label>
		<div id="AdressesTypes">
		    <?php
		    	if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
					$field_key = "field_59b7cfd163213";
				}else{
					$field_key = "field_59b7e6f084abc";
				}

				$field = get_field_object($field_key);
				if( $field )
				{
				        foreach( $field['choices'] as $k => $v )
				        {
				        	if(in_array($v, $_GET['loctype'])){ 
				           	echo '<label class="checkbox" for="' . $k . '"><input checked="" type="checkbox" id="' . $k . '" name="loctype[]" value="' . $k . '">' . $v . '</label>';
				            }else{
				        	echo '<label class="checkbox" for="' . $k . '"><input type="checkbox" id="' . $k . '" name="loctype[]" value="' . $k . '">' . $v . '</label>';
				        	}
				        }
				}
		    ?>
		</div>
	</div>

	<div class="form-group form-group-adresses">
	<label class="control-label required"><?php _e("Ou ?", 'html5blank' ); ?></label>
		<div id="AdressesVille">
		    <?php
		    	if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
					$field_key = "field_59b7d2d14fe4b";
				}else{
					$field_key = "field_59b7e69d84abb";
				}

				$field = get_field_object($field_key);
				if( $field )
				{
				        foreach( $field['choices'] as $k => $v )
				        {
				        	if(in_array($v, $_GET['loc'])){ 
				           	echo '<label class="checkbox" for="' . $k . '"><input checked="" type="checkbox" id="' . $k . '" name="loc[]" value="' . $k . '">' . $v . '</label>';
				            }else{
				        	echo '<label class="checkbox" for="' . $k . '"><input type="checkbox" id="' . $k . '" name="loc[]" value="' . $k . '">' . $v . '</label>';
				        	}
				        }
				}
		    ?>
		</div>
	</div>
	<?php } ?>

	<span class="custom-cta submit-sidebar-form"><?php _e("Rechercher", 'html5blank' ); ?></span>
	
</div>
<!-- /search -->