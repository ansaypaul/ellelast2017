<?php global $magazine ?>
<div class="template-parts unfold-menu">
	<ul>
		<li>
			<button><?php _e( "S'abonner au magazine", 'html5blank' ); ?></button>
			<div class="unfold">
				<div>
					<span class="abo-cover">
						<img src="<?php echo cover_img_src(); ?>" alt="ELLE Cover">
					</span>
					<span class="abo-img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/digital_magazine_abo.jpg" alt="ELLE Cover">
					</span>
				</div>
				<a target="_blank" href="<?php echo get_field("magazine_viapress",$magazine[0]->ID ); ?>" class="btn-v1"><?php _e( "Abonnez-vous au magazine", 'html5blank' ); ?></a>
			</div>
		</li>
	</ul>
</div>