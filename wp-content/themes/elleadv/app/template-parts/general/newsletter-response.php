<?php global $member ?>
<?php $lang_identifiant = get_field('variable_js','option')['lang_identifiant']; ?>

<?php if($lang_identifiant == 'FR'){ ?>
<h4>Merci pour votre inscription !</h4>
<p>Vous recevrez notre newsletter hebdomadaire très prochainement.</p>
<form method="POST" role="form">
<input type="hidden" name="attributes[7][name]" value="emailAddress">
<input type="hidden" name="attributes[14][name]" value="creationSource">
<input type="hidden" name="attributes[14][value]" value="ELLE-FR">
<input type="hidden" name="attributes[15][name]" value="motherLanguage">
<input type="hidden" name="attributes[15][value]" value="FR">
<input class="newsletter-input" name="attributes[7][value]" value='<?php echo $member->_profil_email; ?>' type="hidden" placeholder="Entrez votre e-mail" required>
<input type='hidden' name='subscriptions[]' value='ELLE'>
<input class="newsletter-optin hidden" type="checkbox" name="subscriptions[]" value="ELLE Partenaire" checked>

<?php if(!in_array( 'ELLE Partenaire',$member->_profil_subscriptions))
{
 echo "<input id='newsletter-submit' class='partenaire-submit btn-v1' value='Je m inscris aux bons plans partenaires' type='button'>";
}
else {
echo '<p>Merci de vous être inscrit à nos partenaires !</p>';
} 
?> 
</form>
<?php } else { ?>
<h4>Bedankt voor uw inschrijving !</h4>
<p>U ontvangt onze wekelijkse nieuwsbrief zeer binnenkort.</p>
<form method="POST" role="form">
<input type="hidden" name="attributes[7][name]" value="emailAddress">
<input type="hidden" name="attributes[14][name]" value="creationSource">
<input type="hidden" name="attributes[14][value]" value="ELLE-NL">
<input type="hidden" name="attributes[15][name]" value="motherLanguage">
<input type="hidden" name="attributes[15][value]" value="NL">
<input class="newsletter-input" name="attributes[7][value]" value='<?php echo $member->_profil_email; ?>' type="hidden" placeholder="Vul je emailadres is" required>
<input type='hidden' name='subscriptions[]' value='ELLE'>
<input class="newsletter-optin hidden" type="checkbox" name="subscriptions[]" value="ELLE Partenaire" checked>

<?php if(!in_array( 'ELLE Partenaire',$member->_profil_subscriptions))
{
 echo "<input id='newsletter-submit' class='partenaire-submit btn-v1' value='Ik wens berichten over acties van partners te ontvangen' type='button'>";
}
else {
echo '<p>Bedankt om je in te schrijven bij onze partners !</p>';
} 
?> 
</form>
<?php }?>