<?php $autopromo = get_posts(array('post_type' => 'autopromo', 'posts_per_page' => 1, 'order' => 'DESC'));
	if($autopromo){ 
		$imu_img = get_field('autopromo_imu', $post->ID);
		$imu_link = get_field('autopromo_link', $post->ID);
		foreach ( $autopromo as $post ) : setup_postdata( $post ); ?>
		<a href="<?php echo $imu_link; ?>" class="auto-promo" target="_blank">
	        <img src="<?php echo $imu_img; ?>" alt="ELLE Autopromo">
	   	</a>
<?php endforeach; wp_reset_postdata();?>
<?php } ?>