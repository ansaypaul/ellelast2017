<!-- Ajouter un affichage Random sur les 9 derniers postés ds le repeater  -->
<!-- Ajouter le slider -->
<?php 
$editorshops = get_posts(array('posts_per_page' => 1,'post_type' => 'editors-shop','html5blank')); ?>
<?php 
foreach ($editorshops as $editorshop) {
	$rows = get_field('editors_shop_listing',$editorshop->ID);
	if($rows) { 
	shuffle( $rows);
	 ?>
		<div class="editorsshop_wrapper">
			<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
				<div class="title-h2">Nos coups de cœur</div>
			<?php } else { ?>
				<div class="title-h2">Onze favorieten</div>
			<?php } ?>
			<div class="swiper-container-editorsshop">
				<div class="swiper-wrapper">
				<?php foreach($rows as $row) { 
				//var_dump($row);
				$product_image = $row['product_image'];
				$brand_name = $row['brand_name'];
				$product_url = $row['product_url'];
				//$productcategory = get_field('product_category');
				?>
					<?php if( $product_url ){ ?>
					<div class="swiper-slide editorsshop_product"> 
						<span class="editorshop_name"><?php echo $brand_name; ?></span>
						<a href="<?php echo $product_url; ?>" class="editorshop_item" target="_blank" rel="nofollow">
							<img src="<?php echo $product_image['sizes']['content2018mid']; ?>" alt="<?php echo $product_image['alt'] ?>" />
						</a>
						<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
							<a href="<?php echo $product_url; ?>" class="editorshop_btn custom-cta" target="_blank" rel="nofollow">Shoppez-moi</a>
						<?php } else { ?>
							<a href="<?php echo $product_url; ?>" class="editorshop_btn custom-cta" target="_blank" rel="nofollow">Shop hier</a>
						<?php } ?>
					</div>
					<?php } ?>
				<?php } ?>
				</div>
			<!-- Add Arrows -->
		    <div class="sprite swiper-button-next"></div>
		    <div class="sprite swiper-button-prev"></div>
			</div>
		</div>
	<?php 
	}  
}
?>

