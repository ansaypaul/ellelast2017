<?php
/**
 * @by laurent.fulster@ventures.be
 */
?>
<script id="analytics__code">
	var device = 'not_set';
	width = document.body.clientWidth;
	if ( width >= 970 ) { device = 'desktop'; }
	if ( width < 970 && width >= 768 ) { device = 'tablet'; }
	if ( width < 768 && width >= 320 ) { device = 'mobile'; }

	var pp_gemius_identifier = 'ndA1NDLaFQVM9ngenGLz93YY.fOZMwcZhJxjU_J1hzP.I7';
	var pp_gemius_extraparameters = new Array( 'lan=FR', 'key=FR-<?php echo stripAccents( ad_position() ); ?>', 'subs=FR-' + device );

	// Google Analytics
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	/*ga('provide', 'adblockTracker', function(tracker, opts) {
		  var ad = document.createElement('ins');
		  ad.className = 'AdSense';
		  ad.style.display = 'block';
		  ad.style.position = 'absolute';
		  ad.style.top = '-1px';
		  ad.style.height = '1px';
		  document.body.appendChild(ad);
		  tracker.set('dimension' + opts.dimensionIndex, !ad.clientHeight);
		  document.body.removeChild(ad);
	});*/

<?php
if ( is_single() && !is_attachment() ) {
?>
	ga('create', 'UA-42249957-1', 'elle.be');
	ga('require', 'displayfeatures');
	ga('require', 'linkid', 'linkid.js');
	//ga('require', 'adblockTracker', {dimensionIndex: 6});
	ga('send', 'pageview', {
		'dimension1': '<?php echo get_the_author_meta( 'user_login' ); ?>',
		'dimension2': adposition +' (Article)',
		'dimension3': '<?php echo the_ID(); ?> - '+ adposition +" - <?php echo html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8' ); ?> - <?php echo get_the_author_meta( 'user_login' ); ?>",
		'dimension4': adpositionbis +' (Article)',
		'dimension5': adpositionter +' (Article)'
	});

	ga('create', 'UA-42249957-3', 'elle.be', {'name': 'newTracker'});
	ga('newTracker.require', 'displayfeatures');
	ga('newTracker.require', 'linkid', 'linkid.js');
	//ga('newTracker.require', 'adblockTracker', {dimensionIndex: 6});
	ga('newTracker.send', 'pageview', {
		'dimension1': 'FR - <?php echo get_the_author_meta( 'user_login' ); ?>',
		'dimension2': 'FR - '+ adposition +' (Article)',
		'dimension3': 'FR - <?php echo the_ID(); ?> - '+ adposition +" - <?php echo html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8' ); ?> - <?php echo get_the_author_meta( 'user_login' ); ?>",
		'dimension4': 'FR - '+ adpositionbis +' (Article)',
		'dimension5': 'FR - '+ adpositionter +' (Article)'
	});

<?php
}
elseif ( is_attachment() ) {
?>
	ga('create', 'UA-42249957-1', 'elle.be');
	ga('require', 'displayfeatures');
	ga('require', 'linkid', 'linkid.js');
	//ga('require', 'adblockTracker', {dimensionIndex: 6});
	ga('send', 'pageview', {
		'dimension1': '<?php echo get_the_author_meta( 'user_login' ); ?>',
		'dimension2': adposition +' (Media)',
		'dimension3': "<?php echo get_post()->post_parent; ?> - MEDIA - <?php echo html_entity_decode( get_the_title( get_post()->post_parent ), ENT_QUOTES, 'UTF-8' ); ?> - <?php echo get_the_author_meta( 'user_login' ); ?>",
		'dimension4': adpositionbis +' (Media)',
		'dimension5': adpositionter +' (Media)'
	});

	ga('create', 'UA-42249957-3', 'elle.be', {'name': 'newTracker'});
	ga('newTracker.require', 'displayfeatures');
	ga('newTracker.require', 'linkid', 'linkid.js');
	//ga('newTracker.require', 'adblockTracker', {dimensionIndex: 6});
	ga('newTracker.send', 'pageview', {
		'dimension1': 'FR - <?php echo get_the_author_meta( 'user_login' ); ?>',
		'dimension2': 'FR - '+ adposition +' (Media)',
		'dimension3': "FR - <?php echo get_post()->post_parent; ?> - MEDIA - <?php echo html_entity_decode( get_the_title( get_post()->post_parent ), ENT_QUOTES, 'UTF-8' ); ?> - <?php echo get_the_author_meta( 'user_login' ); ?>",
		'dimension4': 'FR - '+ adpositionbis +' (Media)',
		'dimension5': 'FR - '+ adpositionter +' (Media)'
	});

<?php
}
else {
?>
	ga('create', 'UA-42249957-1', 'elle.be');
	ga('require', 'displayfeatures');
	ga('require', 'linkid', 'linkid.js');
	//ga('require', 'adblockTracker', {dimensionIndex: 6});
	ga('send', 'pageview', {
		'dimension2': adposition +' (Navigation)',
		'dimension4': adpositionbis +' (Navigation)',
		'dimension5': adpositionter +' (Navigation)'
	});

	ga('create', 'UA-42249957-3', 'elle.be', {'name': 'newTracker'});
	ga('newTracker.require', 'displayfeatures');
	ga('newTracker.require', 'linkid', 'linkid.js');
	//ga('newTracker.require', 'adblockTracker', {dimensionIndex: 6});
	ga('newTracker.send', 'pageview', {
		'dimension2': 'FR - '+ adposition +' (Navigation)',
		'dimension4': 'FR - '+ adpositionbis +' (Navigation)',
		'dimension5': 'FR - '+ adpositionter +' (Navigation)'
	});

<?php
}
?>
	window.google_analytics_uacct = "UA-42249957-1";

// gemiusPrism
	<!--//--><![CDATA[//><!--
	(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');gt.setAttribute('defer','defer'); gt.src=l+'://gabe.hit.gemius.pl/xgemius.js'; document.getElementById('analytics__code').parentNode.insertBefore(gt, document.getElementById('analytics__code').nextSibling);} catch (e) {}})(document,'script');
	//--><!]]>
</script>
<!-- <div class="ad-wallpaper" id="ad-wallpaper-1" style="height: 0;width: 0;">
	<div id="ad-wallpaper-1-root"></div>
	<script>
		googletag.cmd.push(function() { googletag.display('ad-wallpaper-1'); });
	</script>
</div> -->
<?php if ( $_SERVER['REMOTE_ADDR'] == '194.78.138.23' && is_admin_bar_showing() ) { ?>
<script>
	document.write( '<div style="left: 100px;position: fixed;top: 100px;z-index: 2;">'+ adposition +' | '+ adpositionclean +' | '+ adpositionbis +' | '+ adpositionter +' | '+ adpositionkw +'</div>' );
</script>
<?php } ?>
