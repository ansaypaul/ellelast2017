<!-- EAT Adresses-->
<div class="eat-adresses">
	<span class="eat-adresses-title"><?php _e( 'Trouver une adresse', 'html5blank' ); ?></span>
	<form method="post" class="search-recherche search-adresse eat-adresses-items">
		<ul>
			<li class="eat-adresses-items"><?php _e( 'Quoi', 'html5blank' ); ?> ?</li>
			<li>Restau</li>
			<li>Bar</li>
		</ul>
		<ul>
			<li class="eat-adresses-items"><?php _e( 'Quel style', 'html5blank' ); ?> ?</li>
		</ul>
		<ul>
			<li class="eat-adresses-items"><?php _e( 'Ou', 'html5blank' ); ?> ?</li>
		</ul>
	    <input id="input_recherche_adresse" type="submit" name="input_recherche_adresse" value="<?php _e('Valider', 'html5blank' ); ?>">
	</form>
</div>
<!-- EAT Adresses -->