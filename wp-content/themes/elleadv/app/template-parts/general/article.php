<?php
	global $post;
	$list_categorie = get_the_category(); 
	$main_categorie = $list_categorie[0];
	$sous_categorie = $list_categorie[0];
	if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[0];
	}

	if(count($list_categorie) > 1 ){
		if (in_array($list_categorie[1]->cat_ID, category_principales())) { 
			$main_categorie = $list_categorie[1];
			$sous_categorie = $list_categorie[0];
		}
	}
?>
<article class="article-design-1 lazy">
	<span class="post-thumbnail atc" data-atc="<?php _encode_base64(get_the_permalink($post->ID)); ?>">
		<?php the_post_thumbnail('content2018',array('lazy',1,0)); ?>
	</span>
	<div class="post-side">
		<div class="post-side-inner">
			<div class="post-category">
				<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-parent">
					<?php echo $sous_categorie->name; ?>
				</a>
			</div>
		    <a href="<?php the_permalink();?>" class="post-title">
		        <span class="title-h2"><?php relevanssi_the_title(); ?></span>
		    </a>
		    <?php 
		    $author_link =  get_author_posts_url( $post->post_author);  
		    $author_name = get_the_author_meta('display_name', $post->post_author);
		    ?>
			<div class="post-author"><span><?php _e( 'par', 'html5blank' ); ?> </span><span class="author"><?php echo ucfirst($author_name); ?></span></div>
	    </div>
	</div>
</article>