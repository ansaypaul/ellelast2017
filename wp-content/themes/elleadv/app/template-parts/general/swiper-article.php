<?php global $post, $excludeIDS, $main_categorie; $current_post = 0; ?>
<?php 
$author = ''; $category_id = ''; $category_name = ''; $title_swiper_article = '';
if(isset($main_categorie)){ $category_id = $main_categorie->cat_ID; }else{ $category_id = 0; }
$category_name = get_cat_name($category_id);
$title_swiper_article = $category_name;
if(isset($_POST['cat'])){$title_swiper_article = get_cat_name($_POST['cat']); $category_id = $_POST['cat'];}
if(isset($_POST['author'])){
	if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
		$title_swiper_article = 'du même auteur'; $author = $_POST['author']; 
	}else{
		$title_swiper_article = 'van deze auteur'; $author = $_POST['author']; 
	}
}
?>
<?php $posts = get_posts(array('author' => $author, 'posts_per_page' => 9, 'category'=> $category_id , 'post__not_in' => $excludeIDS, 'orderby'   => 'rand', 'date_query' => array(array('after' => '60 days ago')))); ?>
<?php if($posts){ ?>
	<div class="swiper-article-block">
		<a href="<?php echo get_category_link($category_id); ?>" class="swiper-article-title">+ <?php echo $title_swiper_article; ?></a>
		<div class="swiper-article swiper-article-<?php echo 1 ?>">

			<div class="swiper-container-article">
					<div class="swiper-wrapper">
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++ ?>
						<?php if($current_post != 1){ ?>
							<?php $excludeIDS[] = $post->ID; ?> 
								<div class="swiper-slide">
									<div class="swiper-image">
										<a href="<?php the_permalink($post->ID); ?>">
											<?php the_post_thumbnail('content2018',array('lazy',0,0)); ?>
										</a>
									</div>
									<div class="swiper-titre">
										<h2><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
									</div>
									<!-- <a href="#" class="sprite sprite-social"></a>
									<a href="#" class="sprite sprite-pinit"></a> -->
								</div>
						<?php } ?>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="swiper-button-prev swiper-button-prev-article sprite"><span></span></div>
			<div class="swiper-button-next swiper-button-next-article sprite"><span></span></div>
		</div>
	</div> 
<?php } ?>