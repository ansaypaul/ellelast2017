<?php 
global $post;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
$sous_categorie = $list_categorie[0];
if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
	$main_categorie = $list_categorie[0];
	$sous_categorie = $list_categorie[0];
}

if(count($list_categorie) > 1 ){
	if(in_array($list_categorie[0]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[0];
		$sous_categorie = $list_categorie[1];
	}

	if(in_array($list_categorie[1]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[1];
		$sous_categorie = $list_categorie[0];
	}
}

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
if($sous_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
if($main_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
?>
<div class="main-content-wrapper recettes-wrapper" data-io-article-url="<?php the_permalink(); ?>">
	<div class="content" data-tag="<?php echo $main_tag ?>" data-cat="<?php echo $sous_categorie->cat_ID; ?>">
	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class("h-entry category-recherche"); ?> data-id = "<?php the_ID(); ?>" 
	data-cat = "" data-title="<?php the_title(); ?>" data-adposition="<?php echo ad_position()['cat']; ?>" 
	data-adpositionclean = "<?php echo ad_position()['cat']; ?>" data-adpositionbis="<?php echo html_entity_decode(ad_position()['subcat']); ?>"
	data-author = "<?php the_author(); ?>" data-tag="<?php echo html_entity_decode(ad_position_ter()); ?>" data-link="<?php the_permalink(); ?>">
		<div vocab="http://schema.org/" typeof="Recipe">
		  <meta content="<?php the_permalink(); ?>" />
			<header class="entry-header">
				<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icons/elleatable.png" alt="ELLE Logo" class="elle_logo only_print">
				<?php } else { ?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icons/elleeten.png" alt="ELLE Logo" class="elle_logo only_print">
				<?php } ?>
				<!-- post-category -->
				<div class="post-category">
					<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
						<?php echo $main_categorie->name; ?>
					</a>
					<?php if(isset($sous_categorie) && ($main_categorie->cat_ID != $sous_categorie->cat_ID)){ ?>
						<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
					<?php } ?>
				</div>
				<!-- post-title -->
				<h1 property="name">
					<a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
				</h1>
				<?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings($pid); endif; ?>
				<!-- post-details -->
				<div class="post-details">
					<span class="date">	
						<?php $str_update =  __( 'Publié le', 'html5blank' ); ?>
						<?php if(get_the_modified_time('Y-m-j') !=  get_the_time('Y-m-j')){ $str_update = __( 'Mis à jour le', 'html5blank' ); } ?>
							<span class="dt-modified" property="dateModified" content="<?php the_modified_time('Y-m-j'); ?>"> <?php echo $str_update; ?> <?php the_modified_time('j F Y'); ?></span>
							<span class="dt-published hidden" property="datePublished" content="<?php the_time('Y-m-j'); ?>"> <?php the_time('l, j F Y'); ?>
							</span>
					</span>
					<a id="print-button" class="print-button"><span>
					<?php _e( 'Imprimer la fiche recette', 'html5blank' ); ?></span></a>
					<span class="author vcard p-author h-card" property="author" typeof="Person">	<?php _e( 'par', 'html5blank' ); ?>
				      	<span property="name" class="hidden"><?php the_author(); ?></span>
				      	<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author(); ?></a>
				      	<?php if( $authortitle = get_the_author_meta( 'authortitle' ) ) { ?>
							<span class="author-title hidden" property="jobTitle"><?php echo $authortitle; ?></span>
						<?php } ?>
						<?php $credits_auteur_initial = get_field( "credits_auteur_initial" );
						if ( $credits_auteur_initial ) { ?>
							<span class="credit-trad"> <?php _e( 'et', 'html5blank' ); ?> <?php echo $credits_auteur_initial; ?></span>
						<?php } ?>
						<?php $credits_link = get_field( "credits_link" );
						$credits_photographe = get_field( "credits_photographe" );
						if ( $credits_photographe ) { ?>
							<span class="credit-photo"><?php _e( 'Photos', 'html5blank' ); ?>: 
								<?php if ( $credits_link ) { ?>
									<a href="<?php echo $credits_link; ?>" target="_blank">
								<?php } ?>
								<?php echo $credits_photographe; ?></span>
								<?php if ( $credits_link ) { ?>
									</a>
								<?php } ?>
						<?php } ?>
						<div property="image" class="hidden"><?php echo get_avatar(get_the_author_meta( 'user_email'),220); ?></div>
					</span>
					<div property="publisher" typeof="Organization" class="hidden">
						<span property="name"><?php _e( 'ELLE Belgique', 'html5blank' ); ?></span>
						<div property="logo" typeof="ImageObject">
							<link property="url" href="ellebelgique.png" />
						    <meta property="height" content="125" />
						    <meta property="width" content="320" />
						</div>
					</div>
				</div>
				<div class="recipe-informations">
					<?php 
					$recipepreptime = explode(':',get_field( "recipe-preptime" )); 
					$recipepreptime_structure = "00";
					if(count($recipepreptime) > 1){
					$recipepreptime_heure = intval($recipepreptime[0]);
					$recipepreptime_min = intval($recipepreptime[1]);
					$recipepreptime_s = intval($recipepreptime[2]);
					$recipepreptime_structure = $recipepreptime_heure * 60 + $recipepreptime_min + ($recipepreptime_s / 60);
					}
					?>
					<?php $recipecooktime = get_field( "recipe-cooktime" ); ?>
					
					<?php 
					$recipecooktime = explode(':',get_field( "recipe-cooktime" )); 
					$recipecooktime_structure = "00";
					if(count($recipecooktime) > 1){
						$recipecooktime_heure = intval($recipecooktime[0]);
						$recipecooktime_min = intval($recipecooktime[1]);
						$recipecooktime_s = intval($recipecooktime[2]);
						$recipecooktime_structure = $recipecooktime_heure * 60 + $recipecooktime_min + ($recipecooktime_s / 60);
					}
					?>

					<?php $recipeyield = get_field( "recipe-yield" ); ?>
					<?php $recipedifficulties = get_field( "recipe-difficulties" ); ?>
					<ul class="informations-list">
						
						<?php 
							if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
								$recipetypes = get_field( "recipe-type" ); ?>
								<?php if( $recipetypes ) { ?>
									<li class="recipe-cat-wrapper">
										<div class="sprite sprite-cat"></div>
										<?php foreach( $recipetypes as $recipetype ): ?>
											<span class="cat-type" property="recipeCategory"><?php echo $recipetype; ?></span>
										<?php endforeach; ?>
									</li>
								<?php } ?>
							<?php } else if(get_field('variable_js','option')['lang_identifiant'] == 'NL'){
								$recipetypes = get_field( "soort_gerecht" ); ?>
								<?php if( $recipetypes ) { ?>
									<li class="recipe-cat-wrapper">
										<div class="sprite sprite-cat"></div>
										<?php foreach( $recipetypes as $recipetype ): ?>
											<span class="cat-type" property="recipeCategory"><?php echo $recipetype; ?></span>
										<?php endforeach; ?>
									</li>
								<?php } ?>
							<?php } ?>
						<!-- /Recipe category -->
						<?php if( $recipepreptime_structure != "" ) { ?>
		     			<li>
		     				<div class="sprite sprite-prep"></div>
		     				<meta property="prepTime" content="PT<?php echo $recipepreptime_structure; ?>M">
		     				<span><?php if(isset($recipepreptime_heure) && $recipepreptime_heure != '00') { echo $recipepreptime_heure. ' h'; } ?> 
		     				<?php if(isset($recipepreptime_min) && $recipepreptime_min != '00') { echo $recipepreptime_min. ' mn'; } ?></span>
						</li>
						<?php }; ?>
		     			<?php if( $recipecooktime_structure != "" ) { ?>
			    			<li>
			    				<div class="sprite sprite-cook"></div>
			    				<meta property="cookTime" content="PT<?php echo $recipecooktime_structure; ?>M">
			    				<span><?php if(isset($recipecooktime_heure) && $recipecooktime_heure != '00') { echo $recipecooktime_heure. ' h'; } ?> 
								<?php if(isset($recipecooktime_min) && $recipecooktime_min != '00') { echo $recipecooktime_min. ' mn'; } ?></span>
			    			</li>
		    			<?php }; ?>
		   				<li>
		   					<div class="sprite sprite-quant"></div>
		   					<span property="recipeYield"><?php echo $recipeyield; ?></span></li>
		   				<li>
		   					<div class="sprite sprite-diff"></div>
		   					<span><?php echo $recipedifficulties; ?></span>
		   				</li>
			   		</ul>
				</div>
				<!-- post-thumbnail -->
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
						<?php the_post_thumbnail('content2018',array('',0,1,1)); ?>				
					</a>
					<a href="<?php echo get_the_post_thumbnail_url();?>" property="image" class="hidden"></a>
					<!-- PAUL display la vidéo en image à la une si champs rempli -->
					<div id="player_youtube_url" class="hidden">
						<?php echo get_post_meta(get_the_ID(), 'recipe_url_video', true); ?>
					</div>
				<?php endif; ?>
			</header>
				<div class="site-inner-ads no_print">
					<div class="rmgAd center" data-device="mobile" data-type="RECT_ABOVE"></div>
				</div>
			<div class="entry-content">
				<?php $recipeintro = get_field( "recipe-intro" ); ?>
				<p class="content-intro" property="description"><?php echo $recipeintro; ?></p>
			    	<div class="recipe-ingredients">
			    		<h2>
			    			<?php _e( 'Ingrédients', 'html5blank' ); ?>
			    		</h2>
			    		<ul class='ingredients-list'>
				    		<?php
							if( have_rows('recipe-ingredients') ):
							    while ( have_rows('recipe-ingredients') ) : the_row();
							    	echo "<li property='recipeIngredient'>";
							        the_sub_field('ingredient');
							        echo "</li>";
							    endwhile;
							endif; ?>
						</ul>
			    		
			    	</div>
			    	<div class="entry-content e-content">
			    		<h2><?php _e( 'Préparation', 'html5blank' ); ?></h2>
					    <?php echo get_content_without_first_paragraph_content(); ?>
						<?php echo get_content_without_first_paragraph(); ?>
			    	</div>
			</div>
			<div class="elleteam_cta">
				<?php _e( "Rejoignez notre communauté de foodies sur le compte Instagram ", 'html5blank' ); ?><a href="<?php _e( "https://www.instagram.com/elleatable.be/", "html5blank" ); ?>" target="_blank"><?php _e( "@elleatable.be", "html5blank" ); ?></a><?php _e( ". Likez, commentez et partagez vos propres recettes avec le hashtag ", "html5blank" ); ?>
				<a href="<?php _e( "https://www.instagram.com/explore/tags/ellefoodteam/", "html5blank" ); ?>" target="_blank"> <?php _e( "#ELLEfoodteam", "html5blank" ); ?></a>!
				<span><?php _e( 'Source', 'html5blank' ); ?>: ELLE.be</span><br>
				<a href="mailto:<?php _e( "redaction@elleatable.be", "html5blank" ); ?>" class="custom-cta"><?php _e( "Proposer une recette", "html5blank" ); ?></a>
			</div>
			<div class="site-breadcrumb">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			</div>
			<footer class="entry-footer">
				<span class="tags-wrapper">
					<?php $post_tags = get_the_tags();
					if ( $post_tags ) { ?>
						<span class="site-tags">Tags: </span>
						<span class="tags-list">
							<?php
							$list_tags_str = '';
						    foreach( $post_tags as $tag ) {
						    	$list_tag_link = "<a href='".home_url()."/tag/".$tag->slug."' property='keywords' typeof='text'>".ucfirst($tag->name)."</a>";
						    	$list_tags_str .= $list_tag_link.', '; 
						    } 
						    $list_tags_str = substr($list_tags_str, 0, -2).'.';
						    echo $list_tags_str;
						    ?>
						</span>
					<?php } ?>
				</span>
				<div id="social-share">
					<?php display_single_RS(); ?>
				</div>
			</footer>
		</div>
	</article>
	<!-- /article -->
	</div>
		<!-- sidebar -->
		<aside class="sidebar" role="complementary">
			<!-- single-sidebar-related -->
			<div class="site-inner-ads no_print">
				<?php if($ajax_load_var != 1) { ?>
				<div id="gpt-ad-RECT_ABOVE" class="rmgAd center margin_bottom_20" data-device="desktop" data-type="RECT_ABOVE"></div>
				<?php } else { ?> 
				<div id="gpt-ad-RECT_MIDDLE" class="rmgAd center" data-device="desktop" data-type="RECT_MIDDLE"></div>
				<?php } ?>
			</div>

			<?php if(get_field('identifiant_youtube','option')){ ?>
				<!-- (1) video wrapper -->
				<div class="sidebar_youtube" data-embed="<?php echo get_field('identifiant_youtube','option'); ?>" data-picture="<?php echo get_field('image_de_couverture','option'); ?>"> 		 
				    <!-- (2) the "play" button -->
				    <div class="play-button"></div> 		     
				</div>
			<?php } ?>
			<?php 
				if(is_commercial()){
					$posts = get_posts(array('author' => '-'.get_field('variable_js','option')['id_commercial'], 'posts_per_page' => 6, 'category'=> $main_categorie->cat_ID, 'orderby' => 'rand', 'order' => 'DESC','date_query' => array(array('after' => '30 days ago')), 'post__not_in' => $excludeIDS));
				}else {
					$posts = get_posts(array('posts_per_page' => 6, 'category'=> $main_categorie->cat_ID, 'orderby' => 'rand', 'order' => 'DESC','date_query' => array(array('after' => '30 days ago')), 'post__not_in' => $excludeIDS)); }
					
				_sidebar_article_block($posts,$main_categorie->cat_ID);
			?>
				<div class="site-inner-ads center no_print margin_top_20">
					<div id="gpt-ad-RECT_MIDDLE" class="rmgAd center" data-device="desktop" data-type="RECT_MIDDLE"></div>
				</div>
		</aside>
		<!-- /sidebar -->
</div> 

<!-- Intégration OUTBRAIN -->
<?php 
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>			
<div class="OUTBRAIN margin_bottom_20" data-src="<?php echo $current_url; ?>" data-widget-id="GS_5"></div> 
<script type="text/javascript" async="async"src="//widgets.outbrain.com/outbrain.js"></script>

<!-- Intégration OUTBRAIN -->

<?php get_template_part('template-parts/general/article-related-bottom');  ?>

<div class="site-inner-ads center no_print margin_top_20 margin_bottom_20">
	<div id="gpt-ad-BANNER_BELOW" class="rmgAd" data-device="all" data-type="BANNER_BELOW">
	</div>
</div>