<!-- single-sidebar-related -->
<?php global $excludeIDS; global $main_categorie; ?>
<?php 
	$i = 0; 
	//$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'orderby'   => 'rand', 'numberposts' => 4, 'post__not_in' => array($post->ID) , 'date_query' => array(array('after' => '30 days ago' )) ) );
	
	$url     = wp_get_referer();
    $post_id = url_to_postid( $url ); 

    $id_commercial = get_field('variable_js','option')['id_commercial'];
    $id_author_nb = get_post_field( 'post_author', $post_id );

	if($id_commercial == $id_author_nb){
		$related = get_posts( array('author' => '-'.get_field('variable_js','option')['id_commercial'], 'category__in' => wp_get_post_categories($post->ID), 'orderby'   => 'rand', 'numberposts' => 4, 'post__not_in' => array($post->ID) , 'date_query' => array(array('after' => '90 days ago' )) ) );
			}else {
		$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'orderby'   => 'rand', 'numberposts' => 4, 'post__not_in' => array($post->ID) , 'date_query' => array(array('after' => '90 days ago' )) ) );
	}
  
	$categories = wp_get_post_categories( $post->ID ); 
	if( $related ) { ?>
	 <div class="no_print">
		<div id="billboard-<?php display_id_random(); ?>" class="ads ads-first leaderboard" data-categoryAd="" data-formatMOB="[[300, 250],[640,150],[320,75]]" data-format="[[970, 250], [970, 90], [728, 90]]" data-location="" data-position="2"></div>
	</div>
	<div class="bottom-related">
		<span class="title-h3"><?php _e( 'A lire aussi', 'html5blank' ); ?></span>
				<div>
					<?php 
					foreach( $related as $post ) {
					setup_postdata($post); ?>
						<article id="post-<?php the_ID(); ?>" class="article-related">
							<a href="<?php the_permalink(); ?>" rel="bookmark" >
								<?php the_post_thumbnail('content2018mid',array('lazy',1,0)); ?>
							</a>
							<span class="title-h4">
								<a href="<?php echo the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</span>
						</article>
					<?php } ?>
				</div>
			<?php
			wp_reset_postdata(); ?>
	</div> 
<?php } ?>