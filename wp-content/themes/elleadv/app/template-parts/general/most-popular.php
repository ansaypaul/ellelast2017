<!-- most-popular -->
<?php global $id_author; ?>
<div class="most-popular">
	<h3><?php _e( 'les + lus', 'html5blank' ); ?></h3>
	<?php 
		$time_most_popular = '3000 days ago';

		$posts_popular = get_posts(array('posts_per_page' => 9, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular))));

		if(is_home()){  
			$posts_popular = get_posts(array('author'  =>  $id_author , 'posts_per_page' => 9, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular))));
		} 


		if(is_page()){  
			$posts_popular = get_posts(array('posts_per_page' => 9, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular))));
		} 

		if(is_category()){ 
			$category = get_queried_object();
			$posts_popular = get_posts(array('author'  =>  $id_author ,'category'=> $category->term_id, 'posts_per_page' => 9, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular))));
		} 

		if(is_tag()){ 
			$tag_id = get_query_var('tag_id');
			$posts_popular = get_posts(array('author'  =>  $id_author ,'tag__in' => array($tag_id), 'posts_per_page' => 9, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num'));
		} 

		if(is_author()){
			$posts_popular = get_posts(array('author'  =>  $id_author , 'posts_per_page' => 9, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular))));
		} 

	?>
	<ul class="most-popular-titles" >
	<?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); ?>
		<li>
			<h4><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h4>
		</li>
	<?php endforeach; wp_reset_postdata(); ?>
	</ul>
</div>
<!-- most-popular -->