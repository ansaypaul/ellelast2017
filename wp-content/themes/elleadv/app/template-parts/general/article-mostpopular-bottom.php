<div class="most-popular">
	<h3><?php _e( 'les + plus', 'html5blank' ); ?></h3>
	<?php 
	$time_most_popular = '15 days ago';
	$posts_popular = get_posts(array('posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)))); ?>
	<div>
		<?php 
		foreach( $posts_popular as $post ) {
		setup_postdata($post); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('article-mostpopular'); ?>>
				<a href="<?php the_permalink(); ?>" rel="bookmark" >
					<?php the_post_thumbnail('large',array('',1,0)); ?>
				</a>
				<h4>
					<a href="<?php echo the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</h4>
			</article>
		<?php } ?>
	</div>
</div> 