<?php
	global $post;
?>
<div class="speakers_eaf_main">
	<div class="post-thumbnail atc" data-atc="<?php _encode_base64(get_the_permalink($post->ID)); ?>">
		<?php the_post_thumbnail('content2018',array('lazy',0,0)); ?>
	</div>
	<a href="<?php the_permalink();?>" class="post-title">
		<span class="title-h2"><?php relevanssi_the_title(); ?></span>
	</a>
	<div class="fonction"><?php echo get_field('fonction'); ?></div>
	<div class="description"><?php the_content(); ?></div>
	<div class="social-links">
		<?php if(get_field('link_facebook')) { ?><span class="link_facebook sprite"><a rel="nofollow" href="<?php echo get_field('link_facebook'); ?>" target="_blank">Facebook</a></span> <?php } ?>
		<?php if(get_field('link_siteweb')) { ?><span class="link_siteweb sprite"><a rel="nofollow" href="<?php echo get_field('link_siteweb'); ?>" target="_blank">Site Web</a></span> <?php } ?>
		<?php if(get_field('link_linkedin')) { ?><span class="link_linkedin sprite"><a rel="nofollow" href="<?php echo get_field('link_linkedin'); ?>" target="_blank">Linkedin</a></span> <?php } ?>
	</div>
</div>