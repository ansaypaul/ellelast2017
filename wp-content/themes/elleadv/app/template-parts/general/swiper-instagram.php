<?php global $excludeIDS; ?>
<?php $instagrams = get_posts(array('posts_per_page' => 6,'post_type' => 'instagram')); ?>
<?php if($instagrams){ ?>
<div class="swiper-instagram-block">
	<div class="swiper-instagram">
		<div class="swiper-instagram-wrapper">
			<div class="swiper-container-instagram">
				<div class="swiper-wrapper">
				<?php foreach ( $instagrams as $post ) : setup_postdata( $post ); ?>
				<?php $excludeIDS[] = $post->ID; ?>
					<div class="swiper-slide">
						<!-- Changer logo en fonction de la catégorie insta mise en avant! Rajouter une class à swiper logo avec le nom de la cat -->
						<div class="swiper-logo">
							<!-- linker vers la cat concernée sur insta https://www.instagram.com/explore/tags/ellefoodteam/ -->
							<a rel="noopener" target="_blank" class="<?php echo get_field('categorie_instagram',$post->ID); ?>" href="<?php echo "https://www.instagram.com/explore/tags/".get_field('categorie_instagram',$post->ID); ?>"></a>
						</div>
						<!-- Récupérer la photo en format carré -->
						<div class="swiper-image">
							<a rel="noopener" target="_blank" href="<?php echo get_field('source',$post->ID); ?>"><?php the_post_thumbnail('instagram',array('lazy',0,0)); ?></a>
						</div>
						<!-- Catcher le nom d'utilisateur de la photo -->
						<div class="swiper-author"><a rel="noopener" target="_blank" href="<?php echo "https://www.instagram.com/".get_field('auteur',$post->ID); ?>" ><?php echo get_field('auteur',$post->ID); ?></a></div>
						<!-- Catcher le contenu du post insta -->
						<div class="swiper-description"><?php echo substr(get_field('description',$post->ID),0,330); ?></div>
					</div>
				<?php endforeach; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<div class="swiper-instagram-static">
			<span class="title-h4"><?php _e( "Les posts", 'html5blank' ); ?><div class="sprite"></div><span>Instagram</span><?php _e( "de la semaine", 'html5blank' ); ?></span>
			<ul>
				<!-- <li>#ellefashionteam</li>
				<li>#ellebeautyteam</li> -->
				<li>#ellefoodteam</li>
			</ul>
			<p><?php _e( "Rejoignez nos teams", 'html5blank' ); ?><br><?php _e( "d’influenceuses!", 'html5blank' ); ?></p>
		</div>
	</div>
	<div class="sprite swiper-button-prev swiper-button-prev-insta sprite"><span></span></div>
	<div class="sprite swiper-button-next swiper-button-next-insta sprite"><span></span></div>
	<div class="swiper-pagination swiper-pagination-insta"></div>
</div>
<?php } ?>