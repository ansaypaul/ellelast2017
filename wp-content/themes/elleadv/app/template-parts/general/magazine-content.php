<?php 
global $last_magazine;
foreach ( $last_magazine as $post ) : setup_postdata( $post ); 
?>
<!-- template-parts / footer-magazine -->
<div class="template-parts footer-magazine">
	<div class="block-left">
		<?php 
			$img_html = "<img src='". get_the_post_thumbnail_url($last_magazine[0]->ID)."' alt='ELLE Cover' />";
			$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
			echo $img_html;
		?>
		<?php if($_theme == 'elle'){ ?>
		<a target="_blank" href="<?php echo get_field("magazine_viapress",$last_magazine[0]->ID ); ?>" class="mag-abo"><?php _e( "Abonnez-vous au magazine", 'html5blank' ); ?></a>
		<?php } ?>
	</div>

	<div class="block-right">
		<span class="title-h3">
			<?php the_title(); ?>
		</span>
		<div class="block-content">
			<?php the_content(); ?>
		</div>
		<div class="block-1">
			<?php 
			$img_html = "<img src='".get_field('thumb_images')[0]['image']['sizes']['thumbnail']."' alt='".get_field('thumb_images')[0]['extrait']."' />";
			$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
			echo $img_html;
			?>
			<p class="block-page">p.<?php echo get_field('thumb_images')[0]['page'];?></p>
			<p class="block-p-content"><?php echo get_field('thumb_images')[0]['extrait'];?></p>
		</div>
		<div class="block-2">
			<?php 
			$img_html = "<img src='".get_field('thumb_images')[1]['image']['sizes']['thumbnail']."' alt='".get_field('thumb_images')[1]['extrait']."' />";
			$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
			echo $img_html;
			?>
			<p class="block-page">p.<?php echo get_field('thumb_images')[1]['page'];?></p>
			<p class="block-p-content"><?php echo get_field('thumb_images')[1]['extrait'];?></p>
		</div>
		<div class="block-3">
			<?php 
			$img_html = "<img src='".get_field('thumb_images')[2]['image']['sizes']['thumbnail']."' alt='".get_field('thumb_images')[2]['extrait']."' />";
			$img_html = apply_filters( 'bj_lazy_load_html', $img_html ); 
			echo $img_html;
			?>
			<p class="block-page">p.<?php echo get_field('thumb_images')[2]['page'];?></p>
			<p class="block-p-content"><?php echo get_field('thumb_images')[2]['extrait'];?></p>
		</div>
	</div>

	<!-- <p><a href="<?php echo get_field('magazine_pdf')['url'] ?>">Télécharger PDF</a></p> -->

</div>
<?php endforeach; wp_reset_postdata(); ?>
<!-- template-parts / footer-magazine -->