<!-- health-inset -->
<div class="health-inset">
	<h3>
		<a href="<?php get_field('health','option')['link'] ?>" rel="bookmark"><?php echo get_field('health','option')['title'] ?></a>
	</h3>
	<!-- changer ID santé -->
	<?php 
	$posts_health = get_posts(array('posts_per_page' => 3, 'tag__in' => get_field('health','option')['health_tags'] , 'order' => 'DESC')) ?>
	<ul class="health-titles" >
		<?php foreach ( $posts_health as $post ) : setup_postdata( $post ); ?>
			<li>
				<h4>
					<a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
				</h4>
			</li>
		<?php endforeach; wp_reset_postdata(); ?>
	</ul>
</div>
<!-- most-popular -->