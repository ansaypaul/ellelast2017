<?php 
global $post;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
$sous_categorie = $list_categorie[0];
$tierce_categorie = $list_categorie[0];
if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
	$main_categorie = $list_categorie[0];
	$sous_categorie = $list_categorie[0];
}

if(count($list_categorie) > 1 ){
	if(in_array($list_categorie[0]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[0];
		$sous_categorie = $list_categorie[1];
	}

	if(in_array($list_categorie[1]->cat_ID, category_principales())) { 
		$main_categorie = $list_categorie[1];
		$sous_categorie = $list_categorie[0];
	}
}

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
if($sous_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
if($main_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
?>
<div class="main-content-wrapper content-wrapper" data-io-article-url="<?php the_permalink(); ?>">
	<div class="content" data-tag="<?php echo $main_tag ?>" data-cat="<?php echo $sous_categorie->cat_ID; ?>">
	<!-- article -->
	<article id="post-<?php the_ID(); ?>" data-id = "<?php the_ID(); ?>" 
	data-cat = "" data-title="<?php the_title(); ?>" data-adposition="<?php echo mb_strtoupper(ad_position()['cat']); ?>" 
	data-adpositionclean = "<?php echo ad_position()['cat']; ?>" data-adpositionbis="<?php echo html_entity_decode(ad_position()['subcat']); ?>"
	data-author = "<?php the_author(); ?>" data-tag="<?php echo html_entity_decode(ad_position_ter()); ?>" data-link="<?php the_permalink(); ?>">
			<header class="entry-header">
			<!-- post-category -->
			<div class="post-category">
				<?php if(isset($main_categorie)){ ?>
					<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
						<?php echo $main_categorie->name; ?>
					</a>
				<?php } ?>
				<?php if(isset($sous_categorie) && ($main_categorie->cat_ID != $sous_categorie->cat_ID)){ ?>
					<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
				<?php } ?>
			</div>
			<!-- post-title -->
			<h1 property="headline">
				<a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
			</h1>
			<!-- post-details -->
			<div class="post-details">
				<span class="date">	
					<?php $str_update =  __( 'Publié le', 'html5blank' ); ?>
					<?php if(get_the_modified_time('Y-m-j') !=  get_the_time('Y-m-j')){ $str_update = __( 'Mis à jour le', 'html5blank' ); } ?>
						<span class="dt-modified" property="dateModified" content="<?php the_modified_time('Y-m-j'); ?>"> <?php echo $str_update; ?> <?php the_modified_time('j F Y'); ?></span>
						<span class="dt-published hidden" property="datePublished" content="<?php the_time('Y-m-j'); ?>"> <?php the_time('l, j F Y'); ?>
						</span>
				</span>
				<span class="author vcard p-author h-card" property="author" typeof="Person"><?php _e( 'par', 'html5blank' ); ?>
					<?php $credits_auteur_initial = get_field( "credits_auteur_initial" );
					if ( $credits_auteur_initial ) { ?>
						<span class="credit-trad">  <?php echo $credits_auteur_initial; ?> <?php _e( 'et', 'html5blank' ); ?> </span>
					<?php } ?>
			      	<span property="name" class="hidden"><?php the_author(); ?></span>
			      	<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author(); ?></a>
			      	<?php if( $authortitle = get_the_author_meta( 'authortitle' ) ) { ?>
						<span class="author-title hidden" property="jobTitle"><?php echo $authortitle; ?></span>
					<?php } ?>
					
					<?php $credits_link = get_field( "credits_link" );
					$credits_photographe = get_field( "credits_photographe" );
					if ( $credits_photographe ) { ?>
						<span class="credit-photo"><?php _e( 'Photos', 'html5blank' ); ?>: 
							<?php if ( $credits_link ) { ?>
								<a href="<?php echo $credits_link; ?>" target="_blank">
							<?php } ?>
							<?php echo $credits_photographe; ?></span>
							<?php if ( $credits_link ) { ?>
								</a>
							<?php } ?>
					<?php } ?>
				</span>
				<div property="publisher" typeof="Organization" class="hidden">
					<span property="name"><?php _e( 'ELLE Belgique', 'html5blank' ); ?></span>
					<div property="logo" typeof="ImageObject">
					    <link property="url" href="<?php echo home_url(); ?>/img/icons/ellebelgique.png" />
					    <meta property="height" content="125" />
					    <meta property="width" content="320" />
					</div>
				</div>
			</div>
			<!-- post-thumbnail -->
			<?php if(has_post_thumbnail() && $display_thumb ){ // Check if Thumbnail exists ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
					<?php the_post_thumbnail('content2018',array('',0,1,1)); ?>
				</a>
				<a href="<?php echo get_the_post_thumbnail_url();?>" property="image" class="hidden"></a>
			<?php } ?>
			<!-- /post-thumbnail -->
			</header>

			<!-- <div id="pebbleInarticle"><script type="text/javascript"> adhese.tag({ format: "Inarticle", publication:"elle-fr", location: "others"});</script></div>- -->
			<!-- single-sidebar-related -->

			<div class="entry-content e-content" property="articleBody">
			<?php
				$content_replace = $post->post_content;
				$content_replace = str_replace('http://www.elle.be/','https://www.elle.be/',$content_replace);
				$content_replace = str_replace('http://quizz.elle.be/','https://quizz.elle.be/',$content_replace);
				$content_replace = str_replace('noopener', '', $content_replace);
			?>
				<?php echo apply_filters( 'the_content', $content_replace );  ?>
				<?php if(get_field('liste_article')){ foreach (get_field('liste_article') as $key=>$article) { ?>
						<div class="top-article">
							<h2><a href="<?php the_permalink($article['article']->ID); ?>"><?php echo $article['article']->post_title; ?></a></h2>
							<a href="<?php the_permalink($article['article']->ID); ?>" class="post-thumbnail">
							<?php echo get_the_post_thumbnail($article['article']->ID,'content2018',array('',0,1,1)); ?>
							</a>
							<?php 
							$recipe_intro = get_field('recipe-intro',$article['article']->ID);
							$diy_intro = get_field('diy-intro',$article['article']->ID);	
							if($recipe_intro != ''){
								echo $recipe_intro;
							}elseif($diy_intro != ''){
								echo $diy_intro;
							}else{
								echo get_first_paragraph($article['article']->ID);
							}
							?>
							<a href="<?php the_permalink($article['article']->ID); ?>" class=link-top-article><?php _e( "Lire la suite", 'html5blank' ); ?></a>
						</div>
				<?php } } ?>
				<div class="site-breadcrumb" typeof="BreadcrumbList" vocab="http://schema.org/">
					<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				</div>
			</div>
			<footer class="entry-footer">
				<span class="tags-wrapper">
					<?php $post_tags = get_the_tags();
					if ( $post_tags ) { ?>
						<span class="site-tags">Tags: </span>
						<span class="tags-list">
							<?php
							$list_tags_str = '';
						    foreach( $post_tags as $tag ) {
						    	$list_tag_link = "<a href='".home_url()."/tag/".$tag->slug."' property='keywords' typeof='text'>".ucfirst($tag->name)."</a>";
						    	$list_tags_str .= $list_tag_link.', '; 
						    } 
						    $list_tags_str = substr($list_tags_str, 0, -2).'.';
						    echo $list_tags_str;
						    ?>
						</span>
					<?php } ?>
				</span>
				<div id="social-share">
					<?php display_single_RS(); ?>
				</div>
			</footer>
			<?php  // comments_template(); ?>
	</article>
	<!-- /article -->
	<!--<div id="pebbleInread" style="display:block;"></div>-->
	<div id='pixel_client'><?php $pixel_client = get_field('pixel_client'); echo $pixel_client ?></div>
	</div>
	<!-- sidebar -->
	<aside class="sidebar" role="complementary">
		<!-- single-sidebar-related -->
		<div id="halfpage-<?php display_id_random()?>" class="ads ads-first halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="homepage" data-position=""></div>
		<?php if(get_field('identifiant_youtube','option')){ ?>
			<!-- (1) video wrapper -->
			<div class="sidebar_youtube" data-embed="<?php echo get_field('identifiant_youtube','option'); ?>" data-picture="<?php echo get_field('image_de_couverture','option'); ?>"> 		 
			    <!-- (2) the "play" button -->
			    <div class="play-button"></div> 		     
			</div>
		<?php } ?>
		<?php 
			$posts = get_posts(array('posts_per_page' => 6, 'category'=> $main_categorie->cat_ID, 'orderby' => 'rand', 'order' => 'DESC','date_query' => array(array('after' => '30 days ago')), 'post__not_in' => $excludeIDS));
			_sidebar_article_block($posts,$main_categorie->cat_ID);
		?>
		<div id="halfpage-<?php display_id_random()?>" class="ads halfpage" categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="" data-position="2"></div>
	</aside>
	<!-- /sidebar -->
</div>

<?php get_template_part('template-parts/general/article-related-bottom');  ?>

<!-- Intégration LIGATUS -->			
<?php 
if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
	echo '<div id="lig_elle_be_fr_sb_article"></div>';
}else if(get_field('variable_js','option')['lang_identifiant'] == 'NL'){
	echo '<div id="lig_elle_be_be_sb_article"></div>';
}
?>
<!--<div id="elleRA2MW" class="elleRA2MW"></div>-->

<!-- Intégration LIGATUS -->
 <div class="site-inner no_print">
	<div id="billboard-<?php display_id_random(); ?>" class="ads leaderboard" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position="2"></div>
</div>