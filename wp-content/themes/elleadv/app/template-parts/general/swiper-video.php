<?php global $excludeIDS; ?>
<?php 
$posts = get_posts(array('posts_per_page' => 9, 'cat'=> array($_POST['elletv']) , 'post__not_in' => $excludeIDS ));
if(isset($_POST['category'])){
	$category = get_queried_object(); 
	$posts = get_posts(array('posts_per_page' => 9, 'category__and'=> array($_POST['elletv'], $_POST['category']), 'post__not_in' => $excludeIDS));
} 
?>
<?php if($posts){ ?>
<div class="swiper-video-block">
	<a href="<?php echo get_category_link(id_video_category()); ?>" class="swiper-article-title"><?php _e( '+ Vidéo', 'html5blank' ); ?></a>
	<div class="swiper-video swiper-video-<?php echo id_video_category(); ?>">
		<div class="swiper-container-video">
			<div class="swiper-wrapper">
			<?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
			<?php $excludeIDS[] = $post->ID; ?>
				<div class="swiper-slide">
					<div class="swiper-image">
						<a href="<?php the_permalink($post->ID); ?>">
							<?php the_post_thumbnail('content2018',array('',0,0)); ?>
						</a>
					</div>
					<div class="swiper-titre">
						<h2><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
					</div>
					<!-- changer dynamiquement -->
					<!-- <a href="#" class="sprite sprite-social"></a>
					<a href="#" class="sprite sprite-pinit"></a> -->
				</div>
			<?php endforeach; wp_reset_postdata(); ?>
			</div>
		</div>
		<div class="swiper-button-prev swiper-button-prev-video sprite"><span></span></div>
		<div class="swiper-button-next swiper-button-next-video sprite"><span></span></div>
	</div>
</div>
<?php } ?>