<?php 
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
if(count($list_categorie) > 1 ){
	$sous_categorie = $list_categorie[1];
}

?>
<article class="article-design-1">
	<a href="<?php the_permalink(); ?>" class="post-thumbnail">
		<?php the_post_thumbnail('news-thumb'); ?>
	</a>
	<div class="post-side">
		<div class="post-category">
			<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
				<?php echo $main_categorie->name; ?>
			</a>
			<!-- <?php if(isset($sous_categorie)){ ?>
				<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
			<?php } ?> -->
		</div>
	    <a href="<?php the_permalink();?>" class="post-title">
	        <span class="title-search">
	        	<?php the_title(); ?>
	        </span>
	    </a>
	    <a href="#" class="sprite sprite-social"></a>
	    <<!-- a href="#" class="sprite sprite-pinit"></a> -->
	</div>
</article>