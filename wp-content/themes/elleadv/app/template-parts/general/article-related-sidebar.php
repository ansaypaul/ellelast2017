<!-- single-sidebar-related -->
<?php global $excludeIDS; global $main_categorie; ?>
<?php 
	$title_related = $main_categorie->name; 
	if($title_related == ''){ $title_related = _e( " d'article", 'html5blank' ); }

	?>
<div id="halfpage-<?php display_id_random()?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
<div class="sidebar-related">
	<a href="#"><span title="titre-h3">+ <?php echo $title_related; ?></span></a>
	<div class="related-article">
	<?php
		$args = array('numberposts'=> 5, 'category'=> $main_categorie->cat_ID,'orderby'   => 'rand', 'date_query' => array(array('after' => '90 days ago')));
	    $posts = get_posts($args); 
	    if (count($posts) > 0 ) :
	        $current_post = 0;
	        foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;
	          get_template_part('template-parts/general/article-related-block');
	        endforeach; wp_reset_postdata(); 
	    else :
	    	if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
	            echo "Aucun article n'est disponible !";
	        }
	        else if(get_field('variable_js','option')['lang_identifiant'] == 'NL'){
	            echo "Geen artikel beschikbaar !";
	        } 
	        else{
	            echo "Aucun article n'est disponible !";
	        } 

	  	endif;
    ?>
	</div>
</div> 