<!-- template-parts / social -->
	<ul class='social-links'>
		<li>
			<a href="<?php echo get_field('socials_links_deco','option')['facebook']; ?>" target="_blank" class="sprite facebook sprite-fb" rel="nofollow"></a>
		</li>
		<li>
			<a href="<?php echo get_field('socials_links_deco','option')['twitter']; ?>" target="_blank" class="sprite twitter sprite-twitter" rel="nofollow"></a>
		</li>
		<li>
			<a href="<?php echo get_field('socials_links_deco','option')['instagram']; ?>" target="_blank" class="sprite instagram sprite-instagram" rel="nofollow"></a>
		</li>
		<li>
			<a href="<?php echo get_field('socials_links_deco','option')['youtube']; ?>" target="_blank" class="sprite youtube sprite-youtube" rel="nofollow"></a>
		</li>
		<li>
			<a href="<?php echo get_field('socials_links_deco','option')['pinterest']; ?>" target="_blank" class="sprite pinterest sprite-pint" rel="nofollow"></a>
		</li>
	</ul>
<!-- template-parts / social -->