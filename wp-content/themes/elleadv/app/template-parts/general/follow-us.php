<!-- template-parts / follow-us -->
<?php global $_theme; ?>
<div class="template-parts follow-us">
	<span><?php _e( 'Suivez-nous', 'html5blank' ); ?></span>
	<?php 
	if($_theme == 'elle'){ 
		get_template_part('template-parts/general/social-links'); }
	else{ 
		get_template_part('template-parts/general/social-links-eat');
	} 
	?>
	<form method="post" action="<?php echo get_site_url(); ?>/newsletters">
	   <input type="text" name="footer_email" placeholder="<?php _e( 'Votre e-mail', 'html5blank' ); ?>">
	   <input type="submit" value="<?php _e( "S'inscrire", 'html5blank' ); ?>" class="submit sprite">
	</form>
</div>
<!-- template-parts / follow-us -->