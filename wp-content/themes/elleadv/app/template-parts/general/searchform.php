<?php 
/*
$category_search a adapter en fonction des catgories choisies (rendre dynamique ? option ACF ?)
*/
$category_search = category_principales();
$search_referer = "global";

if(is_category('recettes') || is_category('recepten')){
    $search_referer = 'recipe';
 }else if(is_category('adresses') || is_category('hotspots')){
	$search_referer = 'adress';
}

if(isset($_GET['search_referer'])){ $search_referer = $_GET['search_referer']; }
?>
<!-- search -->
<form class="" role="search"  action="<?php echo get_home_url();?>/" method="get">
	<div class="site-inner">
		<div class="search-input-wrapper">
			<label for="search_input"><?php _e('Recherche', 'html5blank' ); ?>
				<input type="hidden" name="search_referer" value="<?php echo $search_referer; ?>">
				<?php 
					if(is_category()){
						$category = get_queried_object();
						echo '<input type="hidden" name="category[]" value="'.$category->term_id.'">';
					}
				?>
				<input id="search_input" autocomplete="off" class="input_ajax search-input" type="text" name="s" placeholder="<?php _e('Rechercher un article', 'html5blank' ); ?>" value="<?php echo $_GET['s']; ?>" >
				<input type="submit" class="custom-cta"  value="<?php _e( "Rechercher", "html5blank" ); ?>"></span>
			</label>
		</div>
	</div>
</form>
<!-- /search -->