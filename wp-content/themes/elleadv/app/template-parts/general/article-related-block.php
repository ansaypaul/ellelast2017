<?php global $current_post ?>
<div class="related-container-article">
		<div class="related-image">
			<span class="post-thumbnail atc" data-atc="<?php _encode_base64(get_the_permalink($post->ID)); ?>">
				<?php the_post_thumbnail('sidebar_thumb',array('lazy',0,0)); ?>
			</span>
		</div>
		<div class="related-titre">
			<a href="<?php the_permalink(); ?>" class="post-thumbnail"><span class="title-h4"><?php echo $post->post_title; ?></span></a>
		</div>
</div>
<?php if ($current_post%7 == 0){ ?> 
	<div id="halfpage-<?php display_id_random()?>" class="ads halfpage" categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
<?php } ?>