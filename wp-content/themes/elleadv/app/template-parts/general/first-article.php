<?php global $category, $excludeIDS, $main_categorie; $id_first_article ?>
<div class="first-article">
	<?php 
		if(is_home()){  
			$top_post = get_posts(array('posts_per_page' => 1));
		} 

		if(is_tag()){  
			$tag_id = get_query_var('tag_id');
			$top_post = get_posts(array('posts_per_page' => 1, 'tag__in' => array($tag_id)));
		} 

		if(is_category()){ 
			$category = get_queried_object();
			$top_post = get_posts(array('posts_per_page' => 1, 'category'=> $category->term_id));
		} 

		if(is_single()){
			$top_post = get_posts(array('posts_per_page' => 1));
		} 

		if(is_author()){
			//$top_post = get_posts(array('posts_per_page' => 1, 'category'=> $main_categorie->cat_ID));
		} 
	?>

	<?php foreach ( $top_post as $post ) : setup_postdata( $post ); ?>

		<?php 
		$author_name = get_the_author_meta('display_name', $post->post_author);
		$author_link =  get_author_posts_url( $post->post_author); 

		$list_categorie = get_the_category(); 
		$main_categorie = $list_categorie[0];
		if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
			$main_categorie = $list_categorie[0];
		}

		if(count($list_categorie) > 1 ){
			if (in_array($list_categorie[1]->cat_ID, category_principales())) { 
				$main_categorie = $list_categorie[1];
				$sous_categorie = $list_categorie[0];
			}
		}

		?>

		<?php $excludeIDS[] = $post->ID; ?> 
		<article class="article-design-1 first-article-1" data-cat="<?php echo $main_categorie->cat_ID ?>">
			<a href="<?php the_permalink(); ?>" class="post-thumbnail">
				<?php the_post_thumbnail('content2018',array('',1,0)); ?>
			</a>
			<div class="post-side">
				<div class="post-side-inner">
					<div class="post-category" >
						<a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
							<?php echo $main_categorie->name; ?>
						</a>
						<!-- <?php if(isset($sous_categorie)){ ?>
							<a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
						<?php } ?> -->
					</div>
			    	<?php if (is_home()) { ?>
			    		<h1>
				        	<a href="<?php the_permalink();?>" class="post-title"><?php the_title(); ?></a>
				        </h1>
				    <?php  } else{ ?>
						<h2>
				        	<a href="<?php the_permalink();?>" class="post-title"><?php the_title(); ?></a>
				        </h2>
			    	<?php } ?>
				    <!-- Rajouter l'auteur en dynamique -->
				    <div class="post-author"><span><?php _e( 'par', 'html5blank' ); ?></span><a href="<?php echo $author_link ?>" class="post-author"><?php echo ucfirst($author_name); ?> </a></div>
				    <?php display_RS(); ?>
				    <!-- <a href="#" class="sprite sprite-pinit"></a> -->
			    </div>
			</div>
		</article>
	<?php endforeach; wp_reset_postdata(); ?>
</div>