<!--related article-->
<?php global $excludeIDS, $main_categorie; $current_post = 0;  ?>
<div class="related-article-block">
	<a class="swiper-article-title">+ <?php echo get_cat_name($_POST['cat']); ?></a>
	<div class="related-article">
		<?php $posts = get_posts(array('posts_per_page' => 5, 'category'=> $_POST['cat'] , 'post__not_in' => $excludeIDS )); ?>
			<?php foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++  ?>
				<?php if($current_post != 1){ ?>
					<?php $excludeIDS[] = $post->ID; ?>
					<div class="related-container-article">
							<div class="related-image"><a href="<?php the_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail('content2018mid',array('lazy',0,0)); ?></a></div>
							<div class="related-titre"><h2><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2></div>
					</div>
				<?php } ?>
			<?php endforeach; wp_reset_postdata(); ?>	
	</div>
</div> 
<!--related article-->