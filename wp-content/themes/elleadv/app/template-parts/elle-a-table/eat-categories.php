<!-- EAT Catégories -->
<div class="eat-categories">
	<span class="eat-categories-title"><?php _e( 'Catégories', 'html5blank' ); ?></span>
	<ul class="eat-categories-items" >
	<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
		<li><a href="https://www.elle.be/fr/tag/apero">Apéro</a></li>
		<li><a href="https://www.elle.be/fr/tag/entree">Entrée</a></li>
		<li><a href="https://www.elle.be/fr/tag/en-cas">En-cas</a></li>
		<li><a href="https://www.elle.be/fr/tag/plat">Plat</a></li>
		<li><a href="https://www.elle.be/fr/tag/dessert">Dessert</a></li>
		<li><a href="https://www.elle.be/fr/tag/Cocktail">Cocktail</a></li>
	<?php } else { ?>
		<li><a href="https://www.elle.be/nl/tag/aperitief">Aperitief</a></li>
		<li><a href="https://www.elle.be/nl/tag/voorgerecht">Voorgerecht</a></li>
		<li><a href="https://www.elle.be/nl/tag/snack">Snack</a></li>
		<li><a href="https://www.elle.be/nl/tag/hoofdgerecht">Hoofdgerecht</a></li>
		<li><a href="https://www.elle.be/nl/tag/dessert">Dessert</a></li>
		<li><a href="https://www.elle.be/nl/tag/cocktail">Cocktail</a></li>
	<?php } ?>
	</ul>
</div>
<!-- EAT Catégories -->