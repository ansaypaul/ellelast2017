<!-- adresses Catégories-->
<div class="eat-adresses">
	<span class="eat-categories-title"><?php _e( "Carnet d'adresses", 'html5blank' ); ?></span>
	<ul class="eat-categories-items" >
	<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
		<li><a href="https://www.elle.be/fr/tag/boutique">Boutique</a></li>
		<li><a href="https://www.elle.be/fr/tag/restaurant">Restaurant</a></li>
		<li><a href="https://www.elle.be/fr/tag/bar">Bar</a></li>
		<li><a href="https://www.elle.be/fr/tag/hotel">Hôtel</a></li>
	<?php } else { ?>
		<li><a href="https://www.elle.be/nl/tag/boetiek">Boetiek</a></li>
		<li><a href="https://www.elle.be/nl/tag/restaurant">Restaurant</a></li>
		<li><a href="https://www.elle.be/nl/tag/bar">Bar</a></li>
		<li><a href="https://www.elle.be/nl/tag/hotel">Hotel</a></li>
	<?php } ?>
	</ul>
</div>
<!-- adresses Catégories -->