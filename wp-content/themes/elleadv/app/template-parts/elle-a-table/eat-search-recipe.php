<!-- EAT Search Recipes -->
<div class="eat-search-recipe">
	<span class="eat-search-recipe-title"><?php _e( 'Trouver une recette', 'html5blank' ); ?></span>
	<ul class="eat-search-recipe-items" >
		<li>
			Type de recette?
		</li>
		<li>
			Difficulté?
		</li>
		<li>
			Recherche
		</li>
	</ul>
</div>
<!-- EAT Search Recipes -->