<!-- EAT most-popular -->
<?php 
$time_most_popular = '99999 days ago';
$posts_popular = get_posts(array('posts_per_page' => 3, 'category_name' => 'recettes', 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)))); 
//$posts_popular = get_posts(array('posts_per_page' => 3)); 

?>
<div class="eat-most-popular">
	<span class="eat-most-popular-title"><?php _e( 'Le top des recettes', 'html5blank' ); ?></span>
	<ul class="eat-most-popular-items" >
		<?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); ?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
		<?php endforeach; wp_reset_postdata(); ?>
	</ul>
</div>
<!-- EAT most-popular -->