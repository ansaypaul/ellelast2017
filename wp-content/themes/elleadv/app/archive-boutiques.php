<?php get_header(); ?>

<main role="main" class="main main-category main-archive shop">
	<div class="site-inner">
		<h1><?php _e('Nos boutiques'); ?></h1>
		<div class="letter-sorting">
			<a rel="nofollow" rel="nofollow" href="?l=TOUTES"><?php _e( 'Toutes', 'html5blank' ); ?></a> | <a rel="nofollow" href="?l=A">A</a><a rel="nofollow" href="?l=B">B</a><a rel="nofollow" href="?l=C">C</a><a rel="nofollow" href="?l=D">D</a><a rel="nofollow" href="?l=E">E</a><a rel="nofollow" href="?l=F">F</a><a rel="nofollow" href="?l=G">G</a><a rel="nofollow" href="?l=H">H</a><a rel="nofollow" href="?l=I">I</a><a rel="nofollow" href="?l=J">J</a><a rel="nofollow" href="?l=K">K</a><a rel="nofollow" href="?l=L">L</a><a rel="nofollow" href="?l=M">M</a><a rel="nofollow" href="?l=N">N</a><a rel="nofollow" href="?l=O">O</a><a rel="nofollow" href="?l=P">P</a><a rel="nofollow" href="?l=Q">Q</a><a rel="nofollow" href="?l=R">R</a><a rel="nofollow" href="?l=S">S</a><a rel="nofollow" href="?l=T">T</a><a rel="nofollow" href="?l=U">U</a><a rel="nofollow" href="?l=V">V</a><a rel="nofollow" href="?l=W">W</a><a rel="nofollow" href="?l=X">X</a><a rel="nofollow" href="?l=Y">Y</a><a rel="nofollow" href="?l=Z">Z</a>		
		</div>
		<div class="content entry-content">
			<?php 
			$boutiques = get_posts(array( 'post_type' => 'boutiques','orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1));  
			if( $boutiques ){ 
				foreach( $boutiques as $post ) : setup_postdata( $post ); 
					$first_letter = strtoupper(substr($post->post_title,0,1));
      				if((isset($_GET['l']) && ($first_letter == strtoupper($_GET['l']) || $_GET['l'] == "TOUTES")) || !isset($_GET['l'])) { 
						$shop_address = get_field( 'shop_address' ); 
						$shop_telephone = get_field( 'shop_telephone' ); 
						$shop_texte = get_field( 'shop_texte' ); 
						$shop_url = get_field( 'shop_url' ); 
						?>
						<div class="shop-item">
							<span class="title-h3">
								<?php the_title(); ?>
							</span>
							<?php
							 if( $shop_address ) { 
								if( have_rows('shop_address') ):
								    while ( have_rows('shop_address') ) : the_row();
								    	echo "<address>";
								        the_sub_field('shop_address_item');
								        echo "</address>";
								    endwhile;
								endif; 
							 } 
							 ?>
							<?php  if( $shop_telephone ) { ?>
								<a rel="nofollow" href="tel:<?php echo $shop_telephone; ?>" class="shop-tel"><?php echo $shop_telephone; ?></a>
							<?php } ?>
							<div>
								<?php  if( $shop_texte ) { ?>
									<span><?php echo $shop_texte; ?> </span>
								<?php } ?>
								<?php  if( $shop_url ) { ?>
									<a rel="nofollow" rel="nofollow" href="<?php echo $shop_url; ?>" class="shop-url"><?php echo $shop_url; ?></a>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					<?php endforeach; wp_reset_postdata(); ?>
			<?php } ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
 