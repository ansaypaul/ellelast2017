<?php
/**
Template Name: Auteur Stat
 */
$users = new ArrayObject();   
$lang_identifiant =  get_field('variable_js','option')['lang_identifiant']; 
if($lang_identifiant == 'FR'){
	$users = array(3,4,8,13,65,51,6,71,46,73);
}else{
	$users = array(4,5,59,52,24,27,28,25,26);
}

$years = array(2017,2018);
$months = array(1,2,3,4,5,6,7,8,9,10,11,12);
$months_years = array('04:2018','05:2018','06:2018');
$months = array(1,2,3);
$full_article = array(); 

class User // Présence du mot-clé class suivi du nom de la classe.
{
  public $_articles = array();        
  public $_firstname = '';
  public $_countArticle ='';
  public $_quota = 0;
  public $_ID = 0;
  public $_totalArticle = 0;
  public $_totalPageview = 0;

  public function set_user($_firstname,$_quota,$_ID){
	$this->_firstname = $_firstname;
	$this->_ID = $_ID;
	$this->_quota = $_quota;
  }

  public function count_article($months_years,$ID){
  		foreach ($months_years as $month){

  			$data = explode(":",$month);
			$month = $data[0];
			$year =  $data[1];

			$args = array(
			'year' => $year,
			'monthnum' => $month,
			'posts_per_page' => -1,
			'author'=> $ID,
			'post_status' => 'publish');
			$posts = query_posts($args);
			$count = count($posts);
			$pageview = 0;

			foreach ( $posts as $post ) : setup_postdata( $post );
			$pageview += get_post_meta($post->ID, 'wpb_post_views_count', true);
			endforeach; wp_reset_postdata();

			$this->_articles[$year][$month]['pageview'] = $pageview;
			$this->_articles[$year][$month]['count'] = $count;
			$this->_totalArticle += $count;
			$this->_totalPageview += $pageview;
			}	
		}
}


$blogusers = get_users_of_blog();
foreach ($blogusers as $bloguser) 
{
	if(in_array($bloguser->user_id, $users)){
	$userdata = get_userdata($bloguser->user_id); 
	$user = new User;
	$user->set_user($userdata->user_firstname.' '.$userdata->user_lastname, get_usermeta( $bloguser->user_id, 'quota' ) ,$bloguser->user_id);
	$user->count_article($months_years,$bloguser->user_id);
	$users[] = $user;
	}
}
/*
echo '<pre>';
var_dump($users);
echo '<pre>';*/
 
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  crossorigin="anonymous"/>

<style>
.site {max-width: 100% !important;}
table {
color: #333;
font-family: Helvetica, Arial, sans-serif;
width: 640px;
}

td, th {
height: 30px;
transition: all 0.3s; /* Simple transition for hover effect */
}

th {
background: #DFDFDF; /* Darken header a bit */
font-weight: bold;
text-align: center;
}

td {
background: #FAFAFA;
text-align: center;
border: 1px solid black;
}

/* Cells in even rows (2,4,6...) are one color */
/*tr:nth-child(even) td { background: #F1F1F1; }*/

/* Cells in odd rows (1,3,5...) are another (excludes header cells) */
/*tr:nth-child(odd) td { background: #FEFEFE; }*/

/* tr td:hover { background: #666; color: #FFF; } /* Hover cell effect! */ */
/*tr:hover { background-color: green !important;}*/


/*
.green {color:green;}
.red {color:red;}*/

thead:first-child {border-bottom: 3px solid black !important}

tr th, tr td:nth-child(n+2):nth-child(3n+3), tr td:nth-child(2) { border-left: 3px solid black !important;}

table { border: 3px solid black !important;}

</style>

</head>
<body>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="table-responsive">
		<table class="table">
		<thead>
		<tr>
		<th width="200px">Auteur</th>
		<th width="200px">Quota / mois</th>
		<?php 
		foreach ($months_years  as $month) 
		{
				$data = explode(":",$month);
				$month = $data[0];
				$year =  $data[1];
				echo '<th colspan="3">'.$month.'/'.$year.'</th>';

		}
		?>
		<th>Total</th>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach ($users as $user) 
		{
			if($user->_totalArticle > 0 ){
				echo '<tr><td>'.$user->_firstname.'</td><td>'.$user->_quota.'</td>';
				foreach ($years as $year) {
					foreach ($user->_articles[$year] as $article) {
						if($article > 0 ){
							$ratio = $article['pageview'] / $article['count'];
							if($article['count']< $user->_quota ) {
								echo '<td class="red">'.$article['count'].'</td><td>'.$article['pageview'].'</td><td>'.(int)$ratio.'</td>';
							}else{
								echo '<td class="green">'.$article['count'].'</td><td>'.$article['pageview'].'</td><td>'.(int)$ratio.'</td>';
							}
						}else
						{
								echo '<td>0</td>';
						}
					}
					
				}
				echo '<td>'.$user->_totalArticle.'</td>';
				echo '</tr>';
			}
		}

		echo '</tr>';
		echo '<tr><td><b>Total : </b></td><td></td>';
			foreach ($months_years  as $month){
				$data = explode(":",$month);
					$month = $data[0];
					$year =  $data[1];
				foreach ($users as $user) {
					$total_month += $user->_articles[$year][$month]['count'];
					$total_full += $user->_articles[$year][$month]['count'];
					$total_pageview += $user->_articles[$year][$month]['pageview'];
					$total_fullpageview += $user->_articles[$year][$month]['pageview'];
				}	
				$ratio = $total_pageview / $total_month; 
				echo '<td>'.$total_month.'</td><td>'.$total_pageview.'</td><td></td>';
				$total_month = 0;
				$total_pageview = 0;
			}		

		echo '<td>'.$total_full .'</td></tr>';
		?>

		</tbody>
		</table>
</div>
</main>
</div>
</body>
</html>

<?php

	/*	foreach ($users as $user) 
		{
			echo '<tr><td>'.$user->_firstname.'</td>';
			foreach ($user->_articles as $article) {
				echo '<td>'.$article.'</td>';
			}
			echo '</tr>';
		}*/
?>