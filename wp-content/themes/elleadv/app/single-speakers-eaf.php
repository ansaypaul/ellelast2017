<?php get_header(); ?>
<main role="main" class="main main-single">
	<div class="single single-eaf">
			<div class="site-inner single-wrapper">
			<div class="content entry-content">
				<?php get_template_part('template-parts/general/intervenant-eaf'); ?>
			</div>
			<!-- sidebar -->
		    <aside class="sidebar sidebar-index" role="complementary">
		     <div id="halfpage-<?php display_id_random();?>" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="homepage" data-position=""></div> 
		    </aside>   
		</div>
	</div>
</main>
<?php get_footer(); ?>