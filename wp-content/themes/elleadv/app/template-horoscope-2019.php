<?php
/*
Template Name: Horoscope single 2019
*/
?>
<?php get_header(); ?>
<?php $excludeIDS[] = get_the_ID(); ?> 
<?php 
global $post; global $excludeIDS;
$list_tags = get_the_tags();
if($list_tags){$main_tag = $list_tags[0]->term_id;} else {$main_tag = '';}
$display_thumb = 1; 
elle_set_post_views();
$list_categorie = get_the_category(); 
$main_categorie = $list_categorie[0];
$sous_categorie = $list_categorie[0];
$tierce_categorie = $list_categorie[0];
if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
  $main_categorie = $list_categorie[0];
  $sous_categorie = $list_categorie[0];
}

if(count($list_categorie) > 1 ){
  if(in_array($list_categorie[0]->cat_ID, category_principales())) { 
    $main_categorie = $list_categorie[0];
    $sous_categorie = $list_categorie[1];
  }

  if(in_array($list_categorie[1]->cat_ID, category_principales())) { 
    $main_categorie = $list_categorie[1];
    $sous_categorie = $list_categorie[0];
  }
}

$display_image_thumb = get_post_meta(get_the_ID(), 'display_image_thumb' ,false );
if(count($display_image_thumb) > 0){ if($display_image_thumb[0] == 0){ $display_thumb = 0; } }
if($sous_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 
if($main_categorie->cat_ID == category_sans_thumb()) { $display_thumb = 0; } 

?>
<main role="main" class="main main-single">
  <div class="single">
    <!-- site-inner -->
    <div class="site-inner single-wrapper">

<div class="main-wrapper">
  <div class="main-content-wrapper content-wrapper" data-io-article-url="<?php the_permalink(); ?>">
    <div class="content" data-tag="<?php echo $main_tag ?>" data-cat="<?php echo $sous_categorie->cat_ID; ?>">
    <!-- article -->
    <article id="post-<?php the_ID(); ?>" data-id = "<?php the_ID(); ?>" 
    data-cat = "" data-title="<?php the_title(); ?>" data-adposition="<?php echo mb_strtoupper(ad_position()['cat']); ?>" 
    data-adpositionclean = "<?php echo ad_position()['cat']; ?>" data-adpositionbis="<?php echo html_entity_decode(ad_position()['subcat']); ?>"
    data-author = "<?php the_author(); ?>" data-tag="<?php echo html_entity_decode(ad_position_ter()); ?>" data-link="<?php the_permalink(); ?>">
        <header class="entry-header">
        <!-- post-category -->
        <div class="post-category">
          <?php if(isset($main_categorie)){ ?>
            <a href="<?php echo get_category_link($main_categorie->cat_ID) ?>" class="post-category-parent">
              <?php echo $main_categorie->name; ?>
            </a>
          <?php } ?>
          <?php if(isset($sous_categorie) && ($main_categorie->cat_ID != $sous_categorie->cat_ID)){ ?>
            <a href="<?php echo get_category_link($sous_categorie->cat_ID) ?>" class="post-category-child"><?php echo $sous_categorie->name; ?></a>
          <?php } ?>
        </div>
        <!-- post-title -->
        <h1 class="entry-title" property="headline"><?php the_title(); ?></h1>
        <!-- post-details -->
        <div class="post-details">
          <span class="date"> 
            <?php $str_update = __( 'Mis à jour le', 'html5blank' ); ?>
            <?php setlocale(LC_TIME, 'fr_FR');
              date_default_timezone_set('Europe/Paris'); 
             ?>
              <span class="dt-modified" property="dateModified" content="<?php echo date("Y-m-d");?>"> <?php echo $str_update; ?> <?php echo utf8_encode(strftime('%A %d %B %Y'));?></span>
              </span>
          </span>
          <span class="author vcard p-author h-card" property="author" typeof="Person"><?php _e( 'par', 'html5blank' ); ?>
            <?php $credits_auteur_initial = get_field( "credits_auteur_initial" );
            if ( $credits_auteur_initial ) { ?>
              <span class="credit-trad">  <?php echo $credits_auteur_initial; ?> <?php _e( 'et', 'html5blank' ); ?> </span>
            <?php } ?>
                <span property="name" class="hidden"><?php the_author(); ?></span>
                <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author(); ?></a>
                <?php if( $authortitle = get_the_author_meta( 'authortitle' ) ) { ?>
              <span class="author-title hidden" property="jobTitle"><?php echo $authortitle; ?></span>
            <?php } ?>
            
            <?php $credits_link = get_field( "credits_link" );
            $credits_photographe = get_field( "credits_photographe" );
            if ( $credits_photographe ) { ?>
              <span class="credit-photo"><?php _e( 'Photos', 'html5blank' ); ?>: 
                <?php if ( $credits_link ) { ?>
                  <a href="<?php echo $credits_link; ?>" target="_blank">
                <?php } ?>
                <?php echo $credits_photographe; ?></span>
                <?php if ( $credits_link ) { ?>
                  </a>
                <?php } ?>
            <?php } ?>
          </span>
          <div property="publisher" typeof="Organization" class="hidden">
            <span property="name"><?php _e( 'ELLE Belgique', 'html5blank' ); ?></span>
            <div property="logo" typeof="ImageObject">
                <link property="url" href="<?php echo home_url(); ?>/img/icons/ellebelgique.png" />
                <meta property="height" content="125" />
                <meta property="width" content="320" />
            </div>
          </div>
        </div>
        <!-- post-thumbnail -->
        <?php if(has_post_thumbnail() && $display_thumb ){ // Check if Thumbnail exists ?>
           <span title="<?php the_title(); ?>" class="post-thumbnail">
            <?php the_post_thumbnail('content2018',array('',0,1,1)); ?>
          </span>
          <!-- <a href="<?php echo get_the_post_thumbnail_url();?>" property="image" class="hidden"></a> -->
        <?php } ?>
        <!-- /post-thumbnail -->
        </header>

        <div class="site-inner-ads no_print">
          <div id="billboard-<?php display_id_random();?>" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="MOB640x150" data-refadMOB="pebbleMOB640x150" data-format="" data-refad="" data-location="" data-position="">
          </div>
        </div>
        
        <div class="entry-content e-content" property="articleBody">
          <?php 
          	$utf8 = 0;          
          	$post_slug = get_post_field( 'post_name', get_post() );
          	get_af_rss("http://www.asiaflash.com/horoscope/rss_horojour_".$post_slug.".xml");
          ?>
            <div class="site-breadcrumb">
            <div class="breadcrumbs"><span><a href="https://www.elle.be/fr/" class="home"><span>ELLE</span></a></span> <span class="sep">›</span> 
            <span><a href="https://www.elle.be/fr/horoscopes"><span>Horoscopes</span></a></span> 
            <span class="sep">›</span>
            <span class="current"><?php the_title(); ?></span></div>
            </div>
        </div>
        <footer class="entry-footer">
          <span class="tags-wrapper">
            <?php $post_tags = get_the_tags();
            if ( $post_tags ) { ?>
              <span class="site-tags">Tags: </span>
              <span class="tags-list">
                <?php
                $list_tags_str = '';
                  foreach( $post_tags as $tag ) {
                    $list_tag_link = "<a href='".home_url()."/tag/".$tag->slug."' property='keywords' typeof='text'>".ucfirst($tag->name)."</a>";
                    $list_tags_str .= $list_tag_link.', '; 
                  } 
                  $list_tags_str = substr($list_tags_str, 0, -2).'.';
                  echo $list_tags_str;
                  ?>
              </span>
            <?php } ?>
          </span>
          <div id="social-share">
            <?php display_single_RS(); ?>
          </div>
        </footer>
        <?php  // comments_template(); ?>
    </article>
    <!-- /article -->
    </div>
    <!-- sidebar -->
    <aside class="sidebar" role="complementary">
      <!-- single-sidebar-related -->
      <div id="halfpage-<?php display_id_random()?>" class="ads ads-first halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="" data-position=""></div>
      <?php if(get_field('identifiant_youtube','option')){ ?>
        <!-- (1) video wrapper -->
        <div class="sidebar_youtube" data-embed="<?php echo get_field('identifiant_youtube','option'); ?>" data-picture="<?php echo get_field('image_de_couverture','option'); ?>">      
            <!-- (2) the "play" button -->
            <div class="play-button"></div>          
        </div>
      <?php } ?>
      <?php 
        $time_most_popular = '30 days ago';
        $posts_popular = get_posts(array('posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
      ?>
        <div class="most-popular">
          <span class="title-h3"><?php _e( 'les + lus', 'html5blank' ); ?></span>
          <ul class="most-popular-titles" >
          <?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; ?>
              <li>
                  <span class="title-h4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
              </li>
          <?php endforeach; wp_reset_postdata(); ?>
          </ul>
      </div>
      <div id="halfpage-<?php display_id_random()?>" class="ads halfpage" categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="" data-position="2"></div>
    </aside>
    <!-- /sidebar -->
  </div>

  <?php get_template_part('template-parts/general/article-related-bottom');  ?>

  <!-- Intégration LIGATUS -->      
  <?php 
  if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
    echo '<div id="lig_elle_be_fr_sb_article"></div>';
  }else if(get_field('variable_js','option')['lang_identifiant'] == 'NL'){
    echo '<div id="lig_elle_be_be_sb_article"></div>';
  }
  ?>

  <!-- Intégration LIGATUS -->
   <div class="site-inner no_print">
    <div id="billboard-<?php display_id_random(); ?>" class="ads leaderboard" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position="2"></div>
  </div>
</div>
    </div>
  </div>
</main>
<?php get_footer(); ?>