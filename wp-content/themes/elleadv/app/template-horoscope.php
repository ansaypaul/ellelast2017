<?php
/*
Template Name: Horoscope
*/
?>
<?php get_header(); ?>
<?php $excludeIDS[] = get_the_ID(); ?> 
<?php 
  global $post;
  $list_categorie = get_the_category(); 
  $main_categorie = $list_categorie[0];
  if(count($list_categorie) > 1 ){
    $sous_categorie = $list_categorie[1];
  }
  $list_tags = get_the_tags();
  foreach ($list_categorie as $categorie) {
    $list_categorie_name[] = $categorie->slug;
  }
?>
<main role="main" class="main main-single">
  <div class="single">
    <!-- site-inner -->
    <div class="site-inner single-wrapper">
      <?php
        $arg = array('posts_per_page' => 1,'tag' => 'horoscope');
        $horoscopes = get_posts($arg);
      ?>
      <?php foreach ( $horoscopes as $post ) : setup_postdata( $post ); ?>
          <?php get_template_part('template-parts/general/content-wrapper-horoscope'); ?>
      <?php endforeach; wp_reset_postdata(); ?>
    </div>
  </div>
</main>
<?php get_footer(); ?>
