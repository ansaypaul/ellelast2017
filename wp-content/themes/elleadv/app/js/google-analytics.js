(function ($, root, undefined) {

$( document ).ready(function() {
   	  console.log('Ready google-analytics.js : '  + $.now() );

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  console.log("Launch Google Analytics");

      ga('create', lang_identifiantGoogleAnalytics, 'elle.be');
      ga('require', 'displayfeatures');
      ga('require', 'linkid', 'linkid.js');

		if(adpositiontype == ' (Article)'){
			ga('send', 'pageview', {
				'dimension1': id_author,
				'dimension2': adposition + adpositiontype,
				'dimension3': id_post + ' - '+ adposition + ' - ' + article_title + ' - ' + id_author,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});
		}
		else{
			ga('send', 'pageview', {
				'dimension2': adposition + adpositiontype,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});				
		}

      ga('create', id_google_analytics_specifique, 'elle.be', {'name': 'newTracker'});
      ga('newTracker.require', 'displayfeatures');
      ga('newTracker.require', 'linkid', 'linkid.js');

		if(adpositiontype == ' (Article)'){
			ga('newTracker.send', 'pageview', {
				'dimension1': id_author,
				'dimension2': adposition + adpositiontype,
				'dimension3': id_post + ' - '+ adposition + ' - ' + article_title + ' - ' + id_author,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});
		}
		else{
			ga('newTracker.send', 'pageview', {
				'dimension2': adposition + adpositiontype,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});				
		}

	$('.share_btn_facebook').click(function() { 
		ga('send', 'event', 'click', 'share' , 'facebook' , { 'nonInteraction': 1 }); 
		ga('newTracker.send', 'event', 'click', 'share' , 'facebook' , { 'nonInteraction': 1 }); 
	});
	$('.share_btn_twitter').click(function() { 
		ga('send', 'event', 'click', 'share' , 'twitter'  , { 'nonInteraction': 1 }); 
		ga('newTracker.send', 'event', 'click', 'share' , 'twitter'  , { 'nonInteraction': 1 }); 
	});
	$('.share_btn_pinterest').click(function() { 
		ga('send', 'event', 'click', 'share' , 'pintereset'  , { 'nonInteraction': 1 }); 
		ga('newTracker.send', 'event', 'click', 'share' , 'pintereset'  , { 'nonInteraction': 1 }); 
	});
	$('.related-floating').find('a').click(function() {
		ga('send', 'event', 'click', 'related' , 'bottom'  , { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'click', 'related' , 'bottom'  , { 'nonInteraction': 1 });
	});
	$('#newsletter-submit').click(function() { 
		ga('send', 'event', 'form', 'send' , 'newsletter'  , { 'nonInteraction': 1 }); 
		ga('newTracker.send', 'event', 'form', 'send' , 'newsletter'  , { 'nonInteraction': 1 }); 
	});
	if($('body').hasClass('page-template-template-newsletter')){ 
		ga('send', 'event', 'pageview', 'newsletter' , 'subscribe'  , { 'nonInteraction': 1 }); 
		ga('newTracker.send', 'event', 'pageview', 'newsletter' , 'subscribe'  , { 'nonInteraction': 1 }); 
		$('#newsletter-submit').click(function() { ga('send', 'event', 'form', 'newsletter' , 'subscribe'  , { 'nonInteraction': 1 }); });
	}
	if($('body').hasClass('page-template-template-mon-profil')){ 
		ga('send', 'event', 'pageview', 'newsletter' , 'update'  , { 'nonInteraction': 1 }); 
		ga('newTracker.send', 'event', 'pageview', 'newsletter' , 'update'  , { 'nonInteraction': 1 }); 
		$('#newsletter-submit').click(function() { ga('send', 'event', 'form', 'newsletter' , 'update'  , { 'nonInteraction': 1 }); });
	}

	function timer11(){
		ga('send', 'event', 'TimeOnPage', '1', '11-30 seconds', { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'TimeOnPage', '1', '11-30 seconds', { 'nonInteraction': 1 });
	}
	function timer31(){
		ga('send', 'event', 'TimeOnPage', '2', '31-60 seconds', { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'TimeOnPage', '2', '31-60 seconds', { 'nonInteraction': 1 });
	}
	function timer61(){
		ga('send', 'event', 'TimeOnPage', '3', '61-180 seconds', { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'TimeOnPage', '3', '61-180 seconds', { 'nonInteraction': 1 });
	}
	function timer181(){
		ga('send', 'event', 'TimeOnPage', '4', '181-600 seconds', { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'TimeOnPage', '4', '181-600 seconds', { 'nonInteraction': 1 });
	}
	function timer601(){
		ga('send', 'event', 'TimeOnPage', '5', '601-1800 seconds', { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'TimeOnPage', '5', '601-1800 seconds', { 'nonInteraction': 1 });
	}
	function timer1801(){
		ga('send', 'event', 'TimeOnPage', '6', '1801+ seconds', { 'nonInteraction': 1 });
		ga('newTracker.send', 'event', 'TimeOnPage', '6', '1801+ seconds', { 'nonInteraction': 1 });
	}

	ga('send', 'event', 'TimeOnPage', '0', '0-10 seconds', { 'nonInteraction': 1 });
	ga('newTracker.send', 'event', 'TimeOnPage', '0', '0-10 seconds', { 'nonInteraction': 1 });

	setTimeout(timer11,11000);
	setTimeout(timer31,31000);
	setTimeout(timer61,61000);
	setTimeout(timer181,181000);
	setTimeout(timer601,601000);
	setTimeout(timer1801,1801000);

	$(document).scroll(function(){
		if(io_type_generic == 'article'){

		    var h =  document.documentElement,
		        b =  document.body,
		        st =  'scrollTop',
		        sh =  'scrollHeight';
		 
		    var percent = parseInt ( (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100);
			//console.log(percent + "%");
				if (percent == 25)
				{
				    ga('send', 'event', 'Scrolling', 'moreThan25%', article_link, { 'nonInteraction': 1 });
				    ga('newTracker.send', 'event', 'Scrolling', 'moreThan25%', article_link, { 'nonInteraction': 1 });
				}
				else if (percent == 50)
				{
				    ga('send', 'event', 'Scrolling', 'moreThan50%', article_link, { 'nonInteraction': 1 });
				    ga('newTracker.send', 'event', 'Scrolling', 'moreThan50%', article_link, { 'nonInteraction': 1 });
				}
				else if (percent == 75)
				{
				    ga('send', 'event', 'Scrolling', 'moreThan75%', article_link, { 'nonInteraction': 1 });
				    ga('newTracker.send', 'event', 'Scrolling', 'moreThan75%', article_link, { 'nonInteraction': 1 });
				}
				else if (percent == 90)
				{
				    ga('send', 'event', 'Scrolling', 'moreThan90%', article_link, { 'nonInteraction': 1 });
				    ga('newTracker.send', 'event', 'Scrolling', 'moreThan90%', article_link, { 'nonInteraction': 1 });
				}

		}
	});

});


})(jQuery, this);