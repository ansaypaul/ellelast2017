(function ($, root, undefined) {
	var path_windows = window.location.href;
	var arr = path_windows.split("/");
	var elle_path = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];
	/* <![CDATA[ */
	var ajax_search = {"ajaxurl": elle_path + "/wp-admin/admin-ajax.php"};
	/* ]]> */

	var check_thumb = '';
	function check_image_a_la_une(){
			check_thumb = $('body').hasClass('post-type-post');
			if($('#_thumbnail_id').val() != -1 || check_thumb == false ){
				$('#publish').prop('disabled', false);
				$('#publish').attr('title', '');
			}else{
				$('#publish').prop('disabled', true);
				$('#publish').attr('title', 'Feature image is mandatory to publish !');
			}
	}

	function removeEmojis (string) {
	  var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
	  return string.replace(regex, '');
	}

	function ajax_do_instagram_load(){
		
				console.log('Do ajax_do_instagram_load !');
					$.ajax({
						url: ajaxurl,
						type: 'post',
						data: { action: 'erase_uploaded_images', url_instagram: $('#acf-field_5a328a89c61c1').val()},
					
					beforeSend: function() {
						var loading = $('<img style="margin-left:20px" id="loading_gif">');
						loading.attr('src', 'https://www.elle.be/fr/wp-content/themes/ellev2017/img/ajax-loader.gif');
						loading.insertAfter('#field_5a328a89c61c1-button')
							},
					success: function( html ) {
						data = JSON.parse(html);
						title = data['title'];
						titleparser = removeEmojis(title);
						$('#acf-field_5a328ac6992e1').val(data['thumbnail_url']);
						$('#acf-field_5a33903d12e75').val(data['author_name']);
						$('#acf-field_5a33927f003a6').text(titleparser);

						$('#_thumbnail_id').val(data['insta_thumb_img_id']);
						$('#apercu_instagram').remove();
						$('#set-post-thumbnail').remove();
						$('#set-post-thumbnail-desc').remove();
						$('#postimagediv .hide-if-no-js').remove();
						$('#loading_gif').remove();
						console.log(data['thumbnail_url']);
						var img = $('<img id="apercu_instagram">');
						img.attr('src', data['thumbnail_url']);
						img.attr('width', 300);
						img.appendTo('#postimagediv .inside');
						$('html body').fadeTo( "slow", 1 );
							},
					error: function (xhr) {
						alert('Erreur loading instagram !'); 
				   	} 
		  		});
			};




	function maryno(){
		window.setInterval(function(){
			usercheck = 'Marie'
			username = $('.username').html();
			if(username.indexOf(usercheck ) != -1 || $('.username').html() == 'Marie-Noelle' || $('.username').html() == 'Elodie'|| $('.username').html() == 'ansaypaul'){
					console.log('MARYNO');
					$('body').append('<img class="specialcat" src="http://www.stickpng.com/assets/images/580b585b2edbce24c47b2dad.png">');
					$('.specialcat').css({
					    position: 'absolute',
					    top: '10px',
					    left: '10px',
					    width:'10%',
					    zindex:'999',
					});
				animateDiv('.specialcat');
			}
		}, 5000);
	}

	function makeNewPosition(){
	    
	    // Get viewport dimensions (remove the dimension of the div)
	    var h = $(window).height() - 50;
	    var w = $(window).width() - 50;
	    
	    var nh = Math.floor(Math.random() * h);
	    var nw = Math.floor(Math.random() * w);
	    
	    return [nh,nw];    
	    
	}

	function animateDiv(myclass){
	    var newq = makeNewPosition();
	    $(myclass).animate({ top: newq[0], left: newq[1] }, 1000,   function(){
	      animateDiv(myclass);        
	    });
	    
	};


	$(document).ready(function() {
		//maryno();
		$('.acf-field-5a328ac6992e1').hide();
		var input = $('<input style="margin-top:10px" class="wp-core-ui button-primary" id="field_5a328a89c61c1-button" type="button" value="Télécharger le post instagram" />');
		input.appendTo($('.acf-field-5a328a89c61c1'));
		$('#field_5a328a89c61c1-button').click(function() { ajax_do_instagram_load(); });
		check_image_a_la_une();
		setInterval(check_image_a_la_une, 800);
		$('.size').hide();

	});


})(jQuery, this);