(function () {
	function pebblemedia(){}
	pebblemedia.prototype.GetSiteInfo = function(){
		Pebblemedia.SiteInfo = {};
		Pebblemedia.SiteInfo.publication = "elle-nl";
		Pebblemedia.SiteInfo.homepageURL = "http://www.elle.be/nl/";
		Pebblemedia.SiteInfo.language = "nl";
		Pebblemedia.SiteInfo.channel = [ "woman" ];
		Pebblemedia.SiteInfo.BillboardClose = false;
		Pebblemedia.SiteInfo.positions = [
			{
				"location" : "homepage" ,
				"position" : [
                    { "Dformat": "TopLarge","Mformat": "MOB640x150", "position" : "","destination" : "pebbleLeaderboard1", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "TopLarge","Mformat": "MOB640x150"," position" : "2", "destination" : "pebbleLeaderboard2", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "MiddleLarge","Mformat": "MMR", "position" : "", "destination" : "pebbleRectangle1", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "MiddleLarge","Mformat": "MMR", "position" : "2",  "destination" : "pebbleRectangle2", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "MiddleLarge","Mformat": "MMR", "position" : "2", "destination" : "pebbleRectangle3", "loaded" : false, "loadingoffset": 100},
				]
			}, {
				"location" : "others" ,
				"position" : [
                    { "Dformat": "TopLarge","Mformat": "MOB640x150", "position" : "","destination" : "pebbleLeaderboard1", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "TopLarge","Mformat": "MOB640x150"," position" : "2", "destination" : "pebbleLeaderboard2", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "MiddleLarge","Mformat": "MMR", "position" : "", "destination" : "pebbleRectangle1", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "MiddleLarge","Mformat": "MMR", "position" : "2",  "destination" : "pebbleRectangle2", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "Middle","Mformat": "MMR", "position" : "2", "destination" : "pebbleRectangle3", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "Middle","Mformat": "MMR", "position" : "2", "destination" : "pebbleRectangle4", "loaded" : false, "loadingoffset": 100},
                    { "Dformat": "Middle","Mformat": "MMR", "position" : "2", "destination" : "pebbleRectangle5", "loaded" : false, "loadingoffset": 100},
				]
			}
		];
		
    };
	// START THE SCRIPT HERE
	if (typeof Pebblemedia == "undefined")
		window.Pebblemedia = new pebblemedia;
	if (typeof adhese == "undefined")
        window.adhese = new Adhese();	
        adhese.Start();
})();