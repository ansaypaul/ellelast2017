/* footer-script.js */
(function ($, root, undefined) {
		var path_windows = window.location.href;
		var arr = path_windows.split("/");
		var elle_path = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];
		var device;
		var winWidth = $(window).innerWidth();
		var mainOffset = $('header').height() - 20;
	    var var_window = $(window);
	    //var init_GPDR = 0;
	  
	  	function hasAdblock() { var a = document.createElement('div'); a.innerHTML = '&nbsp;'; a.className = 'adsbox pub_300x250 pub_300x250m pub_728x90 text-ad textAd text_ad text_ads text-ads adglare-ad-server text-ad-links'; a.style = 'width: 1px !important; height: 1px !important; position: absolute !important; left: -10000px !important; top: -1000px !important;'; var r = false; try { document.body.appendChild(a); var e = document.getElementsByClassName('adsbox')[0]; if(e.offsetHeight === 0 || e.clientHeight === 0) r = true; if(window.getComputedStyle !== undefined) { var tmp = window.getComputedStyle(e, null); if(tmp && (tmp.getPropertyValue('display') == 'none' || tmp.getPropertyValue('visibility') == 'hidden')) r = true; } document.body.removeChild(a); } catch (e) {} return r; };  
	  	console.log('Detected ADBlock : ' + hasAdblock());

		var country_code_ads = '';

		var delay = (function(){
			var timer = 0;
			return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};})();

		//Track Outbound Link Clicks
		(function trackOutbounds() {
		        
		        var hitCallbackHandler = function(url,win) {
		            if (win) {
		                    window.open(url, win);
		            } else {
		                window.location.href = url;
		        }
		    };
		    
		    var addEvent = function(el, eventName, handler) {
		    
		                if (el.addEventListener) {
		                        el.addEventListener(eventName, handler);
		                        } else {
		                        el.attachEvent('on' + eventName, function(){
		                                handler.call(el);
		                    });
		                }
		        }
		        
		        if (document.getElementsByTagName) {
		                var el = $('.content a');
		                var getDomain = document.domain.split('.').reverse()[1] + '.' + document.domain.split('.').reverse()[0];
		                
		                // Look thru each a element
		                for (var i=0; i < el.length;i++) {
		                
		                        // Extract it's href attribute
		                        var href = (typeof(el[i].getAttribute('href')) == 'string' ) ? el[i].getAttribute('href') : '';
		                        
		                        // Query the href for the top level domain (xxxxx.com)
		                        var myDomain = href.match(getDomain);
		                        
		                        // If link is outbound and is not to this domain        
		                        if ((href.match(/^(https?:|\/\/)/i)  && !myDomain) || href.match(/^mailto\:/i)) {
		                        
		                                // Add an event to click
		                                addEvent(el[i],'click', function(e) {
		                                        var url = this.getAttribute('href'), win = (typeof(this.getAttribute('target')) == 'string') ? this.getAttribute('target') : '';
		                                                        
		                                        console.log ("add event update", url);
		                                        // Log even to Analytics, once done, go to the link

		                                        ga('send', 'event', 'outbound', 'click', url,
		                                                {'hitCallback': hitCallbackHandler(url,win)},
		                                                {'nonInteraction': 1}
		                                        );

		                                        ga('newTracker.send', 'event', 'outbound', 'click', url,
		                                                {'hitCallback': hitCallbackHandler(url,win)},
		                                                {'nonInteraction': 1}
		                                        );

		                                        
		                                        e.preventDefault();
		                                });
		                        }
		                }
		        }
		})();


		function GDPR_cookie_get( cookiename ){
			var GDPR_cookie = '';
			GDPR_cookie = getcookieELLE('gdpr[allowed_cookies]');
			var patt = new RegExp(cookiename);
			var res = patt.test(GDPR_cookie); 
			console.log('Cookie : ' + cookiename + ' : ' + res);
			return res;
		}

		function init_GDPR_cookie(){
			if(init_GPDR == 1 && has_consent( 'ads' ) != true){
				window.canRunAds = undefined;
			}
		}

		function ip_detect(){
			console.log('INIT IP DETECTION');
			country_code_ads = getcookieELLE('country_code_ads');
			if(country_code_ads == null) {
				country_code_ads = '';
				console.log('INIT IP DETECTION NOT SET');
				$.ajax({
				  dataType: "jsonp",
				  url: "https://ssl.geoplugin.net/json.gp?k=e0d5504e23f0444b&jsoncallback=?",
				  success: function(data) {
				  	//console.log(data);
				   	console.log('IP DETECTION COUNTRY : ' + data['geoplugin_countryCode']);
					country_code_ads = data['geoplugin_countryCode'];
					setcookieELLE('country_code_ads', country_code_ads , 3 );
				  },
				  	async: false
				});
			}else{
				console.log('INIT IP DETECTION IS SET TO : ' + country_code_ads);
			}
		}


		function what_device(){
			if (winWidth <= 480) {
				device = 'sm';
			} else if (winWidth <= 640) {
				device = 'md';
			} else{
				device = 'lg';
			}
			console.log('Device : ' + device);
		}

		function setcookieELLE( e, t, n ) {
			var r = new Date(),
				i = new Date();
			i.setTime( r.getTime() + n * 60 * 60 * 1e3 );
			document.cookie = e + '=' + encodeURIComponent( t ) + ';expires=' + i.toGMTString() + ';domain=elle.be;path=/';
		}

		function getcookieELLE( e ) {
				var t = document.cookie,
					n, r, i;
				e = e + '=';
				for ( r = 0, c = t.length; r < c; r++ ) {
					i = r + e.length;
					if ( t.substring( r, i ) == e ) {
						cookEnd = t.indexOf( ';', i );
						if ( cookEnd == -1 ) {
							cookEnd = t.length;
						}
						return decodeURIComponent( t.substring( i, cookEnd ) );
					}
				}
				return null;
		}

		function time() {
		    var timestamp = Math.floor(new Date().getTime() / 1000)
		    return timestamp;
		}


		/* <![CDATA[ */
		var ajax_search = {"ajaxurl": elle_path + "/wp-admin/admin-ajax.php"};
		/* ]]> */

	    function init_slider_instagram(){  	
			/* DESKTOP + MOBILE */ 
			if( $('.swiper-container-instagram').length )         // use this if you are using class to check
			{
				var swiperInstagram = new Swiper('.swiper-container-instagram', {
					loop: true,
			        pagination: '.swiper-pagination-insta',
			        slidesPerView: 'auto',
			        paginationClickable: true,
			        spaceBetween: 30,
			        passiveListeners:true,
			        navigation: {
			        nextEl: '.swiper-button-next-insta',
			        prevEl: '.swiper-button-prev-insta',
			        },
			        pagination: {
	       		 	el: '.swiper-pagination-insta',
	     			},
	     			autoplay: {
					    delay: 3500,
					},
			    });
			}
	    }

		function dump(obj) {
			var out = '';
			for (var i in obj) {
			out += i + ": " + obj[i] + "\n";
			}
		console.log(out);
		}

function init_fancybox(){
		console.log("Lauch fancybox gallery 08012018 AFTER");
		$( '[data-fancybox]' ).fancybox({
			touch : true,
			selector : '',
			baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
						'<div class="fancybox-bg"></div>' +
						'<div class="fancybox-controls"><div class="logo-mobile sprite"></div>' +
							'<div class="fancybox-infobar">' +
								'<div class="fancybox-infobar__body">' +
									'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
								'</div>' +
							'</div>' +
							'<div class="fancybox-back">' +
								'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
							'</div>' +
							'<div class="fancybox-buttons">' +
							'<button data-fancybox-close class="fancybox-button fancybox-button--close fancybox-button--close-right" title="Close (Esc)"></button>'+
							'</div>' +
						'</div>' +
						'<div class="fancybox-slider-wrap">' +
							'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
							'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
								'<div class="fancybox-slider"></div>' +
						'</div>' +
						'<div class="fancybox-caption-wrap"><div class="caption-top"><div class="logo-desktop sprite"></div><div class="fancybox-caption"></div>'+
						'<div id="social-share">'+
						'<a href="https://www.facebook.com/sharer.php?u='+article_link+'" target="_blank" class="facebook sprite sprite-fb"></a>'+
						'<a href="https://twitter.com/intent/tweet?text='+article_title+'&amp;url='+article_link+'&amp;via=Elle.be" target="_blank" class="twitter sprite sprite-twitter"></a>'+
						'<a href="mailto:?subject=ELLE.be | '+article_title+'&amp;body='+article_link+'" class="sprite sprite-mail"></a>'+
						'<a href="whatsapp://send?text='+article_title+' '+article_link+'" target="_blank" class="whatsapp no_desktop sprite sprite-whatsapp"></a>'+
						'<a href="http://pinterest.com/pin/create/bookmarklet/?media='+article_thumb+'&url='+article_link+'&is_video=false&description=ELLE.be | '+article_title+'" target="_blank" class="sprite sprite-pint"></a>'+
						'</div>'+
						'</div>'+
						'<div class="fancybox-AD caption-bottom"><div id="affiche-box-shop" class="over-halfpage"><div id="affiche-shop" class="affiche">'+
						'<div id="article-shop-1">'+
						'<div id="AD_IMU"><div id="halfpage-gallery" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-format="[[300,600],[300,250]]" data-location="" data-position="gallerie"></div></div>'+
						'</div></div></div></div>'+
						'</div>'+
						'<div class="fancybox-bottom-lb">'+
						'<div class="caption-bottom"><div class="fancybox-caption"></div></div>'+
						'<div class="fancybox-AD caption-bottom-lb"><div id="affiche-box-shop-lb" class=""><div id="affiche-shop-lb" class="affiche">'+
						'<div id="article-shop-lb"><div id="AD_Leaderboard">'+
						'<div id="billboard-gallery" class="ads TopLarge" data-categoryAd="" data-formatMOB="" data-format="[[970, 250], [970, 90], [728, 90]]" data-location="" data-position="gallerie"></div>'+
						'</div>'+
						'</div></div></div></div>'+
						'</div>'+
						'</div>'+
					'</div>',

				image : {
					protect: false 
				},

				
			beforeMove 	: function( instance, slide ) {
				console.info('Fancybox switch');

				if(slide['type'] == 'inline'){
					
					/*
					jQuery('#pebbleMiddle-gallery').empty();
					jQuery('#pebbleMiddle-gallery').addClass("lazyload");
			    	jQuery('#pebbleMiddle-gallery').attr("loaded",true);
			    	jQuery('#pebbleMiddle-gallery').attr("dateload",tempsEnMs);
									postscribe('#pebbleMiddle-gallery',
									'<div id="pebbleMiddle" style="text-align:center">'+
									 '<script type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>'+
									 '<script>var adhesePublication = "'+lang_identifiantPebble+'";  var adheseLocation = "others";  var adheseFormat = "Middle"; var adhesePosition = "2";<\/script>'+
									 '<script src="https://pool-pebblemedia.adhese.com/tag/tag3rd.js"></script>'+  
									'</div>');
									*/
				}

			},

			afterMove 	: function( instance, slide ) {
				hash = location.hash;
				hash_exp =hash.split("-"); 
				last_object = $( hash_exp[0] ).closest('article');
				last_id = last_object.data("id");
				user_login = last_object.data("author");
				cat = last_object.data("adposition");
				title = last_object.data("title");
				subcat = last_object.data("adpositionbis");
				tag = last_object.data("tag");


				ga('set', 'title', title);
				ga('set', 'location', link);
				ga('send', 'pageview', {
				'dimension1': user_login,
				'dimension2': cat  + ' (Article)',
				'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
				'dimension4': subcat + ' (Article)',
				'dimension5': tag + ' (Article)',
				'dimension7': 'slider fullscreen',
				});	


				ga('newTracker.set', 'title', title);
				ga('newTracker.set', 'location', link);
				ga('newTracker.send', 'pageview', {
				'dimension1': user_login,
				'dimension2': cat  + ' (Article)',
				'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
				'dimension4': subcat + ' (Article)',
				'dimension5': tag + ' (Article)',
				'dimension7': 'slider fullscreen',
				});	
			},


			  caption : function( instance, item ) {
			    var caption, link, divad, shop_link;

			    if ( item.type === 'image' ) {
			      	if(jQuery(this).parent().parent().parent().find('div.swiper-caption_data').html() != ''){
 						caption = jQuery(this).parent().parent().parent().find('div.swiper-caption_data').html();
			      	}
			    return caption;
			    }

			  },

			  clickContent : function( current, event ) {
				   return current.type === 'image' ? 'zoom' : false;
			  },
	});
}


		var sliders = [];
	    function init_slider_gallery(){  	
	    	var wt = $(window).scrollTop();  
		   	var wb = wt + $(window).height();  
	    	$(".gallery_wrapper").each(function(){
	    		var ot = $(this).offset().top - 100;  
			    var ob = ot + $(this).height(); 
			    if(!$(this).attr("loaded")){
			    		console.log('INIT GALLERY SCROLL');
			      		$(this).attr("loaded",true);
			      		if($(this).hasClass('template-slider')){
	    					format_slider_gallery_first($(this));
	    				}
	    				else if($(this).hasClass('template-mosaique')){
	    					format_mosaique_gallery_first($(this));
	    				}
	    				else{
	    					format_slider_gallery_first($(this));
	    				}

	    			$(this).find('.button_open_fancy').click(function() { open_fancy($(this)); });
					$(this).find('.button_mosaique').click(function() {  
						if($(this).hasClass('active-on')){
							$(this).removeClass('active-on');
							format_slider_gallery($(this).parent().parent()); 
						}else{
							$(this).addClass('active-on');
							format_mosaique_gallery($(this).parent().parent()); 
						}
					});


	    		}
			});
		}	

		function init_slider_gallery_scroll(){  
			init_slider_gallery();
		}


		function format_slider_gallery_first(item){  
				current_gallery = $(item).data('id');
				console.log('17h FIRST GAL ' + current_gallery);
				//$(item).find("div.gallery_ajax").empty();
				//$(item).find("div.gallery_ajax").css('opacity','0');
					/*$(item).find( "div.swiper-slide" ).each(function( index ) {
						if(index != 0 && index % 5 == 0){
							console.log('ADD ADS SLIDERS');
							ads_id = Math.floor(Math.random() * 1000000) + 1;
							ads = document.createElement( "div" );
							ads.className  = 'swiper-slide';
						}
					});*/

					sliders[current_gallery] = new Swiper($(item).find("div.swiper-container-gallery"), {
											   		pagination: '.swiper-pagination',
											   		slidesPerView: 'auto',
											   		paginationaginationClickable: false,
											   		spaceBetween: 30,
											   		passiveListeners:true,
												    navigation: {
												        nextEl: '.swiper-button-next-galerie',
												        prevEl: '.swiper-button-prev-galerie',

											        },

					});  

					sliders[current_gallery].params['ID'] = current_gallery;

					sliders[current_gallery].on('slideChange', function () {
						$(".gallery_index_current_" + this.params['ID']).data('image',this.activeIndex);
						$(".gallery_index_current_" + this.params['ID']).html(this.activeIndex + 1);
						$(item).find(".button_open_fancy").attr('data-image',this.activeIndex);
						window.location.hash = '#slider-'+this.params['ID']+'-'+this.activeIndex;

						console.log('Generate page vue - SLIDER INDEX : ' + this.activeIndex);

						last_object = $(item).closest('article');
						last_id = last_object.data("id");
						user_login = last_object.data("author");
						cat = last_object.data("adposition");
						title = last_object.data("title");
						subcat = last_object.data("adpositionbis");
						tag = last_object.data("tag");
						link = last_object.data("link");

						ga('set', 'title', title);
						ga('set', 'location', link);
						ga('send', 'pageview', {
						'dimension1': user_login,
						'dimension2': cat  + ' (Article)',
						'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
						'dimension4': subcat + ' (Article)',
						'dimension5': tag + ' (Article)',
						'dimension7': 'slider',
						});	

						ga('newTracker.set', 'title', title);
						ga('newTracker.set', 'location', link);
						ga('newTracker.send', 'pageview', {
						'dimension1': user_login,
						'dimension2': cat  + ' (Article)',
						'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
						'dimension4': subcat + ' (Article)',
						'dimension5': tag + ' (Article)',
						'dimension7': 'slider',
						});	

						var next_img = $(item.find('.swiper-slide-next').next().children().children().children());
						var srcset = next_img.data('imgsrc');
						var setImageSource = $(next_img).attr('src', srcset);
						/**** LOAD MORE PICTURE (5 EACH) **/

					});


				number_slide = $(item).find("div.swiper-slide").length;
				$(".gallery_index_total_" + current_gallery).html(number_slide);

				(item).find('.gallery_index_current').removeClass('hidden');
				(item).find("div.gallery_ajax").fadeTo( "slow", 1 );
	    	lazy_load();
	    	init_fancybox();
		}

		function format_slider_gallery(item){  
				current_gallery = $(item).data('id');
				$(item).find("div.gallery_ajax").empty();
	    			$.ajax({
	    						async: false,
								context: this,
								url: stylesheet_directory_uri + '/template-mustache/template.htm',
								type: 'get',
								beforeSend: function() {

								},
								success: function( templates ) {
								$(item).find("div.gallery_ajax").css('opacity','0');
								var template = $(templates).filter('#template-1col').html();	
		    					$(item).find("div.gallery_ajax").html(Mustache.render(template,window["gallery_ajax_" + current_gallery]));

			    					$(item).find( "div.swiper-slide" ).each(function( index ) {
										if(index != 0 && index % 3 == 0 && index < 12){
											console.log('ADD ADS SLIDERS');
											ads_id = Math.floor(Math.random() * 1000000) + 1;
											ads = document.createElement( "div" );
											ads.className  = 'swiper-slide swiper-slide-ads';
											/*ads.innerHTML = '<div class="swiper-picture_wrapper">'+
											'<div id="halfpage-'+ads_id+'" class="ads ads-slider no_reload halfpage" categoryAd="" formatMOB="MMR" refadMOB="pebbleMMR" format="Middle" refad="pebbleMiddle" location="" position="2"></div>'+
											'</div>';
											this.after(ads);*/
											console.log(index);
										}
									});

			    					sliders[current_gallery] = new Swiper($(item).find("div.swiper-container-gallery"), {
															   		pagination: '.swiper-pagination',
															   		slidesPerView: 'auto',
															   		paginationaginationClickable: false,
															   		spaceBetween: 30,
															   		passiveListeners:true,
																    navigation: {
																        nextEl: '.swiper-button-next-galerie',
																        prevEl: '.swiper-button-prev-galerie',

															        },

									});  

			    					sliders[current_gallery].params['ID'] = current_gallery;

									sliders[current_gallery].on('slideChange', function () {
										$(".gallery_index_current_" + this.params['ID']).data('image',this.activeIndex);
										$(".gallery_index_current_" + this.params['ID']).html(this.activeIndex + 1);
										$(item).find(".button_open_fancy").attr('data-image',this.activeIndex);
										window.location.hash = '#slider-'+this.params['ID']+'-'+this.activeIndex;

										console.log('Generate page vue - SLIDER');

										last_object = $(item).closest('article');
										last_id = last_object.data("id");
										user_login = last_object.data("author");
										cat = last_object.data("adposition");
										title = last_object.data("title");
										subcat = last_object.data("adpositionbis");
										tag = last_object.data("tag");
										link = last_object.data("link");

										ga('set', 'title', title);
										ga('set', 'location', link);
										ga('send', 'pageview', {
										'dimension1': user_login,
										'dimension2': cat  + ' (Article)',
										'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
										'dimension4': subcat + ' (Article)',
										'dimension5': tag + ' (Article)',
										'dimension7': 'slider',
										});	

										ga('newTracker.set', 'title', title);
										ga('newTracker.set', 'location', link);
										ga('newTracker.send', 'pageview', {
										'dimension1': user_login,
										'dimension2': cat  + ' (Article)',
										'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
										'dimension4': subcat + ' (Article)',
										'dimension5': tag + ' (Article)',
										'dimension7': 'slider',
										});	

									});


								number_slide = $(item).find("div.swiper-slide").length;
								$(".gallery_index_total_" + current_gallery).html(number_slide);

								(item).find('.gallery_index_current').removeClass('hidden');
								(item).find("div.gallery_ajax").fadeTo( "slow", 1 );
					}

			});
	    	lazy_load();
	    	init_fancybox();
		}


		function format_mosaique_gallery_first(item){  
				current_gallery = $(item).data('id');
	
				$(item).find("div.grid").css('opacity','0');
			    $(function() {
						// init Masonry
						var $grid = $('.grid').imagesLoaded( function() {
						  // init Masonry after all images have loaded
						  $grid.masonry({
						  				itemSelector: '.grid-item',
						  				gutter: 5,
						  });
						 $(item).find("div.grid").fadeTo( "slow", 1 );
						});

				});
				$(item).find('.gallery_index_current').addClass('hidden');
				$(item).find('.button_mosaique').addClass('active-on');
				number_slide = $(item).find("div.grid-item").length;
				$(".gallery_index_total_" + current_gallery).html(number_slide);

				/*(item).find('.gallery_index_current').removeClass('hidden');
				(item).find("div.gallery_ajax").fadeTo( "slow", 1 );*/


	    	init_fancybox();
		}

		function format_mosaique_gallery(item){  
				current_gallery = $(item).data('id');
				$(item).find("div.gallery_ajax").empty();
	    			$.ajax({
	    						async: false,
								context: this,
								url: stylesheet_directory_uri + '/template-mustache/template.htm',
								type: 'get',
								beforeSend: function() {
						
								},
								success: function( templates ) {
								var template = $(templates).filter('#template-all').html();		
		    					$(item).find("div.gallery_ajax").html(Mustache.render(template,window["gallery_ajax_" + current_gallery]));
								$(item).find("div.grid").css('opacity','0');
							    $(function() {
										// init Masonry
										var $grid = $('.grid').imagesLoaded( function() {
										  // init Masonry after all images have loaded
										  $grid.masonry({
										  				itemSelector: '.grid-item',
										  				gutter: 5,
										  });
										 $(item).find("div.grid").fadeTo( "slow", 1 );
										});

								});
								$(item).find('.gallery_index_current').addClass('hidden');
								$(item).find('.button_mosaique').addClass('active-on');
								number_slide = $(item).find("div.grid-item").length;
								$(".gallery_index_total_" + current_gallery).html(number_slide);

								/*(item).find('.gallery_index_current').removeClass('hidden');
								(item).find("div.gallery_ajax").fadeTo( "slow", 1 );*/
					}

			});
	    	init_fancybox();
		}

		function open_fancy(item){

				 //$('.fancy_img').attr( "data-fancybox", "gallery" );
				 //init_fancybox();

				index_slider_image = $(item).attr( "data-image");
			    $(item).parent().parent().find('img.fancy_img:eq('+index_slider_image +')').click();
			 	//$(item).find('img.fancy_img').off("click.fb-start");
		}

	    function lazy_load(){
		   	var wt = $(window).scrollTop();   
		   	var wb = wt + $(window).height(); 
		    $("picture.lazy source, .lazy-img").each(function(){
			    var ot = $(this).offset().top - 100;  
			    var ob = ot + $(this).height();
			    if(!$(this).attr("loaded") && wt<=ob && wb >= ot){
			      		$(this).attr("loaded",true);
			      		var srcset = $(this).data('srcset');
			      		if(typeof srcset !== typeof undefined){
							var imageDataSource = $(this).data('srcset').toString();
							$(this).removeAttr('data-srcset');
		  					var setImageSource = $(this).attr('srcset', imageDataSource);
		  					console.log("Picture Loaded : " + imageDataSource);
			      		}
					}
			});
		}
		// Ventures
		function unfold_ventures(){
			$('.footer-ventures').click(function() {
					$('.sites_selector, .footer-right').toggleClass('active-on');
			});
		}


	function count_news(){
				console.log('Do count news!');
				diff_time = time() - getcookieELLE('last_click_news');
				diff_time = parseInt(diff_time/60);
				$.ajax({
					url: ajax_search.ajaxurl,
					type: 'post',
					data: { action: 'ajax_count_news', time_news: diff_time },
					beforeSend: function() {
					
					},
					success: function( html ) {
						if(html != ''){
								$('.notif-number').text('');
								$('.notif-number').append( html );
								$('.notif-number').removeClass('hidden');
							}else{
								$('.notif-number').text('');								
							}

							},
							error: function (xhr) {
								console.log('error'); 
				   			} 
					})
		}


	function ajax_do_news(){
				if(!$('.main-news').hasClass("loaded")){
				console.log('Do SEARCH news!');
				diff_time = time() - getcookieELLE('last_click_news');
				diff_time = parseInt(diff_time/60);
				$('.main-news').addClass("loaded");
						$.ajax({
							url: ajax_search.ajaxurl,
							type: 'post',
							data: { action: 'ajax_load_news', time_news: diff_time },
							beforeSend: function() {
							
							},
							success: function( html ) {
								if(html != ''){
									$('.main-news .ajax-news').text('');
									$('.main-news .ajax-news').append( html );
									$(".main-news .ajax-news article").fadeIn();
								}else{
									$('.main-news .ajax-news').text('');
									html = 'Aucun articles trouvés !';
									$('.main-news .ajax-news').append( html );
									$(".ajax-news").fadeIn();
								}

							},
							error: function (xhr) {
								console.log('error'); 
				   			} 
					})
		    }
		    //alert(time());
		    $('.notif-number').addClass('hidden');
		    setcookieELLE('last_click_news', time() , 360 );
	}

		function ajax_do_sidebar_article(){
			if(device == 'lg'){
				$(".content-wrapper .sidebar-related .related-article,.recettes-wrapper .sidebar-related .related-article").each(function(){
				if(!$(this).hasClass("loaded")){
					console.log('Do  = Sidebar news!');
				
					more_article = $( ".content" ).data("more");
					tag_article = $( ".content" ).data("tag");
					cat_article = $( ".content" ).data("cat");

					console.log('Do  = Sidebar news tag_article :' + tag_article);
					console.log('Do  = Sidebar news cat_article :' + cat_article);
					
					$content_height = parseInt(($(this).parent().parent().parent().height() - 400 ) /  150);
					$nombre_pub = parseInt($content_height/5);
					$nombre_article_sidebar = $content_height - ($nombre_pub/2);
					$category = $(this).parent().parent().parent().find('.content').attr('data-cat');

					$(this).addClass("loaded");
					$.ajax({
						context: this,
						url: ajax_search.ajaxurl,
						type: 'post',
						data: { action: 'ajax_load_siderbar_article',nb: $nombre_article_sidebar,cat:cat_article,tag:tag_article},
						beforeSend: function() {

						},
						success: function( html ) {
							if(html != ''){
								$(this).text('');
								$(this).append( html );
								init_obf_link();
							}else{
								$(this).text('');
								html = 'Aucun articles trouvés !';
								$(this).append( html );

							}

						},
						error: function (xhr) {
							console.log('error sidebar'); 
			   			} 
					})
			    }
		    });
		}
	}


	/****** AJAX CONTENT ******/
	function ajax_do_content_article(){
			if(!$('.content-article-ajax').hasClass("loaded")){
				nombre_article_sidebar = 5;
				if(device == 'lg'){ nombre_article_sidebar = 10;}
				console.log('Do Content article ajax!');
				$('.content-article-ajax').addClass("loaded");
					$.ajax({
						url: ajax_search.ajaxurl,
						type: 'post',
						data: { action: 'ajax_load_content_article', nb: nombre_article_sidebar },
						beforeSend: function() {
			
						},
						success: function( html ) {
							if(html != ''){
								$('.content-article-ajax').text('');
								$('.content-article-ajax').append( html );
								lazy_load();								
							}else{
								$('.content-article-ajax').text('');
								html = 'Aucun articles trouvés !';
								$('.content-article-ajax').append( html );
							}

						},
						error: function (xhr) {
							console.log('error content'); 
			   			} 
					});
		    }
	}

	function generate_pageview(last_object,index,type){
		last_id = last_object.data("id");
		user_login = last_object.data("author");
		cat = last_object.data("adposition").toUpperCase();
		title = last_object.data("title");
		subcat = last_object.data("adpositionbis");
		tag = last_object.data("tag");
		link = last_object.data("link");
		console.log('Generate PV INDEX : '+ index);
		index = '#p-'+index; 
		//window.history.pushState("Details", "Title", link + index);	

		ga('set', 'title', title);
		ga('set', 'location', link);
		ga('send', 'pageview', {
		'dimension1': user_login,
		'dimension2': cat  + ' (Article)',
		'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
		'dimension4': subcat + ' (Article)',
		'dimension5': tag + ' (Article)',
		'dimension7': type,
		});		

		ga('newTracker.set', 'title', title);
		ga('newTracker.set', 'location', link);
		ga('newTracker.send', 'pageview', {
		'dimension1': user_login,
		'dimension2': cat  + ' (Article)',
		'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
		'dimension4': subcat + ' (Article)',
		'dimension5': tag + ' (Article)',
		'dimension7': type,
		});	
	}

	var canBeLoaded = true

	function load_article_scroll(){
		if (device=='md' || device=='lg'){ bottomOffset = 3000; } else { bottomOffset = 2000; }
		if( $(document).scrollTop() > ( $(document).height() - bottomOffset ) && canBeLoaded == true ){
			canBeLoaded = false;
			console.log('Load one more article');
			more_article = $( ".content" ).data("more");
			tag_article = $( ".content" ).data("tag");
			cat_article = $( ".content" ).data("cat");
			console.log('TAG TESTING SCROLL' + tag_article);
			var excluded_id = [];
			var numItems = $('.main-content-wrapper').length;
			if(numItems < 3) {
				$(".content article").each(function(){
					excluded_id.push($( this ).data("id"));
				});

				/** CONFLIT ADD tag: tag_article au data */
					$.ajax({
						url: ajax_search.ajaxurl,
						type: 'post',
						data: { action: 'ajax_load_article',cat:cat_article,tag:tag_article,excluded_id: excluded_id },
						beforeSend: function() {
							canBeLoaded = false;
						},
						success: function( html ) {
							$('.loading').remove();
							if(html != ''){
								$('.single-wrapper').append( html );
								last_object = $(".content article").last();
								load_total_multipage(last_object);
								//init_slider_gallery();
								generate_pageview(last_object,0,'article');
								//triggerInernalLinkClicks();
								init_youtube_sidebar();
								init_obf_link();
								//add_inread_block(last_object);
								canBeLoaded = true;
								if ( typeof window.instgrm !== 'undefined' ) {
								    window.instgrm.Embeds.process();
								}
							}else{
								//$('.single-wrapper').append( html );
							}
						},
						error: function (xhr) {
							console.log('error more content'); 
			   			} 
				})
			}	  
		}
	}

	/**  load total Multipage   **/
	function load_total_multipage(item){
		var numMultipages = $(item).find('.segment-multipage').length;
		$(item).find('.total-multipage').html(numMultipages);
	}

	/**  Fonction Multipage   **/
	function multipage_scroll(item){
		var wt = $(window).scrollTop();   
		var wb = wt + $(window).height();  
		$(item).find('.segment-multipage').each(function(){
			var ot = $(this).offset().top;  
	    	var ob = ot + $(this).height(); 	
			if(!$(this).attr("loaded") && wt<=ob && wb >= ot){
			$(this).attr("loaded",true);	
			current_index = $(this).find(".current-multipage").html();
			if(current_index != '' && typeof(current_index) != "undefined"){
				generate_pageview(item,current_index,'multipage');
					if(window.innerWidth > 955){
					console.log("Load multipage Dekstop");
					}else{
					console.log("Load multipage Mobile");
					}
				}
			}
		});
	}



	/** ADS **/

	function load_pebble_script(){
		console.log('INIT peeble script');
		//$('body').append('<script async defer type="text/javascript" src="//c.pebblemedia.be/js/c.js"></script>');
		//$('body').append('<script async defer type="text/javascript" src="https://pool-pebblemedia.adhese.com/tag/tag.js"></script>');
	}

	function reload_ads(){
		window.setInterval(function(){
		console.log("Check ADS");
		 var tempsEnMs = Date.now();
            $(".ads").each(function(){
            var tempsEnMsAds = parseInt($(this).attr("dateload")) + 15000;
              if(tempsEnMs >= tempsEnMsAds)
                {
               $(this).removeAttr("loaded");
                }
            });;
		}, 5000);
	}

	function postscribe_ads(item){
				var wt = $(window).scrollTop();   
				var wb = wt + $(window).height();
				var ot = item.offset().top - 200;  
		    	var ob = ot + item.height(); 
				if((!item.attr("loaded")) || (!item.attr("loaded") && item.attr("id") == 'halfpage-gallery')){

				var tempsEnMs = Date.now();
				var ads_number = item.attr('ads_number');
				if(typeof ads_number !== typeof undefined && ads_number !== false) {}
				else{
					ads_number = 1;
					item.attr("ads_number",1);
				}
				item.attr("loaded",true);	
				item.attr("dateload",tempsEnMs);
				item.attr("viewport_time",1);
				if((hasAdblock() != true) && adpositionpebble != 'no-ads'){ 
					if(country_code_ads == 'BE' || country_code_ads == '')
					{
						// BELGIQUE PAYS
						item.empty();
						if(window.innerWidth > 955){
								if(item.data("format") != ''){
									console.log('Loading : ' + item.attr("id"));
									postscribe('#'+item.attr("id"),'<div class="ads-block '+item.data("format")+'" id="'+item.data("refad")+'"><script type="text/javascript">adhese.tag({ format: "'+
					      			item.data("format")+'", publication:"'+lang_identifiantPebble+'", location: "'+adpositionpebble+'", position: "'+
					      			item.data("position")+'",categories:["'+adpositionpebble_categories+'"]});<\/script>');

									
									ga('send', 'event', {
									    eventCategory: 'ads_test_2',
									    eventAction: 'display_desktop',
									    eventLabel: item.data("format") + ' ' + item.data("position")
									});

									ga('newTracker.send', 'event', {
									    eventCategory: 'ads_test_2',
									    eventAction: 'display_desktop',
									    eventLabel: item.data("format") + ' ' + item.data("position")
									});
									
								}
						}else{
							//console.log("Load ads Normal Mobile");
					      		if(item.data("formatmob") != ''){
					      			console.log('Loading Mobile : ' + item.attr("id"));
					      			postscribe('#'+item.attr("id"),'<div class="ads-block '+item.data("formatmob")+'" id="'+item.data("refadmob")+'"><script type="text/javascript">adhese.tag({ format: "'+
					      			item.data("formatmob")+'", publication:"'+lang_identifiantPebble+'", location: "'+adpositionpebble+'", position: "'+
					      			item.data("position")+'",categories:["'+adpositionpebble_categories+'"]});<\/script>');

									ga('send', 'event', {
									    eventCategory: 'ads_test_2',
									    eventAction: 'display_mobile',
									    eventLabel: item.data("formatmob") + ' ' + item.data("position")
									});

									ga('newTracker.send', 'event', {
									    eventCategory: 'ads_test_2',
									    eventAction: 'display_mobile',
									    eventLabel: item.data("formatmob") + ' ' + item.data("position")
									});
					      		}
						}
					}
					else
					{
						// AUTRE PAYS
						if(window.innerWidth < 955){
							if(item.data("format") == 'TopLarge' && item.attr("id") == 'billboard'){
								console.log('TopLarge');
								postscribe('#'+item.attr("id"),'<div id="17656-2"><script src="//ads.themoneytizer.com/s/gen.js?type=2"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=17656&formatId=2" ></script></div>');
							}
						}else{
							if(item.data("format") == 'TopLarge' && item.attr("id") == 'billboard'){
									postscribe('#'+item.attr("id"),'<div id="17656-1"><script src="//ads.themoneytizer.com/s/gen.js?type=1"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=17656&formatId=1" ></script></div>');
							}

							if(item.data("format") == 'MiddleLarge'){
								postscribe('#'+item.attr("id"),'<div id="17656-3"><script src="//ads.themoneytizer.com/s/gen.js?type=3"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=17656&formatId=3" ></script></div>');
							}

							if(item.data("format") == 'Middle'){
								//postscribe('#'+item.attr("id"),'<div id="17656-2"><script src="//ads.themoneytizer.com/s/gen.js?type=2"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=17656&formatId=2"></script></div>');
							}
						}

					}
				}
				else{
					ads_blocker(item);
			}
		}else{
			/** INCREMATION TEMPS **/
			isOnScreen(item);

		}
	}

	function ads_blocker(item){
		if(window.innerWidth > 955){
			if(item.data("format") != ''){
				if(item.data("format") == 'TopLarge'){
					console.log('LOADED ADBLOCKER TopLarge');
					$(item).parent().removeClass('site-inner-ads');
					$(item).replaceWith('<a target="_blank" href="' + autopromo_link_src + '"><img src="' + autopromo_lb_src + '"/></a>');
				}else if((item.data("format") == 'MiddleLarge' || item.data("format") == 'Middle')){
					console.log('LOADED ADBLOCKER MiddleLarge');
					$(item).parent().removeClass('site-inner-ads');
					$(item).replaceWith('<a target="_blank" href="' + autopromo_link_src + '"><img src="' + autopromo_imu_src + '"/></a>');
				}
			}
		}else{
			if(item.data("formatmob") != ''){
				if(item.data("formatmob") == 'MOB640x150'){
					console.log('LOADED ADBLOCKER MOB640x150');
					$(item).parent().removeClass('site-inner-ads');
					$(item).replaceWith('<a target="_blank" href="' + autopromo_link_src + '"><img src="'+ autopromo_mlb_src +'"/></a>');
				}else if((item.data("formatmob") == 'MMR')){
					console.log('LOADED ADBLOCKER MMR');
					$(item).parent().removeClass('site-inner-ads');
					$(item).replaceWith('<a target="_blank" href="' + autopromo_link_src + '"><img src="' + autopromo_imu_src + '"/></a>');
				}

			}
		}
	}


	/** VIEWPORT **/
	function isOnScreen(el){
		if (!$(el).hasClass("no_reload")) {
			if
			(
				(
					(el.data("format") != 'Inarticle') && 
					(el.data("formatMOB") != 'MInarticle') && 
					(el.attr("viewport_time"))
				)
				|| 
				(
					(el.data("format") || el.data("formatMOB")) && (el.data("position") == 'gallerie')
				)
			){
				if(country_code_ads == 'BE' || country_code_ads == '')
				{

					var win = $(window);
					var viewport = {
					top : win.scrollTop(),
					left : win.scrollLeft()
					};
					viewport.right = viewport.left + win.width();
					viewport.bottom = viewport.top + win.height();

					var bounds = el.offset();
					bounds.right = bounds.left + el.outerWidth();
					bounds.bottom = bounds.top + el.outerHeight();
						var time = el.attr('viewport_time');
						var ads_number = el.attr('ads_number');
						time = parseInt(time) + 1;
						el.attr('viewport_time',time);
						if(time > 60 && ads_number <= 3){ 
							ads_number = parseInt(ads_number) + 1;
							el.attr('ads_number',ads_number); 
							el.attr('viewport_time',1);
							el.removeAttr("loaded");
						}
					
				}
			}
		}
	};

	/** ADS **/

	/** Front **/
	var winHeight = $(window).innerHeight();
	
	function goSticky() {
		scrollTop = $(document).scrollTop();
		var $responWinHeight;
		responWinHeight = winHeight - 50;
		if (scrollTop >= responWinHeight) {
			$('#backtotop').addClass('active-on');
		} else{
			$('#backtotop').removeClass('active-on');
		}
	}
	function goBacktotop() {
		$('#backtotop').click(function() {
			$("html, body").stop().animate({scrollTop:1},1500);
		});
	}

	function openSocials() {
		$('.article-social-links').click(function() {
			$(this).toggleClass('active-on');
		});
	}

	function init_back_to_prev_url(){
		var referrer =  document.referrer;
		/*if((referrer.indexOf("http://localhost/ellelast2017/") >= 0)){ } else{
			$('.sprite-goback').addClass('active-off');
			$('.sprite-goback').attr("disabled","disabled");
		}*/
	}

	function back_to_prev_url(){
		history.back();
	}

	function activate_btn(item,item_hide,item_move){
		$('html, body').toggleClass('active-on'); 
		$('#backtotop').toggleClass('hidden');
		if(item_move){ $('.main, .footer, .side-fade, .second-nav').toggleClass('active-on'); }
		item_hide.toggleClass('active-on');
		if((item).hasClass("active-on")){
			$('.btn-activate').removeAttr("disabled");
			$('.btn-activate').removeClass('active-off');
			item.removeClass('active-on');
		}
		else{
			$('.btn-activate').attr("disabled","disabled");
			$('.btn-activate').not(item).addClass('active-off');
			item.removeAttr("disabled");
			item.addClass('active-on');
		}
	}


	function show_submenu() {
		$('.main-nav ul.main-nav-menu>li').click(function(e) {
			if ($(this).find('.sub-menu').hasClass( 'active-on' ) ) {
				$(this).removeClass('active-on');
				$('.sub-menu').removeClass('active-on');
			} else{
				$('.sub-menu').removeClass('active-on').parent('li').removeClass('active-on');
				$(this).find('.sub-menu').toggleClass('active-on');
				$(this).addClass('active-on');
			};
		});
	}

	function unfold_menu() {
		$('.unfold-menu li').click(function(e) {
			if ($(this).children('button').hasClass( 'active-on' ) ) {
				$(this).children('.unfold, button').removeClass('active-on');
			}else{
				$('.unfold-menu li .unfold, .unfold-menu li button').removeClass('active-on');
				$(this).children('.unfold, button').toggleClass('active-on');
			};
		});
	}

	function scrollbar_init() {
		if (device=='md' || device=='lg'){
			$('.main-nav>div').mCustomScrollbar({theme:'dark-2'});	
			$('.search-form').mCustomScrollbar({theme:'dark-2'});		
			$('.main-news>div').mCustomScrollbar({theme:'dark-2'});
		}
	}


	function show_secondnav() {
		scrollTop = $(document).scrollTop();
		if (device=='md' || device=='lg'){
			if ( var_window.scrollTop() >= mainOffset ) {
			        $('.floating-second-nav, .site-logo-wrapper').addClass('active-on');
			        $('.search-form, .main-nav, .social-wrapper').addClass('light-on')
			} else{
				$('.floating-second-nav, .site-logo-wrapper').removeClass('active-on');
				$('.search-form, .main-nav, .social-wrapper').removeClass('light-on')
			}
		}
	}

	function recipe_dispay() {
		var count_ingredients = $(".ingredients-list").children().length;
		$(".ingredients-list").addClass('alt-display');
		var half_count = ((Math.ceil(count_ingredients/2))+1);
		$(".ingredients-list > *:nth-child(n+"+half_count+")").addClass('ingre-right');
		$('.ingre-right').wrapAll( "<div class='container-right' />");
	}


	/** Front **/

	function init_cookie_display(){
		var display_cookie = getcookieELLE('display_cookie');
		if(display_cookie == null) {
			$('#cookie-box').addClass('active-on');
		}	
	}

	function bind_elem(){
		$('.fb-connect').click(function(){ inscriptionAPI(); });
		//$('.newsletter-btn').click(function(){ $('#newsletter-wrapper').toggleClass('active-on'); $('.newsletter-ask').toggleClass('active-on'); })
		$('#newsletter-wrapper').find('.btn-close').click(function(){ $('#newsletter-wrapper').removeClass('active-on');  })
		$('#cookie-box .btn-close').click(function() { setcookieELLE('display_cookie', 1 , 360 ); $('#cookie-box').removeClass('active-on'); });
		/*$('.button_slider').click(function() { format_slider_gallery($(this).parent().parent()); });*/
		$('.burger-btn').click(function() { activate_btn($(this),$('.main-nav'),1); });

		$('.sprite-close').click(function() {$('.burger-btn').removeClass('active-on');$(this).addClass('active-on'); activate_btn($(this),$('.main-nav'),1); });
		
		$('.sprite-close-news').click(function() {$('.news-btn').removeClass('active-on'); $(this).addClass('active-on'); activate_btn($(this), $('.main-news'),1); });

		$('.news-btn').click(function() { 
			ajax_do_news(); 
			activate_btn($(this),$('.main-news'),1); 
			//setTimeout (function(){ $('.main-news>div').mCustomScrollbar({theme:'dark-2'}) } , 1000); 
			
		});
		$('.sprite-search').click(function() { activate_btn($(this), $('.search-form'), 0); setTimeout (function(){ $('.search-input').focus(); }, 50); }); 
		$('.sprite-goback').click(function() { back_to_prev_url(); }); 
		$('.sprite-social').click(function() { activate_btn($(this), $('.social-wrapper'), 0);  });
		
		//$('.mood-next').click(function() { quote_slot_machine(); });

		$('.side-fade').click(function() { 
			if ($('.burger-btn').hasClass("active-on")){
		      	 activate_btn($('.burger-btn'),$('.main-nav'), 1); e.preventDefault();
		    }
		    if ($('.news-btn').hasClass("active-on")){
		      	activate_btn($('.news-btn'), $('.main-news'), 1); e.preventDefault();
		    }
		});
		
		$('.print-button').click(function() { 
			javascript:window.print();
		});

		$('.gdpr_navbar_open').click(function(){ $('.gdpr-privacy-bar').css('display','block'); });
	}



	function bind_keyboard(){
		$(document).keyup(function(e) {
		     if (e.keyCode == 27) { // escape key maps to keycode `27`
		      	if ($('.burger-btn').hasClass("active-on")){
		      		 activate_btn($('.burger-btn'),$('.main-nav'), 1);
		      	}
		      	if ($('.sprite-search').hasClass("active-on")){
		      		activate_btn($('.sprite-search'), $('.search-form'), 0);
		      	}
		      	if ($('.news-btn').hasClass("active-on")){
		      		activate_btn($('.news-btn'), $('.main-news'), 1);
		      	}
		      	if ($('.sprite-social').hasClass("active-on")){
		      		activate_btn($('.sprite-social'), $('.social-wrapper'), 0);
		      	}
		    }
		});
	}

	function google_analytics_load(){
        // Google Analytics Init //
        console.log("LOAD Google Analytics");
		
		ga('create', lang_identifiantGoogleAnalytics, 'elle.be');
		ga('require', 'displayfeatures');
		ga('require', 'linkid', 'linkid.js');

		ga('create', id_google_analytics_specifique, 'elle.be', {'name': 'newTracker'});
		ga('newTracker.require', 'displayfeatures');
		ga('newTracker.require', 'linkid', 'linkid.js');
 		// Google Analytics //
	}

	function google_analytics_init(){
		console.log("INIT Google Analytics");

		if(adpositiontype == ' (Article)'){
			ga('send', 'pageview', {
				'dimension1': id_author,
				'dimension2': adposition + adpositiontype,
				'dimension3': id_post + ' - '+ adposition + ' - ' + article_title + ' - ' + id_author,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});
		}
		else{
			ga('send', 'pageview', {
				'dimension2': adposition + adpositiontype,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});				
		}

		if(adpositiontype == ' (Article)'){
			ga('newTracker.send', 'pageview', {
				'dimension1': id_author,
				'dimension2': adposition + adpositiontype,
				'dimension3': id_post + ' - '+ adposition + ' - ' + article_title + ' - ' + id_author,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});
		}
		else{
			ga('newTracker.send', 'pageview', {
				'dimension2': adposition + adpositiontype,
				'dimension4': adpositionbis + adpositiontype,
				'dimension5': adpositionter + adpositiontype
			});				
		}		
	}

	function load_font_async(){
		(function( w ){
		    // if the class is already set, we're good.
		    if( w.document.documentElement.className.indexOf( "fonts-loaded" ) > -1 ){
		        return;
		    }
		    var fontA = new w.FontFaceObserver( "Elle Gabor Std");
		    var fontB = new w.FontFaceObserver( "Chronicle Display");

		    w.Promise
		        .all([fontA.load(), fontB.load()])
		        .then(function(){
		        	console.log( "Loaded!" ); 
		            w.document.documentElement.className += " fonts-loaded";
		        });
		}( this ));
	}


function rewrite_url_SSL(){
	var arr = window.location.href.split("/");
	if (arr[2] == 'www.elle.be') {
			$('a').each(function(i, e)
			{
					if (typeof $(this).foo !== 'undefined') {
				    var current = $(this).attr('href');
				    if(current.indexOf('elle.be')  > 0){
					    var current = current.replace("http", "https");
					    var current = current.replace("httpss", "https");
					    var new_link = current;
					    if(current.indexOf('?theme=ellev2017') == '-1'){
					    	var new_link = current + '?theme=ellev2017';
					    }
					}
				    $(this).attr('href', new_link);
				    	}
				});
		}
	}

/** Facebook tracking fct **/
function trackingFacebook_fct(){
	/** SINGLE **/
	if($('body').hasClass('single')){
		fbq('track', 'ViewContent', {title: article_title,url: article_link });
		fbq('track', 'ViewCategory', {title: article_category });
		fbq('track', 'ViewTag', {title: article_tag });
	}
	if($('body').hasClass('category')){
	/** CATEGORY **/
		fbq('track', 'ViewCategory', {title: article_category });
	}
	/** TAG **/
	if($('body').hasClass('tag')){
		fbq('track', 'ViewTag', {title: article_tag });
	}
	/** SEARCH */
	/* fbq('track', 'Search', {search_string: '<?php echo $_GET['s'] ?>'});*/
}

/** Facebook tracking **/
function facebook_tracking(){
		trackingFacebook_fct();
}

/** Pebble inread code **/
function myAdDoneFunction(spotx_ad_found){ if(spotx_ad_found) {} else {}};

function mobileAndTabletCheck(){
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
}

var x_var_scroll = 1;
var line_var_scroll = '';
function theme_ad_takeover_scroll(){
	if($('body').hasClass('takeover')){	
		var e = $('.takeover');
		//var div = $('#GenecyDFPAdWallpaperCont div').eq(3).css('top','-300px');

		//$('#GenecyDFPAdWallpaperCont div').eq(6).css({'cssText': 'top: -300px !important; width: 980px; position: fixed; right: 0px; left: 462px;'});

		if(x_var_scroll == 1){
			line_var_scroll = $('#GenecyDFPAdWallpaperCont div').eq(6).attr('style'); 
			console.log(line_var_scroll);
			x_var_scroll = 2;
		}else{
			console.log(line_var_scroll);
		}

		var offset = e.offset();
		var bottom = e.offset().top + e.outerHeight(true)
		var top  = window.pageYOffset || document.documentElement.scrollTop;

		var windows_top = top;
		var ads_top = 170 - top;
		var ads_max = 1200;

		var height = $(window).height();
		var new_var_test = ads_max - height;

		var ads_max = 1134;

		var ads_height_screen = $(window).height();

		var ads_diff = (ads_height_screen - ads_max)/2;

		if(ads_top <= 0 ){ ads_top = ads_diff; }
		console.log('windows_top : ' + windows_top);
		console.log('offset : ' + ads_height_screen);
		console.log('TakeOverScrool : ' + ads_top);
		console.log('new_var_test : '+ new_var_test);

		$('#GenecyDFPAdWallpaperCont div').eq(6).css({'cssText': line_var_scroll +'top: -'+new_var_test+'px !important;'});

		if(windows_top < 170) {$('#GenecyDFPAdWallpaperCont div').eq(6).css({'cssText': line_var_scroll +'top: 0px !important;'}); }

		//$('.theme-ad-takeover').attr('style', 'background-position: center '+ ads_top + 'px !important');
		
		//div.css('background-position','left '+ads_top+'px');
		//if((windows_top + ads_height_screen) > 1400) { div.css('background-position','left bottom');}
		//if(windows_top < 170) { div.css('background-position','left top'); }

		/*div_right.attr('style', 'background-position: right '+ ads_top + 'px !important; '+ current_attribute_css_right);
		if((windows_top + ads_height_screen) > 1400) { div_right.attr('style', 'background-position: right bottom !important; ' + current_attribute_css_right); }
		if(windows_top < 170) { div_right.attr('style', 'background-position: right '+ ads_top + 'px !important; '+ current_attribute_css_right); }*/
		
	}
}

function init_ligatus(){ 
	console.log('Loading : Ligatus');
	if(lang_identifiant == 'FR'){
		script_ligatus = '<script type="text/javascript" src="https://a-ssl.ligatus.com/?ids=87661&t=js&s=1"></script>';
		script_share = '<script src="//delivery.content-recommendation.net/fr.elle.be/widget.js"></script>';
		postscribe('#lig_elle_be_fr_sb_article',script_ligatus);
	}else if(lang_identifiant == 'NL'){
		script_ligatus = '<script type="text/javascript" src="https://a-ssl.ligatus.com/?ids=87663&t=js&s=1"></script>';
		script_share = '<script src="//delivery.content-recommendation.net/nl.elle.be/widget.js"></script>';	
		postscribe('#lig_elle_be_be_sb_article',script_ligatus);	
	}

	if(device != 'sm'){ /*console.log('Loading : Cross Content'); postscribe('#elleRA2MW',script_share);*/ }
	else{ console.log('No Loading : Cross Content'); }
}

function init_skimlinks(){ 
	if(no_skimlink_script == 1){
		console.log('Loading skimlink : false')
	}else{
		console.log('Loading skimlink : true')
		var script = document.createElement( 'script' );
		script.type = 'text/javascript';
		script.src = 'https://s.skimresources.com/js/117847X1577491.skimlinks.js';
		$("body").append( script );	
	}
}

function init_show_ads(){ 
	console.log('Loading : ShowAds');
	var script = document.createElement( 'script' );
	script.type = 'text/javascript';
	script.src = stylesheet_directory_uri+'/js/show_ads.js';
	$("body").append( script );
}

function obatinCanUrlTrad() {
    var canonical = "";
    var links = document.getElementsByTagName("link");
    for (var i = 0; i < links.length; i++) {
        if (links[i].getAttribute("rel") === "canonical") {
            canonical = links[i].getAttribute("href");
        }
    }
    return canonical;
}


function add_class_menu(){
	$('.sub-menu').parent().addClass('menu-item-has-children');
}

function init_youtube(){
	$(".player_youtube_wrapper iframe").each(function(){
	if(!$(this).attr("loaded")){
		$(this).attr("loaded",true);
			var youtube_src = $(this).data('srcset').toString();
			$(this).removeAttr('data-srcset');
			var setyoutube_src = $(this).attr('src', youtube_src);
			console.log("Youtube Loaded : " + setyoutube_src);
		}
	});
}

function triggerInernalLinkClicks(){
	$('.related-article a').click(function(){
		ga('send', 'event', {
		    eventCategory: 'Sidebar Internal Link',
		    eventAction: 'click',
		    eventLabel: $(this).attr("href")
		});
	});

	$('.bottom-related a').click(function(){
		ga('send', 'event', {
		    eventCategory: 'Bottom Internal Link',
		    eventAction: 'click',
		    eventLabel: $(this).attr("href")
		});
	});
}

function init_youtube_sidebar(){
	var youtube = document.querySelectorAll( ".sidebar_youtube" );
	    
	    for (var i = 0; i < youtube.length; i++) {
	        
	        var source = youtube[i].dataset.picture;
	        
	        var image = new Image();
	                image.src = source;
	                image.addEventListener( "load", function() {
	                    youtube[ i ].appendChild( image );
	                }( i ) );
	        
	                youtube[i].addEventListener( "click", function() {

	                    var iframe = document.createElement( "iframe" );

	                            iframe.setAttribute( "frameborder", "0" );
	                            iframe.setAttribute( "allowfullscreen", "" );
	                            iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

	                            this.innerHTML = "";
	                            this.appendChild( iframe );

	                	console.log('Click Youtube : ' + window.location.href);
						ga('send', 'event', {
						    eventCategory: 'Youtube sidebar',
						    eventAction: 'play',
						    eventLabel: window.location.href 
						});

						ga('newTracker.send', 'event', {
						    eventCategory: 'Youtube sidebar',
						    eventAction: 'play',
						    eventLabel: window.location.href 
						});
					
	                } );    
	    }; 
}

var _obf_link = function(event) {
	var attribute = this.getAttribute("data-atc");              
	var outlink = this.getAttribute("data-outlink");   
        if(event.ctrlKey || outlink == 'true') {                   
             var newWindow = window.open(decodeURIComponent(window.atob(attribute)), '_blank');                    
             newWindow.focus();               
        } else {                    
             document.location.href= decodeURIComponent(window.atob(attribute));
        }
	}

function init_obf_link(){
	var classname = document.getElementsByClassName("atc");
		for (var i = 0; i < classname.length; i++) {
				classname[i].addEventListener('click', _obf_link, false);
		}
}

function add_elle_run(){
	console.log('add_elle_run');
	first_block = $('.site-inner').find('.swiper-article-block:eq(0)');
	if(window.innerWidth > 955){
		if(lang_identifiant == 'FR'){
			$(first_block).after('<div class="center"><a target="_blank" href="https://bit.ly/2kohkmInl"><img src="https://www.elle.be/fr/wp-content/themes/elleprod/img/ads/eaf/leaderboard_elleactive_FR.gif"/></a></div>');
		}else{
			$(first_block).after('<div class="center"><a target="_blank" href="https://bit.ly/2kohkmInl"><img src="hhttps://www.elle.be/fr/wp-content/themes/elleprod/img/ads/eaf/leaderboard_elleactive_NL.gif"/></a></div>');
		}
	}else{
		if(lang_identifiant == 'FR'){
			$(first_block).after('<div class="center"><a target="_blank" href="https://bit.ly/2kohkmInl"><img src="https://www.elle.be/fr/wp-content/themes/elleprod/img/autopromo/imu_elleactive_2_2.gif"/></a></div>');
		}else{
			$(first_block).after('<div class="center"><a target="_blank" href="https://bit.ly/2kohkmInl"><img src="https://www.elle.be/fr/wp-content/themes/elleprod/img/autopromo/imu_elleactive_NL.gif"/></a></div>');
		}
	
	}
}

function add_inread_block(elem){
	if(id_author_nb != id_commercial){
		console.log('Setup : pebbleInarticle_wrapper');
		ads_id = Math.floor(Math.random() * 1000000) + 1;

		second_paragraphe = $(elem).find('.entry-content p:eq(0)');
		if($(second_paragraphe).parent().hasClass('entry-content')){
			console.log('Setup : pebbleInarticle_wrapper 2nd p');
			$(second_paragraphe).after('<div id="pebbleInarticle_wrapper_' + ads_id + '" class="ads Inarticle ads-first" data-categoryAd="" data-formatMOB="[[1,1]]" data-format="[[1,1]]" data-location="" data-position=""></div>');
		}

		/*
		if( 1 != 2 )
		{
			if(lang_identifiant == 'FR'){
				$autopromo_imu_src = 'https://www.elle.be/fr/wp-content/themes/elleprod/img/autopromo/imu_elleactive_2_2.gif';
				$autopromo_imu_link = 'https://bit.ly/2kohkmInl';
			}else{
				$autopromo_imu_src = 'https://www.elle.be/fr/wp-content/themes/elleprod/img/autopromo/imu_elleactive_NL.gif';
				$autopromo_imu_link = 'https://bit.ly/2kohkmInl';
			}
		}
		else
		{
			if(lang_identifiant == 'FR'){
				$autopromo_imu_src = 'https://www.elle.be/fr/wp-content/themes/elleprod/img/autopromo/imu_elleactive_2_2.gif';
				$autopromo_imu_link = 'https://bit.ly/2kohkmInl';
			}else{
				$autopromo_imu_src = 'https://www.elle.be/fr/wp-content/themes/elleprod/img/autopromo/imu_elleactive_NL.gif';
				$autopromo_imu_link = 'https://bit.ly/2kohkmInl';
			}
		}
		*/

		second_title = $(elem).find('.entry-content h2:eq(1)');
		if($(second_title).parent().hasClass('entry-content')){
			ads_id = Math.floor(Math.random() * 1000000) + 1;
			ads_code = '<div id="mmr-'+ads_id+'" class="ads leaderboard ads-first" data-categoryAd="" data-formatMOB="[[300, 250],[600,500]]" data-format="" data-refad="" data-location="" data-position="2"></div>';
			$(second_title).before(ads_code);
		}

		third_title = $(elem).find('.entry-content h2:eq(2)');
		if($(third_title).parent().hasClass('entry-content')){
			if(lang_identifiant == 'FR'){
				console.log('LOAD QUANTUM 29 FR');
				ads_id = Math.floor(Math.random() * 1000000) + 1;
				$(third_title).before('<div id="quantumInarticle_wrapper_' + ads_id + '"></div>');
				postscribe('#quantumInarticle_wrapper_'+ads_id,'<div class="qtx_placement"></div><script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"><\/script>');
			}
			else if(lang_identifiant == 'NL'){
				console.log('LOAD QUANTUM 29 NL');
				ads_id = Math.floor(Math.random() * 1000000) + 1;
				$(third_title).before('<div id="quantumInarticle_wrapper_' + ads_id + '"></div>');
				postscribe('#quantumInarticle_wrapper_'+ads_id,'<div class="qtx_placement"></div><script type="text/javascript" id="quantx-embed-tag" src="//cdn.elasticad.net/native/serve/js/quantx/nativeEmbed.gz.js"><\/script>');
			}
		}

		/*four_title = $(elem).find('.entry-content h2:eq(3)');
		if($(four_title).parent().hasClass('entry-content')){
			$(four_title).before('<div class="center"><a target="_blank" href="'+$autopromo_imu_link+'"><img src="'+$autopromo_imu_src+'"/></a></div>');
		}*/


		five_title = $(elem).find('.entry-content h2:eq(4)');
		if($(five_title).parent().hasClass('entry-content')){
			ads_id = Math.floor(Math.random() * 1000000) + 1;
			ads_code = '<div id="mmr-'+ads_id+'" class="ads leaderboard ads-first" data-categoryAd="" data-formatMOB="[[300, 250],[640,150],[320,75]]" data-format="" data-refad="" data-location="" data-position="2"></div>';
			$(five_title).before(ads_code);
		}

		/*third_related = $(elem).find('.sidebar .sidebar-related .related-article:eq(2)');
		if($(third_related).parent().hasClass('sidebar-related')){
			$(third_related).after('<div class="center"><a target="_blank" href="'+$autopromo_imu_link+'"><img src="'+$autopromo_imu_src+'"/></a></div>');
		}*/

	}
}

function recherche_submit(){
	$( "#searchform" ).submit(function( event ) {
	  alert( "Handler for .submit() called." );
	  event.preventDefault();
	});
}

function init_recherche_sidebar(){

	$('.submit-sidebar-form').on('click', function() {
	    $(this).closest("form").submit();
	});

}

/*
function add_inread_quantum_block(elem){
		four_paragraphe = $(elem).find('.entry-content p:eq(4)');
		if($(four_paragraphe).parent().hasClass('entry-content')){
			if(lang_identifiant == 'FR'){
			$(four_paragraphe).after('<iframe src="https://www.elle.be/fr/wp-content/themes/elleprod/iframe_quantum.html?siteid35743=true" width="670" height="250" frameborder="0" class="" scrolling="no"></iframe>');
			}
			else if(lang_identifiant == 'NL'){
			$(four_paragraphe).after('<iframe src="https://www.elle.be/nl/wp-content/themes/elleprod/iframe_quantum.html" width="670" height="250" frameborder="0" class="" scrolling="no"></iframe>');
			}
		}
}
*/

function displayPopup(){
	$.ajax({
		url: ajax_search.ajaxurl,
		type: 'post',
		data: { action: 'ajax_display_popup'},
		beforeSend: function() {

		},
		success: function( html ) {
			if(html != ''){
				$('.footer-ventures').after( html );

				$('.btn-close').click(function() {
					$('#popup-container').removeClass('active-on');
				});

				if($('#popup-container').hasClass("active-on")){
					$('#popup-container').removeClass('active');
				}			
			}else{
				$('.footer-ventures').after( html );
			}
		},
		error: function (xhr) {
			console.log('Error loading popup'); 
		} 
	})
}

function newsletters_popup(){
	var display = getcookieELLE('display_news_popup');
	console.log('Display news : ' + display);
    if(display == null ){ display = 0; setcookieELLE( 'display_news_popup', 0, 90 ); }
    if(display == 0){
    	setcookieELLE( 'display_news_popup', 1 , 90 );
    	displayPopup();
 	}
}

function magazin_online_popup(){
	var display = getcookieELLE('display_promo_online');
	console.log('Display promo : ' + display);
	$('#btn-close-popup-online').click(function() {
		console.log('close popup promo obline');
		$('.popup-online').removeClass('active-on');
	});
    if(display == null ){ display = 0; setcookieELLE( 'display_promo_online', 0, 90 ); }
    if(display == 0){
    	$('.popup-online').addClass('active-on');
    	setcookieELLE( 'display_promo_online', 1 , 90 );
 	}
}



function animate_progress_bar(){
	$(window).scroll(function () {
   		$('.main-wrapper').each(function (i) {
				console.log(i);		
			    winHeight = $(window).height(); 
				docHeight = $(this).height() * (i + 1) - (i * 100);
				max = docHeight - winHeight;
				value = $(window).scrollTop();

				width = (value/max) * 100;
				width = width + '%';
				console.log(width);
				$('.LagHeader-progress-bar').css({'width': width});		
		});
	});
}

function unfold_eaf(){
    $('.event_eaf_main .programme_sujet').click(function() {
        $(this).toggleClass('active-on');
        $(this).parent('.programme').toggleClass('active-on');
    });
}

function init_editorshop_home(){
	if( $('.swiper-container-editorsshop').length )         // use this if you are using class to check
	{
		if(device == 'sm'){
			slidesPerView = 1; 
			initialSlide = 0;
			delai = 3500;
		}else{
			slidesPerView = 5;
			initialSlide = 2;
			delai = 99999999;
		}
		var swiperEditorShop = new Swiper('.swiper-container-editorsshop', {
			loop: true,
			watchSlidesProgress: true,
            watchSlidesVisibility: true,
	        slidesPerView: slidesPerView,
	        paginationClickable: true,
	        spaceBetween: 30,
	        passiveListeners:true,
	        centeredSlides: true,
	        initialSlide : initialSlide,
 			autoplay: {
			   delay: delai,
			},
			navigation: {
	        nextEl: '.swiper-button-next',
	        prevEl: '.swiper-button-prev',
	    	 },
	    });
	}
}

function init_editorshop_sidebar(){
	if( $('.swiper-container-editorsshopsidebar').length )         // use this if you are using class to check
	{
		if(device != 'sm'){
			slidesPerView = 1; 
			initialSlide = 0;
			delai = 3500;

			var swiperEditorShopSidebar = new Swiper('.swiper-container-editorsshopsidebar', {
				loop: true,
				watchSlidesProgress: true,
	            watchSlidesVisibility: true,
		        slidesPerView: slidesPerView,
		        paginationClickable: true,
		        spaceBetween: 30,
		        passiveListeners:true,
		        centeredSlides: true,
		        initialSlide : initialSlide,
	 			autoplay: {
				   delay: delai,
				},
				navigation: {
		        nextEl: '.swiper-button-next',
		        prevEl: '.swiper-button-prev',
		    	 },
		    });
		}
	}
}

function fire_desktop(){
	/* FIRE	 */
	console.log('Fire Desktop');
	if(device == 'lg'){
	  setTimeout(function() {
		  location.reload();
		}, 3000);
	}
}

$(document).ready(function() {

	let d = new Date();
	console.log('DATE READY '+ d);

	//console.log('Google Analytics : GDPR')
	//google_analytics_load();
	//google_analytics_init();

	//animate_progress_bar();
	last_object = $(".content article").last();
	last_object_wrapper = $(".content").last();

	what_device();
	init_skimlinks();

	//fire_desktop();

	if($('body').hasClass('home')){
		init_editorshop_home();
	}

	if($('body').hasClass('single')){
		console.log('Single');
		init_editorshop_sidebar();
		load_total_multipage($('article'));
	}

	magazin_online_popup();
	init_recherche_sidebar();
	init_youtube_sidebar();

	goBacktotop();
	openSocials();
	add_class_menu();
	init_youtube();
	init_slider_gallery();
	unfold_ventures();
    unfold_eaf();
    recipe_dispay();

    if($('body').hasClass('home') || $('body').hasClass('category')){
 	   //add_elle_run();
	}

	bind_elem();
	bind_keyboard();
 	lazy_load();	

 	show_submenu();
	unfold_menu();
	init_obf_link();


  	window.setInterval(function(){
		$('.ads').each(function(){ 
			//postscribe_ads($(this)); 
			if($(this).visible( true )){
				//postscribe_ads($(this)); 
				if($(this).data("format") != '' && $(this).data("formatmob") != '' ){
					//$(this).css("border", "1px solid purple");
  			}else{
	  				if($(this).data("format") != ''){
	  					//$(this).css("border", "1px solid red");
	  				}else if($(this).data("formatMOB") != ''){
	  					//$(this).css("border", "1px solid blue");
	  				}
				}
			}else{
				//$(this).css("border", "0px");
			}
		});
	}, 500 )
	  	
});

window.addEventListener("load", function(){

	let d = new Date();
	console.log('DATE LOAD '+ d);
	
	if($('body').hasClass('category') || $('body').hasClass('home') || $('body').hasClass('author') || $('body').hasClass('tag')){
		init_slider_instagram();
	}

	facebook_tracking();

window.addEventListener("scroll", function(){
		theme_ad_takeover_scroll();
		init_slider_gallery();
		goSticky();
				

		lazy_load();
		if($('body').hasClass('single')){
			load_article_scroll();
			multipage_scroll(last_object);
		}				

		show_secondnav();
	})
});

})(jQuery, this);