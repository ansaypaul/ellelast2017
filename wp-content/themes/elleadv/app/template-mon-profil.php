<?php
/*
Template Name: mon-profil 2018
*/
?>

<?php get_header(); ?>

<?php 

$username='elle/WebServices_Paul';
$password='87BOI8HC';
$URL='https://www.actito.be/ActitoWebServices/ws/v4/entity/Ventures/table/ventures/profile?search=Token%3D'.$_GET['Token'];
$fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_STDERR, $fp);
curl_setopt($ch, CURLOPT_URL,$URL);
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
$result  = curl_exec($ch);

$days = array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
$months = array('01:Janvier','02:Février','03:Mars','04:Avril','05:Mai','06:Juin','07:Juillet','08:Août','09:Septembre','10:Octobre','11:Novembre','12:Décembre');
$years = array_combine(range(date("Y"), 1930), range(date("Y"), 1930));
$subscriptions_init = array('Marie Claire','ELLE Partenaire', 'ELLE', 'Life Magazine','Psychologies','Déco Idées');

if (empty($result)) {
    // some kind of an error happened
   /* echo "No server responded<br />";*/
    die(curl_error($ch));
    curl_close($ch); // close cURL handler
} else {
    $info = curl_getinfo($ch);
    curl_close($ch); // close cURL handler
    /*echo "Server responded: <br />";*/
    if (empty($info['http_code'])) {
            die("No HTTP code was returned");
    } else {
      
       /* echo "The server responded: <br />";
        echo $info['http_code'];
        echo $result ;*/
    }

}

$profil = json_decode($result);

if(isset($_POST)  && !empty($_POST))
{
  $lastname = $_POST['attributes'][0]['value'];
  $firstName = $_POST['attributes'][1]['value'];
  $addressBox = $_POST['attributes'][2]['value'];
  $addressLocality = $_POST['attributes'][3]['value'];
  $addressNumber = $_POST['attributes'][4]['value'];
  $gsmNumber = $_POST['attributes'][5]['value'];
  $emailAddress = $_POST['attributes'][7]['value'];
  $addressPostalCode = $_POST['attributes'][8]['value'];
  $addressStreet = $_POST['attributes'][9]['value'];
  $birthDate = $_POST['attributes'][10]['value'];
  //$civilState = $_POST['attributes'][11]['value'];
  $sex = $_POST['attributes'][11]['value'];

  $day_profil = $_POST['dob_day'];
  $month_profil = $_POST['dob_month'];
  $year_profil = $_POST['dob_year'];

  $subscriptions_profil = $_POST['subscriptions'];
  /*$motherLanguage =
  $civilState =
  $creationSource =*/
}
else 
{
  $id_profile = $profil->profiles[0]->attributes[0]->value;
  $lastname = $profil->profiles[0]->attributes[3]->value;
  $firstName = $profil->profiles[0]->attributes[4]->value;
  $addressBox = $profil->profiles[0]->attributes[14]->value;
  $addressLocality = $profil->profiles[0]->attributes[9]->value; 
  $addressNumber = $profil->profiles[0]->attributes[7]->value; 
  $gsmNumber = $profil->profiles[0]->attributes[12]->value;
  $emailAddress = $profil->profiles[0]->attributes[5]->value;
  $addressPostalCode = $profil->profiles[0]->attributes[8]->value;
  $addressStreet = $profil->profiles[0]->attributes[6]->value; 
  $birthDate = $profil->profiles[0]->attributes[10]->value; 
 // $civilState = $profil->profiles[0]->attributes[16]->value; 
  $sex = $profil->profiles[0]->attributes[19]->value; 

  foreach ($profil->profiles[0]->subscriptions as $subscription) {
    $subscriptions_profil[] = $subscription->name;
  }

if(isset($birthDate)){
  $day_profil = explode('/',$birthDate)[0];
  $month_profil = explode('/',$birthDate)[1];
  $year_profil = explode('/',$birthDate)[2];
}


} ?>
<main role="main" class="main main-page">
  <div class="page">
		<!-- site-inner -->
		<div class="site-inner">
      	<div class="profil-page">
				<h1 class="entry-title"><?php _e( 'Complétez votre profil', 'html5blank' ); ?></h1>
				<div class="newsletter-content">
				<form method="POST" role="form" id="profil_news" name="profil_popup_form"> 
    		      <input type='hidden' name='Token' value='<?php echo $_GET['Token']; ?>'>
              <input type="hidden" name="profil_creationSource" value="<?php _e('ELLE-FR', 'html5blank' ); ?>">
              <input type="hidden" name="profil_motherLanguage" value="<?php _e('FR', 'html5blank' ); ?>">
		          <input type='hidden' name='subscriptions[]' value='ELLE'>
		          <!--<input type='hidden' name='attributes[12][name]' value='birthDate'>-->
		          <div class="civil">
			          <input <?php if($sex == 'M'){ echo'checked'; } ?> name="profil_civilite" id="civilMonsieur" value="M"  type="radio"><label for="civilMonsieur"><?php _e( 'Monsieur', 'html5blank' ); ?></label>
			          <input <?php if($sex == 'F'){ echo'checked'; } ?> name="profil_civilite" id="civilMadame" value="F" type="radio"><label for="civilMadame"><?php _e( 'Madame', 'html5blank' ); ?></label>
			      </div>
			      <div class="label-input">
			      	<label for="form-firstname"><?php _e( 'Prénom', 'html5blank' ); ?><sup>*</sup></label>
			        <input id="form-firstname" name='profil_prenom' value='<?php echo $firstName ?>'>
			      </div>
			       <div class="label-input">
			      	<label for="form-lastname"><?php _e( 'Nom', 'html5blank' ); ?><sup>*</sup></label>
			        <input id="form-lastname" name='profil_nom'  value='<?php echo $lastname ?>'>
			      </div>
			       <div class="label-input">
			      	<label for="form-email"><?php _e( 'Email', 'html5blank' ); ?><sup>*</sup></label>
			        <input class="newsletter-input" id="form-email" name='profil_email' value='<?php echo $emailAddress; ?>' readonly>
			      </div>
			      <div class="label-input select-date">
			      	<label><?php _e( 'Date de naissance', 'html5blank' ); ?><sup>*</sup></label>	
			        <select id="form_day" name="dob_day">
			        <option value="-"><?php _e( 'Jour', 'html5blank' ); ?></option>
			        <?php foreach ($days  as $day) 
			        {
			          if($day_profil == $day){
			          echo '<option selected value="'.$day.'">'.$day.'</option>';
			          }else{
			          echo '<option value="'.$day.'">'.$day.'</option>';
			          }
			        }
			        ?>
			        </select>
			        <select id="form_month" name="dob_month">
			          <option value="-"><?php _e( 'Mois', 'html5blank' ); ?></option>
				        <?php foreach ($months  as $month) 
				        {
				          if($month_profil == explode(':',$month)[0]){
				          echo '<option selected value="'.explode(':',$month)[0].'">'.explode(':',$month)[1].'</option>';
				          }else{
				          echo '<option value="'.explode(':',$month)[0].'">'.explode(':',$month)[1].'</option>';
				          }
			        	}?>
			        </select>
			        <select id="form_year" name="dob_year">
			          <option value="-"><?php _e( 'Année', 'html5blank' ); ?></option>
			          <?php foreach ($years  as $year) 
			          {
			            if($year_profil == $year){
			            echo '<option selected value="'.$year.'">'.$year.'</option>';
			            }else{
			            echo '<option value="'.$year.'">'.$year.'</option>';
			            }
			          }
			          ?>
			        </select>
			      </div>
			      <div class="label-input">
			      	<label for="form-email"><?php _e( 'Portable', 'html5blank' ); ?></label>
			        <input id="form-email" name='profil_gsm' value='<?php echo $gsmNumber ?>' title="Portable" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" >
			      </div>
			       <div class="label-input">
			      	<label for="form-street"><?php _e( 'Rue', 'html5blank' ); ?><sup>*</sup></label>
			        <input id="form-street" name='profil_adresseStreet' title="Nom de rue" value='<?php echo $addressStreet  ?>'>
			      </div>
			       <div class="label-input">
			      	<label for="form-number"><?php _e( 'Numéro de rue', 'html5blank' ); ?><sup>*</sup></label>
			        <input id="form-number" name='profil_addressNumber'  title="Numéro de rue" value='<?php echo $addressNumber  ?>'  maxlength="10">
			      </div>
			       <div class="label-input">
			      	<label for="form-box"><?php _e( 'Boîte postale', 'html5blank' ); ?></label>
			        <input id="form-box" name='profil_addressBox' title="Boîte postale" value='<?php echo $addressBox ?>' maxlength="10">
			      </div>
			       <div class="label-input">
			      	<label for="form-code"><?php _e( 'Code postal', 'html5blank' ); ?><sup>*</sup></label>
			        <input id="form-code" name='profil_addressPostalCode' title="Code postal" value='<?php echo $addressPostalCode ?>' maxlength="10">
			      </div>
			      <div class="label-input">
			     	<label for="form-city"><?php _e( 'Ville', 'html5blank' ); ?><sup>*</sup></label>
			        <input id="form-city" name='profil_addressLocality' title="Ville" value='<?php echo $addressLocality ?>' maxlength="50">
			      </div>
			      <input type='hidden' name='profil_motherLanguage' value='fr'>
			      	<div class="all-label-check">
	            		<div class="label-check">
	            			<label><?php _e( "Des newsletters générales reprenant les articles du ELLE Belgique comprenant du contenu rédactionnel &amp; sponsorisé mais aussi les évènements du ELLE Belgique, les concours, actions et promotions.", 'html5blank' ); ?></label>
	             			 <input class="newsletter-optin" type="checkbox" name="profil_subscriptions[]" value="ELLE" <?php if(in_array( 'ELLE',$subscriptions_profil)){echo 'checked';}?>>
			            </div>
			            <div class="label-check">
			             <label><?php _e( "Des newsletters dédiées à nos partenaires comprenant exclusivement des publications sponsorisées et les concours.", 'html5blank' ); ?></label>
			              	<input class="newsletter-optin" type="checkbox" name="profil_subscriptions[]" value="ELLE Partenaire" <?php if(in_array( 'ELLE Partenaire',$subscriptions_profil)){echo 'checked';}?>>  
			            </div>
			            <div class="label-check">
			            	<label><?php _e( "Des sondages et enquêtes pour participer à l’évolution constante du ELLE Belgique.", 'html5blank' ); ?></label>
			              	<input class="newsletter-optin" type="checkbox" name="profil_subscriptions[]" value="ELLE Sondage" <?php if(in_array( 'ELLE Sondage',$subscriptions_profil)){echo 'checked';}?>>
			            </div>
			            <div class="label-check">
			            	<label><?php _e( "J’ai lu et j’accepte la déclaration sur la vie privée instaurée par Edition Ventures.", 'html5blank' ); ?></label>
			            	<input class="newsletter-optin" type="checkbox" name="profil_marketingConsent" value="1" required>
			            </div>
            		</div>
			      <h2><?php _e( "Je veux m'inscrire à plus de newsletters", 'html5blank' ); ?>:</h2><br>
	
			      <div class="label-mag">
			      	<input type='checkbox' name='profil_subscriptions[]' value='Marie Claire' <?php if(in_array( 'Marie Claire',$subscriptions_profil)){echo 'checked';}?>>
			      	<a href="<?php _e( "http://www.marieclaire.be/fr/", 'html5blank' ); ?>" target="_blank" class="mag-logo sprite sprite-mc"></a>
			      	<label><?php _e( "Des newsletters générales reprenant les articles du Marie Claire comprenant du contenu rédactionnel &amp; sponsorisé mais aussi les évènements du Marie Claire, les concours, actions et promotions.", 'html5blank' ); ?></label>
			      </div>
			      <div class="label-mag">
			      	<input type='checkbox' name='profil_subscriptions[]' value='Psychologies' <?php if(in_array( 'Psychologies',$subscriptions_profil)){echo 'checked';}?> >
			      	<a href="http://www.psychologies.com/" target="_blank" class="mag-logo sprite sprite-psycho"></a>
			      	<label><?php _e( "Des newsletters générales reprenant les articles du Psychologies comprenant du contenu rédactionnel &amp; sponsorisé mais aussi les évènements du Psychologies, les concours, actions et promotions.", 'html5blank' ); ?></label>
			      </div>
			      <div class="label-mag">
			      	<input type='checkbox' name='profil_subscriptions[]' value='Déco Idées' <?php if(in_array( 'Déco Idées',$subscriptions_profil)){echo 'checked';}?> ><a href="<?php _e( "http://www.decoidees.be/", 'html5blank' ); ?>" target="_blank" class="mag-logo sprite sprite-di"></a>
			      	<label><?php _e( "Des newsletters générales reprenant les articles du Déco Idées comprenant du contenu rédactionnel &amp; sponsorisé mais aussi les évènements du Déco Idées, les concours, actions et promotions.", 'html5blank' ); ?></label>
			      </div>
			    <div class="mentions-legales">
			    <?php _e( "
				<h4>Sachez que vous disposez des droits suivants :</h4>
				    <ol>
				  	  <li>Vous avez le droit d'information et d'accès à vos données personnelles.</li>
				  	  <li>Vous avez le droit de rectification, d’effacement et de limitation du traitement de vos données personnelles.</li>
				  	  <li>Vous avez le droit d'opposition.</li>
				  	  <li>Vous avez le droit à la portabilité de vos données personnelles. </li>
				    </ol>
				    <p>Sachez également que vous avez le droit de retirer votre consentement au traitement de vos données à tout moment.</p>
				    <p>Nous recueillons uniquement les données dont nous avons besoin pour vous informer des activités du Elle Belgique.</p>
				    <p>Si vous souhaitez exercer un de ces droits, nous vous invitons à nous envoyer un e-mail à l’adresse suivante <a href='mailto:privacy@editionventures.be'>privacy@editionventures.be</a> ou à consulter la déclaration sur le respect de la vie privée du Groupe Edition Ventures.</p>
				    <p>Vous pouvez vous désabonner à tout moment en cliquant sur le lien présent dans le bas de chacun de nos courriels.</p>"
				    , 'html5blank' ); 
				?>
			    </div>
			    
			      <input id="newsletter-submit" type='submit' class="formulaire-submit btn-v1" value='Valider'/>
			      <?php if(isset($_POST)  && !empty($_POST)) {?> <p><?php _e( "Merci d'avoir complété votre profil", 'html5blank' ); ?> !</p><?php } ?>
				</form>
			</div>
       	<div class="newsletter-result hide">Votre profil a bien été mis à jour.</div>
		</div>
      </div>
			<footer class="entry-footer"></footer>
		</div>
	</div>
</main>

<style>
    .newsletter-result{font-size: 15px;}
</style>
<?php get_footer(); ?>