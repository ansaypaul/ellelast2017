<?php
/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');

    register_post_type('membre', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => 'Membre', // Rename these to suit
            'singular_name' => 'Membre',
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'publicly_queryable'  => false,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));

      register_post_type('newsletter', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => 'Newsletters', // Rename these to suit
            'singular_name' => 'Newsletters',
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
      
}

    //Add custom type Cover
    register_post_type(
      'magazines',
      array(
        'label' => 'Magazines',
        'labels' => array(
          'name' => 'Magazines',
          'singular_name' => 'Magazine',
          'all_items' => __('Tous les magazines', 'html5blank'),
          'add_new_item' => __('Ajouter un magazine', 'html5blank'),
          'edit_item' => __('Éditer le magazine', 'html5blank'),
          'new_item' => __('Nouveau magazine', 'html5blank'),
          'view_item' => __('Voir le magazine', 'html5blank'),
          'search_items' => __('Rechercher parmi les magazines', 'html5blank'),
          'not_found' => __('Pas de magazine trouvé', 'html5blank'),
          'not_found_in_trash'=> __('Pas de magazine dans la corbeille', 'html5blank')
          ),
        'public' => true,
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-book',
        'menu_position' => 4,
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ),
        'has_archive' => false
      )
    );

        //Add custom type Cover
    register_post_type(
      'magazines-eat',
      array(
        'label' => 'Magazines-eat',
        'labels' => array(
          'name' => 'Magazines EAT',
          'singular_name' => 'Magazine-eat',
          'all_items' => __('Tous les magazines Eat', 'html5blank'),
          'add_new_item' => __('Ajouter un magazine Eat', 'html5blank'),
          'edit_item' => __('Éditer le magazine Eat', 'html5blank'),
          'new_item' => __('Nouveau magazine Eat', 'html5blank'),
          'view_item' => __('Voir le magazine Eat', 'html5blank'),
          'search_items' => __('Rechercher parmi les magazines Eat', 'html5blank'),
          'not_found' => __('Pas de magazine trouvé Eat', 'html5blank'),
          'not_found_in_trash'=> __('Pas de magazine dans la corbeille Eat', 'html5blank')
          ),
        'public' => true,
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-book',
        'menu_position' => 4,
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ),
        'has_archive' => false
      )
    );
    //Add custom type Cover
    register_post_type(
      'instagram',
      array(
        'label' => 'Instagram',
        'labels' => array(
          'name' => 'Instagram',
          'singular_name' => 'Instagrams',
          'all_items' => __('Tous les Instagrams', 'html5blank'),
          'add_new_item' => __('Ajouter un Instagram', 'html5blank'),
          'edit_item' => __("Éditer l'Instagram", 'html5blank'),
          'new_item' => __('Nouveau Instagram', 'html5blank'),
          'view_item' => __("Voir l'Instagram", 'html5blank'),
          'search_items' => __('Rechercher parmi les Instagrams', 'html5blank'),
          'not_found' => __("Pas d'Instagram trouvé", 'html5blank'),
          'not_found_in_trash'=> __("Pas d'Instagram dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-format-image',
        'menu_position' => 5,
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ),
        'has_archive' => false
      )
    );

    //Add custom type Autopromo
    register_post_type(
      'autopromo',
      array(
        'label' => 'Autopromo',
        'labels' => array(
          'name' => 'Autopromo',
          'singular_name' => 'Autopromo',
          'all_items' => __('Toutes les autopromos', 'html5blank'),
          'add_new_item' => __('Ajouter une autopromo', 'html5blank'),
          'edit_item' => __("Éditer l'autopromo", 'html5blank'),
          'new_item' => __('Nouvelle autopromo', 'html5blank'),
          'view_item' => __("Voir l'autopromo", 'html5blank'),
          'search_items' => __('Rechercher parmi les autopromos', 'html5blank'),
          'not_found' => __("Pas d'autopromo trouvée", 'html5blank'),
          'not_found_in_trash'=> __("Pas d'autopromo dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-tag',
        'menu_position' => 7,
        'supports' => array(
          'title',
          'thumbnail'
        ),
        'has_archive' => false
      )
    );
    register_post_type(
      'editors-shop',
      array(
        'label' => 'editors-shop',
        'labels' => array(
          'name' => 'Editors Shop',
          'singular_name' => 'Editors Shop',
          'all_items' => __('Tous les editors shop', 'html5blank'),
          'add_new_item' => __('Ajouter un shop', 'html5blank'),
          'edit_item' => __("Éditer le shop", 'html5blank'),
          'new_item' => __('Nouveau shop', 'html5blank'),
          'view_item' => __("Voir le shop", 'html5blank'),
          'search_items' => __('Rechercher parmi les shops', 'html5blank'),
          'not_found' => __("Pas de shops trouvés", 'html5blank'),
          'not_found_in_trash'=> __("Pas de shop dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-cart',
        'menu_position' => 8,
        'supports' => array(
          'title',
          'author',
          'editor',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );
  
    //Add custom type Boutiques
    register_post_type(
      'Boutiques',
      array(
        'label' => 'Boutiques',
        'labels' => array(
          'name' => 'Boutiques',
          'singular_name' => 'Boutique',
          'all_items' => __('Toutes les boutiques', 'html5blank'),
          'add_new_item' => __('Ajouter une boutique', 'html5blank'),
          'edit_item' => __('Éditer la boutique', 'html5blank'),
          'new_item' => __('Nouvelle boutique', 'html5blank'),
          'view_item' => __('Voir la boutique', 'html5blank'),
          'search_items' => __('Rechercher parmi les boutiques', 'html5blank'),
          'not_found' => __('Pas de boutiques trouvées', 'html5blank'),
          'not_found_in_trash'=> __('Pas de boutiques dans la corbeille', 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-store',
        'menu_position' => 10,
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );
    //Add custom type Job/stage
    register_post_type(
      'jobsstages',
      array(
        'label' => 'jobsstages',
        'labels' => array(
          'name' => 'Job/Stage',
          'singular_name' => 'Job/Stage',
          'all_items' => __('Tous les jobs/stages', 'html5blank'),
          'add_new_item' => __('Ajouter un job/stage', 'html5blank'),
          'edit_item' => __("Éditer le job/stage", 'html5blank'),
          'new_item' => __('Nouveau job/stage', 'html5blank'),
          'view_item' => __("Voir le job/stage", 'html5blank'),
          'search_items' => __('Rechercher parmi les jobs/stages', 'html5blank'),
          'not_found' => __("Pas de jobs/stages trouvés", 'html5blank'),
          'not_found_in_trash'=> __("Pas de job/stage dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-welcome-learn-more',
        'menu_position' => 5,
        'supports' => array(
          'title',
          'author',
          'editor',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );

        //Add custom type Job/stage
    register_post_type(
      'speakers-eaf',
      array(
        'label' => 'speakers-eaf',
        'labels' => array(
          'name' => 'Speakers EAF',
          'singular_name' => 'Intervenant EAF',
          'all_items' => __('Tous les Intervenants EAF', 'html5blank'),
          'add_new_item' => __('Ajouter un Intervenant EAF', 'html5blank'),
          'edit_item' => __("Éditer le Intervenant EAF", 'html5blank'),
          'new_item' => __('Nouveau Intervenant EAF', 'html5blank'),
          'view_item' => __("Voir le Intervenant EAF", 'html5blank'),
          'search_items' => __('Rechercher parmi les jobs/stages', 'html5blank'),
          'not_found' => __("Pas de d'intervenants trouvés", 'html5blank'),
          'not_found_in_trash'=> __("Pas de Intervenant EAF dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-admin-users',
        'menu_position' => 9,
        'supports' => array(
          'title',
          'author',
          'editor',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );

            //Add custom type Job/stage
    register_post_type(
      'programme-eaf',
      array(
        'label' => 'programme-eaf',
        'labels' => array(
          'name' => 'Programme EAF',
          'singular_name' => 'Programme EAF',
          'all_items' => __('Tous les Programmes EAF', 'html5blank'),
          'add_new_item' => __('Ajouter un Programme EAF', 'html5blank'),
          'edit_item' => __("Éditer le Programme EAF", 'html5blank'),
          'new_item' => __('Nouveau Programme EAF', 'html5blank'),
          'view_item' => __("Voir le Programme EAF", 'html5blank'),
          'search_items' => __('Rechercher parmi les Programmes', 'html5blank'),
          'not_found' => __("Pas de Programmes trouvés", 'html5blank'),
          'not_found_in_trash'=> __("Pas de Programme EAF dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-admin-users',
        'menu_position' => 8,
        'supports' => array(
          'title',
          'author',
          'editor',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );

                //Add custom type Job/stage
    register_post_type(
      'partnenaire-eaf',
      array(
        'label' => 'partnenaire-eaf',
        'labels' => array(
          'name' => 'Partnenaire EAF',
          'singular_name' => 'Partnenaire EAF',
          'all_items' => __('Tous les Partnenaires EAF', 'html5blank'),
          'add_new_item' => __('Ajouter un Partnenaire EAF', 'html5blank'),
          'edit_item' => __("Éditer le Partnenaire EAF", 'html5blank'),
          'new_item' => __('Nouveau Partnenaire EAF', 'html5blank'),
          'view_item' => __("Voir le Partnenaire EAF", 'html5blank'),
          'search_items' => __('Rechercher parmi les Partnenaires', 'html5blank'),
          'not_found' => __("Pas de Partnenaires trouvés", 'html5blank'),
          'not_found_in_trash'=> __("Pas de Partnenaire EAF dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-admin-users',
        'menu_position' => 7,
        'supports' => array(
          'title',
          'author',
          'editor',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );


  register_post_type(
      'salle-eaf',
      array(
        'label' => 'salle-eaf',
        'labels' => array(
          'name' => 'Salle EAF',
          'singular_name' => 'salle EAF',
          'all_items' => __('Tous les salles EAF', 'html5blank'),
          'add_new_item' => __('Ajouter un salles EAF', 'html5blank'),
          'edit_item' => __("Éditer le salle EAF", 'html5blank'),
          'new_item' => __('Nouveau salle EAF', 'html5blank'),
          'view_item' => __("Voir le salle EAF", 'html5blank'),
          'search_items' => __('Rechercher parmi les salles', 'html5blank'),
          'not_found' => __("Pas de salle trouvés", 'html5blank'),
          'not_found_in_trash'=> __("Pas de salle EAF dans la corbeille", 'html5blank')
          ),
        'public' => true,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-admin-users',
        'menu_position' => 5,
        'supports' => array(
          'title',
          'author',
          'editor',
          'thumbnail'
        ),
        'has_archive' => true
      )
    );

    //Add custom type Video SEO
    register_post_type(
      'video-seo',
      array(
        'label' => 'Video SEO',
        'labels' => array(
          'name' => 'Video SEO',
          'singular_name' => 'video-seo',
          'all_items' => __('Tous les videos Seo', 'html5blank'),
          'add_new_item' => __('Ajouter un video Seo', 'html5blank'),
          'edit_item' => __('Éditer le video Seo', 'html5blank'),
          'new_item' => __('Nouveau video Seo', 'html5blank'),
          'view_item' => __('Voir le video Seo', 'html5blank'),
          'search_items' => __('Rechercher parmi les videos Seo', 'html5blank'),
          'not_found' => __('Pas de video trouvé Seo', 'html5blank'),
          'not_found_in_trash'=> __('Pas de video dans la corbeille Seo', 'html5blank')
          ),
        'public' => true,
        'hierarchical' => false,
        'show_ui' => true,
        'taxonomies' => array('post_tag','category'),
        'publicly_queryable'  => false,
        'capability_type' => 'post',
        'menu_icon'   => 'dashicons-book',
        'menu_position' => 4,
        'supports' => array(
          'title',
        ),
        'has_archive' => false
      )
    );

    function add_tags_categories() {
        register_taxonomy_for_object_type('category', 'video-seo');
        register_taxonomy_for_object_type('post_tag', 'video-seo');
      }
    add_action('init', 'add_tags_categories');