<?php
/** GENERATIONBLOCK **/
global $excludeIDS;

function _sidebar_article_block($posts = '',$category_id = '', $title = ''){
global $excludeIDS;
?>
<div class="sidebar-related">
    <span class="title-h3"><a href="<?php echo get_category_link( $category_id ); ?> ">+ <?php echo get_cat_name($category_id);?></a></span>
     <?php foreach ( $posts as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; ?>
    <div class="related-article">
        <div class="related-container-article">
            <div class="related-image">
                <span class="post-thumbnail atc" data-atc="<?php _encode_base64(get_the_permalink($post->ID)); ?>">
                    <?php echo get_the_post_thumbnail($post->ID,'sidebar_thumb',array('lazy',0,0)); ?>
                </span>
            </div>
            <div class="related-titre">
                <a href="<?php echo get_the_permalink($post->ID) ?>" class="post-thumbnail"><span class="title-h4"><?php echo $post->post_title; ?></span></a>
            </div>
        </div>
    </div>
    <?php endforeach; wp_reset_postdata(); ?>
 </div> 


<?php }

function _top_article_block($posts = '',$category_id = '', $title = ''){
global $excludeIDS,$no_ads_display;
if($posts){
	if($title == ''){$title = __('LE TOP ','html5blank').get_cat_name($category_id); } ?>
	<!-- EAT most-popular -->
	<div class="eat-most-popular">
	    <span class="eat-most-popular-title"><?php echo $title; ?></span>
	    <ul class="eat-most-popular-items" >
	        <?php foreach ( $posts as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; ?>
	            <li><a href="<?php echo get_the_permalink($post->ID) ?>"><?php echo $post->post_title; ?></a></li>
	        <?php endforeach; wp_reset_postdata(); ?>
	    </ul>
	</div>
	<?php if($no_ads_display == 0){ ?>
	<div class="site-inner-ads center no_print">
        <div id="gpt-ad-BANNER_MIDDLE" class="rmgAd" data-device="all" data-type="BANNER_MIDDLE">
        </div>
	</div>
	<?php }
	}
}

function _swiper_article_block_old($posts = '',$category_id = '', $title_swiper_article = ''){
    global $excludeIDS;
    if($posts){ ?>
    <div class="swiper-article-block">
        <a href="<?php echo get_category_link($category_id); ?>" class="swiper-article-title"><?php echo get_cat_name($category_id);?></a>
        <div class="swiper-article swiper-article">
            <div class="swiper-container-article">
                    <div class="swiper-wrapper">
                    <?php foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++ ?>
                        <?php if($current_post != 1){ ?>
                            <?php $excludeIDS[] = $post->ID; ?> 
                                <div class="swiper-slide">
                                    <div class="swiper-image">
                                        <a href="<?php the_permalink($post->ID); ?>">
                                            <?php the_post_thumbnail('content2018',array('',0,0)); ?>
                                        </a>
                                    </div>
                                    <div class="swiper-titre">
                                        <h2><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h2>
                                    </div>
                                    <!-- <a href="#" class="sprite sprite-social"></a>
                                    <a href="#" class="sprite sprite-pinit"></a> -->
                                </div>
                        <?php } ?>
                    <?php endforeach; wp_reset_postdata(); ?>
                </div>
            </div>
            <div class="swiper-button-prev swiper-button-prev-article sprite"><span></span></div>
            <div class="swiper-button-next swiper-button-next-article sprite"><span></span></div>
        </div>
    </div> 
<?php
    }
}

function _swiper_article_block($posts = '',$category_id = '',$tag_id = '',$title_swiper_article = ''){
    global $excludeIDS;
    if($posts){ ?>
     <!-- _swiper_article -->
        <div class="swiper-article-block">
            <?php if($title_swiper_article != ''){ ?>
                <span class="swiper-article-title"><?php echo $title_swiper_article; ?></span>
            <?php }else { ?>
                <?php if($category_id != ''){ ?> <a href="<?php echo get_category_link($category_id); ?>" class="swiper-article-title"><?php _e('+ de ','html5blank');?> <?php echo get_cat_name($category_id);?></a> <?php } ?>
                <?php if($tag_id != ''){  $tag = get_tag($tag_id); ?> 
                <a href="<?php echo get_tag_link($tag_id); ?>" class="swiper-article-title"><?php echo $tag->name; ?></a>
                <?php } ?>
            <?php } ?>
            <div class="swiper-article swiper-article-2">
                <div class="swiper-container-article no-swiper">
                        <div class="swiper-wrapper">
                        <?php foreach ( $posts as $post ) : setup_postdata( $post ); $current_post++;  $excludeIDS[] = $post->ID; ?>
                                    <div class="swiper-slide">
                                        <div class="swiper-image">
                                            <span class="atc" data-atc="<?php _encode_base64(get_the_permalink($post->ID)); ?>">
                                                <?php echo get_the_post_thumbnail($post->ID,'content2018',array('lazy',0,0)); ?>
                                            </span>
                                        </div>
                                        <div class="swiper-titre">
                                            <span><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></span>
                                        </div>
                                    </div>
                        <?php endforeach; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div> 
<?php
    }
}

function _first_article_block($posts = '',$id_category = '', $title_swiper_article = '',$title_display = 1){
    global $excludeIDS;    
        $no_ads_display = 0;
        $list_categorie = get_the_category(); 
        $main_categorie = $list_categorie[0];
        if (in_array($list_categorie[0]->cat_ID, category_principales())) { 
            $main_categorie = $list_categorie[0];
        }

        if(count($list_categorie) > 1 ){
            if (in_array($list_categorie[1]->cat_ID, category_principales())) { 
                $main_categorie = $list_categorie[1];
                $sous_categorie = $list_categorie[0];
            }
        }

        if($id_category == ''){
            $post_categories = wp_get_post_categories( $posts[0]->ID );
            $id_category = $post_categories[0];
            $no_ads_display = 1;
        }

        $category_link = get_category_link($id_category);
        $category_name = get_cat_name($id_category);

    foreach ( $posts as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; 
        $author_name = get_the_author_meta('display_name', $post->post_author);
        $author_link =  get_author_posts_url( $post->post_author); 
    ?>
    <?php if($title_display == 1){ ?> <h2><?php echo get_cat_name($id_category);?></h2><?php } ?>
    <div class="first-article">
        <article class="article-design-1 first-article-1" data-cat="<?php echo $main_categorie->cat_ID ?>">
            <span class="post-thumbnail atc" data-atc="<?php _encode_base64(get_the_permalink($post->ID)); ?>">
               <?php echo get_the_post_thumbnail($post->ID,'content2018',array('lazy',1,0)); ?>
            </span>
            <div class="post-side">
                <div class="post-side-inner">
                    <div class="post-category" >
                        <a href="<?php echo $category_link; ?>" class="post-category-parent">
                            <?php echo $category_name; ?>
                        </a>
                    </div>
                    <span class="title-h2">
                        <a href="<?php the_permalink($post->ID);?>" class="post-title"><?php echo $post->post_title; ?></a>
                    </span>
                    <div class="post-author"><span><?php _e( 'par', 'html5blank' ); ?></span> <span class="author atc" data-atc="<?php _encode_base64($author_link); ?>"><?php echo ucfirst($author_name); ?></span></div>
                </div>
            </div> 
        </article>
    </div>
    <?php endforeach; wp_reset_postdata(); 
}



function _feed_article_block($posts_feeds = '',$posts_popular = ''){
global $excludeIDS;
?>
<div class="articles">
    <div class="content-article-ajax content entry-content">
        <?php 
        $current_post = 0;
        foreach ( $posts_feeds as $post ) : setup_postdata( $post ); $current_post++;  $excludeIDS[] = $post->ID;
            global $post;
            get_template_part('template-parts/general/article'); ?>
            <?php if ($current_post == 1){ ?> 
            <div class="site-inner-ads center no_print">
                <div id="gpt-ad-RECT" class="rmgAd center" data-device="mobile" data-type="RECT"></div>
            </div>
            <?php } ?>
            <?php if ($current_post == 3){ ?> 
            <div id="gpt-ad-RECT" class="rmgAd center" data-device="mobile" data-type="RECT"></div>
            <?php } ?> 
            <?php if ($current_post == 8){ ?> 
            <div id="gpt-ad-RECT" class="rmgAd center" data-device="mobile" data-type="RECT"></div>
            <?php } ?> 
            <?php endforeach; wp_reset_postdata(); 
        ?>
    </div>
    <!-- sidebar -->
    <aside class="sidebar sidebar-index" role="complementary">
        <div class="rmgAd margin_bottom_20" data-device="desktop" data-type="RECT_ABOVE"></div>
            <div class="most-popular">
                <span class="title-h3"><?php _e( 'les + lus', 'html5blank' ); ?></span>
                <ul class="most-popular-titles" >
                <?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; ?>
                    <li>
                        <span class="title-h4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
                    </li>
                <?php endforeach; wp_reset_postdata(); ?>
                </ul>
            </div>
            <?php if(isset(autopromo_objet_mag()[0])) { ?>
               <!--
               <a class="autopromo-abonnement" href="<?php echo get_field('autopromo_link',autopromo_objet_mag()[0]->ID); ?>">
                    <img src="<?php echo get_field('autopromo_imu',autopromo_objet_mag()[0]->ID); ?>" alt="Abonnement ELLE" />
                </a>
                -->
            <?php } ?>
        <?php /*  get_template_part('template-parts/general/health-inset'); */ ?>
        <div id="gpt-ad-RECT_MIDDLE" class="rmgAd" data-device="desktop" data-type="RECT_MIDDLE"></div>
    </aside>     
    <div class="navigation">
            <?php pressPagination('',3); ?>
    </div>       
</div>
<?php }