<?php 
function fct_header_2018(){
global $post;
?>
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 10px 0 0 0; text-align: center">
                     <p style="margin: 0;color:#4b4b4b"> <a class="footer-menu" style="text-decoration: none;color: #4b4b4b;" href="$webversion">View online</a></p>
                </td>
            </tr>

            <tr>
                <td style="padding: 0px 0; text-align: center;">
                    <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/fr/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">
                        <img src="http://www.elle.be/img/ELLE_nws_nl.gif" width="262" height="134" alt="alt_text" border="0" style="height: auto; background: #FFFFFF; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #FFFFFF;" class="logo-img">
                    </a>
                </td>
            </tr>
        </table>
        <!-- Space : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 0px 0; text-align: center; border-bottom: 1px #cdcdcd solid"></td>
            </tr>
        </table>
        <!-- Space : END -->
        <!-- Menu : BEGIN -->
        <!--
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="500" style="margin: auto;" class="menu-container">
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 10px 10px 40px 10px;font-family: georgia">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td width="25.00%" class="stack-column-center" valign="top">
                                <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/nl/fashion/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">
                                    <p style="margin: 0;color:#4b4b4b;text-align: center;text-transform: uppercase;">Fashion</p>
                                </a>
                            </td>

                            <td width="25.00%" class="stack-column-center" valign="top">
                                <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/nl/beauty-hair/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">
                                    <p style="margin: 0;color:#4b4b4b;text-align: center;text-transform: uppercase;">Beauty</p>
                                </a>
                            </td>

                            <td width="25.00%" class="stack-column-center" valign="top">
                                <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/nl/lifestyle/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">
                                    <p style="margin: 0;color:#4b4b4b;text-align: center;text-transform: uppercase;">lifestyle</p>
                                </a>
                            </td>
                            <td width="25.00%" class="stack-column-center" valign="top">
                                <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/nl/hotspots/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">
                                    <p style="margin: 0;color:#4b4b4b;text-align: center;text-transform: uppercase;">Hotspots</p>
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        -->
        <!-- Menu : END -->
<?php } 

function fct_hero_2018($index = 0){ 
    global $post;
    global $blank_ligne;
    $blank_ligne .=  get_field('newsletter')[$index]['objet_article']->post_title . ' - ';
    ?>
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
    <!-- Hero Image, Flush : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 15px 15px 10px 15px; text-align: center;">
                <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>">
                    <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['objet_article']->ID,'content'); ?>" width="600" height="" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555; margin: auto;" class="g-img">
                    </a>
                </td>
            </tr>
<!-- Hero Image, Flush : BEGIN -->         

<!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 15px 15px 10px 15px; text-align: center;">
                    <h2 style="margin: 0; font-family: sans-serif; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;"><span style="font-family: georgia;">
                        <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['objet_article']->ID)[0]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                        <?php 
                        echo get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['objet_article']->ID)[0]); ?>
                        </a>
                     <?php 
                     $nb_category = count(wp_get_post_categories(get_field('newsletter')[$index]['objet_article']->ID));
                     if($nb_category  > 1){ ?>
                    </span> | 
                        <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['objet_article']->ID)[1]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;"> 
                        <?php 
                        echo get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['objet_article']->ID)[1]); ?>
                        </a>
                    <?php } ?>
                    </h2>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0px 20px 0px; text-align: center;">
                    <h1 style="margin: 0; font-family: sans-serif; font-size: 30px; line-height: 100%; color: #000000; font-weight: bold;text-transform: uppercase;">
                        <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>">
                             <?php if(get_field('newsletter')[$index]['titre_article']){ echo get_field('newsletter')[$index]['titre_article'];}else{ echo get_field('newsletter')[$index]['objet_article']->post_title; } ?>
                        </a>
                    </h1>
                </td>
            </tr>
            <?php if(get_field('newsletter')[$index]['extrait']){ ?>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 20px 15px 20px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #000000; text-align: center;font-style: italic;">
                    <?php echo get_field('newsletter')[$index]['extrait']; ?>
                </td>
            </tr>
            <?php } else { ?>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 20px 15px 20px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #000000; text-align: center;font-style: italic;">
                    <?php echo get_excerpt_by_id(get_field('newsletter')[$index]['objet_article']->ID); ?>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 40px 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #000000;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                        <tr>
                            <td style="text-align: center;" class="button-td">
                                <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>" style="font-family: sans-serif; font-size: 18px; line-height: 110%; text-align: center; text-decoration: none; display: block; font-weight: bold;" class="button-a">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#000000;">LEES MEER</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                </a>
                            </td>
                        </tr>
                    </table>
                    <!-- Button : END -->
                </td>
            </tr>
            <!-- 1 Column Text + Button : END -->
    <!-- Article Hero Image, Flush : END -->
    </table>
<?php }

function fct_2articles_2018($index = 0){

$post_id_1 = get_field('newsletter')[$index]['article_1']->ID;
$id_1 = get_post_thumbnail_id($post_id_1); // gets the id of the current post_thumbnail (in the loop)
if(get_field('image_verticale',$post_id_1)){$id_1 = get_field('image_verticale',$post_id_1);}
$src_1 = wp_get_attachment_image_src($id_1, 'content2018')[0];

$post_id_2 = get_field('newsletter')[$index]['article_2']->ID;
$id_2 = get_post_thumbnail_id($post_id_2); // gets the id of the current post_thumbnail (in the loop)
if(get_field('image_verticale',$post_id_2)){$id_2 = get_field('image_verticale',$post_id_2);}
$src_2 = wp_get_attachment_image_src($id_2, 'content2018')[0];

global $post; ?>
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <!-- 2 Even Columns : BEGIN -->
        <tr>
            <td bgcolor="#ffffff" align="center" valign="top" style="padding: 15px 15px 10px 15px;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <!-- Column : BEGIN -->
                        <td width="50%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 0 10px 0 10px; text-align: center">
                                        <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>">
                                            <img src="<?php echo $src_1; ?>" width="100%" height="auto"  alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#ffffff" style="padding: 5px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: sans-serif; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;"><span style="font-family: georgia;">
                                         <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['article_1']->ID)[0]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                        <?php 
                                        echo get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['article_1']->ID)[0]); ?>
                                        </a>                                   
                                      </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555; padding: 0 10px 10px; text-align: center;" class="center-on-narrow">
                                        <h2 class="h2paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: bold;text-transform: uppercase;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>">
                                            <?php if(get_field('newsletter')[$index]['article_1_text']){ echo get_field('newsletter')[$index]['article_1_text'];}else{ echo get_field('newsletter')[$index]['article_1']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="50%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img"  style="padding: 0 10px 0 10px; text-align: center">
                                        <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>">
                                            <img src="<?php echo $src_2; ?>" width="100%" height="auto"  alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#ffffff" style="padding: 5px 15px 10px 15px; text-align: center;">
                                        <h2 style="margin: 0; font-family: sans-serif; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;"><span style="font-family: georgia;">
                                             <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['article_2']->ID)[0]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                            <?php 
                                            echo get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['article_2']->ID)[0]); ?>
                                            </a>                              
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555; padding: 0 10px 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="h3paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 20px; line-height: 125%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>">
                                            <?php if(get_field('newsletter')[$index]['article_2_text']){ echo get_field('newsletter')[$index]['article_2_text'];}else{ echo get_field('newsletter')[$index]['article_2']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                    </tr>
                </table>
            </td>
        </tr>
        <!-- 2 Even Columns : END -->
</table>
<?php }

function fct_3articles_2018($index = 0){
global $post; ?>
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <tr>
            <td bgcolor="#ffffff" align="center" valign="top" style="padding: 15px 15px 10px 15px;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 10px; text-align: center">
                                        <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_1']->ID,'content'); ?>" width="100%" height="auto" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                        </a>
                                    </td>
                                </tr>
                               <tr>
                                    <td bgcolor="#ffffff" style="padding: 0px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: georgia; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;">
                                            <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['article_1']->ID)[0]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                             <?php echo substr(get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['article_1']->ID)[0]),0,18);?>
                                            </a>
                                      </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family: sans-serif; font-size: 14px; line-height: 140%; color: #555555; padding: 0 10px 0 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="columns-title h2paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 14px; line-height: 100%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>">
                                                <?php if(get_field('newsletter')[$index]['article_1_text']){ echo get_field('newsletter')[$index]['article_1_text'];}else{ echo get_field('newsletter')[$index]['article_1']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 10px; text-align: center">
                                    <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_2']->ID,'content'); ?>" width="100%" height="auto" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                    </a>
                                    </td>
                                </tr>
                               <tr>
                                    <td bgcolor="#ffffff" style="padding: 0px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: georgia; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;">
                                            <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['article_2']->ID)[0]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                             <?php echo substr(get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['article_2']->ID)[0]),0,18);?>
                                            </a>
                                      </h2>
                                    </td>
                                </tr>
                                <tr>
                                   <td style="font-family: sans-serif; font-size: 14px; line-height: 140%; color: #555555; padding: 0 10px 0 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="columns-title h2paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 14px; line-height: 100%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>">
                                                <?php if(get_field('newsletter')[$index]['article_2_text']){ echo get_field('newsletter')[$index]['article_2_text'];}else{ echo get_field('newsletter')[$index]['article_2']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 10px; text-align: center">
                                    <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_3']->ID,'content'); ?>" width="100%" height="auto" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                    </a>
                                    </td>
                                </tr>
                               <tr>
                                    <td bgcolor="#ffffff" style="padding: 0px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: georgia; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;">
                                            <a href="<?php echo get_category_link(wp_get_post_categories(get_field('newsletter')[$index]['article_3']->ID)[0]); ?>?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                             <?php echo substr(get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['article_3']->ID)[0]),0,18);?>
                                            </a>
                                      </h2>
                                    </td>
                                </tr>
                                <tr>
                                   <td style="font-family: sans-serif; font-size: 14px; line-height: 140%; color: #555555; padding: 0 10px 0 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="columns-title h3paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 14px; line-height: 100%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID); ?>">
                                                <?php if(get_field('newsletter')[$index]['article_3_text']){ echo get_field('newsletter')[$index]['article_3_text'];}else{ echo get_field('newsletter')[$index]['article_3']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                    </tr>
                 </table>
            </td>
        </tr>
</table>
<?php }

function fct_concours_2018($index = 0){
global $post; ?>
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <tr>
            <td bgcolor="#ffffff" align="center" valign="top" style="padding: 15px 15px 10px 15px;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 10px; text-align: center">
                                        <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_1']->ID,'content'); ?>" width="100%" height="auto" alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                        </a>
                                    </td>
                                </tr>
                               <tr>
                                    <td bgcolor="#ffffff" style="padding: 0px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: georgia; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;">
                                            <a href="http://www.elle.be/nl/wedstrijden/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                             WEDSTRIJDEN
                                            </a>
                                      </h2>
                                </tr>
                                <tr>
                                    <td style="font-family: sans-serif; font-size: 14px; line-height: 140%; color: #555555; padding: 0 10px 0 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="columns-title h2paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 14px; line-height: 125%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>">
                                                <?php if(get_field('newsletter')[$index]['article_1_text']){ echo get_field('newsletter')[$index]['article_1_text'];}else{ echo get_field('newsletter')[$index]['article_1']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 10px; text-align: center">
                                    <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_2']->ID,'content'); ?>" width="100%" height="auto"  alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                    </a>
                                    </td>
                                </tr>
                               <tr>
                                    <td bgcolor="#ffffff" style="padding: 0px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: georgia; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;">
                                            <a href="http://www.elle.be/nl/wedstrijden/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                            WEDSTRIJDEN
                                            </a>
                                      </h2>
                                </tr>
                                <tr>
                                   <td style="font-family: sans-serif; font-size: 14px; line-height: 140%; color: #555555; padding: 0 10px 0 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="columns-title h2paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 14px; line-height: 125%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>">
                                                <?php if(get_field('newsletter')[$index]['article_2_text']){ echo get_field('newsletter')[$index]['article_2_text'];}else{ echo get_field('newsletter')[$index]['article_2']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center" valign="top">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="fct_2_3_articles_2018_img" style="padding: 10px; text-align: center">
                                    <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_3']->ID,'content'); ?>" width="100%" height="auto"  alt="alt_text" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                    </a>
                                    </td>
                                </tr>
                               <tr>
                                    <td bgcolor="#ffffff" style="padding: 0px 15px 10px 15px; text-align: center;">
                                      <h2 style="margin: 0; font-family: georgia; font-size: 15px; line-height: 125%; color: #4b4b4b; font-weight: normal;text-transform: uppercase;">
                                            <a href="http://www.elle.be/nl/wedstrijden/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="text-decoration: none;color: #4b4b4b;">
                                            WEDSTRIJDEN
                                            </a>
                                      </h2>
                                </tr>
                                <tr>
                                   <td style="font-family: sans-serif; font-size: 14px; line-height: 140%; color: #555555; padding: 0 10px 0 10px; text-align: center;text-transform: uppercase;" class="center-on-narrow">
                                        <h2 class="columns-title h3paddingbottom" style="margin: 0; font-family: sans-serif; font-size: 14px; line-height: 125%; color: #000000; font-weight: bold;">
                                            <a style="text-decoration: none;color: #000000;" href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID); ?>">
                                                <?php if(get_field('newsletter')[$index]['article_3_text']){ echo get_field('newsletter')[$index]['article_3_text'];}else{ echo get_field('newsletter')[$index]['article_3']->post_title; } ?>
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                    </tr>
                 </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff" align="center" valign="top" style="padding: 10px;">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #000000;">
                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                                <tr>
                                    <td style="background: #FFFFFF; text-align: center;" class="button-td">
                                        <a href="http://www.elle.be/nl/wedstrijden/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>" style="padding:10px;background: #FFFFFF; border: 2px solid #000000; font-family: sans-serif; font-size: 15px; line-height: 110%; text-align: center; text-decoration: none; display: block; font-weight: bold;" class="button-a">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#000000;">MEER WEDSTRIJDEN</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                        </a>
                                    </td>
                                </tr>
                            </table>

                        </td> 
                    </tr>
                 </table>
            </td>
        </tr>
</table>
<?php }

function fct_2articles_v2_2018($index = 0){
global $post; ?>
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <tr>
            <td bgcolor="#ffffff" dir="ltr" align="center" valign="top" width="100%" style="padding: 10px;">
                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center">
                            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td dir="ltr" valign="top" style="padding: 0 10px;">
                                        <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="66.66%" class="stack-column-center">
                            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555; padding: 10px; text-align: left;" class="center-on-narrow">
                                        <h2 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 125%; color: #333333; font-weight: bold;">15 robes de mariées canons pour un mariage en hiver</h2>
                                        <p style="margin: 0 0 10px 0;">Qui a dit que les mariages en été étaient les plus beaux ? Inspirez-vous de ces looks pour un mariage hivernal bien au chaud ! </p>
                                        <!-- Button : BEGIN -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
                                            <tr>
                                                <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                    <a href="http://www.google.com" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                                                      <span style="color:#ffffff;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Lire la suite&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                  </a>
                                              </td>
                                          </tr>
                                      </table>
                                      <!-- Button : END -->
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                    </tr>
                </table>
            </td>
        </tr>
        <!-- Thumbnail Left, Text Right : END -->

        <!-- Thumbnail Right, Text Left : BEGIN -->
        <tr>
            <td bgcolor="#ffffff" dir="rtl" align="center" valign="top" width="100%" style="padding: 10px;">
                <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <!-- Column : BEGIN -->
                        <td width="33.33%" class="stack-column-center">
                            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td dir="ltr" valign="top" style="padding: 0 10px;">
                                        <img src="http://placehold.it/170" width="170" height="170" alt="alt_text" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                        <!-- Column : BEGIN -->
                        <td width="66.66%" class="stack-column-center">
                            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555; padding: 10px; text-align: left;" class="center-on-narrow">
                                        <h2 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 125%; color: #333333; font-weight: bold;">Manger du chocolat noir pour booster son immunité</h2>
                                        <p style="margin: 0 0 10px 0;"> Voilà le genre d'infos que l'on aime lire ! Combattre les bactéries hivernales grâce à quelques gestes simples tout en se régalant de chocolat noir.</p>
                                        <!-- Button : BEGIN -->
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="float:left;">
                                            <tr>
                                                <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                    <a href="http://www.google.com" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                                                        <span style="color:#ffffff;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;Lire la suite&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Button : END -->
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- Column : END -->
                    </tr>
                </table>
            </td>
        </tr>
        <!-- Thumbnail Right, Text Left : END -->
</table>
<?php }


function fct_textlibre_2018($index = 0){
global $post; ?>
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <tr>
            <td bgcolor="#ffffff">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td style="padding: 40px 20px; font-family: sans-serif; font-size: 15px; line-height: 140%; color: #555555;">
                          <?php echo get_field('newsletter')[$index]['texte']; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- 1 Column Text : END -->
</table>
<?php }

function fct_footer_2018(){
global $post; ?>
    <!-- Email Body : END -->

    <!-- Space : BEGIN -->
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="margin: auto;" class="email-container">
        <tr>
              <td style="padding: 0px 0; text-align: center; border-bottom: 1px #cdcdcd solid"></td>
        </tr>
     </table>
    <!-- Space : END -->    
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <!-- Article Hero Image, Flush : BEGIN -->
             <tr>
                <td bgcolor="#ffffff" style="padding: 15px 15px 15px 15px; text-align: center;">
                    <h2 style="margin: 0; font-family: sans-serif; font-size: 18px; line-height: 125%; color: #000000; font-weight: bold;"><span style="border-bottom: 2px solid #000000">FOLLOW US</span></h2>
                </td>
            </tr>
            <tr>
                <td>
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="180" style="margin: auto;" class="email-container">
                            <tr>
                                <td width="100.00%" class="stack-column-center">
                                    <a style="text-decoration: none;color: #000000;" href="https://www.facebook.com/ELLEbelgie" >
                                      <img src="http://www.elle.be/img/newsletters/facebook.png" alt="Facebook">
                                    </a>
                                    <a style="text-decoration: none;color: #000000;" href="https://twitter.com/ELLEbelgie">
                                        <img src="http://www.elle.be/img/newsletters/twitter.png" alt="Twitter">
                                    </a>
                                    <a style="text-decoration: none;color: #000000;" href="https://www.instagram.com/elle_belgie/">
                                        <img src="http://www.elle.be/img/newsletters/instagram.png" alt="Instagram">
                                    </a>
                                    <a style="text-decoration: none;color: #000000;" href="https://www.pinterest.com/ellebelgie/">
                                        <img src="http://www.elle.be/img/newsletters/pinterest.png" alt="Pinterest">
                                     </a>
                                    <a style="text-decoration: none;color: #000000;" href="https://www.youtube.com/user/ellebelgium">
                                        <img src="http://www.elle.be/img/newsletters/youtube.png" alt="Youtube">
                                    </a>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td aria-hidden="true" height="40" style="font-size: 0; line-height: 0;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 10px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <!-- Column : BEGIN -->
                            <td width="33.33%" class="stack-column-center" valign="top">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="text-align: left;" class="center-on-narrow icon-fleche">
                                           <img src="http://www.elle.be/img/newsletters/fleche.png" alt="fleche">
                                        </td>
                                         <td style="font-family: sans-serif; font-size: 16px; line-height: 140%; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                            <h2 class="lien-footer-h2" style="margin: 0; font-family: sans-serif; font-size: 16px; line-height: 100%; color: #000000; font-weight: bold;">
                                              <a style="text-transform:uppercase;text-decoration: none;color: #000000;" href="https://www.viapress.be/magazine-elle-belgique.html">Abonneer <br/> je hier </a>
                                            </h2>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                            <!-- Column : BEGIN -->
                            <td width="33.33%" class="stack-column-center" valign="top">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="text-align: left;" class="center-on-narrow icon-fleche">
                                           <img src="http://www.elle.be/img/newsletters/fleche.png" alt="fleche">
                                        </td>
                                        <td style="font-family: sans-serif; font-size: 16px; line-height: 140%; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                            <h2 class="lien-footer-h2" style="margin: 0; font-family: sans-serif; font-size: 16px; line-height: 100%; color: #000000; font-weight: bold;">
                                              <a style="text-transform:uppercase;text-decoration: none;color: #000000;" href="http://www.elle.be/nl/?utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">Surf naar ELLE.BE</a>
                                            </h2>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                            <!-- Column : BEGIN -->
                            <td width="33.33%" class="stack-column-center" valign="top">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="text-align: left;" class="center-on-narrow icon-fleche">
                                           <img src="http://www.elle.be/img/newsletters/fleche.png" alt="fleche">
                                        </td>
                                        <td style="font-family: sans-serif; font-size: 16px; line-height: 140%; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                            <h2 class="lien-footer-h2" style="margin: 0; font-family: sans-serif; font-size: 16px; line-height: 100%; color: #000000; font-weight: bold;">
                                              <a style="text-transform:uppercase;text-decoration: none;color: #000000;" href="http://www.elle.be/nl/hotspots/">Onze favoriete<br/>adresjes</a>
                                            </h2>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!-- Column : END -->
                        </tr>
                     </table>
                </td>
            </tr>
    <!-- Menu footer 3 Column: END -->
    </table>

    <!-- Menu : BEGIN -->
    <!-- Space : BEGIN -->
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="margin: auto;" class="email-container">
        <tr>
              <td style="padding: 0px 0; text-align: center; border-bottom: 1px #cdcdcd solid"></td>
        </tr>
     </table>
    <!-- Space : END -->    
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="footer-container">
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 15px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td style="padding: 15px; text-align: center">
                                 <p style="margin: 0;font-family: georgia;color:#000000;font-size: 12px;" class="footer-menu"><a style="text-decoration: none;color: #000000;" href="$unsubscribe">Uitschrijven</a> - <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/nl/contact/">Contact</a> - <a style="text-decoration: none;color: #000000;" href="http://www.elle.be/nl/mijn-profiel/?Token=$token&amp;utm_medium=email&amp;utm_source=newsletter&amp;utm_campaign=<?php echo $post->post_name; ?>">Mijn profiel beheren</a></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
    </table>
     <!-- Menu : END -->

<?php }

function fct_header(){ ?>
<tr>
<td class="link" height="30" align="center" style="font-family:Georgia, Times New Roman; font-size:12px;color:#000000;padding:0 30px 0 30px">
 Si vous n'arrivez pas à visualiser ce message : <a href="http://l.adherent.elle.fr/rts/go2.aspx?h=207861&tp=i-H8B-Q8O-4hD-36u9j-1o-A7vM-1c-G-36gax-2CwxXm&amp;x=17baaf861e795859e03ece905aeb51e1%7c10253908" style="color:#000000;text-decoration:none;">suivez ce lien</a></td>
</tr>
<tr>
<td>
<table width="700" border="0" cellpadding="0" cellspacing="0" class="entete-infos" style="padding-top: 10px">
<tr>
<td height="28" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#000000;font-weight:bold">05/10/2017</td>
<td height="28" align="right" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#000000;font-weight:normal;text-transform:uppercase;"><a href="http://l.adherent.elle.fr/rts/go2.aspx?h=207862&amp;tp=i-H8B-Q8O-4hD-36u9j-1o-A7vM-1c-G-36gax-2CwxXm&amp;x=10253908" style="color:#000000;text-decoration:none; border: 1px solid #000000; border-radius: 3px; padding: 3px 10px 3px 10px">Je m'abonne</a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="left" class="logo_head" style="border-bottom: 5px solid #000000; margin-top: 20px; margin-bottom: 17px">
<tbody>
<tr>
<td class="logo_head" width="35" valign="middle"><img src="http://cdn-elle.ladmedia.fr/bundles/elleintegration/images/newsletter/logo.png" border="0" alt="ELLE NEWSLETTER" width="28" height="70" /></td>
<td class="mode_head" width="665" valign="middle" style="font-family: georgia; font-weight: normal; font-style: normal; color: #000000; font-size: 55px; padding-left: 5px">
Mode
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<?php
}

function fct_header_2017(){ ?>
                        <tr>
                            <td align="center" bgcolor="#000000" height="42">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;max-width:730px;" width="90%">
                                    <tbody>
                                        <tr>
                                            <td style="font-family:Arial,Helvetica,sans-serif;font-size:10px;">
<a href="${webversion}" style="color:#ffffff;text-decoration:none;" target="_blank" title="Consulter en ligne">
    <strong style="color:#ffffff;">View online</strong>
</a>
                                            </td>
                                            <td align="right" width="70">
<a href="http://www.elle.be/" style="display:inline-block;" target="_blank" title="ELLE.be">
    <img alt="ELLE.be" border="0" src="http://www.elle.be/images/logos/elle-70.png" style="display:block;" width="100%" />
</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
<?php
}

function fct_hero_articles($index = 0){ ?>
<tr>
<td>
<table width="700" border="0" cellpadding="0" cellspacing="0" class="adecouvrir">
<tr>
<td align="center" style="padding:0 0 10px 0">
<span style="font-family:Arial, Helvetica, sans-serif;font-size:48px;color:#000000;font-weight:bold;text-transform:uppercase;">
 <?php echo get_cat_name(wp_get_post_categories(get_field('newsletter')[$index]['objet_article']->ID)[0]); ?>
</span>
</td>
</tr>
<tr>
<td align="center" style="padding:0 0 10px 0">
 <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>" style="display:block;">
 <img alt="" src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['objet_article']->ID,'small'); ?>" border="0" class="adecouvrir-img" />
 </a>
</td>
</tr>
<tr>
<td>
<a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:29px;color:#000000;font-weight:bold;text-transform:uppercase;text-decoration:none;" class="adecouvrir-titre">
<?php 
if(get_field('newsletter')[$index]['titre_article']){
echo get_field('newsletter')[$index]['titre_article'];    
}else{
echo get_field('newsletter')[$index]['objet_article']->post_title;
}
?>
</a>
</td>
</tr>
<tr>
<td style="padding:10px 0 0 0" class="adecouvrir-description">
<a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>" style="font-family:Georgia, Helvetica, sans-serif;font-size:16px;color:#000000;text-decoration:none;line-height:24px;">
<?php echo get_excerpt_by_id(get_field('newsletter')[$index]['objet_article']->ID); ?>
</a>
</td>
</tr>
 <tr>
 <td style="padding:10px 0 0 0">
 <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['objet_article']->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#e04659;text-decoration:none;font-weight: bold">[ Découvrir ]</a>
</td>
</tr>
</table>
</td>
</tr>
<tr><td height="35" class="divider"></td></tr>
<?php 
}

function fct_3_articles($index = 0){ ?>
<tr>
<td>
<table width="700" border="0" cellpadding="0" cellspacing="0" class="listing">
<tr>                                
<td align="center" valign="top" class="listing-bloc">
  <table width="220" border="0" cellpadding="0" cellspacing="0" align="center" style="padding:0 5px 0 0">
     <tr>
        <td valign="top" style="padding:0 0 10px 0">
           <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>" style="display:block;"><img alt="" src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_1']->ID,'newsletter'); ?> " border="0" /></a>
           </td>
           </tr>
           <tr>
           <td valign="top"><a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#000000;font-weight:bold;text-transform:uppercase;text-decoration:none;">
           <?php echo get_field('newsletter')[$index]['article_1']->post_title ?>
           </a></td>
           </tr>
           <tr>
           <td valign="top" class="listing-link"><a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_1']->ID);  ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#e04659;text-decoration:none;font-weight: bold">[ Lire l'article ]</a></td>
       </tr>
    </table>
</td>

<td align="center" valign="top" class="listing-bloc">
  <table width="220" border="0" cellpadding="0" cellspacing="0" align="center" style="padding:0 5px 0 0">
     <tr>
        <td valign="top" style="padding:0 0 10px 0">
           <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>" style="display:block;"><img alt="" src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_2']->ID,'newsletter'); ?> " border="0" /></a>
           </td>
           </tr>
           <tr>
           <td valign="top"><a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#000000;font-weight:bold;text-transform:uppercase;text-decoration:none;">
           <?php echo get_field('newsletter')[$index]['article_2']->post_title ?>
           </a></td>
           </tr>
           <tr>
           <td valign="top" class="listing-link"><a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_2']->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#e04659;text-decoration:none;font-weight: bold">[ Lire l'article ]</a></td>
       </tr>
    </table>
</td>

<td align="center" valign="top" class="listing-bloc">
  <table width="220" border="0" cellpadding="0" cellspacing="0" align="center" style="padding:0 5px 0 0">
     <tr>
        <td valign="top" style="padding:0 0 10px 0">
           <a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID); ?>" style="display:block;"><img alt="" src="<?php echo get_the_post_thumbnail_url(get_field('newsletter')[$index]['article_3']->ID,'newsletter'); ?> " border="0" /></a>
           </td>
           </tr>
           <tr>
           <td valign="top"><a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID);?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#000000;font-weight:bold;text-transform:uppercase;text-decoration:none;">
           <?php echo get_field('newsletter')[$index]['article_3']->post_title ?>
           </a></td>
           </tr>
           <tr>
           <td valign="top" class="listing-link"><a href="<?php echo get_the_permalink_news(get_field('newsletter')[$index]['article_3']->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#e04659;text-decoration:none;font-weight: bold">[ Lire l'article ]</a></td>
       </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
<tr><td height="35" class="divider"></td></tr>
<?php 
}

function fct_related_article($index = 0){
$posts = get_posts(array('posts_per_page' => 4, 'ignore_sticky_posts' => 1, 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => '1000 days ago')))); 
?>
<tr>
<td>
<table width="700" border="0" cellpadding="0" cellspacing="0" style="padding:0 0 15px 0;" class="filinfo">
    <tr>
     <td height="43" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:30px;color:#000000;font-weight:bold;text-transform:uppercase;" class="filinfo-titre">Article similaire</td>
     </tr>
     <tr>
     <td>
     <table width="700" border="0" cellpadding="0" cellspacing="0" class="filinfo-liste">
         <tr>
         <td valign="top" class="filinfo-bloc" style="padding:15px 0 0 0;">
             <table width="300" border="0" cellpadding="0" cellspacing="0">
             <tr>
             <td style="font-family:Georgia, Helvetica, sans-serif;font-size:16px;line-height:24px;">
             <span style="font-family:Arial, Helvetica, sans-serif;font-size:17px;color:#e04659;font-weight: bold;">
              <?php echo $posts[0]->post_date ?>
              </span>
              <a href="<?php echo get_the_permalink_news($posts[0]->ID); ?>" style="color:#000000;text-decoration:none;"> 
               <?php echo $posts[0]->post_title ?>
              </a>
              <br/>
              <a href="<?php echo get_the_permalink_news($posts[0]->ID); ?>" style="font-family:Arial;font-size:15px;color:#e04659;text-decoration:none;line-height:24px;font-weight: bold;">[Voir l'article]</a>
              </td>
              </tr>
              </table>
          </td>

          <td valign="top" align="right" class="filinfo-bloc" style="padding:15px 0 0 0;">
             <table width="300" border="0" cellpadding="0" cellspacing="0">
             <tr>
             <td style="font-family:Georgia, Helvetica, sans-serif;font-size:16px;line-height:24px;">
             <span style="font-family:Arial, Helvetica, sans-serif;font-size:17px;color:#e04659;font-weight: bold;">
              <?php echo $posts[1]->post_date ?>
              </span>
              <a href="<?php echo get_the_permalink_news($posts[1]->ID);?>" style="color:#000000;text-decoration:none;"> 
               <?php echo $posts[1]->post_title ?>
              </a>
              <br/>
              <a href="<?php echo $posts[1]->guid ?>" style="font-family:Arial;font-size:15px;color:#e04659;text-decoration:none;line-height:24px;font-weight: bold;">[Voir l'article]</a>
              </td>
              </tr>
              </table>
          </td>
          </tr>
      </table>
      </td>
      </tr>
      <tr>
      <td>
     <table width="700" border="0" cellpadding="0" cellspacing="0" class="filinfo-liste">
         <tr>
         <td valign="top" class="filinfo-bloc" style="padding:15px 0 0 0;">
             <table width="300" border="0" cellpadding="0" cellspacing="0">
             <tr>
             <td style="font-family:Georgia, Helvetica, sans-serif;font-size:16px;line-height:24px;">
             <span style="font-family:Arial, Helvetica, sans-serif;font-size:17px;color:#e04659;font-weight: bold;">
              <?php echo $posts[2]->post_date ?>
              </span>
              <a href="<?php echo get_the_permalink_news($posts[2]->ID); ?>" style="color:#000000;text-decoration:none;"> 
               <?php echo $posts[2]->post_title ?>
              </a>
              <br/>
              <a href="<?php echo $posts[2]->guid ?>" style="font-family:Arial;font-size:15px;color:#e04659;text-decoration:none;line-height:24px;font-weight: bold;">[Voir l'article]</a>
              </td>
              </tr>
              </table>
          </td>

          <td valign="top" align="right" class="filinfo-bloc" style="padding:15px 0 0 0;">
             <table width="300" border="0" cellpadding="0" cellspacing="0">
             <tr>
             <td style="font-family:Georgia, Helvetica, sans-serif;font-size:16px;line-height:24px;">
             <span style="font-family:Arial, Helvetica, sans-serif;font-size:17px;color:#e04659;font-weight: bold;">
              <?php echo $posts[3]->post_date ?>
              </span>
              <a href="<?php echo get_the_permalink_news($posts[3]->ID); ?>" style="color:#000000;text-decoration:none;"> 
               <?php echo $posts[3]->post_title ?>
              </a>
              <br/>
              <a href="<?php echo $posts[3]->guid ?>" style="font-family:Arial;font-size:15px;color:#e04659;text-decoration:none;line-height:24px;font-weight: bold;">[Voir l'article]</a>
              </td>
              </tr>
              </table>
          </td>
          </tr>
      </table>
       </td>
       </tr>
       </table>
</td>
</tr>
<tr><td height="35" class="divider"></td></tr>
<?php 
}

function fct_navigation($index = 0){
?>
<tr>
<td class="subItems" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#000000;font-weight:normal;text-transform:uppercase;padding:0 0 45px 0;">
<a href="http://www.elle.be/fr/mode/" style="color:#000000;text-decoration:none;" target="_blank">Mode</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/beaute/" style="color:#000000;text-decoration:none;" target="_blank">Beauté</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/actu/" style="color:#000000;text-decoration:none;" target="_blank">Actu</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/food/" style="color:#000000;text-decoration:none;" target="_blank">Food</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/elle-decoration/" style="color:#000000;text-decoration:none;" target="_blank">Culture</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/sport/" style="color:#000000;text-decoration:none;" target="_blank">Sport</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/sexe/" style="color:#000000;text-decoration:none;" target="_blank">Sexe</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/adresses/" style="color:#000000;text-decoration:none;" target="_blank">Adesses</a>
<span style="padding:0 8px;"> - </span>
<a href="http://www.elle.be/fr/185022-inscrivez-vous-a-la-elle-fashion-night-du-20-octobre-a-anvers.html" style="color:#e04659;text-decoration:none;" target="_blank">ELLE FASHION NIGHT</a></td>
</tr>
<?php
}

function fct_texte($index = 0){
?>
    <tr>
        <td>
            <div style="font-family:Georgia, Helvetica, sans-serif;font-size:16px;color:#000000;text-decoration:none;line-height:24px;">
            <?php echo get_field('newsletter')[$index]['texte']; ?>
            </div>
        </tr>
    <tr>
    <tr><td height="35" class="divider"></td></tr>
    <?php
}

function fct_concours($index = 0){
$posts = get_posts(array('posts_per_page' => 2, 'ignore_sticky_posts' => 1, 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => '1000 days ago')))); 
?>
<tr>
<td height="43" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:30px;color:#000000;font-weight:bold;text-transform:uppercase;" class="filinfo-titre">Concours</td>
</tr>
<tr>
<td>
<table width="700" border="0" cellpadding="0" cellspacing="0" class="listing">
<tr>                                
<td align="center" valign="top" class="listing-bloc">
  <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="padding:0 5px 0 0">
     <tr>
        <td valign="top" style="padding:0 0 10px 0">
           <a href="<?php echo get_the_permalink_news($posts[0]->ID); ?>" style="display:block;"><img alt="" src="<?php echo get_the_post_thumbnail_url($posts[0]->ID,'vsmall'); ?> " border="0" /></a>
           </td>
           </tr>
           <tr>
           <td valign="top"><a href="<?php echo get_the_permalink_news($posts[0]->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#000000;font-weight:bold;text-transform:uppercase;text-decoration:none;">
           <?php echo $posts[0]->post_title; ?>
           </a></td>
           </tr>
           <tr>
           <td valign="top" class="listing-link"><a href="<?php echo get_the_permalink_news($posts[0]->ID);  ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#e04659;text-decoration:none;font-weight: bold">[ Lire l'article ]</a></td>
       </tr>
    </table>
</td>

<td align="center" valign="top" class="listing-bloc">
  <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="padding:0 5px 0 0">
     <tr>
        <td valign="top" style="padding:0 0 10px 0;">
           <a href="<?php echo get_the_permalink_news($posts[1]->ID); ?>" style="display:block;"><img alt="" src="<?php echo get_the_post_thumbnail_url($posts[1]->ID,'vsmall'); ?> " border="0" /></a>
           </td>
           </tr>
           <tr>
           <td valign="top"><a href="<?php echo get_the_permalink_news($posts[1]->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#000000;font-weight:bold;text-transform:uppercase;text-decoration:none;">
           <?php echo $posts[1]->post_title ?>
           </a></td>
           </tr>
           <tr>
           <td valign="top" class="listing-link"><a href="<?php echo get_the_permalink_news($posts[1]->ID); ?>" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#e04659;text-decoration:none;font-weight: bold">[ Lire l'article ]</a></td>
       </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
<tr><td height="35" class="divider"></td></tr>
<?php
}