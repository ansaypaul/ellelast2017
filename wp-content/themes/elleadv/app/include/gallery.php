<?php
global $post;
function wp_get_attachment( $attachment_id ) {
      $attachment = get_post( $attachment_id );
      if(isset($attachment)){
        $args = array(
              'number_index' => '',
              'id' => $attachment->ID,
              'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
              'caption' => $attachment->post_excerpt,
              'description' => $attachment->post_content,
              'href' => get_permalink( $attachment->ID ),
              'src' => $attachment->guid,
              'title' => $attachment->post_title,
              'desc_rich' => get_field('desc_rich', $attachment->ID ),
              'shop_link' => get_post_meta( $attachment->ID, 'shop_link', true ),
              'shop_price' => get_post_meta( $attachment->ID, 'shop_price', true ),
              'gallery_link_url' => get_post_meta( $attachment->ID, '_gallery_link_url', true ),
              'gallery_pic-thumb' => wp_get_attachment_image_src( $attachment->ID, 'gallery2018mid' )[0],
              'gallery_pic' => wp_get_attachment_image_src( $attachment->ID, 'gallery2018' )[0],
              'slug'=> basename(get_permalink( $attachment->ID )),
              'srcset'=> elle_get_picture_srcs($attachment->ID,'gallery2018'),
              'cta_title'=> get_post_meta( $attachment->ID, 'cta_title', true ),
          );
          
        $image_alt = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true); if($image_alt == ''){ $image_alt = get_the_title($attachment->ID); }
        if($args['desc_rich'] == ''){ $args['desc_rich'] = $args['caption']; }

        $args['desc_rich'] = str_replace('http://www.elle.be','https://www.elle.be',$args['desc_rich']);
        $args['desc_rich'] = str_replace('http://quizz.elle.be','https://quizz.elle.be',$args['desc_rich']);

        if($args['alt'] == ''){ $args['alt'] = $image_alt; }
        return $args;
        }
    }


     // Galeries
    function wptester_post_gallery($output, $attr) {
        global $post, $wp_locale, $idgallery;

        $attr['columns'] = (isset($attr['columns']) && !empty($attr['columns'])) ? $attr['columns']  : 0;
        if($attr['columns'] == null){ $attr['columns'] = 3; }
        $attr['ds_firstpic'] = (isset($attr['ds_firstpic']) && !empty($attr['ds_firstpic'])) ? $attr['ds_firstpic'] : array();
        $ids = explode( ',', $attr['ids'] );
        $output = '';
        $i = 0;
        setup_postdata( $post );
        $idgallery++;

        if(is_page_template( 'template-amp-2.php' ) || is_page_template( 'template-amp-2018.php' )){  
            $output = '';
            $output = '<h2 class="h3 mb1">Gallerie</h2><amp-carousel width="500" height="600" layout="responsive" type="slides" class="mb4">';
            while ($i <= 9) {
              $pic = wp_get_attachment($ids[$i]);
                $output .= '
                <figure class="ampstart-image-with-caption m0 relative mb4">
                  <amp-img src="'.$pic['gallery_pic-thumb'].'" width="500" height="500" alt="" layout="responsive" class=""></amp-img>
                  <figcaption class="h5 mt1 px3">'.$pic['caption'].'</figcaption>
                </figure>';
                 $i++;
            }
            $output .= '</amp-carousel>';
            return $output;
            //break;
        }


    if(!is_page_template( 'template-amp-2.php' )){
        $template = "";
        if(array_key_exists('ds_template',$attr)){ $template =  $attr['ds_template']; }

            $i = 0;$slider = 0;$row = 0;
            $nb_picture_max = count($ids);
            //if($nb_picture_max > 30){$nb_picture_max = 30;}
            while ($i <  $nb_picture_max) {
                    $picture = wp_get_attachment($ids[$i]);
                    $picture['number_index'] = $i + 1;
                    if($i % 3){ $picture['ads'] = 1; }
                    $data[] = $picture;
                    $i++;
            }

           
            $data_slider = array_chunk($data, 9);
            $number_array = count($data_slider) -1;
            for($i = 0; $i <= $number_array; $i++) {
                $data_slider['picture'][$i] = array_chunk($data_slider[$i], 3);
            }

            /*echo '<pre>';
            var_dump($data); 
            echo '<pre>';*/

            $name_gallery = $post->ID .'_'.$idgallery;
            $data_slider['id_gallery'] = $name_gallery;

            /*$output .= '<div class="site-inner site-inner-ads no_print mob_ad_gallery" style="padding:0"><div id="billboard-gallery-'._display_id_random().'" class="ads TopLarge" data-categoryAd="" data-formatMOB="MOB640x150" data-refadMOB="pebbleMOB640x150" data-format="" data-refad="" data-location="" data-position="2"></div></div>';*/
            $output .= '<div data-id="'.$name_gallery.'" class="gallery_wrapper '.$template.'" id="'.$name_gallery.'">';
            //$output .= "<script>var gallery_ajax_".$name_gallery." =".json_encode($data_slider).";</script>";
            $output .= '<div class="buttons_wrapper"><div class="gallery_index"><div><span class="gallery_index_current gallery_index_current_'.$name_gallery.'">1</span><span class="gallery_index_total gallery_index_total_'.$name_gallery.'"></span></div></div>';
            $output .= '<button data-image="0" class="button_open_fancy">Open fancybox</button></div> ';
            $output .= '<div class="gallery_ajax gallery_ajax_'.$name_gallery.'">';

              if($template == 'template-mosaique'){

                $templategrid .='<div class="template-all">';
                $templategrid .='<div class="grid">';
                $templategrid .='<div class="grid-sizer"></div>';

                $index_picture = 0;
                $index_picture_total = count($data) - 1;
                for($index_picture = 0; $index_picture <= $index_picture_total; $index_picture++) {
                  
                  $templategrid .='<div class="grid-item">';
                  $templategrid .='<span class="grid-item-wrapper">';
                  $templategrid .='<picture><img class="fancy_img" data-fancybox="'.$name_gallery.'" href="'.$data[$index_picture]['gallery_pic-thumb'].'" data-srcset="" alt="" src="'.$data[$index_picture]['gallery_pic'].'"/></picture>';
                  $templategrid .='<div class="swiper-caption_data hidden">';
                  if($data[$index_picture]['desc_rich'] != ''){ $templategrid .='<div class="swiper-caption">'.$data[$index_picture]['desc_rich'].'</div>'; }
                  if($data[$index_picture]['shop_link'] != ''){ $templategrid .='<div class="swiper-shop btn-v1"><a rel="nofollow" target="_blank" href="'.$data[$index_picture]['shop_link'].'">'.$data[$index_picture]['cta_title'].'</a></div>'; }
                  $templategrid .='</div>';
                  $templategrid .='</span>';
                  $templategrid .='</div>';
                }

                $templategrid .= '</div>';
                $templategrid .= '</div>';

                $output .= $templategrid;

              }else if($template == 'template-inline'){
                $templateinline = '<div class="template-inline">';
                $index_picture = 0;
                $index_picture_total = count($data) - 1;
                for($index_picture = 0; $index_picture <= $index_picture_total; $index_picture++) {
                  
                  $templateinline .='<div class="inline-item">';
                  $templateinline .='<span class="inline-item-wrapper">';
                  $templateinline .='<picture><img class="fancy_img" data-fancybox="'.$name_gallery.'" href="'.$data[$index_picture]['gallery_pic-thumb'].'" data-srcset="" alt="" src="'.$data[$index_picture]['gallery_pic'].'"/></picture>';
                  $templateinline .='<div class="inline-caption_data hidden">';
                  if($data[$index_picture]['desc_rich'] != ''){ $templateinline .='<div class="inline-caption">'.$data[$index_picture]['desc_rich'].'</div>'; }
                  if($data[$index_picture]['shop_link'] != ''){ $templateinline .='<div class="swiper-shop btn-v1"><a rel="nofollow" target="_blank" href="'.$data[$index_picture]['shop_link'].'">'.$data[$index_picture]['cta_title'].'</a></div>'; }
                  $templateinline .='</div>';
                  $templateinline .='</span>';
                  $templateinline .='</div>';
                }
                $templateinline .= '</div>';

                $output .= $templateinline;

              } else{

                $templatecol1 =  '<div class="swiper-gallery template-1col">';
                $templatecol1 .= '<div class="swiper-container-gallery swiper-container-horizontal">';
                $templatecol1 .= '<div class="swiper-wrapper">';
                /** BOUCLE PICTURE **/
                $index_picture = 0;
                $index_picture_total = count($data) - 1;
                for($index_picture = 0; $index_picture <= $index_picture_total; $index_picture++) {
                //  var_dump($picture);
                  $templatecol1 .=  '<div class="swiper-slide">';
                  $templatecol1 .=  '<div class="swiper-picture_wrapper">';
                  if($index_picture < 2){
                      $templatecol1 .= '<picture class=""><img class="imagify-no-webp no-lazy-img fancy_img" rel="gallery" data-fancybox="'.$name_gallery.'" data-caption="" data-ctatitle="'.$data[$index_picture]['cta_title'].'" data-shoplink= "'.$data[$index_picture]['shop_link'].'" href="'.$data[$index_picture]['gallery_pic-thumb'].'" data-srcset="" alt="'.$data[$index_picture]['alt'].'" src="'.$data[$index_picture]['gallery_pic'].'" data-imgsrc="'.$data[$index_picture]['gallery_pic'].'"/></picture>';
                  }else{
                      $templatecol1 .= '<picture class=""><img class="imagify-no-webp no-lazy-img fancy_img" rel="gallery" data-fancybox="'.$name_gallery.'" data-caption="" data-ctatitle="'.$data[$index_picture]['cta_title'].'" data-shoplink= "'.$data[$index_picture]['shop_link'].'" href="'.$data[$index_picture]['gallery_pic-thumb'].'" data-srcset="" alt="'.$data[$index_picture]['alt'].'" data-imgsrc="'.$data[$index_picture]['gallery_pic'].'"/></picture>';
                  }
                  $templatecol1 .= '</div>';
                  $templatecol1 .= '<div class="swiper-caption_wrapper swiper-caption_data">';
                  $templatecol1 .= '<div class="swiper-caption">'.$data[$index_picture]['desc_rich'].'</div>';
                  if($data[$index_picture]['shop_link'] !=''){ $templatecol1 .= '<div class="swiper-shop btn-v1"><a rel="nofollow" target="_blank" href="'.$data[$index_picture]['shop_link'].'">'.$data[$index_picture]['cta_title'].'</a></div>'; }
                  $templatecol1 .= '</div>';
                  $templatecol1 .= '</div>';
                } /** BOUCLE PICTURE **/

                $templatecol1 .= '</div>';
                $templatecol1 .= '<div class="swiper-button-prev swiper-button-prev-galerie sprite"><span></span></div>';
                $templatecol1 .= '<div class="swiper-button-next swiper-button-next-galerie sprite"><span></span></div>';
                $templatecol1 .= '</div>';
                $templatecol1 .= '</div>';
                $output .= $templatecol1;

              }

            $output .= '</div>';
            $output .= '</div>';
            //var_dump($data_slider);
            return $output;
            //break;
        
    }
        return $output;
    }

add_filter('post_gallery', 'wptester_post_gallery', 10, 2 );