<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_59b7cf6232590',
	'title' => 'Adresses',
	'fields' => array (
		array (
			'key' => 'field_59b7cfd163213',
			'label' => 'Quoi ?',
			'name' => 'quoi_',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Boutique' => 'Boutique',
				'Restaurant' => 'Restaurant',
				'Bar' => 'Bar',
				'Hôtel' => 'Hôtel',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59b7d07eca944',
			'label' => 'Quel style ?',
			'name' => 'quel_style_boutique',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59b7cfd163213',
						'operator' => '==',
						'value' => 'Boutique',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Mode' => 'Mode',
				'Beauté' => 'Beauté',
				'Déco' => 'Déco',
				'Food' => 'Food',
				'Bien-être' => 'Bien-être',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59b7d10d292b1',
			'label' => 'Quel style ?',
			'name' => 'quel_style_restaurant',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59b7cfd163213',
						'operator' => '==',
						'value' => 'Restaurant',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Romantique' => 'Romantique',
				'Famille' => 'Famille',
				'Brunch' => 'Brunch',
				'Healthy' => 'Healthy',
				'Sur le pouce' => 'Sur le pouce',
				'Gastronomique' => 'Gastronomique',
				'Relax' => 'Relax',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59b7d171292b2',
			'label' => 'Quel style ?',
			'name' => 'quel_style_bar',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59b7cfd163213',
						'operator' => '==',
						'value' => 'Bar',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Cocktails' => 'Cocktails',
				'Fête' => 'Fête',
				'Éphémère' => 'Éphémère',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59b7d1b8292b3',
			'label' => 'Quel style ?',
			'name' => 'quel_style_hotel',
			'type' => 'checkbox',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59b7cfd163213',
						'operator' => '==',
						'value' => 'Hôtel',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Palace' => 'Palace',
				'Boutique' => 'Boutique',
				'Bien-être' => 'Bien-être',
				'Bed & Breakfast' => 'Bed & Breakfast',
				'Atypique' => 'Atypique',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59b7d2d14fe4b',
			'label' => 'Où ?',
			'name' => 'ou_',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Bruxelles' => 'Bruxelles',
				'Anvers' => 'Anvers',
				'Liège' => 'Liège',
				'Namur' => 'Namur',
				'Gand' => 'Gand',
				'Louvain' => 'Louvain',
				'Knokke' => 'Knokke',
				'Ostende' => 'Ostende',
				'Hasselt' => 'Hasselt',
				'Charleroi' => 'Charleroi',
				'Mons' => 'Mons',
				'Bruges' => 'Bruges',
				"à l'étranger"=> "à l'étranger",
			),
			'default_value' => array (
			),
			'allow_null' => 1,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_category',
				'operator' => '==',
				'value' => 'category:adresses',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;