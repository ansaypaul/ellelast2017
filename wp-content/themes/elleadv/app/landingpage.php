<?php
/*
	Template Name: Landing page
 */

if (isset($_GET['l'])) {
    $l = $_GET['l'];
} elseif (isset($_COOKIE['l'])) {
    $l = $_COOKIE['l'];
}

if (isset( $l )) {
    switch( $l ) {
        case 'nl':
        upd_cookie( 'nl' );
		header('Location: https://www.elle.be/nl/');
		//header('Location: http://www.elle.be/splash?l=nl');
        exit;
        break;
    default:
        upd_cookie( 'fr' );
		header('Location: https://www.elle.be/fr/');
		//header('Location: http://www.elle.be/splash?l=fr');
        exit;
    }
}

function upd_cookie( $lng ) {
    setcookie('l', $lng , time() + 60*60*24*365);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<head>
		<meta name="google-site-verification" content="ganpHPm_1U0EJbEjQam_0jjV18mzMLV24WhoO0JrD7M" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="Bienvenue sur le site du ELLE Belgique, le magazine des tendances mode, beauté, des conseils en amour, sexe et santé. Welkom op de site van ELLE België, met alle mode-en beautyniews, gossip, recepten, deco-nieuws, lifestyle en meer. Dagelijkse updates!" />
		<meta name="keywords" content="elle, magazine, mode, fashion, beauté, lifestyle, people" />
		<meta name="p:domain_verify" content="2da865fadfec17231d649f79bef2c378"/>
		<meta name="p:domain_verify" content="30d7c293b147b04a5a4a143c2e8c0827"/>
		<meta name="p:domain_verify" content="029dc870cd524314863829211313e5eb"/>
		<link rel="canonical" href="https://www.elle.be" />
		<?php
			$agent_langs = split( ",", $_SERVER['HTTP_ACCEPT_LANGUAGE'] );
			foreach( $agent_langs as $agent_lang ) {
				$language = strtolower( $agent_lang );
				$pos = strpos( $language, "-" );
				if ( $pos > 0 ) {
					$language = substr( $language, 0, $pos );
				}
				$pos = strpos( $language, ";" );
				if ( $pos > 0 ) {
					$language = substr( $language, 0, $pos );
				}
				if($language=="fr"){
					echo "<meta http-equiv=\"Refresh\" content=\"0; url=https://www.elle.be/fr/\">\n";
					exit;
				}
				else if($language=="nl"){
					echo "<meta http-equiv=\"Refresh\" content=\"0; url=https://www.elle.be/nl/\">\n";
					exit;
				}
			}
		?>	
		<title>ELLE Belgique / ELLE België</title>
		<!-- build:baseInlineStyles --> <style type="text/css">a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:"";content:none}table{border-collapse:collapse;border-spacing:0}@font-face{font-family:Elle Gabor Std;src:url(https://www.elle.be/fonts/ElleGaborStd-Text.eot);src:url(https://www.elle.be/fonts/ElleGaborStd-Text.eot#iefix) format("embedded-opentype"),url(https://www.elle.be/fonts/ElleGaborStd-Text.woff) format("woff"),url(https://www.elle.be/fonts/ElleGaborStd-Text.ttf) format("truetype");font-weight:400;font-style:normal}@font-face{font-family:Elle Gabor Std;src:url(https://www.elle.be/fonts/ElleGaborStd-DemiBold.eot);src:url(https://www.elle.be/fonts/ElleGaborStd-DemiBold.eot#iefix) format("embedded-opentype"),url(https://www.elle.be/fonts/ElleGaborStd-DemiBold.woff) format("woff"),url(https://www.elle.be/fonts/ElleGaborStd-DemiBold.ttf) format("truetype");font-weight:600;font-style:normal}body header{-webkit-box-sizing:border-box;box-sizing:border-box;text-align:center;height:42px;background:#fff;padding:5px 0;position:fixed;top:0;left:0;border-bottom:1px solid #cdcdcd;z-index:400;width:100%}@media screen and (min-width:480px){body header{height:64px;padding:0}}@media screen and (min-width:640px){body header{position:relative;height:128px;padding-bottom:0}}body header .site-inner .site-logo{margin:0 auto}body header .site-inner .site-logo.sprite{background:url(https://www.elle.be/img/icons/elle_2018_sprite.svg) no-repeat;-webkit-background-size:500px 700px;background-size:500px 700px;-webkit-transition:none;-o-transition:none;transition:none;display:block;background-position:0 -33px;width:94px;height:32px}@media screen and (min-width:480px){body header .site-inner .site-logo.sprite{width:172px;height:64px;background-position:0 -335px;margin-top:5px}}@media screen and (min-width:640px){body header .site-inner .site-logo.sprite{width:230px;height:80px;background-position:0 -420px;top:25px;position:relative}}body #container{max-width:500px;margin:0 auto;font-size:0;vertical-align:top;margin-top:80px}@media screen and (min-width:480px){body #container{margin-top:140px}}@media screen and (min-width:640px){body #container{margin-top:60px}}body #container div{display:inline-block;position:relative;width:100%}body #container div.bottom a,body #container div.top a{height:100%;width:100%;display:block}body #container div.bottom a #fr,body #container div.top a #fr{vertical-align:top;display:inline-block;opacity:.8;margin-top:5%;margin-left:5%;width:50%;padding-top:50%;text-align:center;position:relative}body #container div.bottom a #fr div,body #container div.top a #fr div{display:block;text-decoration:none;font-family:Elle Gabor Std,sans-serif;font-size:.9rem;line-height:1.1rem;color:#000;top:50%;position:absolute;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-transform:uppercase}@media screen and (min-width:640px){body #container div.bottom a #fr div,body #container div.top a #fr div{font-size:1.1rem;line-height:1.3rem}}body #container div.bottom a #fr div span,body #container div.top a #fr div span{font-weight:700;display:block}body #container div.bottom a #fr:after,body #container div.top a #fr:after{content:"";display:block;border:2px solid #000;width:90%;height:90%;position:absolute;top:3%;left:3%;z-index:-1}body #container div.bottom a #nl,body #container div.top a #nl{vertical-align:top;display:inline-block;opacity:.8;margin-top:-10%;margin-right:-10%;width:50%;padding-top:50%;text-align:center;position:relative}body #container div.bottom a #nl div,body #container div.top a #nl div{display:block;text-decoration:none;font-family:Elle Gabor Std,sans-serif;font-size:.9rem;line-height:1.1rem;color:#000;top:50%;position:absolute;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);text-transform:uppercase}@media screen and (min-width:640px){body #container div.bottom a #nl div,body #container div.top a #nl div{font-size:1.1rem;line-height:1.3rem}}body #container div.bottom a #nl div span,body #container div.top a #nl div span{font-weight:700;display:block}body #container div.bottom a #nl:after,body #container div.top a #nl:after{content:"";display:block;border:2px solid #000;width:90%;height:90%;position:absolute;top:5%;left:5%;z-index:-1}body #container div.bottom a .img-fr,body #container div.top a .img-fr{max-width:45%;display:inline-block;width:45%;margin-bottom:30px}body #container div.bottom a .img-fr img,body #container div.top a .img-fr img{width:100%;max-width:100%;display:block}body #container div.bottom a .img-nl,body #container div.top a .img-nl{max-width:45%;display:inline-block;width:45%;margin-bottom:30px}body #container div.bottom a .img-nl img,body #container div.top a .img-nl img{width:100%;max-width:100%;display:block} h1{font-size: 1.4rem;display: block;text-transform: uppercase;font-family: Elle Gabor Std,sans-serif;text-align: center;margin-bottom: 40px;}</style> <!-- endbuild -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-42249957-3', 'auto');
		  ga('require', 'displayfeatures');
		  ga('require', 'linkid', 'linkid.js');
		  ga('send', 'pageview');

		</script>
	</head>
	<body>
		<header>
			<div class="site-inner">
				<a href="https://www.elle.be" class="site-logo sprite"></a>
			</div>
		</header>
		<div id="container">
			<h1>ELLE Belgique / ELLE België</h1>
			<div class="top">
				<a href="https://www.elle.be/fr/">
					<div id="fr">
						<div>Continuer<span>En fran&#xE7;ais</span></div>
					</div>
					<div class="img-fr">
						<img src="https://www.elle.be/img/elle_landing_1.jpg" alt="ELLE portait FR" />
					</div>
				</a>
			</div>
			<div class="bottom">
				<a href="https://www.elle.be/nl/">
					<div class="img-nl">
						<img src="https://www.elle.be/img/elle_landing_2.jpg" alt="ELLE portait NL" />
					</div>
					<div id="nl">
						<div>Verdergaan in<span>het nederlands</span></div>
					</div>
				</a>
			</div>
		</div>
	</body>
</html>
