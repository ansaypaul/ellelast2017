<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<?php

	$tag_id = get_query_var('tag_id');
	$args = array('numberposts'=> 1, 
				'post_type' => 'magazines',
               'meta_query' => array(
               	array(
               'key' => 'tag_mag',
               'value'=> $tag_id,
               'compare' => 'LIKE',
           		)
            )
          );
	$post_mag = get_posts($args);
	$display_mag = count($post_mag);
?>

<main role="main" class="main main-category">
	<div class="site-inner">

		<?php if ($display_mag == 0) { ?>
			<h1>
				<?php single_cat_title(); ?>
			</h1>
			<?php echo category_description(); ?>
		<?php } else { ?>
		<?php 
			foreach ( $post_mag as $post ) : setup_postdata( $post ); 
			?>
			<div class="template-parts footer-magazine">
				<div class="block-left">
					<?php the_post_thumbnail('',array('lazy',0,0)); ?>
				</div>
				<div class="block-right">
					<h1>
						<?php echo get_field('title-mag');?>
					</h1>
					<span class="block-content">
						<?php echo get_field('description-mag');?>
					</span>
					<div class="block-1">
						<img class="lazy-img" data-srcset="<?php echo get_field('thumb_images')[0]['image']['sizes']['thumbnail']; ?>" alt="" />
						<p class="block-page">p.<?php echo get_field('thumb_images')[0]['page'];?></p>
						<p class="block-p-content"><?php echo get_field('thumb_images')[0]['extrait'];?></p>
					</div>
					<div class="block-2">
						<img class="lazy-img" data-srcset="<?php echo get_field('thumb_images')[1]['image']['sizes']['thumbnail']; ?>" alt="" />
						<p class="block-page">p.<?php echo get_field('thumb_images')[1]['page'];?></p>
						<p class="block-p-content"><?php echo get_field('thumb_images')[1]['extrait'];?></p>
					</div>
					<div class="block-3">
						<img class="lazy-img" data-srcset="<?php echo get_field('thumb_images')[2]['image']['sizes']['thumbnail']; ?>" alt="" />
						<p class="block-page">p.<?php echo get_field('thumb_images')[2]['page'];?></p>
						<p class="block-p-content"><?php echo get_field('thumb_images')[2]['extrait'];?></p>
					</div>
				</div>
			</div>
			<?php endforeach; wp_reset_postdata(); ?>
		<?php } ?>
		

		
		<?php 
		if($paged < 2){

        	$tag_id = get_query_var('tag_id');

			 $posts = get_posts(array('posts_per_page' => 1,'tag__in' => array($tag_id), 'post__not_in' => $excludeIDS));
			_first_article_block($posts,'','',0); 

			/* if($paged == 1){ ?><div class="swiper-article-ajax"><span class="loading"><?php _e( 'Chargement en cours', 'html5blank' ); ?>...</span></div> <?php } */

			$posts = get_posts(array('posts_per_page' => 6,'tag__in' => array($tag_id), 'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,'',$tag_id,''); 

			/*

			$time_most_popular = '120 days ago';
			$posts = get_posts(array('posts_per_page' => 3,'tag__in' => array($tag_id), 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			if($posts){ _top_article_block($posts,''); }

			*/
		}

		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$posts_feeds = get_posts(array('posts_per_page' => get_option( 'posts_per_page' ), 'tag__in' => array($tag_id), 'post__not_in' => $excludeIDS, 'paged' => $paged));
		$time_most_popular = '9999 days ago';
		$posts_popular = get_posts(array('tag__in' => array($tag_id), 'posts_per_page' => 9 , 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC',
		            'date_query' => array(array('after' => $time_most_popular, 'post__not_in' => $excludeIDS)))); 
		?>
		<div class="articles">
		    <div class="content-article-ajax content entry-content">
		        <?php 
		        $current_post = 0;
		        foreach ( $posts_feeds as $post ) : setup_postdata( $post ); $current_post++;  $excludeIDS[] = $post->ID;
		            global $post;
		            get_template_part('template-parts/general/article'); ?>
		            <?php if ($current_post == 1){ ?> 
		            <div class="site-inner-ads no_print">
		               <div class="rmgAd" data-device="mobile" data-type="RECT"></div>
		            </div>
		            <?php } ?>
		            <?php if ($current_post == 3){ ?> 
		            <div class="rmgAd" data-device="mobile" data-type="RECT"></div>
		            <?php } ?> 
		            <?php if ($current_post == 8){ ?> 
		            <div class="rmgAd" data-device="mobile" data-type="RECT"></div>
		            <?php } ?> 
		            <?php endforeach; wp_reset_postdata(); 
		        ?>
		    </div>
		    <!-- sidebar -->
		    <aside class="sidebar sidebar-index" role="complementary">
		        <div class="rmgAd margin_bottom_20" data-device="desktop" data-type="RECT_ABOVE"></div>
		            <div class="most-popular">
		                <span class="title-h3"><?php _e( 'les + lus', 'html5blank' ); ?></span>
		                <ul class="most-popular-titles" >
		                <?php foreach ( $posts_popular as $post ) : setup_postdata( $post ); $excludeIDS[] = $post->ID; ?>
		                    <li>
		                        <span class="title-h4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
		                    </li>
		                <?php endforeach; wp_reset_postdata(); ?>
		                </ul>
		            </div>
		            <?php if(isset(autopromo_objet_mag()[0])) { ?>
		               <!--
		               <a class="autopromo-abonnement" href="<?php echo get_field('autopromo_link',autopromo_objet_mag()[0]->ID); ?>">
		                    <img src="<?php echo get_field('autopromo_imu',autopromo_objet_mag()[0]->ID); ?>" alt="Abonnement ELLE" />
		                </a>
		                -->
		            <?php } ?>
		        <?php /*  get_template_part('template-parts/general/health-inset'); */ ?>
		        <div class="rmgAd" data-device="desktop" data-type="RECT_MIDDLE"></div>
		    </aside>     
		    <div class="navigation">
		            <?php pressPagination('',3); ?>
		    </div>       
		</div>
	</div>
</main>
<?php get_footer(); ?>