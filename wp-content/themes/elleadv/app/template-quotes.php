<?php
/*
Template Name: Quotes 2019
*/
?>
<?php get_header(); ?>
<main role="main" class="main main-page">
  <div class="page">
    <div class="site-inner">
      <div class="main-content-wrapper content-wrapper">
        <div class="content content-quotes">
            <h1><?php _e( "Choisis ton mood", "html5blank" ); ?></h1>
            <p><?php _e( "Random intro  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt impedit quo rem laboriosam tempora modi ratione, possimus commodi, praesentium ex id repellendus dolores quae nobis temporibus ipsa ut? Officia, quisquam!", "html5blank" ); ?></p>
              <div class="mood-choice filter">
                <!-- Toggle active-on sur le btn actif -->
                <button class="all mood-select active-on mood-0" data-filter="all">tous</button>
                <button class="mood-select mood-1"><?php _e( "BFF", "html5blank" ); ?></button>
                <button class="mood-select mood-2"><?php _e( "BAD", "html5blank" ); ?></button>
                <button class="mood-select mood-3"><?php _e( "GOOD", "html5blank" ); ?></button>
                <button class="mood-select mood-4"><?php _e( "Anti-régime", "html5blank" ); ?></button>
                <button class="mood-select mood-5"><?php _e( "Girl PWR", "html5blank" ); ?></button>
                <button class="mood-select mood-6"><?php _e( "LOVER", "html5blank" ); ?></button>
              </div>
            <div class="mood-result" id="mood-result">
                <div class="mood-result-wrapper" id="mood-result-wrapper">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/quotes-mood/quotejeudi20.jpg" alt="ELLE Current Mood">
                </div>
                <button class="mood-next">
                  <?php _e( "Suivant", "html5blank" ); ?>
                </button>
                <!-- Rajouter messenger dans les RS -->
                <div id="social-share">
                  <div><?php _e( "C'est trop moi, je partage", "html5blank" ); ?></div>
                  <?php display_single_RS(); ?>
                </div>
            </div>
          </div>
          <!-- sidebar -->
          <aside class="sidebar" role="complementary">
            <!-- single-sidebar-related -->
            <div id="halfpage-<?php display_id_random()?>" class="ads ads-first halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="" data-position=""></div>
            <?php if(get_field('identifiant_youtube','option')){ ?>
              <!-- (1) video wrapper -->
              <div class="sidebar_youtube" data-embed="<?php echo get_field('identifiant_youtube','option'); ?>" data-picture="<?php echo get_field('image_de_couverture','option'); ?>">      
                  <!-- (2) the "play" button -->
                  <div class="play-button"></div>          
              </div>
            <?php } ?>
            <div id="halfpage-<?php display_id_random()?>" class="ads halfpage" categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="" data-position="2"></div>
          </aside>
          <!-- /sidebar -->
        </div>
      </div>
    </div>
  </div>
</main>
<style>
/*
.mood-result-wrapper{
  max-height: 536px;
  overflow: hidden;
}
*/

/*
inner .content-wrapper .content.content-quotes .mood-result img {
    max-width: 80%;
    margin-bottom: 0px;
}
*/
</style>
<?php get_footer(); ?>


