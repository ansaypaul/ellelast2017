<?php header( "HTTP/1.1 410 Gone" ); ?>
<?php get_header(); ?>
<main role="main" class="main main-404">
	<div class="site-inner">
		<article id="post-404">
			<div class="entry-header wrapper-404">
				<h1><?php _e( 'Erreur 410', 'html5blank' ); ?></h1>
				<div class="img-wrapper-404">
					<span><?php _e( "Oupsi, cette page n'existe pas!", 'html5blank' ); ?></span>
					<img src="<?php echo get_template_directory_uri(); ?>/img/404_queenb.gif" alt="404 Error">
				</div>
			</div>
			<div class="content entry-content">
				<p><?php _e( 'La page que vous recherchez n’existe pas', 'html5blank' ); ?>,</p>
				<p><?php _e( 'mais pas de panique', 'html5blank' ); ?> !</p>
				<h2><?php _e( 'Effectuez une recherche', 'html5blank' ); ?></h2>
				<?php get_template_part('template-parts/general/searchform');  ?>
			</div>
			<?php get_template_part('template-parts/general/article-mostpopular-bottom');  ?>
			<?php get_template_part('template-parts/general/article-related-bottom');  ?>
		</article>
	</div>
</main>

<?php get_footer(); ?>