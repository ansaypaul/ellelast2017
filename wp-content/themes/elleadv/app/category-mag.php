<?php
	/*
	Template Name: tag magazine
	*/
?>
<?php get_header(); ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" class="main main-category">
	<div class="site-inner">
		<?php 
			global $magazine;
			foreach ( $magazine as $post ) : setup_postdata( $post ); 
			$tag_mag = get_field('tag_mag'); 
			$current_tagname = $tag_mag->slug;
			?>
			<div class="template-parts footer-magazine">
				<div class="block-left">
					<?php the_post_thumbnail('',array('lazy',0,0)); ?>
				</div>

				<div class="block-right">
					<h1>
						<?php echo get_field('title-mag');?>
					</h1>
					<span class="block-content">
						<?php echo get_field('description-mag');?>
						<p><?php echo $current_tagname; ?></p>
					</span>
					<div class="block-1">
						<img class="lazy-img" data-srcset="<?php echo get_field('thumb_images')[0]['image']['sizes']['thumbnail']; ?>" alt="" />
						<p class="block-page">p.<?php echo get_field('thumb_images')[0]['page'];?></p>
						<p class="block-p-content"><?php echo get_field('thumb_images')[0]['extrait'];?></p>
					</div>
					<div class="block-2">
						<img class="lazy-img" data-srcset="<?php echo get_field('thumb_images')[1]['image']['sizes']['thumbnail']; ?>" alt="" />
						<p class="block-page">p.<?php echo get_field('thumb_images')[1]['page'];?></p>
						<p class="block-p-content"><?php echo get_field('thumb_images')[1]['extrait'];?></p>
					</div>
					<div class="block-3">
						<img class="lazy-img" data-srcset="<?php echo get_field('thumb_images')[2]['image']['sizes']['thumbnail']; ?>" alt="" />
						<p class="block-page">p.<?php echo get_field('thumb_images')[2]['page'];?></p>
						<p class="block-p-content"><?php echo get_field('thumb_images')[2]['extrait'];?></p>
					</div>
				</div>
			</div>
			<?php endforeach; wp_reset_postdata(); ?>
		<div id="halfpage-mob-1" class="ads halfpage" categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="" data-position=""></div>
		<div class="articles"> 
			<div class="content entry-content">
			<?php if( $posts ){ ?>
					<?php foreach ( $posts as $post ) : setup_postdata( $post ); the_post(); $current_post_id++; ?>
						<?php if ($current_post_id == 1){ ?> <?php } ?>
						<?php if ($current_post_id == 3){ ?> 
						<div id="halfpage-mob-2" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="" data-position="2"></div>
						<?php } ?> 
						<?php if ($current_post_id == 8){ ?> 
						<div id="halfpage-mob-3" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="" data-position="2"></div>
						<?php } ?> 
						<?php endforeach; wp_reset_postdata(); ?>
			<?php } else { ?>
				<p><?php _e( "Il n'y a pas de contenu pour le moment.", 'html5blank' ); ?></p>
			<?php } ?>
			</div>
			<!-- sidebar -->
			<aside class="sidebar sidebar-index" role="complementary">
				<div id="halfpage-1" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="MiddleLarge" data-refad="pebbleMiddleLarge" data-location="" data-position=""></div>
				<?php get_template_part('template-parts/general/most-popular'); ?>
				<div id="halfpage-2" class="ads halfpage" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="Middle" data-refad="pebbleMiddle" data-location="" data-position="2"></div>
			</aside> 
			<!-- /sidebar -->
			<div class="navigation">
				<?php pressPagination('',3); ?>
			</div>
		</div>

		<?php if($paged == 1 && $display_slider == 1){ ?> 
		<div class="swiper-article-ajax">
			<span class="loading"><?php _e( 'Chargement en cours', 'html5blank' ); ?>...</span>
		</div> 
		<?php } ?>
	</div>
</main>
<?php get_footer(); ?>