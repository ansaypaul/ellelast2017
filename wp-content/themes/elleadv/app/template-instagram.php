<?php
/*
Template Name: Instagram
*/

?>

<!DOCTYPE html>
<html>
  <head>
    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://rawgit.com/atmist/snazzy-info-window/master/dist/snazzy-info-window.min.js"></script>
    <script>
      function initMap() {
        var centreCarte = new google.maps.LatLng(50.863103,4.3973633)

        var uluru = {lat: 50.863103, lng: 4.3973633};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: uluru,
          disableDefaultUI: true,
          scaleControl: false,
		  scrollwheel: false
        });
        var image = 'http://www.elle.be/img/marker.png';
        var beachMarker = new google.maps.Marker({
          position: {lat: 50.863103, lng: 4.3973633},
          map: map,
          icon: image
        });

       var contenuInfoBulle = 
       		   '<div id="conteneurInfoBulle">' +
               '<h1>Love X ELLE</h1>' +
               '<div id="haut">'+
               '<div id="texteInfoBulle">' +
               '<p>Boulevard Général Wahis 16/F, 1030 Schaerbeek</p>' +
               '</div>' +
               '</div> ';

	      var infoBulle = new google.maps.InfoWindow({
	          content: contenuInfoBulle
	     })



		infoBulle.open(map, beachMarker);

      }


    $(function() {
    // Snazzy Map Style

    // Add a Snazzy Info Window to the marker
    var info = new SnazzyInfoWindow({
        marker: marker,
        placement: 'right',
        offset: {
            left: '20px'
        },
        content: '<div>STYLING</div>' +
                 '<div>WITH</div>' +
                 '<div><strong>JAVASCRIPT</strong></div>',
        showCloseButton: false,
        closeOnMapClick: false,
        padding: '48px',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        border: false,
        borderRadius: '0px',
        shadow: false,
        fontColor: '#fff',
        fontSize: '15px'
    });
    info.open();
});


    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAax4X0w-SplnzNSts5ZmJ4xi-ooSliBfk&callback=initMap">
    </script>
  </body>
</html>