<?php 

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5de5296827707',
	'title' => 'GOOGLE DV360',
	'fields' => array (
		array (
			'key' => 'field_5de52a20c327a',
			'label' => 'TAG DV ACCOUNT',
			'name' => 'tag_dv_account',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_5de52a3a765f2',
			'label' => 'TAG SITE',
			'name' => 'tag_dv_site',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_5de52a4bae5fa',
			'label' => 'TAG CATEGORY',
			'name' => 'tag_dv_category',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_5de52a3a76748',
			'label' => 'Active Takeover',
			'name' => 'activeTakeover',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'message' => '',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array (
			'key' => 'field_5de52a3a76521',
			'label' => 'Active DEBUG',
			'name' => 'activeDebug',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'message' => '',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'category',
			),
		),
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}


function generate_format_desktop($format){
	switch ($format){
		case 'leaderboard': 
		echo '[[970, 250], [960, 150], [970, 90], [728, 90], [840,150]]';
		break;
		default:
        echo "";
	}
}

function generate_format_mobile($format){
	switch ($format){
		case 'leaderboard': 
		echo '[[300,50],[300,75],[300,250],[640,150]]';
		break;
		default:
        echo "";
	}
}

function dfp_script() {
	$tag_dv_site = get_field('tag_dv_site','option');
	$tag_dv_category = get_field('tag_dv_category','option');
	$activeTakeover = get_field('activeTakeover','option');
	$activeDebug = get_field('activeDebug','option');

	if( current_user_can('editor') || current_user_can('administrator') ) {
			$is_user_login = 1;
	}else{
			$is_user_login = 0;
	}

	if ( is_category() ) {
		$cat_id = get_query_var( 'cat' );
		$tag_dv_category = get_field("tag_dv_category", 'category_'.$cat_id);
	}

	if ( is_single() ) {
		$cat_id_array = get_the_category();
		foreach ($cat_id_array as $cat_objet) {
			if(get_field("tag_dv_category", 'category_'.$cat_objet->term_id)) { $tag_dv_category = get_field("tag_dv_category", 'category_'.$cat_objet->term_id); }
		}
	}

	wp_add_inline_script( 
			'jquery', '
			console.log("Init DV 360 VAR");
			var dvSiteID = "'.$tag_dv_site.'";
			var dvEmplacementID = "'.$tag_dv_category.'";
			var activeTakeover="'.$activeTakeover.'"
			var is_user_login="'.$is_user_login.'"
			var activeDebugDV360="'.$activeDebug.'"
			' );
	wp_enqueue_script( 'jqueryvisible', get_template_directory_uri() . '/ad360/jquery.visible.js', array( 'jquery' ),'',false);  
	wp_enqueue_script('gpt', 'https://securepubads.g.doubleclick.net/tag/js/gpt.js', array('jquery'),'',false); // Custom scripts
	//wp_enqueue_script('dfpads', get_template_directory_uri() . '/ad360/dfp_ads.js', array('jquery'),'',true); // Custom scripts
	wp_enqueue_script('dfpads_r', get_template_directory_uri() . '/ad360/dfp_ads_r.js', array('jquery'),'',true); // Custom scripts
}

add_action( 'wp_enqueue_scripts', 'dfp_script' );
