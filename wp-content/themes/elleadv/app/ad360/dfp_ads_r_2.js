RmgGoogleAds = function () {
	this.slotCode = '';
	this.placeholderIDs = [];
	this.mapping = [];
	this.slots = [];
};

RmgGoogleAds.prototype.createAd = function(adType) {
	var adTypeElements = document.querySelectorAll('.rmgAd[data-type="' + adType + '"]');
	var adTypeCount = adTypeElements.length;
	var adTypePlaceholderIDs = [];
    
	if (adTypeCount > 0) {
		if (!rmgGoogleAds.mapping[adType]) {
			rmgGoogleAds.mapping[adType] = googletag.sizeMapping();
			for (var i = 0; i < rmgGoogleAds.config[adType]['sizeSet'].length; i++) {
				rmgGoogleAds.mapping[adType] = rmgGoogleAds.mapping[adType].addSize(rmgGoogleAds.config[adType]['sizeSet'][i].breakpoint, rmgGoogleAds.config[adType]['sizeSet'][i].sizes)
			}
			rmgGoogleAds.mapping[adType] = rmgGoogleAds.mapping[adType].build();
		}

		if (adTypeCount > 1) {
			for (var j = 0; j < adTypeCount; j++) {
				if (!document.querySelectorAll('.rmgAd[data-type=' + adType + ']')[j].hasAttribute('requested')) {
					adTypeElements[j].id = 'gpt-ad-' + adType + '_' + j;
					adTypePlaceholderIDs.push(adTypeElements[j].id);
				}
			}
		} else {
			adTypeElements[0].id = 'gpt-ad-' + adType;
			adTypePlaceholderIDs.push('gpt-ad-' + adType);
		}
	}
	rmgGoogleAds.defineSlot(adType, rmgGoogleAds.mapping[adType], adTypePlaceholderIDs);
};

RmgGoogleAds.prototype.defineSlot = function(adType, mapping, placeholders) {
	for (var i = 0; i < placeholders.length; i++) {
		var showAd = false;
		var device = document.getElementById(placeholders[i]).getAttribute('data-device');

        /*
        ga('send', 'event', 'adsTypes', 'display', '');
        ga('newTracker.send', 'event', 'adsTypes', 'display', '');
        */


		if (device !== 'all') {
			if (window.innerWidth >= rmgGoogleAds.devices[device][0] && window.innerWidth < rmgGoogleAds.devices[device][1]) {
				showAd = true
			}
		} else {
			showAd = true;
		}

		if (showAd) {

            console.log('ID: '+placeholders[i] + ' / Device : ' + device);

			rmgGoogleAds.placeholderIDs.push(placeholders[i]);
			rmgGoogleAds.slots[placeholders[i] + '_slot'] = googletag.defineSlot(rmgGoogleAds.slotCode, [], placeholders[i])
				.defineSizeMapping(mapping)
				.addService(googletag.pubads())
				.setTargeting('AdType', [adType]);
		}

		document.getElementById(placeholders[i]).setAttribute('requested', '');
	}
};

var rmgGoogleAds = new RmgGoogleAds();

rmgGoogleAds.slotCode="/21768925892/"+dvSiteID+"/"+dvEmplacementID;

rmgGoogleAds.devices= {
    "mobile": [0, 727], "tablet": [728, 1199], "desktop": [728, 10000], "desktop_only": [1200, 10000]
};

window.googletag=window.googletag || {
    cmd: []
};

googletag.cmd.push(function () {
    //initPageTargeting();
});

function addTakeover(){
	    // add a div containing the dynamic ad
	    elem = $('body');
	    var container = document.createElement('div');
	    container.id = 'slot-takeover';
	    elem.append(container);

		googletag.cmd.push(function() {
				var slot = googletag.defineOutOfPageSlot('/21768925892/'+dvSiteID+'/'+dvEmplacementID,container.id).addService(googletag.pubads());
				googletag.cmd.push(function() { googletag.display(container.id); googletag.pubads().refresh([slot]); });
		});

	    console.log('Slot : Takeover');
};

document.addEventListener("DOMContentLoaded", function() {
	//addTakeover();
    googletag.cmd.push(function () {
        //setPersonalisedAds();
        googletag.pubads().setCentering(true);
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
        googletag.defineOutOfPageSlot(rmgGoogleAds.slotCode, "gpt-ad-OUT_OF_PAGE") .addService(googletag.pubads()) .setTargeting("AdType", ["FLOORAD", "BANNER_ABOVE"]);
        rmgGoogleAds.config= {
            "BANNER": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 75]]
                }
                , {
                    "breakpoint": [980, 0], "sizes": [[980, 150], [960, 150], [970, 250], [840, 150], [840, 250], [728, 90]]
                }
                ], "targeting":[], "widget":false, "td_composer":true
            }
            ,
            "BANNER_MIDDLE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 75]]
                }
                , {
                    "breakpoint": [980, 0], "sizes": [[980, 150], [960, 150], [970, 250], [840, 150], [840, 250], [728, 90]]
                }
                ], "targeting":[], "widget":false, "td_composer":true
            }
            , "BANNER_ABOVE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 75]]
                }
                , {
                    "breakpoint": [980, 0], "sizes": [[980, 150], [960, 150], [970, 250], [840, 150], [840, 250], [728, 90]]
                }
                ], "widget":true, "td_composer":true
            }
            , "BANNER_BELOW": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 75]]
                }
                , {
                    "breakpoint": [980, 0], "sizes": [[960, 150], [970, 250], [840, 150], [840, 250], [728, 90]]
                }
                ], "widget":true, "td_composer":true
            }
            , "RECT": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 250]]
                }
                , {
                    "breakpoint": [640, 0], "sizes": [[300, 250]]
                }
                ], "targeting":[], "widget":false, "td_composer":true
            }
            , "RECT_ABOVE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 250]]
                }
                , {
                    "breakpoint": [640, 0], "sizes": [[300, 600], [300, 250]]
                }
                ], "widget":true, "td_composer":true
            }
            , "RECT_MIDDLE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 250]]
                }
                ], "widget":true, "td_composer":true
            }
            , "RECT_MIDDLE2": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 250]]
                }
                ], "widget":true, "td_composer":true
            }
            , "RECT_BELOW": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 250]]
                }
                ], "widget":true, "td_composer":true
            }
            , "HALFRECT_ABOVE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 125]]
                }
                ], "widget":true, "td_composer":false
            }
            , "HALFRECT_BELOW": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 125]]
                }
                ], "widget":true, "td_composer":false
            }
            , "INPAGE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[300, 300], [650, 250]]
                }
                , {
                    "breakpoint": [980, 0], "sizes": [[640, 480], [800, 300], [620, 620], [650, 250]]
                }
                ], "widget":false, "td_composer":true
            }
            , "SKY_RIGHT": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": [[600, 100]]
                }
                , {
                    "breakpoint": [1150, 0], "sizes": [[120, 600], [160, 600]]
                }
                ], "widget":false, "td_composer":false
            }
            , "NATIVE": {
                "sizeSet":[ {
                    "breakpoint": [320, 0], "sizes": "fluid"
                }
                ], "widget":true, "td_composer":true
            }
        };

        rmgGoogleAds.createAd("BANNER_ABOVE");
        rmgGoogleAds.createAd("RECT_ABOVE");
       	rmgGoogleAds.createAd("BANNER");
        rmgGoogleAds.createAd("BANNER_MIDDLE");
        rmgGoogleAds.createAd("BANNER_BELOW");
        rmgGoogleAds.createAd("RECT");
        rmgGoogleAds.createAd("RECT_MIDDLE");
        rmgGoogleAds.createAd("RECT_MIDDLE2");
        rmgGoogleAds.createAd("RECT_BELOW");
        //rmgGoogleAds.createAd("HALFRECT_ABOVE");
        //rmgGoogleAds.createAd("HALFRECT_BELOW");
        rmgGoogleAds.createAd("INPAGE");
        rmgGoogleAds.createAd("SKY_RIGHT");
        //rmgGoogleAds.createAd("NATIVE");

          // Run this in an interval (every 0.1s) just in case we are still waiting for consent
          var cnt = 0;
          var consentSetInterval = setInterval(function(){
            cnt += 1;
            // Bail if we have not gotten a consent response after 60 seconds.
            if( cnt === 600 )
              clearInterval(consentSetInterval);
            if( typeof window.__tcfapi !== 'undefined' ) { // Check if window.__tcfapi has been set
              console.log('window.__tcfapi has  been set');
              clearInterval( consentSetInterval );
              window.__tcfapi( 'addEventListener', 2, function( tcData,listenerSuccess ) {
                if ( listenerSuccess ) {
                  if( tcData.eventStatus === 'tcloaded' || tcData.eventStatus === 'useractioncomplete' ) {
                    if ( ! tcData.gdprApplies ) {
                    console.log('GDPR DOES NOT APPLY');
                      // GDPR DOES NOT APPLY
                      // Insert adsbygoogle.js onto the page.
                        for (var p=0; p < rmgGoogleAds.placeholderIDs.length; p++) {
                                console.log("Placement :" + rmgGoogleAds.placeholderIDs[p]);
                                googletag.display(rmgGoogleAds.placeholderIDs[p]);
                        }

                        googletag.display("gpt-ad-OUT_OF_PAGE");
                    }
                    else {
                    console.log('GDPR DOES APPLY');
                      // GDPR DOES APPLY
                      // Purpose 1 refers to the storage and/or access of information on a device.
                      var hasDeviceStorageAndAccessConsent = tcData.purpose.consents[1] || false;
                      // Google Requires Consent for Purpose 1
                      if (hasDeviceStorageAndAccessConsent) {
                        // GLOBAL VENDOR LIST - https://iabeurope.eu/vendor-list-tcf-v2-0/
                        // CHECK FOR GOOGLE ADVERTISING PRODUCTS CONSENT. (IAB Vendor ID 755)
                        var hasGoogleAdvertisingProductsConsent = tcData.vendor.consents[755] || false;
                        // Check if the user gave Google Advertising Products consent (iab vendor 755)
                        if(hasGoogleAdvertisingProductsConsent) {
                          // Insert adsbygoogle.js onto the page.
                          //insertAdsByGoogleJs();
                            for (var p=0; p < rmgGoogleAds.placeholderIDs.length; p++) {
                                console.log("Placement :" + rmgGoogleAds.placeholderIDs[p]);
                                googletag.display(rmgGoogleAds.placeholderIDs[p]);
                            }

                            googletag.display("gpt-ad-OUT_OF_PAGE");
                        }else{
                          console.log('CONTENT ADS NO OKAI');
                        }
                      }else{
                        console.log('CONTENT ADS NO OKAI');
                      }
                    }
                  }
                }
              } );
            }else{
                console.log('window.__tcfapi has not been set');
            }
            cnt++;
          },100);

    });

	googletag.cmd.push(function () {
    	googletag.pubads().addEventListener('slotRenderEnded', function(event) {

			console.log(event);
			console.log('Creative with id: ' + event.creativeId + ' is rendered to slot of size: ' + event.size[0] + 'x' + event.size[1]);

            ga('send', 'event', 'ads', 'display', event.size[0]+'x'+event.size[1]);
            ga('newTracker.send', 'event', 'ads', 'display', event.size[0]+'x'+event.size[1]);

            if(activeTakeover == '1'){ 
                console.log('activeTakeover : ' + activeTakeover);
                if($('#gpt-ad-OUT_OF_PAGE').css('display') != 'none'){
                    $('body').addClass('takeover');

                    /** HOT FIX MARIJO 26/02/2020 **/
                    $('.site-inner').css('max-width','970px');
                    $('.entry-header').css('max-width','970px');
                    $('.entry-header').css('top','5px');
                    $('.main-single').css('margin','0 auto');
                    $('.main-single').css('max-width','970px');
                    $('.main-index').css('max-width','970px');
                    $('.main-index').css('margin','0 auto');

                    if(event.size[0] == '1' && event.size[1] == '1') {
                        console.log('Slot : Takeover Display'); 
                        //ga('send', 'event', 'ads', 'display', 'takeover');
                        //ga('newTracker.send', 'event', 'ads', 'display', 'takeover');
                    }
                }
            }
		});
    });

    /** INIT TEST SCROLL **/ 
    

    window.addEventListener('scroll', function() {


    	var scroll = jQuery(window).scrollTop();

        //>=, not <=
        if (scroll >= 400) {
            //clearHeader, not clearheader - caps H
            jQuery("#gpt-ad-SKY_RIGHT").removeClass("u-visually-hidden");
        }else{
            jQuery("#gpt-ad-SKY_RIGHT").addClass("u-visually-hidden");
        }
		/*
	 	rmgGoogleAds.createAd("RECT");
		
		$('.rmgAd').each(function(){ 
			if($(this).visible(true) && !$(this)[0].hasAttribute("data-google-query-id")){
				//$(this).css("border", "1px solid purple");
				console.log('visible :' + $(this).attr('id'));
				googletag.display($(this).attr('id'));
			}else{

				}
		});
		*/

	});

/** DOCUMENT LOAD **/
});