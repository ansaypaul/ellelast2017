/* footer-script.js */
(function ($, root, undefined) {

window.googletag = window.googletag || {cmd: []};

	googletag.cmd.push(function() {
	//googletag.pubads().disableInitialLoad();
	googletag.pubads().collapseEmptyDivs();
	googletag.pubads().enableSingleRequest();
	googletag.pubads().setCentering(true);
	googletag.enableServices();

});

var slotId = 1;

function addNewSlot(elem){
    // add a div containing the dynamic ad
    elem.empty();
    var container = document.createElement('div');
    container.id = 'slot-' + slotId;
    elem.append(container);
    elem.addClass("loaded");
    
	var tempsEnMs = Date.now();
	var ads_number = elem.attr('ads_number');

	if(typeof ads_number !== typeof undefined && ads_number !== false) {}
	else{
		ads_number = 1;
		elem.attr("ads_number",1);
	}

	elem.attr("loaded",true);	
	elem.attr("dateload",tempsEnMs);
	elem.attr("viewport_time",1);

	if(window.innerWidth > 955 && elem.data( "format" ) != '' ){
		/* DESKTOP */
		console.log(elem.attr("id") + 'LOADING ADS DESK format : ' + elem.data( "format" ) + ' / adtypes : ' + elem.data( "adtypes" ));
		googletag.cmd.push(function() {
		var slot = googletag.defineSlot('/21768925892/'+dvSiteID+'/'+dvEmplacementID, elem.data( "format" ), container.id).addService(googletag.pubads()).setTargeting("AdType", elem.data( "adtypes" ));
		googletag.cmd.push(function() { /*googletag.display(container.id);*/ googletag.pubads().refresh([slot]); });
		});
	}else if(window.innerWidth <= 955 && elem.data( "formatmob" ) != '' ){
		/* MOBILE */
		console.log(elem.attr("id") + 'LOADING ADS MOB formatmob : ' + elem.data( "formatmob" ) + ' / adtypes : ' + elem.data( "adtypes" ));
		googletag.cmd.push(function() {
		var slot = googletag.defineSlot('/21768925892/'+dvSiteID+'/'+dvEmplacementID, elem.data( "formatmob" ), container.id).addService(googletag.pubads()).setTargeting("AdType", elem.data( "adtypes" ));
		googletag.cmd.push(function() { /*googletag.display(container.id);*/ googletag.pubads().refresh([slot]); });
		});
	}else{
		console.log(elem.attr("id") + 'NO MOB NO DESK')
	}

    slotId++;
    console.log(slotId);
};


function addTakeover(){
	    // add a div containing the dynamic ad
	    elem = $('body');
	    var container = document.createElement('div');
	    container.id = 'slot-takeover';
	    elem.append(container);

		googletag.cmd.push(function() {
				var slot = googletag.defineOutOfPageSlot('/21768925892/'+dvSiteID+'/'+dvEmplacementID,container.id).addService(googletag.pubads());
				googletag.cmd.push(function() { googletag.display(container.id); googletag.pubads().refresh([slot]); });
		});

	    console.log('Slot : Takeover');
};

function refreshVisibleAdSlot(elem){
	var time = elem.attr('viewport_time');
	var ads_number = elem.attr('ads_number');
	time = parseInt(time) + 1;
	elem.attr('viewport_time',time);
	if(time > 60 && ads_number <= 3){ 
		ads_number = parseInt(ads_number) + 1;
		elem.attr('ads_number',ads_number); 
		elem.attr('viewport_time',1);
		elem.removeClass("loaded");
	}
}

function visibleAdSlot(){
	$('.ads').each(function(){ 
		if($(this).visible(true) && !$(this).hasClass("loaded")){
			if($(this).data("format") != '' && $(this).data("formatmob") != '' ){
				if(is_user_login == 1 && activeDebugDV360 == 1){$(this).css("border", "1px solid purple");}
				addNewSlot($(this));
			}else{
					if($(this).data("format") != ''){
						if(is_user_login == 1 && activeDebugDV360 == 1){$(this).css("border", "1px solid red")};
						addNewSlot($(this));
					}else if($(this).data("formatMOB") != ''){
						if(is_user_login == 1 && activeDebugDV360 == 1){$(this).css("border", "1px solid blue")};
						addNewSlot($(this));
					}
			}
		}else if( $(this).visible(true) && $(this).hasClass("loaded")){
				//refreshVisibleAdSlot($(this));
		}
		else{

		}
	});
}

$( document ).ready(function() {

	console.log('window.innerWidth :'+ window.innerWidth);
	console.log('INIT DFP TAG');

	if(activeTakeover == '1'){ addTakeover(); }

	visibleAdSlot();


	googletag.cmd.push(function() {
		googletag.pubads().addEventListener('slotRenderEnded', function(event) {
			if(is_user_login == 1 && activeDebugDV360 == 1){
				console.log(event);
				console.log('Creative with id: ' + event.creativeId +
				  ' is rendered to slot of size: ' + event.size[0] + 'x' + event.size[1]);
				//if(event.size[0] == '1' && event.size[1] == '1'){ $('body').addClass('takeover'); }
			}
			
			ga('send', 'event', 'ads', 'display', event.size[0]+'x'+event.size[1]);
			ga('newTracker.send', 'event', 'ads', 'display', event.size[0]+'x'+event.size[1]);

			if(activeTakeover == '1'){ 
				if($('#slot-takeover').css('display') != 'none'){
					$('body').addClass('takeover');

					/** HOT FIX MARIJO 26/02/2020 **/
					$('.site-inner').css('max-width','970px');
					$('.entry-header').css('max-width','970px');
					$('.entry-header').css('top','5px');
					$('.main-single').css('margin','0 auto');
					$('.main-single').css('max-width','970px');
					$('.main-index').css('max-width','970px');
					$('.main-index').css('margin','0 auto');

					if(event.size[0] == '1' && event.size[1] == '1') {
						console.log('Slot : Takeover Display'); 
						//ga('send', 'event', 'ads', 'display', 'takeover');
						//ga('newTracker.send', 'event', 'ads', 'display', 'takeover');
					}

				}
			}
			//if(!$('#slot-takeover').is(':empty')){console.log('TAKEOVER LOADED')}; 
		});
	});
  	/*
	  	window.setInterval(function(){
			visibleAdSlot();
		}, 500 )
	*/

    console.log('Ready dfpads.js : '  + $.now() );
    
	$(document).scroll(function(){
		visibleAdSlot();
	});		

});

})(jQuery, this);