
// Initialize the rmgParamObject
var rmgParamObject = {};

RmgGoogleAds = function () {
    // Variables
    this.slotCode = '';
    this.adPlaceholders = {};
    this.slots = {};
    this.sizes = {};
    this.sizeLabels = {};
    this.devices = {
        'mobile': [0, 639],
        'mobile_tablet': [0, 979],
        'tablet': [640, 979],
        'tablet_desktop': [640, 10000],
        'desktop': [980, 10000]
    };
    this.renderBlockers = [
        'consent',
        'modal',
        'moat',
        'prebid'
    ];
};

RmgGoogleAds.prototype.toggleAdWhenAdOutOfViewport = function(adPlaceholderID, triggerAdType) {
    var triggerEl = document.querySelector('.rmgAd[data-type="' + triggerAdType + '"]');
    var placeholderEl = document.querySelector('.rmgAd[data-type="' + rmgGoogleAds.adPlaceholders[adPlaceholderID].adType + '"]');

    placeholderEl.classList.add('u-visually-hidden');

    /*
    rmgAddMultiEventListener(document, 'DOMContentLoaded scroll', function() {
        if (!rmgParamObject.isElementInViewport(triggerEl.children[0]) && !triggerEl.children[0].classList.contains('u-sticky')) {
            placeholderEl.classList.remove('u-visually-hidden');
        } else {
            placeholderEl.classList.add('u-visually-hidden');
        }
    });
    */
};

RmgGoogleAds.prototype.resizeRefresh = function() {
    var currentScreen;
    var resizedScreen;

    if (window.innerWidth <= 640) {
        currentScreen = 'mobile';
    } else if (window.innerWidth < 980 && window.innerWidth > 640) {
        currentScreen = 'tablet';
    } else if (window.innerWidth < 1150 && window.innerWidth > 980) {
        currentScreen = 'desktop-S';
    } else {
        currentScreen = 'desktop-L'
    }

    /*
    rmgAddMultiEventListener(window, 'resize orientationchanged', function() {
        if (window.innerWidth <= 640) {
            resizedScreen = 'mobile';
        } else if (window.innerWidth < 980 && window.innerWidth > 640) {
            resizedScreen = 'tablet';
        } else if (window.innerWidth < 1150 && window.innerWidth > 980) {
            resizedScreen = 'desktop-S';
        } else {
            resizedScreen = 'desktop-L';
        }

        if (currentScreen != resizedScreen) {
            rmgGoogleAds.refreshAds();
            currentScreen = resizedScreen;
        }
    });
    */

}

RmgGoogleAds.prototype.pushAdPlaceholder = function(adType, adTypePlaceholders, mapping, bidders) {
    for (var i = 0; i < adTypePlaceholders.length; i++) {
        var showAd = false;
        console.log('adTypePlaceholders : ' + adTypePlaceholders);
        var device = document.getElementById(adTypePlaceholders[i]).getAttribute('data-device') ? document.getElementById(adTypePlaceholders[i]).getAttribute('data-device') : 'all';

        if (device !== 'all') {
            if (window.innerWidth >= rmgGoogleAds.devices[device][0] && window.innerWidth < rmgGoogleAds.devices[device][1]) {
                showAd = true
            }
        } else {
            showAd = true;
        }

        if (showAd) {
            rmgGoogleAds.adPlaceholders[adTypePlaceholders[i]] = {
                'adType': adType,
                'mapping': mapping,
                'bidders': bidders
            };
        }
    }
};

RmgGoogleAds.prototype.defineSlot = function(adPlaceholderID) {

    console.log(rmgGoogleAds);

    if (!rmgGoogleAds.slots[adPlaceholderID + '_slot']) {

        /*
        console.log('adPlaceholderID : ' + adPlaceholderID);
        console.log(rmgGoogleAds.sizes[rmgGoogleAds.adPlaceholders[adPlaceholderID].adType]);
        console.log(rmgGoogleAds.sizes);
        */

        rmgGoogleAds.slots[adPlaceholderID + '_slot'] = googletag.defineSlot(rmgGoogleAds.slotCode, rmgGoogleAds.sizes[rmgGoogleAds.adPlaceholders[adPlaceholderID].adType], adPlaceholderID)
            .defineSizeMapping(rmgGoogleAds.adPlaceholders[adPlaceholderID].mapping)
            .addService(googletag.pubads())
            .setTargeting('AdType', [rmgGoogleAds.adPlaceholders[adPlaceholderID].adType]);

        /*
        rmgGoogleAds.slots[adPlaceholderID + '_slot'] = googletag.defineSlot(rmgGoogleAds.slotCode, [], adPlaceholderID)
        .defineSizeMapping(rmgGoogleAds.adPlaceholders[adPlaceholderID].mapping)
        .addService(googletag.pubads())
        .setTargeting('AdType', [rmgGoogleAds.adPlaceholders[adPlaceholderID].adType]);
        */

        document.getElementById(adPlaceholderID).setAttribute('requested', '');
    }

};

RmgGoogleAds.prototype.renderAds = function() {
    var renderInterval = setInterval(function() {

        /*
        if (rmgGoogleAds.renderBlockers.length === 0) {
            googletag.display('gpt-ad-OUT_OF_PAGE');
            for (var p in rmgGoogleAds.adPlaceholders) {
                googletag.display(p);
                document.getElementById(p).removeAttribute('style'); // Remove inline styling from Google (breaks layout sometimes)
            }
            clearInterval(renderInterval);
        }
        */


           if( typeof window.__tcfapi !== 'undefined' ) { // Check if window.__tcfapi has been set
              console.log('window.__tcfapi has  been set');
              window.__tcfapi( 'addEventListener', 2, function( tcData,listenerSuccess ) {
                if ( listenerSuccess ) {
                  if( tcData.eventStatus === 'tcloaded' || tcData.eventStatus === 'useractioncomplete' ) {
                    if ( ! tcData.gdprApplies ) {
                    console.log('GDPR DOES NOT APPLY');
                      // GDPR DOES NOT APPLY
                      // Insert adsbygoogle.js onto the page.
                        for (var p=0; p < rmgGoogleAds.placeholderIDs.length; p++) {
                                console.log("Placement :" + rmgGoogleAds.placeholderIDs[p]);
                                googletag.display(rmgGoogleAds.placeholderIDs[p]);
                        }

                        googletag.display("gpt-ad-OUT_OF_PAGE");
                    }
                    else {
                    console.log('GDPR DOES APPLY');
                      // GDPR DOES APPLY
                      // Purpose 1 refers to the storage and/or access of information on a device.
                      var hasDeviceStorageAndAccessConsent = tcData.purpose.consents[1] || false;
                      // Google Requires Consent for Purpose 1
                      if (hasDeviceStorageAndAccessConsent) {
                        // GLOBAL VENDOR LIST - https://iabeurope.eu/vendor-list-tcf-v2-0/
                        // CHECK FOR GOOGLE ADVERTISING PRODUCTS CONSENT. (IAB Vendor ID 755)
                        var hasGoogleAdvertisingProductsConsent = tcData.vendor.consents[755] || false;
                        // Check if the user gave Google Advertising Products consent (iab vendor 755)
                        if(hasGoogleAdvertisingProductsConsent) {
                          // Insert adsbygoogle.js onto the page.
                            googletag.display('gpt-ad-OUT_OF_PAGE');
                            for (var p in rmgGoogleAds.adPlaceholders) {
                                googletag.display(p);
                                document.getElementById(p).removeAttribute('style'); // Remove inline styling from Google (breaks layout sometimes)
                            }
                            clearInterval(renderInterval);
                            googletag.display("gpt-ad-OUT_OF_PAGE");

                        }else{
                          console.log('CONTENT ADS NO OKAI');
                        }
                      }else{
                        console.log('CONTENT ADS NO OKAI');
                      }
                    }
                  }
                }
              } );
            }else{
                console.log('window.__tcfapi has not been set');
            }

    }, 100);
};




RmgGoogleAds.prototype.refreshAds = function() {
    if (rmgGoogleAds.renderBlockers.length === 0) {
        if (document.getElementById('OutOfPage')) {
            document.getElementById('OutOfPage').parentNode.removeChild(document.getElementById('OutOfPage')); // Remove OutOfPage generated html
        }
        googletag.pubads().refresh();
    }
}

RmgGoogleAds.prototype.showNativesContainer = function() {
    if (document.querySelector('.js-natives')) {
        document.querySelector('.js-natives').classList.remove('u-visually-hidden');
    }
};

RmgGoogleAds.prototype.setStickyAdOnScroll = function(adType, duration, limitEl) {
    var adPlaceholder = document.querySelector('.rmgAd[data-type="' + adType + '"]');
    var adContainer = document.querySelector('.rmgAd[data-type="' + adType + '"] > div');
    var ad = document.querySelector('.rmgAd[data-type="' + adType + '"] > div > iframe');
    var lastScrollTop = window.pageYOffset;

    var setStickyAd = function() {
        var st = window.pageYOffset;

        adPlaceholder.style.height = ad.clientHeight + 'px'; // prevents jumping page

        if (limitEl && limitEl.getBoundingClientRect().top < 0 && !adContainer.classList.contains('u-absolute')) {
            // check if limitEl is already scrolled past
            // the ad will 'float' above the limitEl
            adContainer.classList.add('u-absolute');
            adContainer.style.top = limitEl.getBoundingClientRect().top - adPlaceholder.getBoundingClientRect().top - ad.clientHeight - 30 + 'px';
            return;
        }

        if (st > lastScrollTop){
            // downscroll code
            if (adPlaceholder.getBoundingClientRect().top <= 0 && !adContainer.classList.contains('u-absolute')) {
                // if adPlaceholder hits top of viewport
                adContainer.classList.add('u-sticky');
                if (duration) {
                    setTimeout(function () {
                        removeEventListener();
                    }, duration);
                }
            }

            
            if (limitEl) {
                if (rmgParamObject.collisionDetect(adContainer, limitEl)) {
                    // check if ad collides with limitEl
                    // the ad will 'float' above the limitEl
                    adContainer.classList.remove('u-sticky');
                    adContainer.classList.add('u-absolute');
                    adContainer.style.top = limitEl.getBoundingClientRect().top - adPlaceholder.getBoundingClientRect().top - ad.clientHeight - 30 + 'px';
                }
            }
            
        } else if (st < lastScrollTop) {
            // upscroll code
            if (adContainer.getBoundingClientRect().top >= 0 && adContainer.classList.contains('u-absolute')) {
                // if adContainer hits top of viewport
                adContainer.classList.remove('u-absolute');
                adContainer.classList.add('u-sticky');
                if (duration) {
                    setTimeout(function () {
                        removeEventListener();
                    }, duration);
                }
            }

            if (adPlaceholder.getBoundingClientRect().top >= 0 && adContainer.classList.contains('u-sticky')) {
                // if adPlaceholder hits top of viewport
                adContainer.classList.remove('u-sticky');
                adContainer.style.top = '0px';
            }
        }

        lastScrollTop = st <= 0 ? 0 : st;
    };

    var removeEventListener = function() {
        adContainer.classList.remove('u-sticky');
        adContainer.classList.remove('u-absolute');
        adContainer.style.top = '0px';
        document.removeEventListener('scroll', setStickyAd);
    };

    document.addEventListener('scroll', setStickyAd);
};

RmgGoogleAds.prototype.addMoatYield = function() {
    var timeout = setTimeout(function() {
        window['moatYieldReady'] = false;
       //rmgParamObject.removeValueFromArray(rmgGoogleAds.renderBlockers, 'moat');
    }, 2000);

    window['moatYieldReady'] = function () {
        clearTimeout(timeout);
        moatPrebidApi.setMoatTargetingForAllSlots();
        //rmgParamObject.removeValueFromArray(rmgGoogleAds.renderBlockers, 'moat');
    }
};

RmgGoogleAds.prototype.slotRenderedLogic = function(e) {
    if (!e.isEmpty) {
        if (e.slot.getSlotElementId() === 'gpt-ad-NATIVE_0') {
            rmgGoogleAds.showNativesContainer(e);
        } else if (e.slot.getSlotElementId() === 'gpt-ad-BANNER_ABOVE') {
            rmgGoogleAds.setStickyAdOnScroll('BANNER_ABOVE', 2000, document.getElementById('gpt-ad-RECT_ABOVE'));
        }

        // Needed for properly layouting the sidebar on section pages
        if (typeof setFullWidthTeasers === 'function') {
            setFullWidthTeasers();
        }

        document.getElementById(e.slot.getSlotElementId()).style.display = '';
    } else {
        document.getElementById(e.slot.getSlotElementId()).style.display = 'none';
    }
};

var rmgGoogleAds = new RmgGoogleAds();


// Create sizes arrays (by adtype + by viewport)
rmgGoogleAds.sizes['BANNER_ABOVE'] = [];
rmgGoogleAds.sizeLabels['BANNER_ABOVE'] = [];rmgGoogleAds.sizes['BANNER_ABOVE'] = rmgGoogleAds.sizes['BANNER_ABOVE'].concat([[300,75]]);
rmgGoogleAds.sizes['to-viewport-9'] = [[300,75]];
rmgGoogleAds.sizeLabels['BANNER_ABOVE'] = rmgGoogleAds.sizeLabels['BANNER_ABOVE'].concat("to-viewport-9");rmgGoogleAds.sizes['BANNER_ABOVE'] = rmgGoogleAds.sizes['BANNER_ABOVE'].concat([[980,150],[960,150],[970,250],[840,150],[840,250],[728,90]]);
rmgGoogleAds.sizes['from-viewport-9'] = [[980,150],[960,150],[970,250],[840,150],[840,250],[728,90]];
rmgGoogleAds.sizeLabels['BANNER_ABOVE'] = rmgGoogleAds.sizeLabels['BANNER_ABOVE'].concat("from-viewport-9");rmgGoogleAds.sizes['BANNER_BELOW'] = [];

rmgGoogleAds.sizeLabels['BANNER_BELOW'] = [];
rmgGoogleAds.sizes['BANNER_BELOW'] = rmgGoogleAds.sizes['BANNER_BELOW'].concat([[300,75]]);
rmgGoogleAds.sizes['to-viewport-9'] = [[300,75]];
rmgGoogleAds.sizeLabels['BANNER_BELOW'] = rmgGoogleAds.sizeLabels['BANNER_BELOW'].concat("to-viewport-9");
rmgGoogleAds.sizes['BANNER_BELOW'] = rmgGoogleAds.sizes['BANNER_BELOW'].concat([[960,150],[970,250],[840,150],[840,250],[728,90]]);
rmgGoogleAds.sizes['from-viewport-9'] = [[960,150],[970,250],[840,150],[840,250],[728,90]];
rmgGoogleAds.sizeLabels['BANNER_BELOW'] = rmgGoogleAds.sizeLabels['BANNER_BELOW'].concat("from-viewport-9");

rmgGoogleAds.sizes['BANNER_MIDDLE'] = [];
rmgGoogleAds.sizeLabels['BANNER_MIDDLE'] = [];
rmgGoogleAds.sizes['BANNER_MIDDLE'] = rmgGoogleAds.sizes['BANNER_MIDDLE'].concat([[300,75]]);
rmgGoogleAds.sizes['to-viewport-9'] = [[300,75]];
rmgGoogleAds.sizeLabels['BANNER_MIDDLE'] = rmgGoogleAds.sizeLabels['BANNER_MIDDLE'].concat("to-viewport-9");
rmgGoogleAds.sizes['BANNER_MIDDLE'] = rmgGoogleAds.sizes['BANNER_MIDDLE'].concat([[960,150],[970,250],[840,150],[840,250],[728,90]]);
rmgGoogleAds.sizes['from-viewport-9'] = [[960,150],[970,250],[840,150],[840,250],[728,90]];
rmgGoogleAds.sizeLabels['BANNER_MIDDLE'] = rmgGoogleAds.sizeLabels['BANNER_MIDDLE'].concat("from-viewport-9");

rmgGoogleAds.sizes['RECT_ABOVE'] = [];
rmgGoogleAds.sizeLabels['RECT_ABOVE'] = [];
rmgGoogleAds.sizes['RECT_ABOVE'] = rmgGoogleAds.sizes['RECT_ABOVE'].concat([[300,250]]);
rmgGoogleAds.sizes['to-viewport-6'] = [[300,250]];
rmgGoogleAds.sizeLabels['RECT_ABOVE'] = rmgGoogleAds.sizeLabels['RECT_ABOVE'].concat("to-viewport-6");rmgGoogleAds.sizes['RECT_ABOVE'] = rmgGoogleAds.sizes['RECT_ABOVE'].concat([[300,600],[300,250]]);
rmgGoogleAds.sizes['from-viewport-6'] = [[300,600],[300,250]];
rmgGoogleAds.sizeLabels['RECT_ABOVE'] = rmgGoogleAds.sizeLabels['RECT_ABOVE'].concat("from-viewport-6");rmgGoogleAds.sizes['RECT_MIDDLE'] = [];

rmgGoogleAds.sizeLabels['RECT_MIDDLE'] = [];rmgGoogleAds.sizes['RECT_MIDDLE'] = rmgGoogleAds.sizes['RECT_MIDDLE'].concat([[300,250]]);
rmgGoogleAds.sizes['from-viewport-3'] = [[300,250]];
rmgGoogleAds.sizeLabels['RECT_MIDDLE'] = rmgGoogleAds.sizeLabels['RECT_MIDDLE'].concat("from-viewport-3");rmgGoogleAds.sizes['RECT_MIDDLE2'] = [];

rmgGoogleAds.sizeLabels['RECT_MIDDLE2'] = [];rmgGoogleAds.sizes['RECT_MIDDLE2'] = rmgGoogleAds.sizes['RECT_MIDDLE2'].concat([[300,250]]);
rmgGoogleAds.sizes['from-viewport-3'] = [[300,250]];


rmgGoogleAds.sizeLabels['RECT_MIDDLE2'] = rmgGoogleAds.sizeLabels['RECT_MIDDLE2'].concat("from-viewport-3");rmgGoogleAds.sizes['RECT_BELOW'] = [];
rmgGoogleAds.sizeLabels['RECT_BELOW'] = [];rmgGoogleAds.sizes['RECT_BELOW'] = rmgGoogleAds.sizes['RECT_BELOW'].concat([[300,250]]);
rmgGoogleAds.sizes['from-viewport-3'] = [[300,250]];


rmgGoogleAds.sizeLabels['RECT_BELOW'] = rmgGoogleAds.sizeLabels['RECT_BELOW'].concat("from-viewport-3");rmgGoogleAds.sizes['HALFRECT_ABOVE'] = [];
rmgGoogleAds.sizeLabels['HALFRECT_ABOVE'] = [];rmgGoogleAds.sizes['HALFRECT_ABOVE'] = rmgGoogleAds.sizes['HALFRECT_ABOVE'].concat([[300,125]]);
rmgGoogleAds.sizes['from-viewport-3'] = [[300,125]];

rmgGoogleAds.sizeLabels['HALFRECT_ABOVE'] = rmgGoogleAds.sizeLabels['HALFRECT_ABOVE'].concat("from-viewport-3");rmgGoogleAds.sizes['HALFRECT_BELOW'] = [];
rmgGoogleAds.sizeLabels['HALFRECT_BELOW'] = [];rmgGoogleAds.sizes['HALFRECT_BELOW'] = rmgGoogleAds.sizes['HALFRECT_BELOW'].concat([[300,125]]);
rmgGoogleAds.sizes['from-viewport-3'] = [[300,125]];

rmgGoogleAds.sizeLabels['HALFRECT_BELOW'] = rmgGoogleAds.sizeLabels['HALFRECT_BELOW'].concat("from-viewport-3");rmgGoogleAds.sizes['SKY_RIGHT'] = [];
rmgGoogleAds.sizeLabels['SKY_RIGHT'] = [];rmgGoogleAds.sizes['SKY_RIGHT'] = rmgGoogleAds.sizes['SKY_RIGHT'].concat([[300,50]]);
rmgGoogleAds.sizes['to-viewport-11'] = [[300,50]];

rmgGoogleAds.sizeLabels['SKY_RIGHT'] = rmgGoogleAds.sizeLabels['SKY_RIGHT'].concat("to-viewport-11");rmgGoogleAds.sizes['SKY_RIGHT'] = rmgGoogleAds.sizes['SKY_RIGHT'].concat([[120,600],[160,600]]);
rmgGoogleAds.sizes['from-viewport-11'] = [[120,600],[160,600]];


rmgGoogleAds.sizeLabels['SKY_RIGHT'] = rmgGoogleAds.sizeLabels['SKY_RIGHT'].concat("from-viewport-11");rmgGoogleAds.sizes['INPAGE'] = [];
rmgGoogleAds.sizeLabels['INPAGE'] = [];rmgGoogleAds.sizes['INPAGE'] = rmgGoogleAds.sizes['INPAGE'].concat([[300,300],[300,500]]);
rmgGoogleAds.sizes['to-viewport-9'] = [[300,300],[300,500]];

rmgGoogleAds.sizeLabels['INPAGE'] = rmgGoogleAds.sizeLabels['INPAGE'].concat("to-viewport-9");rmgGoogleAds.sizes['INPAGE'] = rmgGoogleAds.sizes['INPAGE'].concat([[640,480],[800,300],[620,620],[650,250]]);
rmgGoogleAds.sizes['from-viewport-9'] = [[640,480],[800,300],[620,620],[650,250]];

rmgGoogleAds.sizeLabels['INPAGE'] = rmgGoogleAds.sizeLabels['INPAGE'].concat("from-viewport-9");rmgGoogleAds.sizes['NATIVE'] = [];
rmgGoogleAds.sizeLabels['NATIVE'] = [];rmgGoogleAds.sizes['NATIVE'] = rmgGoogleAds.sizes['NATIVE'].concat("fluid");
rmgGoogleAds.sizes['from-viewport-3'] = "fluid";

rmgGoogleAds.sizeLabels['NATIVE'] = rmgGoogleAds.sizeLabels['NATIVE'].concat("from-viewport-3"); 

// VARIABLES + CONFIG

window.googletag = window.googletag || {cmd: []};
var pbjs = pbjs || {};
pbjs.que = pbjs.que || [];

rmgGoogleAds.slotCode="/21768925892/"+dvSiteID+"/"+dvEmplacementID;

// INIT
googletag.cmd.push(function() {
    /*
    With lazy loading in SRA, when the first ad slot comes within the viewport specified by the fetchMarginPercent parameter, the call for that ad and all other ad slots is made.
     */
    googletag.pubads().enableSingleRequest();
    googletag.pubads().enableLazyLoad({
        fetchMarginPercent: 200, // Fetch slots within 2 viewports.
        renderMarginPercent: 50,  // Render slots within 1/2 viewport.
        mobileScaling: 2.0  // Double the above values on mobile.
    });
});

// SET TARGETING
googletag.cmd.push(function () {
    //initPageTargeting();
    //googletag.pubads().setTargeting('dossier', '_Site-LeVif-FR-fr-home_');
});

document.addEventListener('DOMContentLoaded', function() {
    /*
    if (!document.querySelector('.rmgModal.m-show')) {
        rmgParamObject.removeValueFromArray(rmgGoogleAds.renderBlockers, 'modal');
    }
    */
    console.log('LAUNCH ADS');
    googletag.cmd.push(function () {

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="BANNER_ABOVE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,75]]).addSize([980,0], [[980,150],[960,150],[970,250],[840,150],[840,250],[728,90]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'BANNER_ABOVE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'BANNER_ABOVE');
            }

            rmgGoogleAds.pushAdPlaceholder('BANNER_ABOVE', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674396},"appnexus_mobile":{"placementId":18674403}});
        }


        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="BANNER_BELOW"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,75]]).addSize([980,0], [[960,150],[970,250],[840,150],[840,250],[728,90]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'BANNER_BELOW' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'BANNER_BELOW');
            }

            rmgGoogleAds.pushAdPlaceholder('BANNER_BELOW', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674399},"appnexus_mobile":{"placementId":18674406}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="BANNER_MIDDLE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,75]]).addSize([980,0], [[960,150],[970,250],[840,150],[840,250],[728,90]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'BANNER_MIDDLE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'BANNER_MIDDLE');
            }

            rmgGoogleAds.pushAdPlaceholder('BANNER_MIDDLE', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674399},"appnexus_mobile":{"placementId":18674406}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="RECT_ABOVE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,250]]).addSize([640,0], [[300,600],[300,250]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'RECT_ABOVE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'RECT_ABOVE');
            }

            rmgGoogleAds.pushAdPlaceholder('RECT_ABOVE', adTypePlaceholderIDs, mapping, {"appnexus":{"placementId":18674397}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="RECT_MIDDLE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,250]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'RECT_MIDDLE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'RECT_MIDDLE');
            }

            rmgGoogleAds.pushAdPlaceholder('RECT_MIDDLE', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674400},"appnexus_mobile":{"placementId":18674405}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="RECT_MIDDLE2"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,250]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'RECT_MIDDLE2' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'RECT_MIDDLE2');
            }

            rmgGoogleAds.pushAdPlaceholder('RECT_MIDDLE2', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674400},"appnexus_mobile":{"placementId":18674405}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="RECT_BELOW"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,250]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'RECT_BELOW' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'RECT_BELOW');
            }

            rmgGoogleAds.pushAdPlaceholder('RECT_BELOW', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674400},"appnexus_mobile":{"placementId":18674405}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="HALFRECT_ABOVE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,125]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'HALFRECT_ABOVE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'HALFRECT_ABOVE');
            }

            rmgGoogleAds.pushAdPlaceholder('HALFRECT_ABOVE', adTypePlaceholderIDs, mapping, "");
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="HALFRECT_BELOW"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,125]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'HALFRECT_BELOW' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'HALFRECT_BELOW');
            }

            rmgGoogleAds.pushAdPlaceholder('HALFRECT_BELOW', adTypePlaceholderIDs, mapping, "");
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="SKY_RIGHT"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,50]]).addSize([1150,0], [[120,600],[160,600]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'SKY_RIGHT' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'SKY_RIGHT');
            }

            rmgGoogleAds.pushAdPlaceholder('SKY_RIGHT', adTypePlaceholderIDs, mapping, {"appnexus_desktop":{"placementId":18674398},"appnexus_mobile":{"placementId":18674404}});
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="INPAGE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], [[300,300],[300,500]]).addSize([980,0], [[640,480],[800,300],[620,620],[650,250]]).build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'INPAGE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'INPAGE');
            }

            rmgGoogleAds.pushAdPlaceholder('INPAGE', adTypePlaceholderIDs, mapping, "");
        }

        var adTypeElements = document.querySelectorAll('.rmgAd[data-type="NATIVE"]');
        var adTypeCount = adTypeElements.length;
        var adTypePlaceholderIDs = [];

        if (adTypeCount > 0) {
            var mapping = googletag.sizeMapping().addSize([320,0], "fluid").build();

            if (adTypeCount > 1) {
                for (var i = 0; i < adTypeCount; i++) {
                    adTypeElements[i].id = 'gpt-ad-' + 'NATIVE' + '_' + i;
                    adTypePlaceholderIDs.push(adTypeElements[i].id);
                }
            } else {
                adTypePlaceholderIDs.push('gpt-ad-' + 'NATIVE');
            }

        

            rmgGoogleAds.pushAdPlaceholder('NATIVE', adTypePlaceholderIDs, mapping, "");
        }

    });


    // PREBID
    if (typeof rmgPrebid !== 'undefined') {
        googletag.cmd.push(function () {
            rmgPrebid.init();
        });
    } else {
        //rmgParamObject.removeValueFromArray(rmgGoogleAds.renderBlockers, 'prebid');
    }

    // DEFINE SLOTS
    googletag.cmd.push(function () {
        googletag.defineOutOfPageSlot(rmgGoogleAds.slotCode, 'gpt-ad-OUT_OF_PAGE')
            .addService(googletag.pubads())
            .setTargeting('AdType', ['FLOORAD', 'BANNER_ABOVE', 'BANNER_TOP']);

        for (var p in rmgGoogleAds.adPlaceholders) {
            rmgGoogleAds.defineSlot(p);
        }
    });

    // ADD MOAT INVALID TRAFFIC TARGETING
    googletag.cmd.push(function () {
        rmgGoogleAds.addMoatYield();
    });

    // EXTRA LOGIC
    googletag.cmd.push(function () {
        rmgGoogleAds.toggleAdWhenAdOutOfViewport('gpt-ad-SKY_RIGHT', 'BANNER_ABOVE');
        rmgGoogleAds.resizeRefresh();
    });

    googletag.cmd.push(function () {
        // SLOT RENDERED LOGIC
        googletag.pubads().addEventListener('slotRenderEnded', function(e) {
            rmgGoogleAds.slotRenderedLogic(e);
        });
    });

    googletag.cmd.push(function() {
        googletag.enableServices();
        rmgGoogleAds.renderAds();
    });




    /** ADD EDITION VENTURES **/

    googletag.cmd.push(function () {
        googletag.pubads().addEventListener('slotRenderEnded', function(event) {


            console.log(event);

            if (event.size !== null){
                console.log('Creative with id: ' + event.creativeId + ' is rendered to slot of size: ' + event.size[0] + 'x' + event.size[1]);

                ga('send', 'event', 'ads', 'display', event.size[0]+'x'+event.size[1]);
                ga('newTracker.send', 'event', 'ads', 'display', event.size[0]+'x'+event.size[1]);

             if(activeTakeover == '1'){ 
                    console.log('activeTakeover : ' + activeTakeover);
                    if($('#gpt-ad-OUT_OF_PAGE').css('display') != 'none'){
                        $('body').addClass('takeover');

                        /** HOT FIX MARIJO 26/02/2020 **/
                        $('.site-inner').css('max-width','970px');
                        $('.entry-header').css('max-width','970px');
                        $('.entry-header').css('top','5px');
                        $('.main-single').css('margin','0 auto');
                        $('.main-single').css('max-width','970px');
                        $('.main-index').css('max-width','970px');
                        $('.main-index').css('margin','0 auto');

                        if(event.size[0] == '1' && event.size[1] == '1') {
                            console.log('Slot : Takeover Display'); 
                            //ga('send', 'event', 'ads', 'display', 'takeover');
                            //ga('newTracker.send', 'event', 'ads', 'display', 'takeover');
                        }
                    }
                }
            }
        });
    });

    /** INIT TEST SCROLL **/ 
    

    window.addEventListener('scroll', function() {

        var scroll = jQuery(window).scrollTop();
        //>=, not <=
        if (scroll >= 400) {
            //clearHeader, not clearheader - caps H
            jQuery("#gpt-ad-SKY_RIGHT").removeClass("u-visually-hidden");
        }else{
            jQuery("#gpt-ad-SKY_RIGHT").addClass("u-visually-hidden");
        }



        /*
        rmgGoogleAds.createAd("RECT");
        
        $('.rmgAd').each(function(){ 
            if($(this).visible(true) && !$(this)[0].hasAttribute("data-google-query-id")){
                //$(this).css("border", "1px solid purple");
                console.log('visible :' + $(this).attr('id'));
                googletag.display($(this).attr('id'));
            }else{

                }
        });
        */

    });


    /** END ADD **/

});

