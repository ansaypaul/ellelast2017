<?php get_header(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
<main role="main" class="main main-index">
	<!-- site-inner -->
	<div class="site-inner"> 
	<?php
			 $posts = get_posts(array('posts_per_page' => 1, 'post__not_in' => $excludeIDS));
			 $postcat = get_the_category( $posts[0]->ID );

			_first_article_block($posts,'','',0); 

			$posts = get_posts(array('posts_per_page' => 6,'post__not_in' => $excludeIDS));
			_swiper_article_block($posts,'','',''); 
			
			get_template_part('template-parts/general/editorshop');

			$time_most_popular = '7 days ago';
			$posts = get_posts(array('posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
			if($posts){ _top_article_block($posts,'',''); }

			$ids_category = get_field('liste_des_categories','option');
			foreach ($ids_category as $id_category) {
				$posts = get_posts(array('posts_per_page' => 1, 'category'=> $id_category, 'post__not_in' => $excludeIDS));
				_first_article_block($posts,$id_category,'',1); 

				$posts = get_posts(array('posts_per_page' => 6, 'category'=> $id_category, 'post__not_in' => $excludeIDS));
				_swiper_article_block($posts,$id_category,'',''); 

				$time_most_popular = '30 days ago';
				$posts = get_posts(array('posts_per_page' => 3,'category'=> $id_category, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 
				if(count($posts) == 0 || !isset($posts)){
					$time_most_popular = '90 days ago';
					$posts = get_posts(array('posts_per_page' => 3,'category'=> $id_category, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => $time_most_popular)), 'post__not_in' => $excludeIDS)); 	
				}
				if($posts){ _top_article_block($posts,$id_category,''); }
			}
	?>
	</div>
	<!-- site-inner -->
</main>
<?php get_footer(); ?>