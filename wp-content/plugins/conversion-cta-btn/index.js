(function() {
    tinymce.create("tinymce.plugins.conv_cta_plugin", {

        //url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            //add new button    
            ed.addButton("dollar", {
                title : "Affiliate button",
                cmd : "dollar_command",
                image : "http://www.editionventures.be/images/dollar.png"
            });

            //button functionality.
            ed.addCommand("dollar_command", function() {
                var selected_text2 = ed.selection.getContent();
                var return_text2 = "<a href='"+ selected_text2 +"' class='custom-cta' rel='nofollow' target='_blank'>Shop it</a>";
                ed.execCommand("mceInsertContent", 0, return_text2);
            });

        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : "Extra Buttons",
                author : "Elodie Buski",
                version : "2"
            };
        }
    });

    tinymce.PluginManager.add("conv_cta_plugin", tinymce.plugins.conv_cta_plugin);
})();