<?php

/*
Plugin Name: Conversion CTA 
Plugin URI: 
Description: Add CTA button to visual editor.
Author: Elodie Buski
*/

function enqueue_plugin_scripts2($plugin_array)
{
    //enqueue TinyMCE plugin script with its ID.
    $plugin_array2["conv_cta_plugin"] =  plugin_dir_url(__FILE__) . "index.js";
    return $plugin_array2;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts2");

function register_buttons_editor2($buttons)
{
    //register buttons with their id.
    array_push($buttons, "dollar");
    return $buttons;
}

add_filter("mce_buttons", "register_buttons_editor2");