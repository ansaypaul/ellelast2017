<?php get_header(); ?>
<?php $email = ''; if(isset($_POST['footer_email'])){ $email = $_POST['footer_email']; } ?>
<main role="main" class="main main-page">
  <div class="page">
    <div class="site-inner">
		<div class="newsletter-page">
			<div class="newsletter-ask">
				<h1 class="entry-title"><?php _e("Inscrivez-vous à notre newsletter", 'ev-newsletter' ); ?></h1>
				<p> <?php echo get_field('profil_profil_introduction', 'option'); ?></p>
				<!--
				<button class="fb-connect"><span><?php _e("Inscription via Facebook", 'ev-newsletter' ); ?></span></button>
				<span>ou</span>
				-->
				<div class="newsletter-content">
				<form method="POST" role="form" action="" name="profil_popup_form" id="profil_popup_form">
					<input type="hidden" name="profil_creationSource" value="<?php the_field('profil_creationSource','option'); ?>">
					<input type="hidden" name="profil_motherLanguage" value="<?php the_field('profil_profil_motherLanguage','option'); ?>">
			    	<span class="sprite"></span>
			        <input type="hidden" name="Token" value="*">
			        <label>
			        	<div class="newsletter-input-wrapper">
				        	<input id="profil_email" class="newsletter-input profil_email" name="profil_email" value="<?php echo $email; ?>" type="email" placeholder="<?php _e('Entrez votre e-mail', 'ev-newsletter' ); ?> ">
				        </div>
				        <!-- <div class="all-select-cta"><?php _e( "Sélectionner toutes les newsletters", 'ev-newsletter' ); ?></div> -->
						<div>
							<label for="check-all" class="n-bold"><?php _e( "Sélectionner toutes les newsletters", 'ev-newsletter' ); ?></label>
					       	<input id="check-all" class="newsletter-optin" type="checkbox" name="check-all" value="">
						</div>
						<div>
							<label for="profil_subscriptions"><?php _e( "J’ai lu et j’accepte la déclaration sur la vie privée instaurée par Edition Ventures.", 'ev-newsletter' ); ?></label>
							<input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_marketingConsent" value="">
						</div>
				     	<button class="btn-v1" type="submit" id="newsletter-submit"/><?php _e( 'Valider', 'ev-newsletter' ); ?></button>
				     	<?php 
						$profil_subscriptions = get_field('profil_subscriptions', 'option');
						foreach($profil_subscriptions as $row){ ?>
						<div>
							<label for="profil_subscriptions"><?php echo $row['description']; ?></label>
							<input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_subscriptions[]" value="<?php echo $row['name']; ?>" required>
						</div>
						<?php } ?>
			     	</label>
			    </form>

			    <div class="mentions-legales">
			    <?php the_field('droit_legal_newsletter', 'option'); ?>
			    </div>
			</div>
		    <div class="newsletter-result hide center"><?php if(get_field('inscription_newsletter_image','option')){ ?> <img src="<?php the_field('inscription_newsletter_image','option'); ?> "/> <?php } else{ _e("<p><b>Votre inscription a bien été prise en compte</b></p>",'ev-newsletter'); }?></div>
		    <div class="newsletter-error hide center"><?php _e( "Une erreur est survenue durant votre inscription. ",'ev-newsletter'); ?></div>
		  </div>
		</div>
	</div>
  </div>
</main>
<?php get_footer(); ?>