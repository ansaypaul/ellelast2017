��          �   %   �      p  t  q  <   �     #     *     9     E     ]     o     u     �  "   �     �  \   �     '  %   .     T     Y     b     f     u     ~     �  $   �  ?   �  2   �     "	     *	    0	    A  +   Y     �     �     �     �     �     �     �     �  !   �       S        l  (   t     �     �     �     �  
   �     �  
   �     �  G   �  0   B     s     |                                                                                         
             	                     
				<h4>Sachez que vous disposez des droits suivants :</h4>
				    <ol>
				  	  <li>Vous avez le droit d'information et d'accès à vos données personnelles.</li>
				  	  <li>Vous avez le droit de rectification, d’effacement et de limitation du traitement de vos données personnelles.</li>
				  	  <li>Vous avez le droit d'opposition.</li>
				  	  <li>Vous avez le droit à la portabilité de vos données personnelles. </li>
				    </ol>
				    <p>Sachez également que vous avez le droit de retirer votre consentement au traitement de vos données à tout moment.</p>
				    <p>Nous recueillons uniquement les données dont nous avons besoin pour vous informer des activités du Elle Belgique.</p>
				    <p>Si vous souhaitez exercer un de ces droits, nous vous invitons à nous envoyer un e-mail à l’adresse suivante <a href='mailto:privacy@editionventures.be'>privacy@editionventures.be</a> ou à consulter la déclaration sur le respect de la vie privée du Groupe Edition Ventures.</p>
				    <p>Vous pouvez vous désabonner à tout moment en cliquant sur le lien présent dans le bas de chacun de nos courriels.</p> <p><b>Votre inscription a bien été prise en compte</b></p> Année Boîte postale Code postal Complétez votre profil Date de naissance Email Entrez votre e-mail Inscription via Facebook Inscrivez-vous à notre newsletter Jour J’ai lu et j’accepte la déclaration sur la vie privée instaurée par Edition Ventures. Madame Merci d'avoir complété votre profil Mois Monsieur Nom Numéro de rue Portable Prénom Rue Sélectionner toutes les newsletters Une erreur est survenue durant la mise à jour de votre profil. Une erreur est survenue durant votre inscription.  Valider Ville Project-Id-Version: broken-link-checker v1.10.8
Report-Msgid-Bugs-To: http://wordpress.org/tag/broken-link-checker
POT-Creation-Date: 2019-04-11 13:39+0200
PO-Revision-Date: 
Last-Translator: Luc Capronnier <lcapronnier@yahoo.com>
Language-Team: Whiler, Luc Capronnier <webmaster@blogs.wittwer.fr, lcapronnier@yahoo.com>
Language: fr_BE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;_ngettext_noop:1,2;_n_noop:1,2;_c;_nc:4c,1,2;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_x:1,2c
X-Poedit-Basepath: ..
X-Generator: Poedit 2.0.4
X-Poedit-SearchPath-0: .
 
				<h4>Weet dat u over volgende rechten beschikt:</h4>
				    <ol>
				  	  <li>U hebt het recht op informatie over en toegang tot uw persoonlijke gegevens.</li>
				  	  <li>U hebt het recht om de verwerking van uw persoonlijke gegevens te verbeteren, te wissen en te beperken.</li>
				  	  <li>U hebt het recht op verzet.</li>
				  	  <li>U hebt het recht op de overdraagbaarheid van uw persoonlijke gegevens.</li>
				    </ol>
				    <p>Weet ook dat u het recht heeft om uw toestemming voor de verwerking van uw gegevens op elk moment in te trekken.</p>
				    <p>Wij verzamelen enkel de gegevens die wij nodig hebben om u op de hoogte te houden van de activiteiten van ELLE België.</p>
				    <p>Als u wenst om één van deze rechten uit te oefenen, stuur ons dan een email op <a href='mailto:privacy@editionventures.be'>privacy@editionventures.be</a> of raadpleeg de privacyverklaring van Edition Ventures.</p>
				    <p>U kan u op elk moment uitschrijven door te klikken op de link die steeds aanwezig is onderaan onze emails.</p> <p><b>Bedankt voor je inschrijving!</b></p> Jaar Bus Postcode Vervolledig je profiel Geboortedatum Email Vul je emailadres is Schrijf je in via Facebook Schrijf je in op onze nieuwsbrief Dag Ik heb de privacyverklaring van Editions Ventures gelezen en ik ga hiermee akkoord. Mevrouw Bedankt voor het invullen van je profiel Maand Meneer Familienaam Nummer GSM-nummer Voornaam Straatnaam Alle nieuwsbrieven selecteren Er heeft zich een fout voorgedaan tijdens het bijwerken van uw profiel. Er is een fout ontstaan tijdens uw registratie.  Bevestig Stad 