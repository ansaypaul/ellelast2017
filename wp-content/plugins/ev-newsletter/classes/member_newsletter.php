<?php 

class member_newsletter
{
  public $_profil_id;
  public $_profil_civilite;
  public $_profil_prenom;
  public $_profil_nom;
  public $_profil_email;
  public $_profil_gsm;
  public $_profil_anniversaire;
  public $_profil_adresseStreet;
  public $_profil_addressNumber;
  public $_profil_addressBox;
  public $_profil_addressPostalCode;
  public $_profil_addressLocality;
  public $_profil_mdp;   
  public $_profil_hash_mdp;
  public $_profil_subscriptions = array();
  public $_profil_no_subscriptions = array();
  public $_profil_motherLanguage;
  public $_profil_creationSource;
  public $_profil_marketingConsent;
  public $_auth; 
  // Nous déclarons une méthode dont le seul but est d'afficher un texte.

	public function auth_membre(){
		$args = array('post_type' => 'membre', 'meta_query' => array( 'relation' => 'AND',
            array( array( 'key'   => 'email', 'value' => $this->_profil_email, 'compare'   => 'LIKE' ) ),
            array( array( 'key'   => 'password', 'value' => $this->_profil_hash_mdp , 'compare'   => 'LIKE' ) ),
			),
    	);

		$member = get_posts($args);
	    if(count($member) > 0){
	    	$this->_auth = 1;
			$this->_profil_id = $member[0]->ID;
			$_SESSION['profil_id'] = $this->_profil_id;

	    	$this->get_membre();
	    	$this->set_session_membre();
	    	
	    	//header('Location: http://localhost:8080/ellelast2017/profile/');
	    }
	    else{
			$this->_auth = 0;
	    }
	}

	public function set_membre(){
	    if(isset($_POST['profil_prenom'])){ $this->_profil_prenom = $_POST['profil_prenom'];}
	    if(isset($_POST['profil_nom'])){ $this->_profil_nom = $_POST['profil_nom'];}
	    if(isset($_POST['profil_email'])){ $this->_profil_email = $_POST['profil_email'];}
	    //if(isset($_POST['profil_mdp'])){ $this->_profil_mdp = $_POST['profil_mdp'];}
	    if(isset($_POST['profil_civilite'])){ $this->_profil_civilite = $_POST['profil_civilite'];}
	    if(isset($_POST['profil_gsm'])){ $this->_profil_gsm = $_POST['profil_gsm'];}
	    if(isset($_POST['profil_adresseStreet'])){ $this->_profil_adresseStreet = $_POST['profil_adresseStreet'];}
	    if(isset($_POST['profil_addressNumber'])){ $this->_profil_addressNumber = $_POST['profil_addressNumber'];}
	    if(isset($_POST['profil_addressBox'])){ $this->_profil_addressBox = $_POST['profil_addressBox'];}
	    if(isset($_POST['profil_addressPostalCode'])){ $this->_profil_addressPostalCode = $_POST['profil_addressPostalCode'];}
	    if(isset($_POST['profil_addressLocality'])){ $this->_profil_addressLocality = $_POST['profil_addressLocality'];}
	    if(isset($_POST['profil_anniversaire'])){ $this->_profil_anniversaire = $_POST['profil_anniversaire'];}
	    if(isset($_POST['profil_motherLanguage'])){ $this->_profil_motherLanguage = $_POST['profil_motherLanguage'];}
	    if(isset($_POST['profil_creationSource'])){ $this->_profil_creationSource = $_POST['profil_creationSource'];}

	    if(isset($_POST['profil_marketingConsent'])){ $this->_profil_marketingConsent = $_POST['profil_marketingConsent'];}

	    if(isset($_POST['profil_subscriptions'])){ $this->_profil_subscriptions = $_POST['profil_subscriptions'];}

	   	if(isset($_POST['profil_mdp'])){ $this->_profil_hash_mdp = hash_hmac('sha256',$_POST['profil_mdp'],'test-hash-2');}
	}

	public function if_exist_membre(){
	    $member = get_posts( array( 'post_type' => 'membre','meta_query' => array(
	    	array('key' => 'email','value' => $this->_profil_email,'compare' => 'LIKE' ))
	    ));

	    if(count($member) > 0){
	    	$this->_profil_id = $member[0]->ID; return false;
	    }
	    else{
			$this->_profil_id = 0; return true;
	    }
	}

	public function create_member(){
		$member_obj = array(
		'ID'=> $this->_profil_id,
		'post_type' => 'membre',
		'post_title' => $this->_profil_prenom . ' ' . $this->_profil_nom,
		'post_status' => 'publish',
		'post_author' => 1,
		);
				 
		// Insert the post into the database.
		$this->_profil_id = wp_insert_post( $member_obj );
		update_field('email', $this->_profil_email, $this->_profil_id);
		update_field('prenom', $this->_profil_prenom, $this->_profil_id);
		update_field('nom', $this->_profil_nom, $this->_profil_id);
		update_field('password', $this->_profil_hash_mdp, $this->_profil_id);
	}

	public function update_member(){
		update_field('email', $this->_profil_email, $_SESSION['profil_id']);
		update_field('prenom', $this->_profil_prenom, $_SESSION['profil_id']);
		update_field('nom', $this->_profil_nom, $_SESSION['profil_id']);
		update_field('civilite', $this->_profil_civilite, $_SESSION['profil_id']);
		update_field('gsm', $this->_profil_gsm, $_SESSION['profil_id']);
		update_field('nom_de_rue', $this->_profil_adresseStreet, $_SESSION['profil_id']);
		update_field('numero_de_rue', $this->_profil_addressNumber, $_SESSION['profil_id']);
		update_field('anniversaire', $this->_profil_anniversaire, $_SESSION['profil_id']);
		//update_field('password', $this->_profil_addressBox, $_SESSION['profil_id']);
		update_field('code_postal', $this->_profil_addressPostalCode, $_SESSION['profil_id']);
		update_field('ville', $this->_profil_addressLocality, $_SESSION['profil_id']);
	}

	public function set_session_membre(){
		$_SESSION['profil_prenom'] = $this->_profil_prenom;
		$_SESSION['profil_nom'] = $this->_profil_nom;
		$_SESSION['profil_email'] = $this->_profil_email;
		$_SESSION['profil_civilite'] = $this->_profil_civilite;
		$_SESSION['profil_gsm'] = $this->_profil_gsm;
		$_SESSION['profil_anniversaire'] = $this->_profil_anniversaire;
		$_SESSION['profil_adresseStreet'] = $this->_profil_adresseStreet;
		$_SESSION['profil_addressNumber'] = $this->_profil_addressNumber;
		$_SESSION['profil_addressBox'] = $this->_profil_addressBox;
		$_SESSION['profil_addressPostalCode'] = $this->_profil_addressPostalCode;
		$_SESSION['profil_addressLocality'] = $this->_profil_addressLocality;
		$_SESSION['profil_subscriptions'] = $this->_profil_subscriptions;
	}

	public function get_membre(){
		$acf_membre = get_fields($this->_profil_id); 
	    if(isset($acf_membre['prenom'])){$this->_profil_prenom = $acf_membre['prenom']; }
	    if(isset($acf_membre['nom'])){$this->_profil_nom = $acf_membre['nom']; }
	    if(isset($acf_membre['email'])){$this->_profil_email = $acf_membre['email']; }
	   	//if(isset($acf_membre['password'])){$this->_profil_hash_mdp = $acf_membre['password']; }
	   	if(isset($acf_membre['civilite'])){$this->_profil_civilite = $acf_membre['civilite']; }
	    if(isset($acf_membre['gsm'])){$this->_profil_gsm = $acf_membre['gsm']; }
	    if(isset($acf_membre['nom_de_rue'])){$this->_profil_adresseStreet = $acf_membre['nom_de_rue']; }
	    if(isset($acf_membre['numero_de_rue'])){$this->_profil_addressNumber = $acf_membre['numero_de_rue']; }
	    if(isset($acf_membre['addressBox'])){$this->_profil_addressBox = $acf_membre['addressBox']; }
	   	if(isset($acf_membre['code_postal'])){$this->_profil_addressPostalCode = $acf_membre['code_postal']; }
	   	if(isset($acf_membre['ville'])){$this->_profil_addressLocality = $acf_membre['ville']; }
	   	if(isset($acf_membre['anniversaire'])){$this->_profil_anniversaire = $acf_membre['anniversaire']; }
	}

	public function set_cookie_member(){

		setcookie('profil_id', $hash_key, time() + (86400 * 30), "/"); // 86400 = 1 day
		setcookie('profil_login_email', $profil_login_email, time() + (86400 * 30), "/"); // 86400 = 1 day
		setcookie('profil_login_name', 'Ansay', time() + (86400 * 30), "/"); // 86400 = 1 day
		setcookie('profil_login_firstname', 'Paul', time() + (86400 * 30), "/"); // 86400 = 1 day

		//if($info['http_code'] == 200) { header("Location: http://localhost:8080/ellelast2017/profile/"); }
	}

	public function generateJSONActito(){
		$JSON_STRING = '';
		$JSON_STRING .= '{"attributes":[';
	    if(isset($this->_profil_prenom) && $this->_profil_prenom != ''){$JSON_STRING .= '{"name":"firstName", "value":"'.$this->_profil_prenom.'"},';}
	    if(isset($this->_profil_nom) && $this->_profil_nom != ''){$JSON_STRING .= '{"name":"lastname", "value":"'.$this->_profil_nom.'"},';}
	    if(isset($this->_profil_email) && $this->_profil_email != ''){$JSON_STRING .= '{"name":"emailAddress", "value":"'.$this->_profil_email.'"},';}
	    if(isset($this->_profil_civilite) && $this->_profil_civilite != ''){$JSON_STRING .= '{"name":"sex", "value":"'.$this->_profil_civilite.'"},';}
	    if(isset($this->_profil_gsm) && $this->_profil_gsm != ''){$JSON_STRING .= '{"name":"gsmNumber", "value":"'.$this->_profil_gsm.'"},';}
	    if(isset($this->_profil_adresseStreet) && $this->_profil_adresseStreet != ''){$JSON_STRING .= '{"name":"addressStreet", "value":"'.$this->_profil_adresseStreet.'"},';}
	    if(isset($this->_profil_addressNumber) && $this->_profil_addressNumber != ''){$JSON_STRING .= '{"name":"addressNumber", "value":"'.$this->_profil_addressNumber.'"},';}
	    if(isset($this->_profil_addressBox) && $this->_profil_addressBox != ''){$JSON_STRING .= '{"name":"addressBox", "value":"'.$this->_profil_addressBox.'"},';}
	    if(isset($this->_profil_addressPostalCode) && $this->_profil_addressPostalCode != ''){$JSON_STRING .= '{"name":"addressPostalCode", "value":"'.$this->_profil_addressPostalCode.'"},';}
	    if(isset($this->_profil_addressLocality) && $this->_profil_addressLocality != ''){$JSON_STRING .= '{"name":"addressLocality", "value":"'.$this->_profil_addressLocality.'"},';}
	    if(isset($this->_profil_anniversaire) && $this->_profil_anniversaire != ''){$JSON_STRING .= '{"name":"birthDate", "value":"'.$this->_profil_anniversaire.'"},';}
		if(isset($this->_profil_motherLanguage) && $this->_profil_motherLanguage != ''){$JSON_STRING .= '{"name":"motherLanguage", "value":"'.$this->_profil_motherLanguage.'"},';}
		if(isset($this->_profil_creationSource) && $this->_profil_creationSource != ''){$JSON_STRING .= '{"name":"creationSource", "value":"'.$this->_profil_creationSource.'"},';}
		if(isset($this->_profil_marketingConsent) && $this->_profil_marketingConsent != ''){$JSON_STRING .= '{"name":"marketingConsent", "value":"'.$this->_profil_marketingConsent.'"},';}

	    //if(isset($this->_profil_subscriptions)){$JSON_STRING .= '{"name":"firstName", "value":"'. $this->_profil_prenom.'"},'};
	    //if(isset($_POST['profil_mdp'])){ $this->_profil_mdp = $_POST['profil_mdp'];}

	   	//if(isset($this->_profil_hash_mdp)){ $this->_profil_hash_mdp = hash_hmac('sha256',$_POST['profil_mdp'],'test-hash-2');}

		//$JSON_STRING .= '{"name":"birthDate","value":"'.$_POST["dob_day"].'/'.$_POST["dob_month"].'/'.$_POST["dob_year"].'"}';

		$JSON_STRING .= '{"name":"Token","value":"'.md5($this->_profil_email).'"},';
		$JSON_STRING .= '{"name":"blank","value":""}';
		$JSON_STRING .= '], ';


		$date_consent = date('Y-m-d\TH:i:sT');
		$datetime = new DateTime();
		$date_consent = $datetime->format(DateTime::ATOM); // Updated ISO8601
		//$date_consent = $d->format(DateTime::RFC3339_EXTENDED);

		$JSON_STRING .= '"dataCollectionInformation": {
    		"date": "'.$date_consent.'",
    		"source": "'.$this->_profil_creationSource.'",
    		"way": "Formulaire inscription site internet"
  		},';

		if(isset($this->_profil_subscriptions)){
			if(count($this->_profil_subscriptions) != 0){
			  $JSON_STRING .= '"subscriptions":[';
			  foreach ($this->_profil_subscriptions as $subscriptions) {
			  		$subscription = explode(":", $subscriptions);
		          	$JSON_STRING .= '{"name":"'.$subscription[0].'","subscribe":"'.$subscription[1].'"},';
			    }
			   $JSON_STRING = substr($JSON_STRING, 0, -1);
			   $JSON_STRING .= ']';
			}
		}
		/*
		if(isset($this->profil_no_subscriptions)){
			if(count($this->profil_no_subscriptions) != 0){
			  $JSON_STRING .= '"subscriptions":[';
			  foreach ($this->profil_no_subscriptions as $subscriptions) {
			    $nb_subscriptions_i++;
		          	$JSON_STRING .= '{"name":"'.$subscriptions.'","subscribe":"true"},';
			    }
			   $JSON_STRING = substr($JSON_STRING, 0, -1);
			   $JSON_STRING .= ']';
			}
		}
		*/

		$JSON_STRING .= '}';

		return $JSON_STRING;
	}

	public function test(){
		return 1;
	}

}