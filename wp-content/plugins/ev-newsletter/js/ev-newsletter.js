/* footer-script.js */

(function ($, root, undefined) {
	$(document).ready(function() {

		var path_windows = window.location.href;
		var arr = path_windows.split("/");
		var ajax_path = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];
		var ajax_search = {"ajaxurl": ajax_path + "/wp-admin/admin-ajax.php"};


		$('#check-all').click(function(event) {   
		    if(this.checked) {
		        $(':checkbox').each(function() {
		            this.checked = true;                        
		        });
		    } else {
		        $(':checkbox').each(function() {
		            this.checked = false;                       
		        });
		    }
		});

		function ev_newsletter_launch(){

			profil_email = $('input[name=profil_email]').val();
			profil_creationSource = $('input[name=profil_creationSource]').val();
			profil_motherLanguage = $('input[name=profil_motherLanguage]').val();
			profil_subscriptions = $('input[name=profil_subscriptions]').val();
			profil_marketingConsent = $('input[name=profil_marketingConsent]').val();

			profil_civilite = $('input[name=profil_civilite]:checked').val();

			profil_prenom = $('input[name=profil_prenom]').val();
			profil_nom = $('input[name=profil_nom]').val();

			if($('select[name=dob_day]').val() != undefined){
				profil_anniversaire = $('select[name=dob_day]').val() + '/' + $('select[name=dob_month]').val() + '/' + $('select[name=dob_year]').val();
			}else{
				profil_anniversaire = '';
			}
			profil_gsm = $('input[name=profil_gsm]').val();
			profil_adresseStreet = $('input[name=profil_adresseStreet]').val();
			profil_addressNumber = $('input[name=profil_addressNumber]').val();
			profil_addressBox = $('input[name=profil_addressBox]').val();
			profil_addressPostalCode = $('input[name=profil_addressPostalCode]').val();
			profil_addressLocality = $('input[name=profil_addressLocality]').val();


			if($('input[name=profil_marketingConsent]').is(':checked')){
				profil_marketingConsent = 1;
			}else{
				profil_marketingConsent = 0;
			}

			var profil_subscriptions = []

			$("input[name='profil_subscriptions[]']:checked").each(function ()
			{
			    profil_subscriptions.push($(this).val() + ':true');
			});

			$("input[name='profil_subscriptions[]']:not(:checked)").each(function ()
			{
			    profil_subscriptions.push($(this).val() + ':false');
			});

			if(profil_email != '' && profil_marketingConsent == 1){
				$.ajax({
					url: my_ajax_object.ajax_url,
					type: 'post',
					data: { 
						action: 'ajax_load_newsletter_ev',
						profil_email: profil_email,
						profil_creationSource: profil_creationSource,
						profil_motherLanguage: profil_motherLanguage,
						profil_subscriptions: profil_subscriptions,
						profil_marketingConsent: profil_marketingConsent,

						profil_prenom: profil_prenom,
						profil_nom: profil_nom,
						profil_civilite: profil_civilite,

						profil_anniversaire: profil_anniversaire,
						profil_gsm: profil_gsm,
						profil_adresseStreet: profil_adresseStreet,
						profil_addressNumber: profil_addressNumber,
						profil_addressBox: profil_addressBox,
						profil_addressPostalCode: profil_addressPostalCode,
						profil_addressLocality: profil_addressLocality,
					},
					beforeSend: function() {

					},
					success: function( html ) {
						if(html == '200'){
							$('.newsletter-content').hide();
							$('.newsletter-result').show();
						}else{
							$('.newsletter-content').hide();
							$('.newsletter-error').show();
						}

					},
					error: function (xhr) {
						console.log('error news'); 
		   			} 
				})
			}else{
				console.log('champ obligatoire');
			}
		}


		 $.validator.addMethod("valueNotEquals", function(value, element, arg){
		  return arg !== value;
		 }, "Value must not equal arg.");


		$("form[name='profil_popup_form']").validate({
		    rules: {
		        profil_email: {
		            required: true,
		            email: true
		        },
		        profil_marketingConsent:"required",
		        profil_prenom:"required",
		        profil_nom:"required",
		        profil_adresseStreet:"required",
		        profil_addressNumber:"required",
		        profil_addressPostalCode:"required",
		        profil_addressLocality:"required",
		        dob_day:"required",
		        dob_month:"required",
		        dob_year:"required",
		    },
		    messages: {
		        profil_email: {
		        },
		    },
		    submitHandler: function(form) {
		        ev_newsletter_launch();
		    }
		});
	})
})(jQuery, this);
