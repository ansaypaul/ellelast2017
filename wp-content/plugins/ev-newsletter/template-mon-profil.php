<?php get_header(); ?>
<?php 
$username='elle/WebServices_Paul';
$password='87BOI8HC';
$URL='https://www.actito.be/ActitoWebServices/ws/v4/entity/Ventures/table/ventures/profile?search=Token%3D'.$_GET['Token'];
$fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_STDERR, $fp);
curl_setopt($ch, CURLOPT_URL,$URL);
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
$result  = curl_exec($ch);

$days = array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
$months = array('01:Janvier','02:Février','03:Mars','04:Avril','05:Mai','06:Juin','07:Juillet','08:Août','09:Septembre','10:Octobre','11:Novembre','12:Décembre');
$years = array_combine(range(date("Y"), 1930), range(date("Y"), 1930));
$subscriptions_init = array('Marie Claire','ELLE Partenaire', 'ELLE', 'Life Magazine','Psychologies','Déco Idées');


if (empty($result)) {
    // some kind of an error happened
   /* echo "No server responded<br />";*/
    die(curl_error($ch));
    curl_close($ch); // close cURL handler
} else {
    $info = curl_getinfo($ch);
    curl_close($ch); // close cURL handler
    /*echo "Server responded: <br />";*/
    if (empty($info['http_code'])) {
            die("No HTTP code was returned");
    } else {
      
       /* echo "The server responded: <br />";
        echo $info['http_code'];
        echo $result ;*/
    }

}

$profil = json_decode($result);

if(isset($_POST)  && !empty($_POST))
{
  $lastname = $_POST['attributes'][0]['value'];
  $firstName = $_POST['attributes'][1]['value'];
  $addressBox = $_POST['attributes'][2]['value'];
  $addressLocality = $_POST['attributes'][3]['value'];
  $addressNumber = $_POST['attributes'][4]['value'];
  $gsmNumber = $_POST['attributes'][5]['value'];
  $emailAddress = $_POST['attributes'][7]['value'];
  $addressPostalCode = $_POST['attributes'][8]['value'];
  $addressStreet = $_POST['attributes'][9]['value'];
  $birthDate = $_POST['attributes'][10]['value'];
  //$civilState = $_POST['attributes'][11]['value'];
  $sex = $_POST['attributes'][11]['value'];

  $day_profil = $_POST['dob_day'];
  $month_profil = $_POST['dob_month'];
  $year_profil = $_POST['dob_year'];

  $subscriptions_profil = $_POST['subscriptions'];
  $marketingConsent = $_POST['marketingConsent'];
  /*$motherLanguage =
  $civilState =
  $creationSource =*/
}
else 
{
  $id_profile = $profil->profiles[0]->attributes[0]->value;
  $creationMoment = $profil->profiles[0]->attributes[1]->value;
  $updateMoment = $profil->profiles[0]->attributes[2]->value;
  $lastname = $profil->profiles[0]->attributes[3]->value;
  $firstName = $profil->profiles[0]->attributes[4]->value;
  $addressBox = $profil->profiles[0]->attributes[14]->value;
  $addressLocality = $profil->profiles[0]->attributes[9]->value; 
  $addressNumber = $profil->profiles[0]->attributes[7]->value; 
  $gsmNumber = $profil->profiles[0]->attributes[12]->value;
  $emailAddress = $profil->profiles[0]->attributes[5]->value;
  $addressPostalCode = $profil->profiles[0]->attributes[8]->value;
  $addressStreet = $profil->profiles[0]->attributes[6]->value; 
  $birthDate = $profil->profiles[0]->attributes[10]->value; 
 // $civilState = $profil->profiles[0]->attributes[16]->value; 
  $sex = $profil->profiles[0]->attributes[19]->value; 
  $marketingConsent = $profil->profiles[0]->attributes[30]->value; 

  foreach ($profil->profiles[0]->subscriptions as $subscription) {
    $subscriptions_profil[] = $subscription->name;
  }

if(isset($birthDate)){
  $day_profil = explode('/',$birthDate)[0];
  $month_profil = explode('/',$birthDate)[1];
  $year_profil = explode('/',$birthDate)[2];
}

} ?>
<main role="main" class="main main-page">
  <div class="page">
		<!-- site-inner -->
		<div class="site-inner">
      	<div class="profil-page">
				<h1 class="entry-title"><?php _e( 'Complétez votre profil', 'ev-newsletter' ); ?></h1>
				<p> <?php echo get_field('profil_profil_introduction', 'option'); ?>
				<div class="newsletter-content">
				<form method="POST" role="form" id="profil_news" name="profil_popup_form"> 
    		      <input type='hidden' name='Token' value='<?php echo $_GET['Token']; ?>'>
	              <input type="hidden" name="profil_creationSource" value="<?php the_field('profil_creationSource','option'); ?>">
				  <input type="hidden" name="profil_motherLanguage" value="<?php the_field('profil_profil_motherLanguage','option'); ?>">

		          <div class="civil">
			          <input <?php if($sex == 'M'){ echo'checked'; } ?> name="profil_civilite" id="civilMonsieur" value="M"  type="radio"><label for="civilMonsieur"><?php _e( 'Monsieur', 'ev-newsletter' ); ?></label>
			          <input <?php if($sex == 'F'){ echo'checked'; } ?> name="profil_civilite" id="civilMadame" value="F" type="radio"><label for="civilMadame"><?php _e( 'Madame', 'ev-newsletter' ); ?></label>
			      </div>
			      <div class="label-input">
			      	<label for="form-firstname"><?php _e( 'Prénom', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input id="form-firstname" name='profil_prenom' value='<?php echo $firstName ?>'>
			      </div>
			       <div class="label-input">
			      	<label for="form-lastname"><?php _e( 'Nom', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input id="form-lastname" name='profil_nom'  value='<?php echo $lastname ?>'>
			      </div>
			       <div class="label-input">
			      	<label for="form-email"><?php _e( 'Email', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input class="newsletter-input" id="form-email" name='profil_email' value='<?php echo $emailAddress; ?>' readonly>
			      </div>
			      <div class="label-input select-date">
			      	<label><?php _e( 'Date de naissance', 'ev-newsletter' ); ?><sup>*</sup></label>	
			        <select id="form_day" name="dob_day">
			        <option value=""><?php _e( 'Jour', 'ev-newsletter' ); ?></option>
			        <?php foreach ($days  as $day) 
			        {
			          if($day_profil == $day){
			          echo '<option selected value="'.$day.'">'.$day.'</option>';
			          }else{
			          echo '<option value="'.$day.'">'.$day.'</option>';
			          }
			        }
			        ?>
			        </select>
			        <select id="form_month" name="dob_month">
			          <option value=""><?php _e( 'Mois', 'ev-newsletter' ); ?></option>
				        <?php foreach ($months  as $month) 
				        {
				          if($month_profil == explode(':',$month)[0]){
				          echo '<option selected value="'.explode(':',$month)[0].'">'.explode(':',$month)[1].'</option>';
				          }else{
				          echo '<option value="'.explode(':',$month)[0].'">'.explode(':',$month)[1].'</option>';
				          }
			        	}?>
			        </select>
			        <select id="form_year" name="dob_year">
			          <option value=""><?php _e( 'Année', 'ev-newsletter' ); ?></option>
			          <?php foreach ($years  as $year) 
			          {
			            if($year_profil == $year){
			            echo '<option selected value="'.$year.'">'.$year.'</option>';
			            }else{
			            echo '<option value="'.$year.'">'.$year.'</option>';
			            }
			          }
			          ?>
			        </select>
			      </div>
			      <div class="label-input">
			      	<label for="form-email"><?php _e( 'Portable', 'ev-newsletter' ); ?></label>
			        <input id="form-email" name='profil_gsm' value='<?php echo $gsmNumber ?>' title="Portable" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" >
			      </div>
			       <div class="label-input">
			      	<label for="form-street"><?php _e( 'Rue', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input id="form-street" name='profil_adresseStreet' title="Nom de rue" value='<?php echo $addressStreet  ?>'>
			      </div>
			       <div class="label-input">
			      	<label for="form-number"><?php _e( 'Numéro de rue', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input id="form-number" name='profil_addressNumber'  title="Numéro de rue" value='<?php echo $addressNumber  ?>'  maxlength="10">
			      </div>
			       <div class="label-input">
			      	<label for="form-box"><?php _e( 'Boîte postale', 'ev-newsletter' ); ?></label>
			        <input id="form-box" name='profil_addressBox' title="Boîte postale" value='<?php echo $addressBox ?>' maxlength="10">
			      </div>
			       <div class="label-input">
			      	<label for="form-code"><?php _e( 'Code postal', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input id="form-code" name='profil_addressPostalCode' title="Code postal" value='<?php echo $addressPostalCode ?>' maxlength="10">
			      </div>
			      <div class="label-input">
			     	<label for="form-city"><?php _e( 'Ville', 'ev-newsletter' ); ?><sup>*</sup></label>
			        <input id="form-city" name='profil_addressLocality' title="Ville" value='<?php echo $addressLocality ?>' maxlength="50">
			      </div>
			      <input type='hidden' name='profil_motherLanguage' value='fr'>
			      <div class="all-label-check">
			      <?php 
					$profil_subscriptions = get_field('profil_subscriptions', 'option');
					foreach($profil_subscriptions as $row){ ?>
					<div class="label-check">
						<label for="profil_subscriptions"><?php echo $row['description']; ?></label>
						<input class="newsletter-optin profil_subscriptions" type="checkbox" name="profil_subscriptions[]" value="<?php echo $row['name']; ?>" <?php if(in_array($row['name'],$subscriptions_profil)){echo 'checked';} ?> required>
					</div>
					<?php } ?>

		            <div class="label-check">
		            	<label><?php _e( "J’ai lu et j’accepte la déclaration sur la vie privée instaurée par Edition Ventures.", 'ev-newsletter' ); ?></label>
		            	<input class="newsletter-optin" type="checkbox" name="profil_marketingConsent" value="1" required <?php if($marketingConsent == 1){echo 'checked';}?>>
		            </div>
        		</div>
			    <input id="newsletter-submit" type='submit' class="formulaire-submit btn-v1" value='Valider'/>
			    <div class="mentions-legales">
			    <?php the_field('droit_legal_newsletter', 'option'); ?>
			    </div>
			    <?php if(isset($_POST)  && !empty($_POST)) {?> <p><?php _e( "Merci d'avoir complété votre profil", 'ev-newsletter' ); ?> !</p><?php } ?>
				</form>
			</div>
       		<div class="newsletter-result hide center"><?php if(get_field('update_newsletter_image','option')){ ?> <img src="<?php the_field('update_newsletter_image','option'); ?> "/> <?php } else{ _e("<p><b>Votre inscription a bien été prise en compte</b></p>",'ev-newsletter'); }?></div>
       		<div class="newsletter-error hide center"><p><?php _e( "Une erreur est survenue durant la mise à jour de votre profil."); ?></p></div>
		</div>
      </div>
			<footer class="entry-footer"></footer>
		</div>
	</div>
</main>

<style>
    .newsletter-result{font-size: 15px;}
</style>
<?php get_footer(); ?>