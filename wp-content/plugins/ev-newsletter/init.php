<?php
/**
 * Plugin Name: Newsletter Edition Ventures
 * Description: 
 * Version: 1.0.0
 * Author: Paul Ansay
 * Author URI: mailto:paul@editionventures.com
 * Text Domain: ev-newsletter
 * Domain Path: /languages
 */
require_once('classes/member_newsletter.php');
require_once('classes/actito_newsletter.php');

$member_newsletter = new member_newsletter;
$actito_newsletter = new actito_newsletter;

function my_enqueue() {

    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );

    wp_localize_script( 'ajax-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
//add_action( 'wp_enqueue_scripts', 'my_enqueue' );

function myplugin_load_textdomain() {
  load_plugin_textdomain( 'ev-newsletter', false, basename( dirname( __FILE__ ) ) . '/languages' ); 
}

add_action( 'init', 'myplugin_load_textdomain' );

wp_register_script('jqueryvalidate', plugins_url( '/js/jquery.validate.js', __FILE__ ),array('jquery'),'1.17.0',true);
wp_enqueue_script( 'jqueryvalidate' );

wp_register_script('jqueryvalidatelangfr', plugins_url( '/js/messages_'.get_locale().'.js', __FILE__ ),array('jqueryvalidate'),'1.17.0',true);
wp_enqueue_script( 'jqueryvalidatelangfr' );

wp_register_script( 'ev-newsletter', plugins_url( '/js/ev-newsletter.js', __FILE__ ),array('jqueryvalidate'),'1.17.1',true);
wp_enqueue_script( 'ev-newsletter' );

function newsletterCSS() {
    wp_register_style( 'newsletterCSS', plugins_url( '/css/style.css', __FILE__ ), array(), '1.0.0', 'all');
    wp_enqueue_style( 'newsletterCSS' );
}
add_action( 'wp_enqueue_scripts', 'newsletterCSS' );

/* NEWSLETTER */
add_action( 'wp_ajax_nopriv_ajax_load_newsletter_ev', 'my_ajax_load_newsletter_ev' );
add_action( 'wp_ajax_ajax_load_newsletter_ev', 'my_ajax_load_newsletter_ev' );

function my_ajax_load_newsletter_ev() {  
        global $member_newsletter, $actito_newsletter;
        $member_newsletter->set_membre();

        $actito_newsletter->_url = "https://www.actito.be/ActitoWebServices/ws/v4/entity/Ventures/table/ventures/profile";
        $actito_newsletter->_JSON_STRING = $member_newsletter->generateJSONActito();

        $username = 'elle/WebServices_Paul';
        $password = '87BOI8HC';
        $URL= $actito_newsletter->_url;
        $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_STDERR, $fp);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $actito_newsletter->_JSON_STRING ); 

        $result  = curl_exec($ch);
        if (empty($result)) {
            die(curl_error($ch));
            curl_close($ch);
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (empty($info['http_code'])) {
                    die("No HTTP code was returned");
            } else {              
                echo $info['http_code'];
           }
        }
    die();
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_evnewsletter',
    'title' => 'Subscribe Edition Ventures Newsletter (settings)',
    'fields' => array (

        array (
            'key' => 'field_profil_creationSource',
            'label' => 'Creation Source',
            'name' => 'profil_creationSource',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_profil_motherLanguage',
            'label' => 'Mother Language',
            'name' => 'profil_profil_motherLanguage',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_profil_introduction',
            'label' => 'Introduction Inscription Newsletter',
            'name' => 'profil_profil_introduction',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_profil_updateprofil',
            'label' => 'Mise à jour Inscription Newsletter',
            'name' => 'profil_profil_updateprofil',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_droit_legal_newsletter',
            'label' => 'Droits des personnes concernées RPGD',
            'name' => 'droit_legal_newsletter',
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array (
            'key' => 'field_5c6bd11525ad4',
            'label' => 'Abonnements',
            'name' => 'profil_subscriptions',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => '',
            'sub_fields' => array (
                array (
                    'key' => 'field_5c6bd12925ad5',
                    'label' => 'Titre',
                    'name' => 'name',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array (
                    'key' => 'field_5c6bd13b25ad6',
                    'label' => 'Description',
                    'name' => 'description',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
        ),

            array (
            'key' => 'field_evnewsletterjoin',
            'label' => 'Inscription newsletter image',
            'name' => 'inscription_newsletter_image',
            'type' => 'image',
            'instructions' => 'GIF Inscription',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'full',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
            array (
            'key' => 'field_evnewsletterupdate',
            'label' => 'Update newsletter image',
            'name' => 'update_newsletter_image',
            'type' => 'image',
            'instructions' => 'GIF Inscription',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'full',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;

class PageTemplater {

    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * The array of templates that this plugin tracks.
     */
    protected $templates;

    /**
     * Returns an instance of this class. 
     */
    public static function get_instance() {

        if ( null == self::$instance ) {
            self::$instance = new PageTemplater();
        } 

        return self::$instance;

    } 

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {

        $this->templates = array();


        // Add a filter to the attributes metabox to inject template into the cache.
        if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

            // 4.6 and older
            add_filter(
                'page_attributes_dropdown_pages_args',
                array( $this, 'register_project_templates' )
            );

        } else {

            // Add a filter to the wp 4.7 version attributes metabox
            add_filter(
                'theme_page_templates', array( $this, 'add_new_template' )
            );

        }

        // Add a filter to the save post to inject out template into the page cache
        add_filter(
            'wp_insert_post_data', 
            array( $this, 'register_project_templates' ) 
        );


        // Add a filter to the template include to determine if the page has our 
        // template assigned and return it's path
        add_filter(
            'template_include', 
            array( $this, 'view_project_template') 
        );


        // Add your templates to this array.
        $this->templates = array(
            'template-newsletter.php' => 'Edition Ventures 2019 - Inscription newsletter',
            'template-mon-profil.php' => 'Edition Ventures 2019 - Mettre à jour profil',
        );
            
    } 

    /**
     * Adds our template to the page dropdown for v4.7+
     *
     */
    public function add_new_template( $posts_templates ) {
        $posts_templates = array_merge( $posts_templates, $this->templates );
        return $posts_templates;
    }

    /**
     * Adds our template to the pages cache in order to trick WordPress
     * into thinking the template file exists where it doens't really exist.
     */
    public function register_project_templates( $atts ) {

        // Create the key used for the themes cache
        $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

        // Retrieve the cache list. 
        // If it doesn't exist, or it's empty prepare an array
        $templates = wp_get_theme()->get_page_templates();
        if ( empty( $templates ) ) {
            $templates = array();
        } 

        // New cache, therefore remove the old one
        wp_cache_delete( $cache_key , 'themes');

        // Now add our template to the list of templates by merging our templates
        // with the existing templates array from the cache.
        $templates = array_merge( $templates, $this->templates );

        // Add the modified cache to allow WordPress to pick it up for listing
        // available templates
        wp_cache_add( $cache_key, $templates, 'themes', 1800 );

        return $atts;

    } 

    /**
     * Checks if the template is assigned to the page
     */
    public function view_project_template( $template ) {
        
        // Get global post
        global $post;

        // Return template if post is empty
        if ( ! $post ) {
            return $template;
        }

        // Return default template if we don't have a custom one defined
        if ( ! isset( $this->templates[get_post_meta( 
            $post->ID, '_wp_page_template', true 
        )] ) ) {
            return $template;
        } 

        $file = plugin_dir_path( __FILE__ ). get_post_meta( 
            $post->ID, '_wp_page_template', true
        );

        // Just to be safe, we check if the file exist first
        if ( file_exists( $file ) ) {
            return $file;
        } else {
            echo $file;
        }

        // Return template
        return $template;

    }

} 
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );