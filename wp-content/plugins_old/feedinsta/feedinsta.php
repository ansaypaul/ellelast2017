<?php
/**
 * Plugin Name: FeedInsta
 * Description: Tu n'as encore rien vu jeune paupiette! 
 * Version: 1.0.0
 * Author: Elodie Buski & Paul Ansay
 * Author URI: mailto:elodie@editionventures.com mailto:paul@editionventures.com
 */

// Unregister style sheet.
//add_action( 'wp_enqueue_scripts', 'remove_unnecessary_stylesheet', 20 );
if (function_exists('add_theme_support'))
{
    add_theme_support('post-thumbnails');
    /* CURRENT IMAGE */
    add_image_size( 'instagram_linkinbio', 300, 300, true );
}

function feedinsta_remove_unnecessary_stylesheet() {
    wp_dequeue_style( 'html5blank_styles' );
    wp_deregister_style( 'html5blank' );
    wp_dequeue_style( 'swiper' );
    wp_deregister_style( 'swiper' );
    wp_dequeue_style( 'customscrollbar' );
    wp_deregister_style( 'customscrollbar' );
}


function feedinsta_my_remove_wp_seo_meta_box() {
	remove_meta_box('wpseo_meta', 'linkinbio', 'normal');
}
add_action('add_meta_boxes', 'feedinsta_my_remove_wp_seo_meta_box', 100);

function myplugin_register_options_page() {
  add_options_page('Feed Insta Option', 'Feed Insta', 'manage_options', 'myplugin', 'myplugin_options_page');
}
add_action('admin_menu', 'myplugin_register_options_page');

function myplugin_options_page()
{
?>
  <div>
  <?php screen_icon(); ?>
  <h2>Feed Insta Option</h2>
  <form method="post" action="options.php">
  <?php settings_fields( 'myplugin_options_group' ); ?>
  <h3>This is my option</h3>
  <p>Some text here.</p>
  <table>
  <tr valign="top">
  <th scope="row"><label for="myplugin_option_name">Label</label></th>
  <td><input type="text" id="myplugin_option_name" name="myplugin_option_name" value="<?php echo get_option('myplugin_option_name'); ?>" /></td>
  </tr>
  </table>
  <?php  submit_button(); ?>
  </form>
  </div>
<?php
} 

// Register style sheet.
//add_action( 'wp_enqueue_scripts', 'register_plugin_styles' );

function FeedInsta_CSS() {
	wp_register_style( 'FeedInsta_CSS', plugins_url( '/css/linkinbio.min.css', __FILE__ ), array(), '1.0.0', 'all');
	wp_enqueue_style( 'FeedInsta_CSS' );
}
add_action( 'wp_enqueue_scripts', 'FeedInsta_CSS' );

function FeedInsta_JS(){
	wp_register_script( 'FeedInsta_JS',  plugins_url( '/js/linkinbio.js', __FILE__ ), array( 'jquery' ));
	wp_enqueue_script( 'FeedInsta_JS');
	wp_localize_script('FeedInsta_JS', 'myLinkinbioJS', array(
    'pluginsUrl' => plugins_url(),
	));
}
add_action( 'admin_enqueue_scripts', 'FeedInsta_JS' );

//ACF Field call
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5b056b17391f9',
	'title' => 'Link in bio (settings)',
	'fields' => array (
		array (
			'key' => 'field_5b056b276aa43',
			'label' => 'Intro phrase',
			'name' => 'linkinbio_intro_phrase',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_5b056b646aa44',
			'label' => 'Website image',
			'name' => 'linkinbio_website_image',
			'type' => 'image',
			'instructions' => 'PNG height 90px',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

if( function_exists('acf_add_local_field_group') ):
acf_add_local_field_group(array (
	'key' => 'group_5afaabb03464f',
	'title' => 'feed insta',
	'fields' => array (
		array (
			'key' => 'field_instagram_link',
			'label' => 'Instagram link',
			'name' => 'instagram_link',
			'type' => 'url',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array (
			'key' => 'field_5afaabe17cfbe',
			'label' => 'Instagram pic link',
			'name' => 'instagram_pic_link',
			'type' => 'image',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'instagram',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_5afaac1c7cfbf',
			'label' => 'Relative link',
			'name' => 'relative_link',
			'type' => 'url',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'feedinsta',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'discussion',
		3 => 'comments',
		4 => 'author',
		5 => 'format',
		6 => 'page_attributes',
		7 => 'categories',
		8 => 'tags',
		9 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
));

endif;

//Register post type Linkinbio
function feedinsta_register_custom_post_type()
{
    register_post_type(
	  'feedinsta',
	  array(
	    'label' => 'Feed Insta',
	    'labels' => array(
	      'name' => 'Feed Insta',
	      'singular_name' => 'feed insta',
	      'all_items' => __('Tous les feed insta', 'html5blank'),
	      'add_new_item' => __('Ajouter un link in bio', 'html5blank'),
	      'edit_item' => __("Éditer le link in bio", 'html5blank'),
	      'new_item' => __('Nouveau link in bio', 'html5blank'),
	      'view_item' => __("Voir le link in bio", 'html5blank'),
	      'search_items' => __('Rechercher parmi les feed insta', 'html5blank'),
	      'not_found' => __("Pas de feed insta trouvés", 'html5blank'),
	      'not_found_in_trash'=> __("Pas de link in bio dans la corbeille", 'html5blank')
	      ),
	    'public' => true,
	    'capability_type' => 'post',
	    'menu_icon'   => 'dashicons-admin-links',
	    'menu_position' => 6,
	    'supports' => array(
	      'title',
	      'author',
	      'editor',
	      'thumbnail'
	    ),
	    'has_archive' => true,
	  )
	);
}
add_action('init', 'feedinsta_register_custom_post_type');

//Including Single- & Archive- templates
add_filter('single_template','linkinbio_single');
add_filter('archive_template','linkinbio_archive');


function Generate_Featured_Image_feedinsta( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $filename_sanitize = sanitize_file_name($filename);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $check_current_media = post_exists($filename);
    if($check_current_media == false){
        $attach_id = wp_insert_attachment( $attachment, $file );
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
        //$res2= set_post_thumbnail( $post_id, $attach_id );
        return $attach_id;
    }else{
        return $check_current_media;
    }
}

/* INSTAGRAM ADMIN AJAX */
add_action( 'wp_ajax_nopriv_erase_uploaded_images_instagram', 'erase_uploaded_images_instagram' );
add_action( 'wp_ajax_erase_uploaded_images_instagram', 'erase_uploaded_images_instagram' );

function erase_uploaded_images_instagram_feedinsta(){

    $url_insta = "https://api.instagram.com/oembed/?url=".$_POST['url_instagram'];
    $json = file_get_contents($url_insta);
    $json_data = json_decode($json,true);
    $insta_author_name = $json_data['author_name'];
    $insta_title = $json_data['title'];
    $insta_thumb = $json_data['thumbnail_url'];
    $insta_thumb_img_id = Generate_Featured_Image_linkinbio($json_data['thumbnail_url']);
    $json_data['insta_thumb_img_id'] = $insta_thumb_img_id;
    echo json_encode($json_data);
    die();
}

/* INSERT POST FEED INSTA */
add_action( 'wp_ajax_nopriv_feedinsta', 'insert_feedinsta' );
add_action( 'wp_ajax_feedinsta', 'insert_feedinsta' );

function insert_feedinsta(){

    $url_insta = "https://api.instagram.com/oembed/?url=".$_POST['url_instagram'];
    $json = file_get_contents($url_insta);
    $json_data = json_decode($json,true);
    $insta_author_name = $json_data['author_name'];
    $insta_title = $json_data['title'];
    $insta_thumb = $json_data['thumbnail_url'];
    $insta_thumb_img_id = Generate_Featured_Image_linkinbio($json_data['thumbnail_url']);
    $json_data['insta_thumb_img_id'] = $insta_thumb_img_id;
    echo json_encode($json_data);
    die();
}

function insert_feedinsta_post(){
	// Gather post data.
	$my_post = array(
	    'post_title'    => 'Myaaaa post',
	    'post_type' => 'feedinsta',
	    'post_content'  => 'This is my post.',
	    'post_status'   => 'publish',
	    'post_author'   => 1,
	    'post_category' => array( 8,39 )
	);
	 
	// Insert the post into the database.
	$id = wp_insert_post($my_post);

}

//add_action('init','insert_feedinsta_post');