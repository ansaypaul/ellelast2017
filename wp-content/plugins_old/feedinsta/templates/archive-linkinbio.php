<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(''); ?></title>
        <link rel="author" href="humans.txt">
        <?php wp_head(); ?>

		<script type="text/javascript">

		var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>",
        adposition = "<?php echo mb_strtoupper(ad_position()['cat']); ?>",
        adpositionclean = "<?php echo ad_position()['cat']; ?>",
        adpositiongemius = "<?php echo stripAccents(ad_position()['cat']); ?>",
        adpositionpebble = "<?php echo ad_position()['pebble']; ?>",
        adpositionpebble_categories = "<?php echo ad_position()['pebble_categories']; ?>",
        adpositionbis = "<?php echo html_entity_decode(ad_position()['subcat']); ?>",
        adpositionter = "<?php echo html_entity_decode(ad_position_ter()); ?>",
        adpositiontype = "<?php echo ad_position()['type']; ?>",
        io_title_generic = "<?php echo get_page_title_generic(); ?>",
        io_type_generic = "<?php echo get_page_type_generic(); ?>",
        id_post = "<?php echo get_the_ID(); ?>",
        id_author = "<?php the_author(); ?>",
        article_title = "<?php echo html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8' ); ?>",
        article_link = "<?php the_permalink(); ?>",
        article_thumb = "<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>",
        article_category = "<?php echo substr(get_category_article()['list_categories'],0,-1); ?>",
        article_date = "<?php echo get_the_date('r'); ?>",
		article_tag = "<?php echo htmlspecialchars_decode(substr(get_category_article()['list_tags'],0,-1)); ?>",
		article_query_search = "<?php echo get_search_query(); ?>",
        lang_identifiant = "<?php echo get_field('variable_js','option')['lang_identifiant']; ?>",
 		lang_identifiantPebble = "<?php echo get_field('variable_js','option')['lang_identifiantpebble']; ?>",
 		lang_identifiantGoogleAnalytics = "<?php echo get_field('variable_js','option')['lang_identifiantgoogleanalytics']; ?>",
 		id_google_analytics_specifique = "<?php echo get_field('variable_js','option')['id_google_analytics_specifique']; ?>",
 		lang_identifiantGemius = "<?php echo get_field('variable_js','option')['lang_identifiantgemius']; ?>",
 		lang_identifiantFacebookSDK= "<?php echo get_field('variable_js','option')['lang_identifiantfacebooksdk']; ?>",
 		id_facebook_pixel = "<?php echo get_field('variable_js','option')['id_facebook_pixel']; ?>",
 		id_inread_pebble = "<?php echo get_field('variable_js','option')['id_inread_pebble']; ?>",
 		id_category_elletv = "<?php echo get_field('categorie_video','option'); ?>",
 		autopromo_imu_src = "<?php echo get_field('autopromo_imu',autopromo_objet()[0]->ID); ?>",
 		autopromo_lb_src = "<?php echo get_field('autopromo_lb',autopromo_objet()[0]->ID); ?>",
 		autopromo_mlb_src = "<?php echo get_field('autopromo_mlb',autopromo_objet()[0]->ID); ?>",
 		autopromo_link_src = "<?php echo get_field('autopromo_link',autopromo_objet()[0]->ID); ?>",
        height,
        width; 

        var init_GPDR = "<?php echo GDPR ?>";
        console.log('Init de la langue : ' + lang_identifiant);
        </script>
        
    </head>
    <body class="linkinbio">
    	<header id="header" class="header header-linkinbio clear" role="banner">
			<div class="site-inner">
				<a href="<?php echo home_url(); ?>/" class="site-logo">
					<img src="<?php echo get_field('linkinbio_website_image','option' )?>" alt="ELLE logo">
				</a>
			</div>
		</header>
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
		<main role="main" class="main main-linkinbio">
			<div class="site-inner">
				<p><?php echo get_field('linkinbio_intro_phrase','option' )?></p>
				<div class="content entry-content">
					<ul class="insta-wrapper">
					<?php 
						$linkinbio = get_posts(array('post_type' => 'linkinbio', 'posts_per_page'=>30, 'post__not_in' => $excluded_ID, 'orderby' => 'date', 'order' => 'DESC'));
						foreach ( $linkinbio as $post ) : setup_postdata( $post ); $excluded_ID[] = $post->ID;?>
							<li class="insta-block">
								<?php 
					 				$insta_link = get_field( "instagram_pic_link", $post->ID );
					 				$relative_link = get_field( "relative_link", $post->ID );
					 				echo "<a class='insta-item' target='blank_' href='".$relative_link."'><img src=".$insta_link['sizes']['instagram_linkinbio']." alt='".$insta_link["caption"]."'></a>";
					 			?>
							</li>
						<?php endforeach; wp_reset_postdata();?>
					</ul>
				</div>
			</div>
		</main>
		<footer id="footer" class="footer footer-linkinbio" role="contentinfo">
			<div class="site-inner">
				<small>© <a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a> <?php echo date("Y"); ?></small>
			</div>
		</footer>
    </body>
    <?php wp_footer(); ?>
</html>