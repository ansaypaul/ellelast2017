<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              ansaypaul@gmail.com
 * @since             1.0.1
 * @package           Wptester
 *
 * @wordpress-plugin
 * Plugin Name:       Cool Stuff 2018
 * Plugin URI:        wptester
 * Description:       Head section cleanup and many usual custom setting used on every website setp as images settings, privacy and basic admins custimisations.
 * Version:           1.0.1
 * Author:            Ansay Paul
 * Author URI:        ansaypaul@gmail.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wptester
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wptester-activator.php
 */
function activate_wptester() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wptester-activator.php';
	Wptester_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wptester-deactivator.php
 */
function deactivate_wptester() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wptester-deactivator.php';
	Wptester_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wptester' );
register_deactivation_hook( __FILE__, 'deactivate_wptester' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wptester.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.1
 */
function run_wptester() {

	$plugin = new Wptester();
	$plugin->run();

}
run_wptester();