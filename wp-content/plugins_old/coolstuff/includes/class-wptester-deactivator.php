<?php

/**
 * Fired during plugin deactivation
 *
 * @link       ansaypaul@gmail.com
 * @since      1.0.0
 *
 * @package    Wptester
 * @subpackage Wptester/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wptester
 * @subpackage Wptester/includes
 * @author     Ansay Paul <ansaypaul@gmail.com>
 */
class Wptester_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
