<?php

/**
 * Fired during plugin activation
 *
 * @link       ansaypaul@gmail.com
 * @since      1.0.0
 *
 * @package    Wptester
 * @subpackage Wptester/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wptester
 * @subpackage Wptester/includes
 * @author     Ansay Paul <ansaypaul@gmail.com>
 */
class Wptester_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
