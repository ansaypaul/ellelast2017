<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       ansaypaul@gmail.com
 * @since      1.0.0
 *
 * @package    Wptester
 * @subpackage Wptester/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
/**
*
* admin/partials/wptester-supergallery_options.php - Don't add this comment
*
**/
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<script type="text/html" id="tmpl-custom-gallery-setting">
            <h3 style="z-index: -1;">________________________________________________________________________________________________________________________________________________________</h3>
            <h3>SuperGallery Options WpTESTv2</h3>
             <!--<label class="setting">
                <input type="text" value="" data-setting="ds_title" style="float:left;">
            </label>-->

             <!--<label class="setting">
                <span>Photo d'ouverture : (url media http)</span>
                <input type="text" value="" data-setting="ds_text" style="float:left;">
            </label>-->
            
            <label class="setting">
                <span>Nombre d'image à afficher</span>
                <input type="text" value="" data-setting="ds_number_image" style="float:left;"/>
            </label>
            <!--
            <label class="setting">
                <span><?php _e('Number'); ?></span>
                <input type="number" value="" data-setting="ds_number" style="float:left;" min="1" max="9">
            </label>-->
              <label class="setting">
              <span>Template : </span>
              <select data-setting="ds_template">
                <option value="template-all">Template mosaique</option>
                <option value="template-slider">Template slider</option>
              </select>
            </label>

            <label class="setting">
              <span>Une seul image : </span>
              <select data-setting="ds_firstpic">
                <option value="Oui">Oui</option>
                <option value="Non">Non</option>
              </select>
            </label>

            <label class="setting">
              <span>Type : </span>
              <select data-setting="ds_select">
                <option value="classique">Classique</option>
                <!--<option value="swiper">Swiper</option>
                <option value="Mosaique">Mosaïque Shop</option>
               <!-- <option value="Slider">Slider</option> -->
               <!-- <option value="Masonry">Masonry</option>
                <!--<option value="Masonry/fancybox">Masonry/Fancybox</option>-->
                <!-- <option value="fancybox">fancybox</option> -->
               <!-- <option value="Shop">Shop</option>-->
              </select>
            </label>

            <!--
            <label class="setting">
              <span>Orientation : </span>
              <select data-setting="ds_orientation">
                <option value="Horizontal">Horizontal</option>
                <option value="Vertical">Vertical</option>
              </select>
            </label>
            -->

            <!--<label class="Légende">
              <span>Légende : </span>
              <select data-setting="ds_legende">
                <option value="Oui">Oui</option>
                <option value="Non">Non</option>
              </select>
            </label>-->


           <!-- <label class="setting">
                <span><?php _e('Bool'); ?></span>
                <input type="checkbox" data-setting="ds_bool">
            </label>-->

</script>
 <script>

        jQuery(document).ready(function()
            {
                _.extend(wp.media.gallery.defaults, {
                ds_title: 'no text',
                ds_text: 'no text',
                ds_number_image: 1,
                ds_number: "3",
                ds_select: '',
                ds_template: '',
                ds_orientation: '',
                ds_legende: '',
                ds_firstpic: 'Non',
                ds_bool: false,
                ds_text1: 'dummdideldei'
                });

                wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
                template: function(view){
                  return wp.media.template('gallery-settings')(view)
                       + wp.media.template('custom-gallery-setting')(view);
                }
                });

            });

</script>