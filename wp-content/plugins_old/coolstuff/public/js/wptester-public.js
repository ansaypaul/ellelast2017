(function( $ ) {
	'use strict';
	 	 $(function() {
			// init Masonry
			var $grid = $('.grid').imagesLoaded( function() {
			  // init Masonry after all images have loaded
			  $grid.masonry({
			  				itemSelector: '.grid-item',
			  });
			});
		});

})( jQuery );
