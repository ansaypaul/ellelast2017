<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       ansaypaul@gmail.com
 * @since      1.0.0
 *
 * @package    Wptester
 * @subpackage Wptester/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wptester
 * @subpackage Wptester/public
 * @author     Ansay Paul <ansaypaul@gmail.com>
 */
class Wptester_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->wp_cbf_options = get_option($this->plugin_name);
		//var_dump($this->wp_cbf_options );
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wptester_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wptester_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wptester-public.css', array(), $this->version, 'all' );

	}

    public function enqueue_styles_extensions() {
        //wp_enqueue_style( $this->plugin_name.'elle_gallery_2017', plugin_dir_url( __FILE__ ) . 'css/elle_gallery_2017.css', array(), $this->version, 'all' );
    }

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wptester-public.js', array( 'jquery' ), $this->version, true );

	}

    public function enqueue_scripts_extensions() {
      global $post;
      if (strpos($post->post_content,'[gallery') !== false || 1 == 1){
               /* wp_enqueue_script( 'masonry',  plugins_url( 'extensions/masonry/masonry.pkgd.min.js', __FILE__ ), array( 'fotterscript' ),$this->version,true);
                wp_enqueue_script( 'fancybox', plugins_url( 'extensions/fancybox/js/jquery.fancybox.min.js', __FILE__ ), array( 'fotterscript' ),$this->version,true);
                wp_enqueue_script( 'fancybox-main', plugins_url( 'extensions/fancybox/js/main.js', __FILE__ ),  array( 'fotterscript' ), $this->version,true);*
                //wp_enqueue_script( 'fancybox',  plugins_url( 'extensions/fancybox/js/jquery.fancybox.js', __FILE__ ), array( 'jquery' ),$this->version,true);
                //wp_enqueue_script( 'ImagesLoaded',  plugins_url( 'extensions/masonry/imagesloaded.pkgd.js', __FILE__ ), array( 'jquery' ));        */      
        }
    }

    public function pass_phpvar_to_js(){ 

        }
    /**
     * Cleanup functions depending on each checkbox returned value in admin
     *
     * @since    1.0.0
     */
    // Cleanup head
    public function wp_cbf_cleanup() {

        if($this->wp_cbf_options['cleanup']){

            remove_action( 'wp_head', 'rsd_link' );                 // RSD link
            remove_action( 'wp_head', 'feed_links_extra', 3 );            // Category feed link
            remove_action( 'wp_head', 'feed_links', 2 );                // Post and comment feed links
            remove_action( 'wp_head', 'index_rel_link' );
            remove_action( 'wp_head', 'wlwmanifest_link' );
            remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );        // Parent rel link
            remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );       // Start post rel link
            remove_action( 'wp_head', 'rel_canonical', 10, 0 );
            remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
            remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Adjacent post rel link
            remove_action( 'wp_head', 'wp_generator' );               // WP Version
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            //remove_action( 'admin_bar_menu', 'wp_admin_bar_wp_menu', 10, 0 );

        }
    }   
    // Cleanup head
    public function wp_cbf_remove_x_pingback($headers) {
        if(!empty($this->wp_cbf_options['cleanup'])){
            unset($headers['X-Pingback']);
            return $headers;
        }
    }

    // Remove Comment inline CSS
    public function wp_cbf_remove_comments_inline_styles() {
        if(!empty($this->wp_cbf_options['comments_css_cleanup'])){
            global $wp_widget_factory;
            if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
                remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
            }

            if ( isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments']) ) {
                remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
            }
        }
    }

    // Remove gallery inline CSS
    public function wp_cbf_remove_gallery_styles($css) {
        if(!empty($this->wp_cbf_options['gallery_css_cleanup'])){
            return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
        }

    }

    // Add post/page slug
    public function wp_cbf_body_class_slug( $classes ) {
        if(!empty($this->wp_cbf_options['body_class_slug'])){
            global $post;
            if(is_singular()){
                $classes[] = $post->post_name;
            }
            $classes[] = 'test';
        }
        //var_dump($classes);
        return $classes;
    }

    // Hide admin bar
    public function wp_cbf_hide_admin() {
        if(!empty($this->wp_cbf_options['hide_admin_bar'])){
           add_filter('show_admin_bar', '__return_false');
           }
    }

    // Load jQuery from CDN if available
    public function wp_cbf_cdn_jquery(){

    }

    public function SuperGalleryInit(){
        $options_supergallery = get_option('wptester');
        $login_logo_id = $options_supergallery['login_logo_id'];
        $login_logo = wp_get_attachment_image_src( $login_logo_id, 'full' );
        $login_logo_url = $login_logo[0];
        echo '<script>var supergallery_logo = "'.$login_logo_url.'"</script>';
    }

/*

    public function wp_get_attachment( $attachment_id ) {
      $attachment = get_post( $attachment_id );
      if(isset($attachment)){
          return array(
              'number_index' => '',
              'id' => $attachment->ID,
              'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
              'caption' => $attachment->post_excerpt,
              'description' => $attachment->post_content,
              'href' => get_permalink( $attachment->ID ),
              'src' => $attachment->guid,
              'title' => $attachment->post_title,
              'desc_rich' => get_post_meta( $attachment->ID, 'desc_rich', true ),
              'shop_link' => get_post_meta( $attachment->ID, 'shop_link', true ),
              'shop_price' => get_post_meta( $attachment->ID, 'shop_price', true ),
              'gallery_link_url' => get_post_meta( $attachment->ID, '_gallery_link_url', true ),
              'gallery_pic-thumb' => wp_get_attachment_image_src( $attachment->ID, 'small-gallerie' )[0],
              'slug'=> basename(get_permalink( $attachment->ID )),
              'srcset'=> elle_get_picture_srcs($attachment->ID),
              'cta_title'=> get_post_meta( $attachment->ID, 'cta_title', true ),
          );
        }
    }*/


    // Galeries
    public function wptester_post_gallery($output, $attr) {
        global $post, $wp_locale, $idgallery;

        $attr['columns'] = (isset($attr['columns']) && !empty($attr['columns'])) ? $attr['columns']  : 0;
        if($attr['columns'] == null){ $attr['columns'] = 3; }
        $attr['ds_firstpic'] = (isset($attr['ds_firstpic']) && !empty($attr['ds_firstpic'])) ? $attr['ds_firstpic'] : array();
        $ids = explode( ',', $attr['ids'] );
        $output = '';
        $i = 0;
        setup_postdata( $post );
        $idgallery++;

        if(is_page_template( 'template-amp-2.php' )){  
        $output = '<h2 class="h3 mb1">Gallerie</h2><amp-carousel width="500" height="550" layout="responsive" type="slides" class="mb4">';
        while ($i <= $attr['ds_number_image']) {
        $pic = $this->wp_get_attachment($ids[$i]);
        //var_dump($pic);
          $output .= '
          <figure class="ampstart-image-with-caption m0 relative mb4">
            <amp-img src="'.$pic['gallery_pic-thumb'].'" width="500" height="500" alt="" layout="responsive" class=""></amp-img>
            <figcaption class="h5 mt1 px3">'.$pic['caption'].'</figcaption>
          </figure>';
           $i++;
        }
        $output .= '</amp-carousel>';
        return $output;
        //break;
        }


    if(!is_page_template( 'template-amp-2.php' )){
        //var_dump($attr);
        switch ($attr['ds_template']) {
            case 'template-1col':
            $output .= '<div class="'.$attr['ds_template'].'">';
            $legend = '';
            $output .= '<div class="swiper-gallery"><div class="swiper-container-gallery">';
            $output .= '<div class="swiper-wrapper">';
            while ($i <= $attr['ds_number_image']) {
                    if ($i%9 == 0 || $i == 0){
                            $output .= '<div class="swiper-slide">';
                    }
                    $pic = $this->wp_get_attachment($ids[$i]);
                    if ($i%3 == 0 || $i == 0){
                            $output .= '<div class="row">';
                    }

                    $output .= '<div class="grid-item" >';      
                    $output .= '<picture>
                            <!--[if IE 9]><video style="display: none;"><![endif]-->'
                            . elle_get_picture_srcs_nolazyload( $ids[$i]).
                            '<!--[if IE 9]></video><![endif]-->
                            <img data-fancybox="gallery" href="'.$pic['src'].'" data-srcset="" alt="">
                            </picture>';
                    $output .=  '</div>';  
                    $i++;

                    if ($i%3 == 0 || $i == 0){
                            $output .= '</div>';
                    }
                    if ($i%9 == 0 || $i == $attr['ds_number_image']){
                            $output .= '</div>';
                    }
                }

            $output .= '</div></div></div>';
            $output .= '</div><div class="modele-gallery"></div>';
            return $output;
            break;

        	case 'template-3col':
            $output .= '<div class="'.$attr['ds_template'].'">';
            $legend = '';
            $output .= '<div class="swiper-gallery"><div class="swiper-container-gallery">';
            $output .= '<div class="swiper-wrapper">';
			$i = 0;
            while ($i <= $attr['ds_number_image']) {
	            	if ($i%9 == 0 || $i == 0){
							$output .= '<div class="swiper-slide">';
	            	}
	                $pic = $this->wp_get_attachment($ids[$i]);
					if ($i%3 == 0 || $i == 0){
							$output .= '<div class="row">';
	            	}

	                $output .= '<div class="grid-item" >';      
	                $output .= '<picture>
	                		<!--[if IE 9]><video style="display: none;"><![endif]-->'
	                		. elle_get_picture_srcs_nolazyload( $ids[$i]).
	                		'<!--[if IE 9]></video><![endif]-->
	                		<img data-fancybox="gallery" href="'.$pic['src'].'" data-srcset="" alt="">
	                		</picture>';
	                $output .=  '</div>';  
	                $i++;

					if ($i%3 == 0 || $i == 0){
							$output .= '</div>';
	            	}
	                if ($i%9 == 0 || $i == $attr['ds_number_image']){
							$output .= '</div>';
	            	}
                }

            $output .= '</div></div></div>';
            $output .= '</div><div class="modele-gallery"></div>';
            return $output;
        	break;

        	case 'ajax':
        		$i = 0;
        		while ($i <= $attr['ds_number_image']) {
						$pics[$i] = $this->wp_get_attachment($ids[$i]);
						$i++;
	       		}
        	$output .= "<script>var gallery_ajax ='".json_encode($pics)."';</script>";
        	$output .= '<div class="gallery_ajax">gallery_ajax</div>';
        	return $output;

        	default:
        	$i = 0;$slider = 0;$row = 0;
        	while ($i < $attr['ds_number_image']) {
                    $picture = $this->wp_get_attachment($ids[$i]);
                    $picture['number_index'] = $i + 1;
					$data[] = $picture;
					$i++;
	       	}

           
            $data_slider = array_chunk($data, 9);
            $number_array = count($data_slider) -1;
            for($i = 0; $i <= $number_array; $i++) {
                $data_slider['picture'][$i] = array_chunk($data_slider[$i], 3);
            }


            $idgallery = rand(1,1000000);
            $data_slider['id_gallery'] = $idgallery;
            $output .= '<div data-id="'.$idgallery.'" class="gallery_wrapper">';
        	$output .= "<script>var gallery_ajax_".$idgallery." =".json_encode($data_slider).";</script>";
            $output .= '<div class="buttons_wrapper"><div class="gallery_index"><div><span class="gallery_index_current gallery_index_current_'.$idgallery.'">1</span><span class="gallery_index_total gallery_index_total_'.$idgallery.'"></span></div></div>';
            $output .= '<button class="button_mosaique">Mosaique</button><button data-image="0" class="button_open_fancy">Open fancybox</button></div> ';
        	$output .= '<div class="gallery_ajax gallery_ajax_'.$idgallery.'"></div>';
            $output .= '</div>';
          	return $output;
        	break;
        }
    }
    	return $output;
    }
}