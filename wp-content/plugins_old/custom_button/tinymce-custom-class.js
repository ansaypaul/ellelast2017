(function() {
    tinymce.PluginManager.add( 'custom_class', function( editor, url ) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('custom_class', {
            title: 'Insert CSS Class',
            cmd: 'custom_class',
            image: url + '/icon.png',
        });

        // Add Command when Button Clicked
        editor.addCommand('custom_class', function() {
            editor.focus();
            selection = tinyMCE.activeEditor.selection.getContent();
            tinyMCE.activeEditor.selection.setContent("<span class='multipage_block'> [multipage_block] <span>");
        });
    });
})();