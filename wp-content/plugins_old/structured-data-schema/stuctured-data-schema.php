<?php
/**
 * Plugin Name: Stuctured data schema
 * Description: Que le web soit avec toi!
 * Version: 1.0.0
 * Author: Elodie Buski & Paul Ansay
 * Author URI: mailto:elodie@editionventures.com mailto:paul@editionventures.com
 */

function awesome_excerpt($text, $raw_excerpt) {
    if( ! $raw_excerpt ) {
        $content = apply_filters( 'the_content', get_the_content() );
        $text = substr( $content, 0, strpos( $content, '</p>' ) + 4 );
    }    
    return $text;
}

add_action( 'wp_head',function(){
	$site_url = get_bloginfo('url');
	$site_name = get_bloginfo('name');
	$article_title = html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8');
	$article_thumb = get_the_post_thumbnail_url();
	$article_excerpt = html_entity_decode(strip_tags(awesome_excerpt()));
	$content = strip_tags(get_the_content());
	$article_id = get_the_ID();
	$permalink = get_the_permalink();
	$post_tags = get_the_tags();$list_tags_str = '';
	$word_count = str_word_count( strip_tags( get_post_field( 'post_content', $article_id ) ) );

    foreach( $post_tags as $tag ) {
    	$list_tag = ucfirst($tag->name);
    	$article_tags .= $list_tag.', '; 
    } 
    $list_tags_str = substr($list_tags_str, 0, -2).'.';
	$date_modified = get_the_modified_time('Y-m-d\TH:i:sO');
	$date_published = get_the_time('Y-m-d\TH:i:sO');;
	$author_name = get_the_author();
	$author_url = esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );
	$author_jobtitle = get_the_author_meta( 'authortitle' );
	$recipe_ID =  get_field('st_id','option')['sd_recette_id'];
	$recipepreptime = explode(':',get_field( "recipe-preptime" )); 
	$recipepreptime_structure = "00";
		if(count($recipepreptime) > 1){
			$recipepreptime_heure = intval($recipepreptime[0]);
			$recipepreptime_min = intval($recipepreptime[1]);
			$recipepreptime_s = intval($recipepreptime[2]);
			$recipepreptime_structure = $recipepreptime_heure * 60 + $recipepreptime_min + ($recipepreptime_s / 60);
		}
	$recipecooktime = explode(':',get_field( "recipe-cooktime" )); 
	$recipecooktime_structure = "00";
	if(count($recipecooktime) > 1){
		$recipecooktime_heure = intval($recipecooktime[0]);
		$recipecooktime_min = intval($recipecooktime[1]);
		$recipecooktime_s  = intval($recipecooktime[2]);
		$recipecooktime_structure = $recipecooktime_heure * 60 + $recipecooktime_min + ($recipecooktime_s / 60);
				}
	$recipetotaltime = $recipepreptime_structure+$recipecooktime_structure;
	$recipeintro = get_field( "recipe-intro" );
	$recipeyield = get_field( "recipe-yield" ); 
	$recipedifficulties = get_field( "recipe-difficulties" );
	$recipetypes = get_field( "recipe-type");
		foreach( $recipetypes as $recipetype ): 
			$list_category = ucfirst($recipetype);
			$recipe_category .= $list_category.', ';
		endforeach;
	$recipetypes = get_field( "recipe-ingredients");
	//$diy_ID = get_field('st_id','option')['sd_diy_id']; 

	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image_logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	
	if (in_category($recipe_ID)) { /*
	?>
		<script type="application/ld+json">
		{
		  "@context": "http://schema.org/",
	      "@type": "Recipe",
	      "name": "<?php echo $article_title; ?>",
	      "image": [
		    "<?php echo $article_thumb; ?>"
		  ],
	      "author": {
		    "@type": "Person",
		    "name": "<?php echo $author_name; ?>",
		    "url": "<?php echo $author_url; ?>",
		    "jobTitle": "<?php echo $author_jobtitle; ?>"
		  },
	      "datePublished": "<?php echo $date_modified; ?>",
	      "dateModified": "<?php echo $date_published; ?>",
	      "description": "<?php echo $recipeintro; ?>",
	      "prepTime": "PT<?php echo $recipepreptime_structure; ?>M",
	      "cookTime": "PT<?php echo $recipecooktime_structure; ?>M",
	      "totalTime": "PT<?php echo $recipetotaltime; ?>M",
	      "keywords": "<?php echo substr($article_tags, 0, -2); ?>",
	      "recipeYield": "<?php echo $recipeyield; ?>",
	      "recipeCategory": "<?php echo substr($recipe_category, 0, -2); ?>",
	      "recipeIngredient": [
	        "2 cups of flour",
	        "3/4 cup white sugar",
	        "2 teaspoons baking powder",
	        "1/2 teaspoon salt",
	        "1/2 cup butter",
	        "2 eggs",
	        "3/4 cup milk"
	       ],
	      "recipeInstructions": [
	          {
	          "@type": "HowToStep",
	          "text": "Preheat the oven to 350 degrees F. Grease and flour a 9x9 inch pan."
	          },
	          {
	          "@type": "HowToStep",
	          "text": "In a large bowl, combine flour, sugar, baking powder, and salt."
	          },
	          {
	          "@type": "HowToStep",
	          "text": "Mix in the butter, eggs, and milk."
	          },
	          {
	          "@type": "HowToStep",
	          "text": "Spread into the prepared pan."
	          },
	          {
	          "@type": "HowToStep",
	          "text": "Bake for 30 to 35 minutes, or until firm."
	          },
	          {
	          "@type": "HowToStep",
	          "text": "Allow to cool."
	         }
	      ],
	        "author": {
			    "@type": "Person",
			    "name": "<?php echo $author_name; ?>",
			    "url": "<?php echo $author_url; ?>",
			    "jobTitle": "<?php echo $author_jobtitle; ?>"
			  },
	        "datePublished": "<?php echo $date_modified; ?>",
	        "publisher": {
			    "@type": "Organization",
			    "name": "<?php echo $site_name; ?>",
			    "logo": {
			      "@type": "ImageObject",
			      "url": "<?php echo get_field('sd_site_logo','option' ) ?>"
			    }
			}
		}
		</script>
	<?php */

	} else if(is_single()){ ?>
			<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Article",
			  "url": "<?php echo $permalink; ?>",
			  "mainEntityOfPage": {
			    "@type": "WebPage",
			    "@id": "<?php echo $permalink; ?>"
			  },
			  "headline": <?php echo json_encode($article_title); ?>,
			  "image": {
				  "@type": "ImageObject",
				  "url": "<?php echo $article_thumb; ?>"
				},
			  "description": <?php echo json_encode($article_excerpt); ?>,
			  "articleBody": <?php echo json_encode($content); ?>,
			  "datePublished": "<?php echo $date_modified; ?>",
			  "dateModified": "<?php echo $date_published; ?>",
			  "author": {
			    "@type": "Person",
			    "name": "<?php echo $author_name; ?>",
			    "url": "<?php echo $author_url; ?>",
			    "jobTitle": <?php echo json_encode($author_jobtitle); ?>
			  },
			   "publisher": {
			    "@type": "Organization",
			    "name": "<?php echo $site_name; ?>",
			    "logo": {
			      "@type": "ImageObject",
			      "url": "<?php echo get_field('sd_site_logo','option' ); ?>"
			    }
			  }
			}
			</script>
		<?php } else {
	}
});

// ACF-OPTIONS
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5b1122c60fb9c',
	'title' => 'Structured data schema',
	'fields' => array (
		array (
			'key' => 'field_5b1122d87fe58',
			'label' => 'Site Logo',
			'name' => 'sd_site_logo',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => 200,
			'min_height' => 200,
			'min_size' => '',
			'max_width' => 200,
			'max_height' => 200,
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_5b112914de062',
			'label' => 'Stuctured_Data_ID',
			'name' => 'st_id',
			'type' => 'group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layout' => 'table',
			'sub_fields' => array (
				array (
					'key' => 'field_5b112a3df1fae',
					'label' => 'Fiche DIY ID',
					'name' => 'sd_diy_id',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5b11296dde063',
					'label' => 'Fiche recette ID',
					'name' => 'sd_recette_id',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
