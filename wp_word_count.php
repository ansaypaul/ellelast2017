<?php 
require_once("wp-load.php");

function bac_post_word_count()
{
    global $post;
    //Variable: Additional characters which will be considered as a 'word'
    $char_list = 'span'; /** MODIFY IF YOU LIKE.  Add characters inside the single quotes. **/
    //$char_list = '0123456789'; /** If you want to count numbers as 'words' **/
    //$char_list = '&@'; /** If you want count certain symbols as 'words' **/
   
    $content = apply_filters( 'the_content', $post->post_content ); 
    $word_number = substr_count( $content, ' ' );
    echo '<a href="'.$post->guid.'">'.$post->post_title.'</a> : '.$word_number.'<br/>';
}


$posts_popular = get_posts(array('posts_per_page' => 1000, 'orderby' => 'meta_value_num', 'order' => 'DESC'));
 foreach ( $posts_popular as $post ) : setup_postdata( $post ); 
	bac_post_word_count();
 endforeach; wp_reset_postdata(); 
