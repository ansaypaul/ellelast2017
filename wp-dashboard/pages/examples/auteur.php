<?php
require_once("../../../wp-load.php");
$users = new ArrayObject();   
$url_website = get_site_url();
if($url_website== 'https://www.elle.be/fr' ){
  $users = array(3,4,8,13,65,51,6,71,46);
}elseif($url_website== 'https://www.elle.be/nl'){
  $users = array(4,5,59,52,24,27,28,25,26);
}elseif($url_website== 'http://marieclaire.be/fr'){
  $users = array(2,5,6,7,8,19,26,28,31);
}elseif($url_website== 'http://marieclaire.be/nl'){
  $users = array(2,7,13,15,23,27,30,34);
}else{
  $users = array(1);
}

$months_years = array('10:2016','10:2017','11:2017','12:2017','01:2018','02:2018','03:2018');
$full_article = array(); 

class User // Présence du mot-clé class suivi du nom de la classe.
{
  public $_articles = array();        
  public $_firstname = '';
  public $_countArticle ='';
  public $_quota = 0;
  public $_ID = 0;
  public $_totalArticle = 0;
  public $_totalPageview = 0;

  public function set_user($_firstname,$_quota,$_ID){
  $this->_firstname = $_firstname;
  $this->_ID = $_ID;
  $this->_quota = $_quota;
  }

  public function count_article($months_years,$ID){
      foreach ($months_years as $month){

        $data = explode(":",$month);
      $month = $data[0];
      $year =  $data[1];

      $args = array(
      'year' => $year,
      'monthnum' => $month,
      'posts_per_page' => -1,
      'author'=> $ID,
      'post_status' => 'publish');
      $posts = query_posts($args);
      $count = count($posts);
      $pageview = 0;
      $pageview_article = 0;

      foreach ( $posts as $post ) : setup_postdata( $post );
        $pageview_article = get_post_meta($post->ID, 'wpb_post_views_count', true);
        $pageview += $pageview_article;
        $post->pageview = $pageview_article;
        $post->category = get_the_category($post->ID);
        $this->_articles[$year][$month]['article'][] = $post;
      endforeach; wp_reset_postdata();

      $this->_articles[$year][$month]['pageview'] = $pageview;
      $this->_articles[$year][$month]['count'] = $count;
      $this->_totalArticle += $count;
      $this->_totalPageview += $pageview;
      } 
    }
}

$blogusers = get_users_of_blog();
foreach ($blogusers as $bloguser) 
{
  if(in_array($bloguser->user_id, $users)){
    $userdata = get_userdata($bloguser->user_id); 
    if($userdata->user_firstname !=''){
      $user = new User;
      $user->set_user($userdata->user_firstname.' '.$userdata->user_lastname, get_usermeta( $bloguser->user_id, 'quota' ) ,$bloguser->user_id);
      $user->count_article($months_years,$bloguser->user_id);
      $users_data[] = $user;
    }
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ELLE Belgique | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Dashboard</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include('nav-bar.php') ?>
  </header>

  <!-- =============================================== -->

  <?php include('sidebar-main.php') ?>
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ELLE Belgique
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">ELLE Belgique</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Liste auteurs</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="box">
            <table border="1" id="author" class="table table-users table-bordered table-hover">
            <thead>
              <tr id="thead">
                <th>Auteur</th>
                <th>Quota</th>
              </tr>
            </thead>
              <tbody id="tbody">

              </tbody>
            </table>
          </div>

          <div class="box">
            <table border="1" id="author" class="table table-articles table-bordered table-hover">
            <thead>
              <tr id="thead">
                <th width="70%">Titre article</th>
                <th width="10%">Categorie</th>
                <th width="5%">Page vues</th>
                <th width="10%">Date</th>
                <th width="5%">Link</th>
              </tr>
            </thead>
              <tbody id="tbody">

              </tbody>
            </table>
          </div>
        </ul>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 : Ansay Paul</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script>
/*
  var dataObj = [];
  function get_JSON_posts(page){
      $.getJSON( "https://www.elle.be/fr/wp-json/wp/v2/posts/?per_page=100&page="+i, function( data ) {
        console.log(data);
        $.merge(dataObj,data);
      });
    }

  $(document).ready(function () {
    $('.sidebar-menu').tree();
    i = 1 ;
    while (i < 3) {
        get_JSON_posts(i);
        i++;
    }

    $('.testJson').click(function() {
      console.log(dataObj);
    });

  })*/
</script>

        <script>
          var data = <?php echo json_encode($users_data)?>;
          console.log(data);
          console.log(data[0]['_articles']['2018']['02']['article']);
           var months_years =  ['10:2016','10:2017','11:2017','12:2017','01:2018','02:2018','03:2018'];

          function show_article(i_year,i_month){
            console.log(data[0]['_articles'][i_year][i_month]['article']);
          }

          /*function get_JSON_posts(page){
            $.getJSON( "https://www.elle.be/fr/wp-json/wp/v2/posts/?per_page=100&page="+page, function( data ) {
              console.log(data);
            });
          }*/


          $(function() {
            $.each(months_years, function(i, item) {
                var $th = $('<th colspan="2">').append(item);
                $th.appendTo('.table-users #thead');
            });

            $.each(data, function(i, item) {

                var $tr = $('<tr>').append(
                    $('<td>').text(item._firstname),
                    $('<td>').text(item._quota),
                ); 
                console.log(item);
                $.each(item['_articles'], function(i_year, item_c) {
                  $.each(item_c, function(i_month, item_d) {
                      $tr.append(
                        $('<td class="s_article" data-userid="'+i+'" data-month="'+i_month+'" data-year="'+i_year+'">').text(item_d['count']),
                        $('<td class="s_article" data-userid="'+i+'" data-month="'+i_month+'" data-year="'+i_year+'">').text(item_d['pageview']),
                      ); 
                  });
                });

                $tr.appendTo('.table-users #tbody');
            });

          $('.s_article').click(function() {
              console.log(data[$(this).data("userid")]['_articles'][$(this).data("year")][$(this).data("month")]['article']);
              var liste_article = data[$(this).data("userid")]['_articles'][$(this).data("year")][$(this).data("month")]['article'];
              $('.table-articles #tbody').html('');
              $.each(liste_article, function(i, item) {
                $('.table-articles #tbody').append('<tr><td>'+item['post_title']+'</td><td>'+item['category'][0]['cat_name']+'</td><td>'+item['pageview']+'</td><td>'+item['post_date']+'</td><td><a href="'+item['guid']+'">Link</a></td></tr>')
              });

          });


        });



        </script>

<style>
  table {cursor: pointer;}
</style>

</body>
</html>
