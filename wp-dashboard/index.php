<?php
session_start();
require_once("../wp-load.php");
if(isset($_POST['hash']) &&  $_POST['hash'] == 'waterloo69' || isset($_SESSION['hash'])){
  if(isset($_POST['hash'])){ $_SESSION['hash'] = $_POST['hash']; }
  $users = new ArrayObject();   
  $url_website = get_site_url();
  if($url_website== 'https://www.elle.be/fr' ){
    $website_title = 'ELLE Belgique FR';
    $users = array(3,4,8,13,65,51,71,46,6);
  }elseif($url_website== 'https://www.elle.be/nl'){
    $website_title = 'ELLE Belgique NL';
    $users = array(4,5,59,52,24,27,28,25,26);
  }elseif($url_website== 'http://marieclaire.be/fr'){
    $website_title = 'MarieClaire FR';
    $users = array(2,5,6,7,8,19,26,28,31);
  }elseif($url_website== 'http://marieclaire.be/nl'){
    $website_title = 'MarieClaire NL';
    $users = array(2,7,13,15,23,27,30,34);
  }else{
    $website_title = 'ELLE Belgique FR';
    $users = array(1);
  }


    $date_array[] = date('m:Y');
    for ($i = 1; $i < 6; $i++) {
      array_unshift($date_array,date('m:Y', strtotime("-$i month")));
    }

  $months_years = $date_array;
  $full_article = array(); 

  class User // Présence du mot-clé class suivi du nom de la classe.
  {
    public $_articles = array();        
    public $_firstname = '';
    public $_countArticle ='';
    public $_quota = 0;
    public $_quotavideo = 0;
    public $_ID = 0;
    public $_totalArticle = 0;
    public $_totalPageview = 0;

    public function set_user($_firstname,$_quotavideo,$_quota,$_ID){
    $this->_firstname = $_firstname;
    $this->_ID = $_ID;
    $this->_quota = $_quota;
    $this->_quotavideo = $_quotavideo;
    }

    public function count_article($months_years,$ID){
        foreach ($months_years as $month){

        $data = explode(":",$month);
        $month = $data[0];
        $year =  $data[1];

        /*$args = array(
        'year' => $year,
        'monthnum' => $month,
        'posts_per_page' => -1,
        'author'=> $ID,
        'post_status' => 'publish',
        'orderby'=> 'title', 
        'order' => 'ASC' );*/

        $args = array(
        'year' => $year,
        'monthnum' => $month,
        'meta_key' => 'date_de_creation', // name of custom field
        'author'=> $ID,
        'posts_per_page' => -1,
        'orderby'=> 'title',
        'order' => 'ASC', 
              'meta_query' => array(
                  'relation' => 'AND',
                  array(
                      'key' => 'date_de_creation',
                      'value' => $year.$month.'01',
                      'compare' => '>=',
                  ),
                  array(
                      'key' => 'date_de_creation',
                      'value' => $year.$month.'31',
                      'compare' => '<=',
                  ),
              ),
          );


        $args = array(
        'year' => $year,
        'monthnum' => $month,
        'posts_per_page' => -1,
        'author'=> $ID,
        'post_status' => 'publish',
        'orderby'=> 'title', 
        'order' => 'ASC' );

        $posts = query_posts($args);
        $count = count($posts);
        $count_video = 0;
        $pageview = 0;
        $pageview_article = 0;

        foreach ( $posts as $post ) : setup_postdata( $post );
          $pageview_article = get_post_meta($post->ID, 'wpb_post_views_count', true);
          $pageview += $pageview_article;
          $post->pageview = $pageview_article;
          $post->category = get_the_category($post->ID);
          $post->date_creation = get_post_meta($post->ID, 'date_de_creation', true);
          $this->_articles[$year][$month]['article'][] = $post;
          $array_category = array(); 
          foreach($post->category as $category){
            $array_category[] = $category->slug;
          }

          /**** SPECIAL ELLE TV ****/
          if(in_array('elle-tv',$array_category)){ $count_video++; }

        endforeach; wp_reset_postdata();

        if($count_video > 0){ $this->_articles[$year][$month]['count_video'] = $count_video; } 
        $this->_articles[$year][$month]['pageview'] = $pageview;
        $this->_articles[$year][$month]['count'] = $count;
        $this->_totalArticle += $count;
        $this->_totalPageview += $pageview;
        } 
      }
  }

  foreach ($users as $user_id) 
  {
      $userdata = get_userdata($user_id); 
      if($userdata->user_firstname !=''){
        $user = new User;
        $user->set_user($userdata->user_firstname.' '.$userdata->user_lastname, get_usermeta( $user_id, 'quotavideo' ), get_usermeta( $user_id, 'quota' ) ,$user_id);
        $user->count_article($months_years,$user_id);
        $users_data[] = $user;
      }
    }
}else{

}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ELLE Belgique | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Dashboard</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <?php include('nav-bar.php') ?>
  </header>

  <!-- =============================================== -->

  <?php include('sidebar-main.php') ?>
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $website_title ?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $website_title ?></a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
        <?php if((!isset($_POST['hash']) ||  $_POST['hash'] != 'waterloo69') && !isset($_SESSION['hash'])){ echo $_SESSION['hash']; ?>
         <form role="form" method="POST">
              <div class="box-body">
                <div class="form-group">
                <label for="inputEmail3" class="col-xs-1 control-label">Password :</label>
                <div class="col-xs-3">
                 <div class="input-group input-group-sm">
                    <input name="hash" class="form-control" type="text">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                        </span>
                  </div>
                </div>
                </div>
          </div>
          </form>
        <?php } else { ?>
          <h3 class="box-title">Liste auteurs</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="box">
            <table border="1" id="author" class="table table-users table-bordered table-hover">
            <thead>
              <tr id="thead">
                <th>Auteur</th>
                <th>Quota</th>
                <th>Video</th>
              </tr>
            </thead>
              <tbody id="tbody">

              </tbody>
            </table>
          </div>

          <div class="box">
            <table border="1" id="articles" class="table table-articles table-bordered table-hover">
            <thead>
              <tr id="thead">
                <th width="60%">Titre article</th>
                <th width="10%">Categorie</th>
                <th width="5%">Page vues</th>
                <th width="10%">Date création</th>
                <th width="10%">Date publication</th>
                <th width="5%">Link</th>
              </tr>
            </thead>
              <tbody id="tbody">

              </tbody>
            </table>
          </div>
        </div>
        <?php } ?>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 : Ansay Paul</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<script>
/*
  var dataObj = [];
  function get_JSON_posts(page){
      $.getJSON( "https://www.elle.be/fr/wp-json/wp/v2/posts/?per_page=100&page="+i, function( data ) {
        console.log(data);
        $.merge(dataObj,data);
      });
    }

  $(document).ready(function () {
    $('.sidebar-menu').tree();
    i = 1 ;
    while (i < 3) {
        get_JSON_posts(i);
        i++;
    }

    $('.testJson').click(function() {
      console.log(dataObj);
    });

  })*/
</script>

        <script>
          var data = <?php echo json_encode($users_data)?>;
          console.log(data);
          console.log(data[0]['_articles']['2018']['02']['article']);

           var months_years = <?php echo json_encode($date_array); ?>;

          function show_article(i_year,i_month){
            console.log(data[0]['_articles'][i_year][i_month]['article']);
          }

          $(document).ready(function () {
            $.each(months_years, function(i, item) {
                var res = item.split(":");
                var $th = $('<th class="text-center">').append(res[0] + '/' + res[1]);
                $th.appendTo('.table-users #thead');
            });

            $.each(data, function(i, item) {
                    var $tr = $('<tr>').append(
                    $('<td>').text(item._firstname),
                    $('<td>').text(item._quota),
                    $('<td>').text(item._quotavideo),
                ); 
                console.log(item);
                $.each(item['_articles'], function(i_year, item_c) {
                  $.each(item_c, function(i_month, item_d) {
                    if (typeof item_d['count_video'] === "undefined") {
                      $tr.append(
                        $('<td class="s_article" data-userid="'+i+'" data-month="'+i_month+'" data-year="'+i_year+'">').text(item_d['count']),
                        //$('<td class="s_article" data-userid="'+i+'" data-month="'+i_month+'" data-year="'+i_year+'">').text(item_d['pageview']),
                      ); 
                    }else{
                      $tr.append(
                        $('<td class="s_article" data-userid="'+i+'" data-month="'+i_month+'" data-year="'+i_year+'">').html(item_d['count'] + ' : <span class="red">' + item_d['count_video'] + '</span>'),
                        //$('<td class="s_article" data-userid="'+i+'" data-month="'+i_month+'" data-year="'+i_year+'">').text(item_d['pageview']),
                      ); 
                    }

                  });
                });

                $tr.appendTo('.table-users #tbody');
            });

          function getMonth(date) {
            var month = date.getMonth() + 1;
            return month < 10 ? '0' + month : '' + month; // ('' + month) for string result
          }  


          $('.s_article').click(function() {
              console.log(data[$(this).data("userid")]['_articles'][$(this).data("year")][$(this).data("month")]['article']);
              var liste_article = data[$(this).data("userid")]['_articles'][$(this).data("year")][$(this).data("month")]['article'];
              $('.table-articles #tbody').html('');
              $.each(liste_article, function(i, item) {
                var d = new Date(item['post_date']);
                var d_temp = item['date_creation'];
       
                var d_cre_parse = d_temp.substr(0,4)+ '-'+d_temp.substr(4,2) +'-'+d_temp.substr(6,2);
            
                var d_cre = new Date(d_cre_parse);

                var d_low = d;
                d_low.setMonth( d.getMonth( ) - 1 );

                d
                var date_string = d.getDate() + '/' + d.getMonth() + '/' +  d.getFullYear();
                var date_string_cre = d_cre.getDate() + '/' + d_cre.getMonth()+ '/' +  d_cre.getFullYear();

                if( d_low > d_cre){
                 $('.table-articles #tbody').append('<tr style="background:#ff9999"><td>'+item['post_title']+'</td><td>'+item['category'][0]['cat_name']+'</td><td>'+item['pageview']+'</td><td>'+date_string_cre+'</td><td>'+date_string+'</td><td><a href="'+item['guid']+'">Link</a></td></tr>');
                }else{
                $('.table-articles #tbody').append('<tr><td>'+item['post_title']+'</td><td>'+item['category'][0]['cat_name']+'</td><td>'+item['pageview']+'</td><td>'+date_string_cre+'</td><td>'+date_string+'</td><td><a href="'+item['guid']+'">Link</a></td></tr>');
                }

               

              });

          });

        });


    /*      function init_datatable(){
              console.log('Init Database');
              $('.table-users').DataTable();
          }
      
        $('.table-users').DataTable();

        setTimeout(init_datatable, 5000);*/

        </script>

<style>
  table {cursor: pointer;}
  .red {color:red;}
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #404040;
  }
  .box {
  position: relative;
  border-radius: 3px;
  background: #ffffff;
  border-top: 3px solid #606060;
  margin-bottom: 20px;
  width: 100%;
  box-shadow: 0 1px 1px rgba(0,0,0,0.1);
}

</style>

</body>
</html>
